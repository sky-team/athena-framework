package com.example.hello.cloud.framework.common.util.oss;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * SubMessageEnvelope
 * 消息消费者消费的消息包装类
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SubMessageEnvelope<T extends Serializable> extends PubMessageEnvelope<T> {

    private static final long serialVersionUID = 628906723613618120L;

    public SubMessageEnvelope() {
    }

    public SubMessageEnvelope(T data) {
        super(data);
    }

    /**
     * 订阅者id
     */
    private String consumerId;

    /**
     * 消息处理时间
     */
    private Date processTime;

    /**
     * 处理状态: 0:未处理，1：已处理，2：不需要处理，3：处理错误
     */
    private Integer processState;

    /**
     * 处理错误码
     */
    private Integer errCode;

    /**
     * 处理错误信息
     */
    private String errMsg;
}

package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunPropertyInfo;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunPropertyInfo;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * @author v-zhongj11
 * @create 2018/6/14
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductParentTreeResp extends YiyunPropertyInfo implements YiyunResponseData {

    /**
     * 数值	id
     */
    private Long productId;

    /**
     * 数值	所属产品id
     */
    private String name;

    /**
     * 上级产品id
     */
    private Long parentProductId;

    /**
     * 产品状态
     */
    private String status;

    /**
     * 所属类目id
     */
    private Long categoryId;

    private Map<String, Object> propMap;
    /**
     * 下级产品列表信息
     */
    private ProductParentTreeResp parent;
}
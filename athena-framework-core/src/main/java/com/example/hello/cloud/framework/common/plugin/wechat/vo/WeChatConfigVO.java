package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @author oup
 *
 */
@Data
public class WeChatConfigVO implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String appId;

    private String timestamp;

    private String nonceStr;

    private String signature;

    public String toString()
    {
        return "appId:" + appId + ",timestamp:"+timestamp + ",nonceStr:"+ nonceStr + ",signature:"+ signature;
    }

}

package com.example.hello.cloud.framework.common.kafka.parse;

import lombok.Data;
import org.springframework.util.CollectionUtils;

import cn.hutool.core.util.StrUtil;

import java.util.List;
import java.util.Map;

@Data
public class CanalDataVo<T> {
    String database;
    String table;
    String type;
    Map<String, String> mysqlType;
    List<T> data;
    private List<Map> oldData;

    public boolean dataCheckOk() {
        if (CollectionUtils.isEmpty(this.getData()) || StrUtil.isEmpty(this.getType())) {
            return false;
        }
        return true;
    }
}

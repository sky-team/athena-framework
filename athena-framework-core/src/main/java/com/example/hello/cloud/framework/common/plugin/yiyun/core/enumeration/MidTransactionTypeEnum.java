package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;/*
package com.example.hello.common.plugin.yiyun.core.enumeration;

import lombok.Getter;
import lombok.Setter;

*/
/**
 *	 交易数据接口-交易类型
 * @author hey54
 *
 *//*

public enum MidTransactionTypeEnum {
	// 认筹 chipOrder ， 认购 buyOrder ，签约 /回款.signOrder
    */
/**
     * 认筹单
     *//*

	CHIP_TAG(5, "chipOrder", "认筹单"),
    */
/**
     * 认购单
     *//*

	ORDER_TAG(6, "buyOrder", "认购单"),
    */
/**
     * 签约单
     *//*

	SIGNED_TAG(7, "signOrder", "签约单"),
	*/
/**
	 * 回款单
	 *//*

	RETURN_TAG(8, "returnOrder", "回款单");
    @Getter
    @Setter
    private Integer transType;
    @Getter
    @Setter
    private String key;
    @Getter
    @Setter
    private String value;

    MidTransactionTypeEnum(Integer transType, String key, String value) {
        this.transType = transType;
        this.key = key;
        this.value = value;
    }

    */
/**
     * 将中台的交易类型转换成之前销售家定义的交易类型
     * @param key 中台交易内省
     * @return 融合后台交易内省
     *//*

    public static Integer getTransType(String key){
        Integer result = null;
        MidTransactionTypeEnum[] values = MidTransactionTypeEnum.values();
        for (MidTransactionTypeEnum e : values){
            if (e.getKey().equals(key)){
                result = e.getTransType();
                break;
            }
        }
        return result;
    }

    */
/**
     * 根据id (id) 返回一个实例
     *
     * @return
     *//*

    public static MidTransactionTypeEnum getInstance(Integer id) {
        for (MidTransactionTypeEnum item : MidTransactionTypeEnum.values()) {
            if (item.getTransType().equals(id)) {
                return item;
            }
        }
        return null;
    }

}
*/

package com.example.hello.cloud.framework.common.plugin.wechat;


import com.example.hello.cloud.framework.common.ApiResponse;

/**
 * 微信公众号服务
 * User: ${v-zhongj11}
 * Date: 2018-10-03
 */
public interface WeChatTokenService {
    /**
     * 获取unionid
     * @param openId
     * @return
     */
    ApiResponse<String> getUnionid(String openId);

    /**
     * 获取关注二维码
     * @return
     */
    ApiResponse<String> weChatUrl();
    
    /**
     * 获取 accessToken值
     * @return
     */
    ApiResponse<String> getAccessToken();
}

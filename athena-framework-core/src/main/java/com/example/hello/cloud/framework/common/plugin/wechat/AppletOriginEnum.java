package com.example.hello.cloud.framework.common.plugin.wechat;



import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * 小程序帐号原始id枚举
 *
 * @author guoc16
 * @date 2020/8/6
 */
public enum AppletOriginEnum {

    /**
     * 易选房小程序
     */
    YXF_APPLET("wxe9692d75c9e71da7", "gh_1b88f6a9dc5b"),
    /**
     * 分享家小程序
     */
    FXJ_APPLET("wx8e7bfe0f4e6d7b29", "gh_5f48711b5d18");

    AppletOriginEnum(String appId, String originId) {
        this.appId = appId;
        this.originId = originId;
    }

    private String appId;

    public String getAppId() {
        return appId;
    }

    private String originId;

    public String getOriginId() {
        return originId;
    }

    public static AppletOriginEnum match(String appId) {
        if (StringUtils.isEmpty(appId)) {
            return null;
        }
        for (AppletOriginEnum value : AppletOriginEnum.values()) {
            if (Objects.equals(appId, value.getAppId())) {
                return value;
            }
        }
        return null;
    }
}

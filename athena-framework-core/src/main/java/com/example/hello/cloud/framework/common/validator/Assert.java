package com.example.hello.cloud.framework.common.validator;

import com.example.hello.cloud.framework.common.exception.CheckedException;
import com.example.hello.cloud.framework.common.exception.CheckedException;

import cn.hutool.core.util.StrUtil;

/**
 * 数据校验
 *
 * @author zhanj04
 * @date 2017/8/1 9:29
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StrUtil.isBlank(str)) {
            throw new CheckedException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new CheckedException(message);
        }
    }
}

package com.example.hello.cloud.framework.common.util.oss;

import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Random;

/**
 * AliyunOssService
 * 阿里云对象存储服务实现
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Slf4j
public class AliyunOssService {
    private static final Logger logger = LoggerFactory.getLogger(AliyunOssService.class);

    private AliyunOssUtil aliyunOssUtil;

    public AliyunOssUtil getAliyunOssUtil() {
        return aliyunOssUtil;
    }

    public void setAliyunOssUtil(AliyunOssUtil aliyunOssUtil) {
        this.aliyunOssUtil = aliyunOssUtil;
    }

    public ApiResponse<Map<String, String>> getAliyunOssPolicy(AliyunClassificationEnum classificationEnum, AliyunFolderTypeEnum folderType) {
        Map<String, String> map;
        String dir = classificationEnum + "/" + folderType.getPath();
        try {
            if (AliyunClassificationEnum.VIDEO == classificationEnum || AliyunClassificationEnum.AUDIO == classificationEnum) {
                map = aliyunOssUtil.getUploadMp4Policy(dir);
            } else {
                map = aliyunOssUtil.getUploadImagePolicy(dir);
            }
        } catch (UnsupportedEncodingException e) {
            String errorMsg = "getAliyunOssPolicy->获取上传图片的阿里云OSS配置失败！";
            logger.error(errorMsg, e);
            return ApiResponse.error(ApiResponseCodeEnum.THIRD_API_ERROR.getCode(), errorMsg);
        }
        return ApiResponse.success(map);
    }

    @Value("${oss.key:}")
    String ossKey;
    @Value("${oss.secret:}")
    String ossSecret;
    @Value("${oss.domain:}")
    String ossDomain;
    @Value("${oss.bucket:}")
    String ossBucket;

    public ApiResponse<AliyunOssConfig> getOssConfig(AliyunFolderTypeEnum folderType) {
        AliyunOssConfig ossConfig = new AliyunOssConfig();
        ossConfig.setBucket(ossBucket);
        ossConfig.setDomain(ossDomain);
        ossConfig.setKey(ossKey);
        ossConfig.setSecret(ossSecret);
        ossConfig.setHost(aliyunOssUtil.getHttpDomain());
        String dir = AliyunClassificationEnum.IMAGE + "/" + folderType.getPath();
        ossConfig.setDir(dir);
        return ApiResponse.success(ossConfig);
    }

    /**
     * 根据url上传图片到阿里云
     */
    public String updateAliyunImageWithUrl(String url, AliyunFolderTypeEnum folderType) {
        try {
            byte[] bytes = getByteByUrl(url);
            return updateAliyunImageWithBytes(bytes, folderType);
        } catch (Exception e) {
            logger.error("图片上传阿里云失败!errMsg:{}", e.getMessage(), e);
            return null;
        }
    }

    /**
     * 根据bytes上传图片到阿里云
     */
    public String updateAliyunImageWithBytes(byte[] bytes, AliyunFolderTypeEnum folderType) {
        try {
            String fileName = System.currentTimeMillis() + new Random().nextInt(100000) + ".jpg";
            aliyunOssUtil.putObject(AliyunClassificationEnum.IMAGE, folderType,
                    fileName, new ByteArrayInputStream(bytes), bytes.length);
            String urlImg = aliyunOssUtil.getUrl(AliyunClassificationEnum.IMAGE, folderType, fileName);
            log.info("图片上传到阿里云地址:{}", urlImg);
            return urlImg;
        } catch (Exception e) {
            logger.error("图片上传阿里云失败!errMsg:{}", e.getMessage(), e);
            return null;
        }
    }

    /**
     * 根据地址获得数据的字节流
     */
    public byte[] getByteByUrl(String reqUrl) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(reqUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5 * 1000);
            InputStream inStream = conn.getInputStream();//通过输入流获取图片数据
            byte[] btImg = readInputStream(inStream);//得到图片的二进制数据
            inStream.close();
            return btImg;
        } catch (Exception e) {
            logger.error("根据地址获得数据的字节流异常!errMsg:{}", e.getMessage(), e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return null;
    }

    /**
     * 从输入流中获取数据
     */
    public byte[] readInputStream(InputStream inStream) {
        try (ByteArrayOutputStream outStream = new ByteArrayOutputStream();) {
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, len);
            }
            return outStream.toByteArray();
        } catch (Exception e) {
            logger.error("字节流转换字节数组异常!errMsg:{}", e.getMessage(), e);
        }
        return null;
    }
}
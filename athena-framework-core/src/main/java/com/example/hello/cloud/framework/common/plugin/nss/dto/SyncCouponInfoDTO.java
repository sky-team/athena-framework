package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author c-linhc
 * @date 2019/9/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SyncCouponInfoDTO implements Serializable {
    /**
     * 礼券
     */
    private String couponCode;
    /**
     * 礼券名称
     */
    private String couponName;
    /**
     * 礼券类型：1(现金券)、2(折扣券)、3(礼品券)、4(支付券)
     */
    private Integer couponType;
    /**
     * 礼券金额
     */
    private String couponAmount;
    /**
     * 开始使用时间
     */
    private Date useStartTime;
    /**
     * 开始使用时间
     */
    private Date useEndTime;
    /**
     * 开始使用时间
     */
    private List<String> buildings;
}

package com.example.hello.cloud.framework.common.plugin.yiyun.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;


/**
 * 阿里云消息队列自动配置信息
 *
 * @author zhoujj07
 * @create 2017/9/7
 */
@Configuration
@EnableConfigurationProperties(YiyunProperties.class)
public class YiyunAutoConfiguration {
}



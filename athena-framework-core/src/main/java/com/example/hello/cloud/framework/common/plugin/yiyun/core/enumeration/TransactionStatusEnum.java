package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

import lombok.Getter;
import lombok.Setter;

/**
 * 翼云-交易状态
 *
 * @author pengdm.
 *         2018/5/21.
 */
public enum TransactionStatusEnum {
    CHIP_ORDER_INIT("chipOrder", "init", "正常"),
    CHIP_ORDER_TO_BUY("chipOrder", "toBuy", "已转定购"),
    CHIP_ORDER_CANCEL("chipOrder", "cancel", "已退筹"),
    CHIP_ORDER_TRANSFER("chipOrder", "transfer", "已转"),
    BUY_ORDER_PENDING_APPROVE("buyOrder", "pendingApprove", "待审"),
    BUY_ORDER_INIT("buyOrder", "init", "正常"),
    BUY_ORDER_TRANSFER("buyOrder", "transfer", "已转"),
    BUY_ORDER_BUY_CANCEL("buyOrder", "buyCancel", "已退"),
    BUY_ORDER_BUY_CHANGE("buyOrder", "buyChange", "已换"),
    SIGN_ORDER_INIT("signOrder", "init", "正常"),
    SIGN_ORDER_INIT_TRANSFER("signOrder", "transfer", "已转"),
    SIGN_ORDER_INIT_SIGN_CANCEL("signOrder", "signCancel", "已退"),
    SIGN_ORDER_INIT_SIGN_CHANGE("signOrder", "signChange", "已换"),
    SIGN_ORDER_REVOCATION("signOrder", "revocation", "撤销"),;

    @Getter
    @Setter
    private String parentKey;
    @Getter
    @Setter
    private String key;
    @Getter
    @Setter
    private String value;

    TransactionStatusEnum(String parentKey, String key, String value) {
        this.parentKey = parentKey;
        this.key = key;
        this.value = value;
    }

}

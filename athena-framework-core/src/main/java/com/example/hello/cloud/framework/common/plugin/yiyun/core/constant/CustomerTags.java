package com.example.hello.cloud.framework.common.plugin.yiyun.core.constant;

/**
 * 客户标签定义
 *
 * @author zhoujj07
 * @create 2018/5/7
 */
public class CustomerTags {
	
	private CustomerTags() {
	}

    /**
     * 万科员工标签
     */
    public static final String example_EMPLOYEE_TAG = "exampleEmployee";

    /**
     * 万科业主标签
     */
    public static final String example_HOUSE_OWNER_TAG = "exampleHouseOwner";

}

package com.example.hello.cloud.framework.common.plugin.openapi.core;

import java.io.Serializable;

/**
 * open api base param
 *
 * @author v-linxb
 * @create 2019/9/5
 **/
public class OpenApiBaseReqData implements Serializable {

}

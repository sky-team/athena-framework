package com.example.hello.cloud.framework.common.util;

import com.google.common.base.Throwables;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.poi.util.IOUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * HTTP连接类，提供POST和GET形式连接
 * Created by zhangdk01 on 2016/10/13.
 */
@Slf4j
public class HTTPConnector {

    // ms
    private static final int SOCKET_TIME_OUT = 60000;

    private static final int CONNECT_TIME_OUT = 30000;

    private static final String CHARACTER_UTF8 = "UTF-8";

    public static String sendJsonDelete(String url,String token)
    {
        HttpDelete delete = new HttpDelete(url);
        delete.setHeader("Authorization",token);
        return sendInternalJsonDelete(url,delete);
    }

    private static String sendInternalJsonDelete(String url, HttpRequestBase request)
    {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SOCKET_TIME_OUT).setConnectTimeout(CONNECT_TIME_OUT).build();//设置请求和传输超时时间
            request.setConfig(requestConfig);
            CloseableHttpResponse closeableHttpResponse = httpClient.execute(request);
            try {
                HttpEntity entity = closeableHttpResponse.getEntity();
                if (entity != null) {
                    String entityString = EntityUtils.toString(entity, CHARACTER_UTF8);
                    return entityString;
                }
                else
                {
                    return StringUtils.EMPTY;
                }
            } finally {
                closeableHttpResponse.close();
            }
        } catch (IOException e) {
            log.error("Failed to http post, URL: " + url, ", cause by:" + e.getMessage(),e);
        }
        finally {
            IOUtils.closeQuietly(httpClient);
        }


        return StringUtils.EMPTY;
    }


    /**
     * 发送JSON格式的POST请求，返回JSON串
     * @param url
     * @param json
     * @return
     */
    public static String sendJsonPost(String url, String json,String token)
    {
        HttpPost post = new HttpPost(url);
        post.setHeader("Authorization",token);
        return sendInternalJsonPost(url,json,post);
    }

    /**
     * 发送JSON格式的POST请求，返回JSON串
     * @param url
     * @param json
     * @return
     */
    public static String sendJsonPost(String url, String json)
    {
        HttpPost post = new HttpPost(url);
        return sendInternalJsonPost(url,json,post);
    }
    public static byte[] sendJsonPostByte(String url, String json){
        HttpPost post = new HttpPost(url);
        return sendInternalJsonPostByte(url,json,post);
    }

    private static String sendInternalJsonPost(String url, String json, HttpPost post)
    {
        try {
            StringEntity entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SOCKET_TIME_OUT).setConnectTimeout(CONNECT_TIME_OUT).build();//设置请求和传输超时时间
            post.setConfig(requestConfig);
            post.setEntity(entity);
            return executePost(post);
        } catch (IOException e) {
            log.error("Failed to http post, URL：{}", Throwables.getStackTraceAsString(e));
            //log.error("Failed to http post, URL: " + url, ", cause by:" + e.getMessage());
        }

        return StringUtils.EMPTY;
    }
    private static byte[] sendInternalJsonPostByte(String url, String json, HttpPost post)
    {
        try {
            StringEntity entity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SOCKET_TIME_OUT).setConnectTimeout(CONNECT_TIME_OUT).build();//设置请求和传输超时时间
            post.setConfig(requestConfig);
            post.setEntity(entity);
            return executePostByte(post);
        } catch (IOException e) {
            log.error("Failed to http post, URL：{}", Throwables.getStackTraceAsString(e));
            //log.error("Failed to http post, URL: " + url, ", cause by:" + e.getMessage());
        }

        return null;
    }

    private static String executePost(HttpPost post) throws IOException {
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse closeableHttpResponse = null;
        try {

             httpClient = HttpClients.createDefault();
             closeableHttpResponse = httpClient.execute(post);

            HttpEntity entity = closeableHttpResponse.getEntity();
            StatusLine statusLine = closeableHttpResponse.getStatusLine();

            if (200 != statusLine.getStatusCode())
            {
                String errorMsg = "执行POST请求返回异常，具体原因:" + statusLine.getReasonPhrase();
                log.error(errorMsg);
            }

            if (entity != null) {
                String entityString = EntityUtils.toString(entity, CHARACTER_UTF8);
                return entityString;
            }
            else
            {
                return StringUtils.EMPTY;
            }
        } finally {
            IOUtils.closeQuietly(httpClient);
            closeableHttpResponse.close();
        }
    }
    private static byte[] executePostByte(HttpPost post) throws IOException {
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse closeableHttpResponse = null;
        try {


            httpClient = HttpClients.createDefault();
            closeableHttpResponse = httpClient.execute(post);

            HttpEntity entity = closeableHttpResponse.getEntity();
            StatusLine statusLine = closeableHttpResponse.getStatusLine();

            if (200 != statusLine.getStatusCode())
            {
                String errorMsg = "执行POST请求返回异常，具体原因:" + statusLine.getReasonPhrase();
                log.error(errorMsg);
            }

            if (entity != null) {
                byte[] entityString = EntityUtils.toByteArray(entity);
                return entityString;
            }
            else
            {
                return null;
            }
        } finally {
            IOUtils.closeQuietly(httpClient);
            closeableHttpResponse.close();
        }
    }

    /**
     * 发送POST请求，返回JSON串
     * @param url
     * @param parameterMap
     * @return
     */
    public static String sendPost(String url, Map<String,String> parameterMap)
    {
        HttpPost post = new HttpPost(url);
        try {
            List<NameValuePair> queryParameter = buildQueryParameter(parameterMap);
            UrlEncodedFormEntity uefEntity = new UrlEncodedFormEntity(queryParameter,CHARACTER_UTF8);
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SOCKET_TIME_OUT).setConnectTimeout(CONNECT_TIME_OUT).build();//设置请求和传输超时时间
            post.setConfig(requestConfig);
            post.setEntity(uefEntity);
            return executePost(post);
        } catch (IOException e) {
            log.error("Failed to http post, URL: " + url, ", cause by:" + e.getMessage(),e);
        }

        return StringUtils.EMPTY;
    }

    private static List<NameValuePair> buildQueryParameter(Map<String, String> parameterMap) {
        List<NameValuePair> postParameter = new ArrayList<>();
        if (MapUtils.isNotEmpty(parameterMap)) {
            for (String key : parameterMap.keySet()) {
                postParameter.add(new BasicNameValuePair(key, parameterMap.get(key)));
            }
        }
        return postParameter;
    }

    /**
     * 获取图片的数组，未采用base64编码
     * @param url
     * @return
     */
    public static byte[] sendGetEntity(String url)
    {
        HttpGet get = new HttpGet(url);

        try {
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SOCKET_TIME_OUT).setConnectTimeout(CONNECT_TIME_OUT).build();//设置请求和传输超时时间
            get.setConfig(requestConfig);
            CloseableHttpClient httpClient = null;
            CloseableHttpResponse closeableHttpResponse = null;
            try {

                 httpClient = HttpClients.createDefault();
                 closeableHttpResponse = httpClient.execute(get);


                HttpEntity entity = closeableHttpResponse.getEntity();
                StatusLine statusLine = closeableHttpResponse.getStatusLine();

                if (200 != statusLine.getStatusCode())
                {
                    String errorMsg = "执行GET请求返回异常，具体原因:" + statusLine.getReasonPhrase();
                    log.error(errorMsg);
                }

                if (entity != null)
                {
                    return IOUtils.toByteArray(entity.getContent());
                }
                else
                {
                    return null;
                }
            } finally {
                IOUtils.closeQuietly(httpClient);
                closeableHttpResponse.close();
            }
        } catch (IOException e) {
            log.error("Failed to http get, URL: " + url, ", cause by:" + e.getMessage(),e);
        }
        return null;
    }

    /**
     * @Author v-pengdm
     * @param url
     * @param param
     * @param header
     * @return
     */
    public static String sendJsonPost(String url, String param,Map<String,String> header)
    {
        HttpPost post = new HttpPost(url);
        setHeader(header,post);
        return sendInternalJsonPost(url,param,post);
    }

    /**
     * 发送x-www-form-urlencoded请求
     * @param url
     * @param params
     * @param header
     * @param encoding
     * @return
     */
    public static String sendFormPost(String url, Map<String,String> params,Map<String,String> header,String encoding){
        HttpPost post = new HttpPost(url);
        try {
            setHeader(header,post);
            //装填参数
            List<NameValuePair> nvps = new ArrayList<>();
            if(params!=null){
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
            }
            //设置参数到请求对象中
            post.setEntity(new UrlEncodedFormEntity(nvps, encoding));
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SOCKET_TIME_OUT).setConnectTimeout(CONNECT_TIME_OUT).build();//设置请求和传输超时时间
            post.setConfig(requestConfig);
            return executePost(post);
        }catch (IOException e) {
            log.error("Failed to http post, URL：{}",url,Throwables.getStackTraceAsString(e));
        }
        return StringUtils.EMPTY;
    }

    /**
     * 设置header
     * @param header
     * @param post
     */
    private static void setHeader(Map<String,String> header,HttpPost post){
        //设置header
        if(null != header){
            Iterator<String> keyIt = header.keySet().iterator();
            while (keyIt.hasNext()){
                String key = keyIt.next();
                post.setHeader(key,header.get(key));
            }
        }
    }
}

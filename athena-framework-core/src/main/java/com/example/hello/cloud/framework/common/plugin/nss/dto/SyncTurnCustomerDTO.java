package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/5.
 */
@Data
public class SyncTurnCustomerDTO  implements Serializable {
   private  String salesOrgCode;
   private  String nssCustomerId;
   private  String nssConsultantId;
   private  String sNumber;

    @Override
    public String toString() {
        return "SyncTurnCustomerDTO{" +
                "salesOrgCode='" + salesOrgCode + '\'' +
                ", nssCustomerId='" + nssCustomerId + '\'' +
                ", nssConsultantId='" + nssConsultantId + '\'' +
                ", sNumber='" + sNumber + '\'' +
                '}';
    }
}

package com.example.hello.cloud.framework.common.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * 非空正则匹配
 *
 * @author zhanj04
 * @date 2017/8/1 9:29
 */
@Target({java.lang.annotation.ElementType.METHOD, java.lang.annotation.ElementType.FIELD})
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = NotEmptyPatternValidator.class)
public @interface NotEmptyPattern {
    String message() default "自定义标注校验失败";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String regexp();

}
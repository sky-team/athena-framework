package com.example.hello.cloud.framework.common.http.client;

import com.example.hello.cloud.framework.common.util.StringUtil;
import com.example.hello.cloud.framework.common.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.http.MediaType;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author guohg03
 * @ClassName: HttpUtil
 * @Description: Http 请求工具类
 * @date 2018年8月10日
 */
@Slf4j
public class HttpUtil {
    private static CloseableHttpClient httpClient;
    private final static String DEFAULT_CHAR_SET = "UTF-8";

    // 超时时间5s
    private static final int TIME_OUT = 5 * 1000;

    private static CloseableHttpClient getHttpClient() {
        if (httpClient == null) {
            httpClient = HttpClientFactory.getPooledClient();
        }
        return httpClient;
    }

    /**
     * 功能：POST请求
     *
     * @param url     请求的地址
     * @param params  请求的键值参数
     * @param headers 头部参数
     * @return 把地址返回的JSON转为Map
     */
    public static String doPostWithMap(String url, Map<String, String> params) {
        return doPostWithMap(url, params, null, DEFAULT_CHAR_SET);
    }

    /**
     * 功能：POST请求
     *
     * @param url     请求的地址
     * @param params  请求的键值参数
     * @param headers 头部参数
     * @return 把地址返回的JSON转为Map
     */
    public static String doPostWithMap(String url, Map<String, String> params, Map<String, String> headers) {
        return doPostWithMap(url, params, headers, DEFAULT_CHAR_SET);
    }

    /**
     * 功能：POST请求
     *
     * @param url     请求的地址
     * @param params  请求的键值参数
     * @param headers 头部参数
     * @param charset 要求返回时转换为某字符集
     * @return 返回请求后的实体内容字符串
     */
    public static String doPostWithMap(String url, Map<String, String> params, Map<String, String> headers,
                                       String charset) {
        log.info("doPostWithMap->start,url:{},params:{},headers:{},charset:{}",
                new Object[]{url, params, headers, charset});
        HttpPost httpPost = new HttpPost(url);
        if (params != null && params.size() > 0) {
            HttpEntity httpEntity = buildHttpEntity(params);
            httpPost.setEntity(httpEntity);
        }
        return post(httpPost, headers, charset);
    }

    /**
     * 功能：POST请求
     *
     * @param url  请求的地址
     * @param JSON 请求的参数为JSON字符串
     * @return 把地址返回的JSON转为Map
     */
    public static String doPostWithJson(String url, String JSON) {
        return doPostWithJson(url, JSON, null, DEFAULT_CHAR_SET);
    }

    /**
     * 功能：POST请求
     *
     * @param url     请求的地址
     * @param JSON    请求的参数为JSON字符串
     * @param headers 头部参数
     * @return 把地址返回的JSON转为Map
     */
    public static String doPostWithJson(String url, String JSON, Map<String, String> headers) {
        log.info("doPostWithJson->start,url:{},JSON:{},headers:{}", new Object[]{url, JSON, headers});
        HttpPost httpPost = new HttpPost(url);
        if (StringUtil.isNotEmpty(JSON)) {
            // 解决中文乱码问题
            StringEntity entity = new StringEntity(JSON, "utf-8");
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
        }
        return post(httpPost, headers, DEFAULT_CHAR_SET);
    }

    /**
     * 功能：Put请求
     *
     * @param url     请求的地址
     * @param JSON    请求的参数为JSON字符串
     * @param headers 头部参数
     * @return 把地址返回的JSON转为Map
     */
    public static String doPutWithJson(String url, String JSON, Map<String, String> headers) {
        log.info("doPutWithJson->start,url:{},JSON:{},headers:{}", new Object[]{url, JSON, headers});
        HttpPut httpPut = new HttpPut(url);
        if (StringUtil.isNotEmpty(JSON)) {
            // 解决中文乱码问题
            StringEntity entity = new StringEntity(JSON, "utf-8");
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            httpPut.setEntity(entity);
        }
        return put(httpPut, headers, DEFAULT_CHAR_SET);
    }

    /**
     * 功能：POST请求
     *
     * @param url     请求的地址
     * @param JSON    请求的参数为JSON字符串
     * @param headers 头部参数
     * @param charset 要求返回时转换为某字符集
     * @return 返回请求后的实体内容字符串
     */
    public static String doPostWithJson(String url, String JSON, Map<String, String> headers, String charset) {
        log.info("doPostWithJson->start,url:{},JSON:{},headers,charset:{}", new Object[]{url, JSON, headers, charset});
        HttpPost httpPost = new HttpPost(url);
        if (StringUtil.isNotEmpty(JSON)) {
            // 解决中文乱码问题
            StringEntity entity = new StringEntity(JSON, "utf-8");
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
        }
        return post(httpPost, headers, charset);
    }

    /**
     * 功能：POST请求
     *
     * @param url     请求的地址
     * @param binary  请求的参数为字节数据,会以流传到url
     * @param headers 头部参数
     * @return 把地址返回的JSON转为Map
     */
    public static String doPostWithByte(String url, byte[] binary) {
        return doPostWithByte(url, binary, null, DEFAULT_CHAR_SET);
    }

    /**
     * 功能：POST请求
     *
     * @param url     请求的地址
     * @param binary  请求的参数为字节数据,会以流传到url
     * @param headers 头部参数
     * @return 把地址返回的JSON转为Map
     */
    public static String doPostWithByte(String url, byte[] binary, Map<String, String> headers) {
        return doPostWithByte(url, binary, headers, DEFAULT_CHAR_SET);
    }

    /**
     * HttpClient的POST请求
     *
     * @param url     请求的地址
     * @param binary  请求的参数为字节数据,会以流传到url
     * @param headers 头部参数
     * @param charset 要求返回时转换为某字符集
     * @return 返回请求后的实体内容字符串
     */
    public static String doPostWithByte(String url, byte[] binary, Map<String, String> headers, String charset) {
        log.info("doPostWithByte->start,url:{},binary:{},headers:{},charset:{}",
                new Object[]{url, new String(binary), headers, charset});
        HttpPost httpPost = new HttpPost(url);
        if (binary != null) {
            HttpEntity httpEntity = buildHttpEntity(binary);
            httpPost.setEntity(httpEntity);
        }
        return post(httpPost, headers, charset);
    }

    /**
     * HttpClient的GET请求
     *
     * @param url 请求的地址
     */
    public static String doGet(String url) {
        log.info("doGet->start,url:{}", url);
        HttpGet httpGet = new HttpGet(url);
        return get(httpGet, null, DEFAULT_CHAR_SET);
    }

    /**
     * HttpClient的GET请求
     *
     * @param url     请求的地址
     * @param headers 头部参数
     */
    public static String doGet(String url, Map<String, String> headers) {
        log.info("doGet->start,url:{},headers:{}", url, headers);
        HttpGet httpGet = new HttpGet(url);
        return get(httpGet, headers, DEFAULT_CHAR_SET);
    }

    /**
     * 功能：执行post请求
     */
    private static String post(HttpPost httpPost, Map<String, String> headers, final String charset) {
        long start = System.currentTimeMillis();
        config(httpPost, headers);
        CloseableHttpClient httpClient = getHttpClient();
        try (CloseableHttpResponse response = httpClient.execute(httpPost, HttpClientContext.create());) {
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity, charset);
            EntityUtils.consume(entity);
            log.info("post->response result:{}，url:{}，cost time:{}ms！", result, httpPost.getURI(),
                    System.currentTimeMillis() - start);
            return result;
        } catch (Exception e) {
            log.error("post->end,err！url:{} , err:{}", httpPost.getURI(), e);
        } finally {
            try {
                httpPost.releaseConnection();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return null;
    }

    /**
     * 功能：执行Put请求
     */
    private static String put(HttpPut httpPut, Map<String, String> headers, String charset) {
        CloseableHttpResponse response = null;
        try {
            long start = System.currentTimeMillis();
            config(httpPut, headers);
            CloseableHttpClient httpClient = getHttpClient();
            response = httpClient.execute(httpPut, HttpClientContext.create());
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity, DEFAULT_CHAR_SET);
            EntityUtils.consume(entity);
            log.info("put->response result:{}，url:{}，cost time:{}ms！", result, httpPut.getURI(),
                    System.currentTimeMillis() - start);
            return result;
        } catch (Exception e) {
            log.error("put->end,err！url:{} , err:{}", httpPut.getURI(), e);
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpPut.releaseConnection();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return null;
    }

    /**
     * 功能：执行get请求
     */
    private static String get(HttpGet httpGet, Map<String, String> headers, String charset) {
        CloseableHttpResponse response = null;
        try {
            long start = System.currentTimeMillis();
            config(httpGet, headers);
            CloseableHttpClient httpClient = getHttpClient();
            response = httpClient.execute(httpGet, HttpClientContext.create());
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity, DEFAULT_CHAR_SET);
            EntityUtils.consume(entity);
            log.info("get->response result:{}，url:{}，cost time:{}ms！", result, httpGet.getURI(),
                    System.currentTimeMillis() - start);
            return result;
        } catch (Exception e) {
            log.error("get->end,err！url:{} , err:{}", httpGet.getURI(), e);
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpGet.releaseConnection();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return null;
    }

    /**
     * 功能: 配置请求的超时设置
     */
    private static void config(HttpRequestBase httpRequestBase, Map<String, String> headers) {
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(TIME_OUT)
                .setConnectTimeout(TIME_OUT).setSocketTimeout(TIME_OUT).build();
        if (headers != null) {
            String contentType = headers.get("Content-Type");
            if (StringUtil.isNullOrEmpty(contentType)) {
                headers.put("Content-Type", MediaType.APPLICATION_JSON_VALUE);
            }
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpRequestBase.setHeader(entry.getKey(), entry.getValue());
            }
        }
        httpRequestBase.setConfig(requestConfig);
    }

    /**
     * 根据键值参数,创建请求的实体
     */
    private static HttpEntity buildHttpEntity(Map<String, String> params) {
        List<NameValuePair> nvp = new ArrayList<NameValuePair>();
        for (String key : params.keySet()) {
            nvp.add(new BasicNameValuePair(key, params.get(key)));
        }
        try {
            return new UrlEncodedFormEntity(nvp, DEFAULT_CHAR_SET);
        } catch (Exception e) {
            log.error("http client post error!" + e.getMessage(), e);
            return null;
        }
    }

    /**
     * 根据字节数据,创建请求的实体
     */
    private static HttpEntity buildHttpEntity(byte[] binary) {
        return EntityBuilder.create().setBinary(binary).build();
    }

    public static String sendGetByToken(String url, String param, String token) {
        String result = "";
        // 打开和URL之间的连接
        try {
            String urlName = url;
            if (null != param) {
                urlName = url + "?" + param;
            }
            URL realUrl = new URL(urlName);
            URLConnection conn = realUrl.openConnection();
            conn.setRequestProperty("Authorization", token);
            result = excuteGet(conn);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public static String excuteGet(URLConnection conn) {
        String result = "";
        BufferedReader in = null;
        try {
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
            // 建立实际的连接
            conn.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = conn.getHeaderFields();
            log.info("conn:{},map:{}", conn.toString(), map);
            // 遍历所有的响应头字段
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            log.info(result);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("a", "123");
        System.out.println(HttpUtil.doGet("https://www.baidu.com", null));
    }

    /**
     * 通过get请求获取服务器数据流，返回字节数组
     *
     * @param url
     * @return
     */
    public static byte[] doGetBackWithByte(String url) {
        log.info("doGetBackWithByte->start url ->{}", url);
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = null;
        try {
            long start = System.currentTimeMillis();
            config(httpGet, null);
            CloseableHttpClient httpClient = getHttpClient();
            response = httpClient.execute(httpGet, HttpClientContext.create());
            HttpEntity entity = response.getEntity();
            InputStream inputStream = entity.getContent();
            byte[] bytes = readInputStream(inputStream);
            log.info("doGetBackWithByte ->response ，url {}，cost time {}ms！", httpGet.getURI(),
                    System.currentTimeMillis() - start);
            return bytes;
        } catch (Exception e) {
            log.error("get->end,err！url:{} , err:{}", httpGet.getURI(), e);
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpGet.releaseConnection();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return null;
    }

    /**
     * 根据url获取文件流对象
     *
     * @param
     * @return
     */
    public static InputStream doGetBackWithStream(String url) {
        log.info("doGetBackWithStream->start url ->{}", url);
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = null;
        try {
            long start = System.currentTimeMillis();
            config(httpGet, null);
            CloseableHttpClient httpClient = getHttpClient();
            response = httpClient.execute(httpGet, HttpClientContext.create());
            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();
            log.info("doGetBackWithStream ->response ，url {}，cost time {}ms！, size{}", httpGet.getURI(),
                    System.currentTimeMillis() - start, content.available());
            return content;
        } catch (Exception e) {
            log.error("get->end,err！url:{} , err:{}", httpGet.getURI(), e);
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpGet.releaseConnection();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return null;
    }

    /**
     * 从输入流中获取数据
     *
     * @param inStream 输入流
     * @return
     * @throws Exception
     */
    public static byte[] readInputStream(InputStream inStream) {
        byte[] buffer = new byte[1024];
        try {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            int len = 0;
            while ((len = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, len);
            }
            inStream.close();
            return outStream.toByteArray();
        } catch (Exception e) {
            log.info("字节流转换字节数组异常", e);
        }
        return buffer;
    }
}
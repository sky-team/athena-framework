package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author v-zhongj11
 * @create 2018/6/7
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrderDataRequest extends BaseYiyunRequestData {

    /**
     * 单据流水号
     */
    private String orderNo;
    private String mobile;

    /**
     * 单据数据类型:默认电子发票
     */
    private String dataType = "invoice";

    private Integer page = 1;

    private Integer pageSize = 100;

}
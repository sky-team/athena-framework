package com.example.hello.cloud.framework.common.plugin.wechat.xml;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

/**
 * 消息Json格式
 * @author guohg03
 * @date 2020/03/10 09:34
 */
@Data
public class WxMpJsonMessage {
	@JSONField(name="ToUserName")
	private String toUser;
	
	@JSONField(name="FromUserName")
	private String fromUser;
	
	@JSONField(name="CreateTime")
	private Long createTime;
	
	@JSONField(name="MsgType")
	private String msgType;
	
	@JSONField(name="Content")
	private String content;

	@JSONField(name="MsgId")
	private Long msgId;
}

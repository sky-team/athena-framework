package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;

import java.util.List;

/**
 * @author v-zhongj11
 * @create 2018/5/31
 */
@Data
public class QueryEstateOrderRespons extends YiyunBasePage implements YiyunResponseData {

    private List<EstateOrder> list;
}
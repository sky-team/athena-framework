package com.example.hello.cloud.framework.common.plugin.yiyun.core;

import lombok.Data;

import java.io.Serializable;

/**
 * 中台批量查询响应基础分页格式
 *
 * @author v-linxb
 * @create 2018/5/17
 **/
@Data
public class YiyunBasePage implements Serializable {
    /**
     * 总数
     */
    private Integer total;
    /**
     * 页面大小
     */
    private Integer pageSize;
    /**
     * 页数
     */
    private Integer pageNo;
    /**
     * 页数据大小
     */
    private Integer pageCount;

}

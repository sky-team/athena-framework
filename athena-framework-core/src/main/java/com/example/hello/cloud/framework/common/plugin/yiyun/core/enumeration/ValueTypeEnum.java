package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 值类型枚举
 *
 * @author v-linxb
 * @create 2018/6/14
 **/
public enum ValueTypeEnum {

    STRING("string"),
    INT("int"),
    LONG("long");

    private String value;

    ValueTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}

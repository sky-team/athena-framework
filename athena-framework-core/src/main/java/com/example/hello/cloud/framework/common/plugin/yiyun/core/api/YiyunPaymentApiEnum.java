package com.example.hello.cloud.framework.common.plugin.yiyun.core.api;

/**
 * 翼云支付api地址枚举
 *
 * @author mall
 * @create 2019/4/1
 **/
public enum YiyunPaymentApiEnum implements YiyunServiceType {

    APPLY_UNIFIED_PAY("v1/payment/pay/unifiedPay", "发起公众号支付", false),
    REFUND_APPLY("v1/payment/refund/refundApply", "退款申请", false),
    QUERY_STATUS("v1/payment/order/queryStatus", "查询支付状态", false),
    QUERY("v1/payment/order/query", "查询支付状态", false);

    YiyunPaymentApiEnum(String url, String desc, boolean needTicket) {
        this.url = url;
        this.desc = desc;
        this.needTicket = needTicket;
    }

    private final static String API_CENTER = "payment/";
    private String url;
    private String desc;
    private boolean needTicket;

    @Override
    public String url() {
        return API_CENTER + this.url;
    }

    @Override
    public String desc() {
        return this.desc;
    }

    @Override
    public boolean needTicket() {
        return this.needTicket;
    }

}
package com.example.hello.cloud.framework.common.plugin.nss.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.nss.NssOrgService;
import com.example.hello.cloud.framework.common.plugin.nss.NssSyncService;
import com.example.hello.cloud.framework.common.util.ResourcesUtil;
import com.example.hello.cloud.framework.common.util.http.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author v-yangym10
 * @date 2019/4/16
 */
@Service
@Slf4j
public class NssOrgServiceImpl implements NssOrgService {

    private String nssUrl;
    private String userName;
    private String password;

    public NssOrgServiceImpl() {
        userName = ResourcesUtil.getProperty("nss.userName");
        password = ResourcesUtil.getProperty("nss.userPwd");
        nssUrl = ResourcesUtil.getProperty("nss.url");
    }

    @Autowired
    private NssSyncService nssSyncService;

    /**
     * 新建销售组织
     * @param postDate
     * @return
     */
    @Override
    public ApiResponse<String> createSalesOrg(String postDate) throws Exception{
        String url = nssUrl + "/api/salesOrgSvc/save";
        log.info("开始新建销售组织 NssOrgServiceImpl createSalesOrg postData ->{} url->{}",postDate ,url);
        //调用销售系统接口
        JSONObject jsonObject;
        String salesOrgId = "";
        try {
            String response = HttpUtil.doPutWithJson(url, postDate, setHeader());
            jsonObject = JSON.parseObject(response);
            log.info("创建销售组织返回 NssOrgServiceImpl createSalesOrg response ->{}" ,response);
        }catch (Exception e){
            log.info("创建销售组织异常 NssOrgServiceImpl createSalesOrg e->{}" , e);
            throw new Exception("创建销售组织异常");
        }

        try {
            JSONObject state = jsonObject.getJSONObject("state");
            if (!"10000".equals(state.getString("errCode"))){
                log.info("创建销售组织异常 解析销售系统返回失败 state->{}" ,state);
                JSONObject body = jsonObject.getJSONObject("body");
                String message = body.getString("message");
                return ApiResponse.error(-1,message);
            }
            salesOrgId = jsonObject.getJSONObject("body").getJSONObject("salesOrg").getString("id");
        }catch (Exception e){
            log.info("创建销售组织异常 解析销售系统返回失败 e->{}" ,e);
            throw new Exception("创建销售组织异常 解析销售系统返回失败");
        }
        //新增销售组织完成====新建项目客户逾期规则
//        try{
//            String overdueRuleUrl = nssUrl + "/api/overdueRuleSvc/save";
//            String requestBody = buildRequestBody(salesOrgId);
//            log.info("新建项目客户逾期规则 NssOrgServiceImpl createSalesOrg psotData ->{} url->{}" ,requestBody ,overdueRuleUrl);
//            String response = HttpUtil.doPutWithJson(overdueRuleUrl, requestBody, setHeader());
//            log.info("新建项目客户逾期规则 NssOrgServiceImpl createSalesOrg ruleResponse ->{}" ,response);
//        }catch (Exception e){
//            log.info("新建项目客户逾期规则异常 e->{}" ,e);
//        }
        return ApiResponse.success(salesOrgId);
    }

    @Override
    public ApiResponse saveSalesProduct(String postDate) throws Exception{
        String url = nssUrl + "/api/salesOrgSvc/saveSalesProduct";
        log.info("开始在销售组织下挂分期 NssOrgServiceImpl saveSalesProduct postData ->{} url->{}",postDate ,url);
        JSONObject jsonObject;
        try {
            String response = HttpUtil.doPostWithJson(url, postDate, setHeader());
            jsonObject = JSON.parseObject(response);
            log.info("销售组织下挂分期返回 NssOrgServiceImpl saveSalesProduct response ->{}" ,response);
        }catch (Exception e){
            log.info("销售组织下挂分期异常 NssOrgServiceImpl saveSalesProduct e->{}" , e);
            throw new Exception("销售组织下挂分期异常");
        }

        try {
            JSONObject state = jsonObject.getJSONObject("state");
            if (!"10000".equals(state.getString("errCode"))){
                log.info("销售组织挂分期异常 解析销售系统返回失败 state->{}" ,state);
                throw new Exception("销售组织挂分期异常 解析销售系统返回失败");
            }
        }catch (Exception e){
            log.info("销售组织挂分期异常 解析销售系统返回失败 e->{}" ,e);
            throw new Exception("销售组织挂分期异常 解析销售系统返回失败");
        }
        return ApiResponse.success("成功");
    }

    @Override
    public ApiResponse saveSalesOrgAndProduct(String postDate) throws Exception {
        String url = nssUrl + "/api/object/popularizeProject/insert";
        log.info("开始新增推广项目与销售组织对应关系 NssOrgServiceImpl saveSalesOrgAndProduct postData ->{} url->{}",postDate ,url);

        JSONObject jsonObject;
        try {
            String response = HttpUtil.doPutWithJson(url, postDate, setHeader());
            jsonObject = JSON.parseObject(response);
            log.info("新增推广项目与销售组织对应关系返回 NssOrgServiceImpl saveSalesOrgAndProduct response ->{}" ,response);
        }catch (Exception e){
            log.info("新增推广项目与销售组织对应关系异常 NssOrgServiceImpl saveSalesOrgAndProduct e->{}" , e);
            throw new Exception("新增推广项目与销售组织对应关系异常");
        }

        try {
            JSONObject state = jsonObject.getJSONObject("state");
            if (!"10000".equals(state.getString("errCode"))){
                log.info("新增推广项目与销售组织对应关系异常 解析销售系统返回失败 state->{}" ,state);
                throw new Exception("新增推广项目与销售组织对应关系异常 解析销售系统返回失败");
            }
        }catch (Exception e){
            log.info("新增推广项目与销售组织对应关系异常 解析销售系统返回失败 e->{}" ,e);
            throw new Exception("新增推广项目与销售组织对应关系异常 解析销售系统返回失败");
        }
        return ApiResponse.success("成功");
    }

    private String buildRequestBody(String salesOrgId) {
        Map<String,Map<String,Object>> ruleMap = new HashMap<>();
        Map<String,Object> rule = new HashMap<>();
        rule.put("id",salesOrgId);
        rule.put("invalidSwitch",0);
        rule.put("recycleSwitch",0);
        rule.put("followSwitch",0);
        rule.put("followRemindDate",7);
        rule.put("dealSwitch",0);
        rule.put("dealOnTimeDate",30);
        rule.put("overdueRecycleSwitch",0);
        rule.put("intentionRecycleSwitch",0);
        rule.put("salesOrgId",salesOrgId);
        rule.put("invalidRuleDays",0);
        rule.put("recyclingDays",0);
        ruleMap.put("overdueRule",rule);
        return JSON.toJSONString(ruleMap);
    }

    private Map<String, String> setHeader() {
        Map<String, String> header = new HashMap<>();

        //封装获取token方法
        header.put("Authorization", nssSyncService.syncToken());

        return header;
    }
}

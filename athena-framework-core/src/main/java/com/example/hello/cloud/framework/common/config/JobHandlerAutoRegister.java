package com.example.hello.cloud.framework.common.config;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.example.hello.cloud.framework.common.annotation.XxlJobAutoRegistry;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.example.hello.cloud.framework.common.annotation.XxlJobAutoRegistry;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.util.XxlJobRemotingUtil;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class JobHandlerAutoRegister  implements ApplicationContextAware {

	private ApplicationContext applicationContext;
	
	
	@Value("${xxl.job.admin.addresses:xxx}")
	private String adminAddresses;
	@Value("${xxl.job.accessToken:xxx}")
	private String accessToken;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
			this.applicationContext = applicationContext;
			initJob();
	}

	private void initJob() {
        log.info("============ registry xxl-job init");
        // init job handler from method
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            Object bean = applicationContext.getBean(beanDefinitionName);
            Method[] methods = bean.getClass().getDeclaredMethods();
            for (Method method: methods) {
            	XxlJobAutoRegistry annotation = AnnotationUtils.findAnnotation(method, XxlJobAutoRegistry.class);
            	 if(annotation != null) {
                 	Map<String, Object> stringMultiValueMap = new HashMap<>();
				        stringMultiValueMap.put("jobGroupName",annotation.jobGroupName());
				        stringMultiValueMap.put("jobCron",annotation.jobCron());
				        stringMultiValueMap.put("jobDesc",annotation.jobDesc());
				        stringMultiValueMap.put("author",annotation.author());
				        stringMultiValueMap.put("alarmEmail",annotation.alarmEmail());
				        stringMultiValueMap.put("executorHandler",annotation.executorHandler());	
				        stringMultiValueMap.put("executorRouteStrategy",annotation.executorRouteStrategy());				
				        stringMultiValueMap.put("executorBlockStrategy",annotation.executorBlockStrategy());				
				        stringMultiValueMap.put("executorTimeout",annotation.executorTimeout());				
				        stringMultiValueMap.put("executorFailRetryCount",annotation.executorFailRetryCount());				
				        stringMultiValueMap.put("glueType",annotation.glueType());				

				        log.info("============ registry xxl-job jsonStr,{}",JSONObject.toJSON(stringMultiValueMap));
				        ReturnT<String> postBody = XxlJobRemotingUtil.postBody(adminAddresses+"/jobinfo/addAndStart", accessToken, JSONObject.toJSON(stringMultiValueMap), 5);
				        log.info("============ registry xxl-job finish,{}",postBody.getMsg());
                 }
            }
        }
        
	}
}

package com.example.hello.cloud.framework.common.interceptor;

import cn.hutool.core.util.StrUtil;
import com.example.hello.cloud.framework.common.trace.KeyPropertyEnum;
import com.example.hello.cloud.framework.common.trace.TraceHttpHeader;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;

/**
 * feignClient的全局拦截器，转发所有header和url参数到下一个服务
 *
 * @author zhanj04
 * @date 2019/10/24 15:22
 */
public class FeignRequestInterceptor implements RequestInterceptor {
    /**
     * 日志
     */
    private static final Logger log = LoggerFactory.getLogger(FeignRequestInterceptor.class);

    @Override
    public void apply(RequestTemplate requestTemplate) {
        String traceId = MDC.get(KeyPropertyEnum.TRACE.getName());
        if (StrUtil.isNotEmpty(traceId)) {
            // 传递traceId
            requestTemplate.header(TraceHttpHeader.HTTP_HEADER_TRACE_ID.getCode(), traceId);
        }

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes == null) {
            return;
        }
        HttpServletRequest request = attributes.getRequest();
        Enumeration<String> headerNames = request.getHeaderNames();
        try {
            // 转发所有header
            if (headerNames != null) {
                Map<String, Collection<String>> headers = requestTemplate.headers();
                while (headerNames.hasMoreElements()) {
                    String name = headerNames.nextElement();
                    String values = request.getHeader(name);
                    if (!headers.containsKey(name) && !"content-length".equalsIgnoreCase(name) && !"content-type".equalsIgnoreCase(name)) {
                        requestTemplate.header(name, values);
                    }
                }
            }
        } catch (Exception e) {
            log.error("执行FeignServiceInterceptor.apply()方法,发生异常: {}", e.getMessage(), e);
        }
    }
}

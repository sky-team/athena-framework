package com.example.hello.cloud.framework.common.plugin.wechat.pay.request;

import java.io.Serializable;
import java.math.BigDecimal;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;


import com.example.hello.cloud.framework.common.plugin.wechat.pay.util.XStreamInitializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName: MiniprogramHbSendRequest.java
 * @Description: 小程序红包发放请求
 * @author guohg03
 * @date 2020年2月28日
 */
@Data
@Accessors(chain = true)
@XStreamAlias("xml")
public class MiniprogramHbSendRequest implements Serializable {
	private static final long serialVersionUID = 5779200441240916736L;

	/**
	 * 随机字符串
	 */
	@XStreamAlias("nonce_str")
	protected String nonceStr;

	/**
	 * 签名
	 */
	@XStreamAlias("sign")
	protected String sign;

	/**
	 * 商户订单号(每个订单号必须唯一 取值范围  0~9 a~z A~Z).
	 */
	@XStreamAlias("mch_billno")
	protected String mchBillno;

	/**
	 * 微信支付分配的商户号.
	 */
	@XStreamAlias("mch_id")
	protected String mchId;

	/**
	 * 微信分配的公众号id
	 */
	@XStreamAlias("wxappid")
	protected String wxappid;

	/**
	 * <pre>
	 * 字段名：红包发送者名称
	 */
	@XStreamAlias("send_name")
	protected String sendName;

	/**
	 * <pre>
	 * 字段名：用户openid(接受红包)
	 * 变量名：re_openid
	 * 是否必填：是
	 * 类型：String(32)
	 * 示例值：wxd678efh567hg6787
	 * 描述：接受红包的用户openid
	 * </pre>
	 */
	@XStreamAlias("re_openid")
	protected String reOpenid;

	/**
	 * 付款金额，单位分
	 */
	@XStreamAlias("total_amount")
	protected Integer totalAmount;

	/**
	 * 红包发放总人数
	 */
	@XStreamAlias("total_num")
	protected Integer totalNum;

	/**
	 * 红包祝福语
	 */
	@XStreamAlias("wishing")
	protected String wishing;

	/**
	 * 活动名称
	 */
	@XStreamAlias("act_name")
	protected String actName;
	
	/**
	 * 备注
	 */
	@XStreamAlias("remark")
	protected String remark;
	
	/**
	 * 通过JSAPI方式领取红包,小程序红包固定传MINI_PROGRAM_JSAPI
	 */
	@XStreamAlias("notify_way")
	protected String notifyWay;

	/**
	 * 发放红包使用场景，红包金额大于200时必传
	   PRODUCT_1:商品促销
	   PRODUCT_2:抽奖
	   PRODUCT_3:虚拟物品兑奖 
	   PRODUCT_4:企业内部福利
	   PRODUCT_5:渠道分润
 	   PRODUCT_6:保险回馈
	   PRODUCT_7:彩票派奖
	   PRODUCT_8:税务刮奖
	 */
	@XStreamAlias("scene_id")
	protected String sceneId;
	
	/**
	 * 将单位为元转换为单位为分.
	 *
	 * @param yuan 将要转换的元的数值字符串
	 * @return the integer
	 */
	public static Integer yuanToFen(String yuan) {
		return new BigDecimal(yuan).setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue();
	}
	
	/**
	 * To xml string.
	 */
	public String toXML() {
		XStream xstream = XStreamInitializer.getInstance();
		xstream.processAnnotations(this.getClass());
		return xstream.toXML(this);
	}
}
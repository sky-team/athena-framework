package com.example.hello.cloud.framework.common.plugin.yiyun.core.api;

/**
 * 翼云中台接口服务
 *
 * @author v-linxb
 * @create 2018/5/3
 **/
public interface YiyunServiceType {
    /**
     * api地址
     *
     * @return api地址
     */
    String url();

    /**
     * api描述
     *
     * @return api描述
     */
    String desc();

    /**
     * 访问中台api是否需要带上ticket
     *
     * @return 是否需要ticket
     */
    boolean needTicket();
}

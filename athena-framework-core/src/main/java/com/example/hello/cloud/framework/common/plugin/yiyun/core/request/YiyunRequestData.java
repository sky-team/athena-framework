package com.example.hello.cloud.framework.common.plugin.yiyun.core.request;

import java.io.Serializable;

/**
 * 中台请求数据接口标识，所有请求的参数必须继承接口
 *
 * @author v-linxb
 * @create 2018/5/4
 **/
public interface YiyunRequestData extends Serializable {

    /**
     * 请求中台接口需要带上此ticket，除登录注册发送短信验证等接口除外
     *
     * @return ticket
     */
    String getTicket();

    /**
     * 设置ticket
     *
     * @param ticket ticket
     */
    void setTicket(String ticket);

    /**
     * 租户id
     *
     * @return 租户id
     */
    String getTenantId();

    /**
     * 设置租户id（1,2,3）
     *
     * @param tenantId 租户id
     */
    void setTenantId(String tenantId);
}

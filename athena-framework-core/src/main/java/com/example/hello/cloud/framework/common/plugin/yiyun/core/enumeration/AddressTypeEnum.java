package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 地址类型枚举
 *
 * @author zhoujj07
 * @create 2018/5/29
 */
public enum AddressTypeEnum {

    /**
     * 联系地址
     */
    CONTACT_ADDRESS("00", "联系地址"),
    /**
     * 收货地址
     */
    DELIVERY_ADDRESS("01", "收货地址"),;

    private String name;
    private String index;

    AddressTypeEnum(String index, String name) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public String getIndex() {
        return index;
    }
}

package com.example.hello.cloud.framework.common.plugin.openapi.core;

import java.util.Map;

/**
 * 万科API服务市场接口
 * 服务市场中各API响应的json结构不统一，因此直接返回响应的json，各自解析
 *
 * @author v-linxb
 * @create 2019/9/4
 **/
public interface exampleOpenApiService {


    /**
     * API服务市场数据中台接口方法(POST)
     *
     * @param api           接口地址
     * @param requestObject 请求参数
     * @return 序列化的json数据
     */
    String postAction(OpenApiServiceType api, OpenApiBaseReqData requestObject);

    /**
     * API服务市场数据中台接口方法(POST)
     *
     * @param api        接口地址
     * @param requestMap 请求参数
     * @return 序列化的json数据
     */
    String postAction(OpenApiServiceType api, Map requestMap);

    /**
     * API服务市场数据中台接口方法(GET)
     *
     * @param api           接口地址
     * @param requestObject 请求参数
     * @return 序列化的json数据
     */
    String getAction(OpenApiServiceType api, OpenApiBaseReqData requestObject);

    /**
     * API服务市场数据中台接口方法(GET)
     *
     * @param api        接口地址
     * @param requestMap 请求参数
     * @return 序列化的json数据
     */
    String getAction(OpenApiServiceType api, Map requestMap);
}

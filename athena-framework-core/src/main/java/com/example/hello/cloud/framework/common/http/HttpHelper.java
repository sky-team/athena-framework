package com.example.hello.cloud.framework.common.http;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Http帮助类
 *
 * @author zhoujj07
 * @create 2018/4/3
 */
@Slf4j
public class HttpHelper {
    /**
     * 获取http request请求中的body信息
     *
     * @param request ServletRequest
     * @return body字符串
     */
    public static String getBodyString(ServletRequest request) {
        StringBuilder sb = new StringBuilder();
        try(InputStream inputStream = request.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")))){
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
        	log.error(e.getMessage(),e);
        }
        return sb.toString();
    }
}

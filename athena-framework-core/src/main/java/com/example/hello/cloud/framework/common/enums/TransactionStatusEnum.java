package com.example.hello.cloud.framework.common.enums;

/**
 * 翼云-交易单据状态
 *
 * @author pengdm.
 * 2018/5/21.
 */
public enum TransactionStatusEnum {

    //认筹
    CHIP_ORDER_INIT(TransactionTypeEnum.CHIP_ORDER, "init", "SD2002002", "正常"),
    CHIP_ORDER_TO_BUY(TransactionTypeEnum.CHIP_ORDER, "toBuy", "SD2002006", "已转认购"),
    CHIP_ORDER_CANCEL(TransactionTypeEnum.CHIP_ORDER, "cancel", "SD2002004", "已退筹"),
    CHIP_ORDER_TRANSFER(TransactionTypeEnum.CHIP_ORDER, "transfer", "SD2002014", "已转方案"),

    //认购
    BUY_ORDER_PENDING_APPROVE(TransactionTypeEnum.BUY_ORDER, "pendingApprove", "SD2002001", "待审"),
    BUY_ORDER_INIT(TransactionTypeEnum.BUY_ORDER, "init", "SD2002002", "正常"),
    BUY_ORDER_TRANSFER(TransactionTypeEnum.BUY_ORDER, "transfer", "SD2002003", "已转"),
    BUY_ORDER_BUY_CANCEL(TransactionTypeEnum.BUY_ORDER, "buyCancel", "SD2002004", "已退"),
    BUY_ORDER_BUY_CHANGE(TransactionTypeEnum.BUY_ORDER, "buyChange", "SD2002005", "已换"),

    //签约
    SIGN_ORDER_INIT(TransactionTypeEnum.SIGN_ORDER, "init", "SD2002002", "正常"),
    SIGN_ORDER_INIT_TRANSFER(TransactionTypeEnum.SIGN_ORDER, "transfer", "SD2002003", "已转"),
    SIGN_ORDER_INIT_SIGN_CANCEL(TransactionTypeEnum.SIGN_ORDER, "signCancel", "SD2002004", "已退"),
    SIGN_ORDER_INIT_SIGN_CHANGE(TransactionTypeEnum.SIGN_ORDER, "signChange", "SD2002005", "已换"),
    SIGN_ORDER_REVOCATION(TransactionTypeEnum.SIGN_ORDER, "revocation", "SD2002007", "撤销");

    private TransactionTypeEnum transactionType;

    private String status;

    private String desc;

    private String tradeStatusId;

    TransactionStatusEnum(TransactionTypeEnum transactionType, String status, String tradeStatusId, String desc) {
        this.transactionType = transactionType;
        this.status = status;
        this.tradeStatusId = tradeStatusId;
        this.desc = desc;
    }

    public TransactionTypeEnum getTransactionType() {
        return transactionType;
    }

    public String getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public String getTradeStatusId() {
        return tradeStatusId;
    }
}

package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author v-hut12
 * @date 2018/9/28
 * 小程序登录响应额外信息
 */
@Data
@ApiModel("小程序登录响应额外信息")
public class AppletSessionInfo implements Serializable{
    private static final long serialVersionUID = -1689175544640036079L;
    
    /**
     * 0,1,2
     * 客户状态:0,数据库没有用户信息
     * 1，只有用户基本信息，没有手机号
     * 2:已经有用户手机号，用户登录成功，拥有所有权限
     */
    @ApiModelProperty("客户状态: 0:数据库没有用户信息;1:只有用户基本信息，没有手机号;2:已经有用户手机号，用户登录成功，拥有所有权限")
    private Integer status;

    /**
     * 如果是经纪人,该值不为空
     */
    @ApiModelProperty("如果是经纪人,该值不为空")
    private String agentId;

    /**
     * 手机号,手机解密时用到，其他无用
     */
    @ApiModelProperty("phone")
    private String phone;

    /**
     * 微信用户昵称
     */
    @ApiModelProperty("微信用户昵称")
    private String nickName;

    /**
     * 微信用户头像
     */
    @ApiModelProperty("微信用户头像")
    private String avatarUrl;

    /**
     * 小程序微信信息（数据不作为response返回）
     */
    @JsonIgnore
    private AppletKeyInfo appletKeyInfo;
}
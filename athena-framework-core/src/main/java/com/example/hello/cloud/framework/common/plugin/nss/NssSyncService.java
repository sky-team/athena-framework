package com.example.hello.cloud.framework.common.plugin.nss;

import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.nss.dto.*;

import java.util.List;

/**
 * 上报相关信息到销售系统，包括token,销售团队，销售顾问，项目经理，代理经理，客户,营销途径等
 *
 * @author zhoujj07
 * @create 2018/9/29
 */
public interface NssSyncService {
    /**
     * 获取token
     * 
     * @return
     */
    String syncToken();

    /**
     * 顶级团队上报（团队上报）
     * 
     * @param syncTeamDTO
     * @return
     */
    SyncTeamResultDTO syncTeam(SyncTeamDTO syncTeamDTO);

    /**
     * 二级团队上报（团队上报）
     * 
     * @param syncTeamDTO
     * @return
     */
    SyncTeamResultDTO syncSecondTeam(SyncTeamDTO syncTeamDTO);

    /**
     * 岗位上报（团队上报）
     * 
     * @param syncTeamDTO
     * @return
     */
    SyncTeamResultDTO syncJobTeam(SyncTeamDTO syncTeamDTO);

    /**
     * 用户权限上报
     * 
     * @param syncConsultantDTO
     * @return
     */
    SyncCosultantResultDTO syncSalers(SyncConsultantDTO syncConsultantDTO);

    /**
     * 到访客户上报
     * 
     * @param syncCustomerDTO
     * @return
     */
    SyncCustomerResultDTO syncCustomer(SyncCustomerDTO syncCustomerDTO);

    /**
     * 修改客户资料上报
     * 
     * @param syncUpdateCustomerDTO
     * @return
     */
    SyncUpdateCustomerResultDTO syncUpdateCustomer(SyncUpdateCustomerDTO syncUpdateCustomerDTO);

    /**
     * 翼销售修改客户资料拉取
     * 
     * @param syncPullCustomerDTO
     * @return
     */
    List<SyncPullCustomerResultDTO> syncPullCustomer(SyncPullCustomerDTO syncPullCustomerDTO);

    /**
     * 转客上报
     * 
     * @param syncTurnCustomerDTO
     * @return
     */
    SyncTurnCustomerResultDTO syncTurnCustomer(SyncTurnCustomerDTO syncTurnCustomerDTO);

    /**
     * 营销途径上报
     * 
     * @param syncCognitiveDTO
     * @return
     */
    SyncCognitiveResultDTO syncCognitive(SyncCognitiveDTO syncCognitiveDTO);

    String getTurnValue(String key, String value);

    SyncCustomerResultDTO syncCustomer(String postData);

    /**
     * 根据销售组织id，客户id查询，这个推广项目下的所有客户
     * 
     * @param postData
     * @return
     */
    List<SyncCustomerResultDTO> getInfoBySalesOrgIdAndCusId(String postData);

    Boolean insertTeamRelation(String postData);

    Boolean updateConsultantName(SysUserDTO sysUserDTO);

    Boolean changeTeam(String postData);

    Boolean syncSalesteamDelete(String postData);

    Boolean saleMemberUpdate(SysUserDTO sysUserDTO);

    void notifyDiscountCoupon(SyncCouponInfoDTO syncCouponInfoDTO);

    void notifyDiscountCouponCustomer(SyncCouponCustomerInfoDTO syncCouponCustomerInfoDTO);
}

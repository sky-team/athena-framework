package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/9.
 */
@Data
public class SyncCognitiveResultDTO extends SyncResultDTO implements Serializable {
    private String nssChannelId;
    private String nssChannelName;

    @Override
    public String toString() {
        return "SyncCognitiveResultDTO{" +
                "nssChannelId='" + nssChannelId + '\'' +
                ", nssChannelName='" + nssChannelName + '\'' +
                "errCode= "+ getErrCode()+" ,errMsg=" + getErrMsg() +
                '}';
    }
}

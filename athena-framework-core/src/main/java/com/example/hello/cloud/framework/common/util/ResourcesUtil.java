package com.example.hello.cloud.framework.common.util;

import cn.hutool.core.util.StrUtil;
import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 配置文件读取工具类
 *
 * @author guohg03
 * @date 2018年7月29日
 */
@Slf4j
public class ResourcesUtil extends PropertyPlaceholderConfigurer {
    private static Map<String, String> ctxPropertiesMap = new HashMap<>(16);

    /**
     * 重写processProperties方法
     */
    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props)
            throws BeansException {
        super.processProperties(beanFactoryToProcess, props);
        String k8S_namespace = System.getenv("K8S_NAMESPACE");
        log.info("当前环境：{}", k8S_namespace);
        if (StrUtil.isNotBlank(k8S_namespace)) {
            ctxPropertiesMap.put("k8s.namespace", k8S_namespace);
        }
        String namespaces = props.getProperty("apollo.bootstrap.namespaces");
        if (StrUtil.isNotEmpty(namespaces)) {
            Config publicConfig = ConfigService.getConfig("hello.cloud.public");
            getProperty(publicConfig);
            publicConfig = ConfigService.getConfig("hello.cloud.psm.public");
            getProperty(publicConfig);

            Config privateConfig = ConfigService.getConfig("application");
            getProperty(privateConfig);
        } else {
            for (Object key : props.keySet()) {
                String keyStr = key.toString();
                String value = props.getProperty(keyStr);
                ctxPropertiesMap.put(keyStr, value);
            }
        }
    }

    private void getProperty(Config config) {
        if (config != null && CollectionUtils.isNotEmpty(config.getPropertyNames())) {
            for (Object key : config.getPropertyNames()) {
                String keyStr = key.toString();
                String value = config.getProperty(keyStr, "");
                ctxPropertiesMap.put(keyStr, value);
            }
        }
    }

    /**
     * 获取对应键的值
     */
    public static String getProperty(String name) {
        if (ctxPropertiesMap != null && ctxPropertiesMap.keySet().contains(name)) {
            return ctxPropertiesMap.get(name);
        }
        return null;
    }

    /**
     * 获取对应键的值(没有值，取默认值defaultVal)
     */
    public static String getProperty(String name, String defaultVal) {
        if (ctxPropertiesMap != null && ctxPropertiesMap.keySet().contains(name)) {
            String localVal = ctxPropertiesMap.get(name);
            return StrUtil.isNotEmpty(localVal) ? localVal : defaultVal;
        }
        return defaultVal;
    }

    public static String getK8sNamespace(String name) {
        if (ctxPropertiesMap != null && ctxPropertiesMap.keySet().contains(name)) {
            return ctxPropertiesMap.get(name);
        }
        String k8S_namespace = System.getenv("K8S_NAMESPACE");
        return k8S_namespace;
    }

    /**
     * 获取对应键的整形值
     */
    public static Integer getInteger(String name) {
        String val = getProperty(name);
        return StrUtil.isNotEmpty(val) ? Integer.valueOf(val) : null;
    }

    /**
     * 获取对应键的整形值(没有值，取默认值defaultVal)
     */
    public static Integer getInteger(String name, Integer defaultVal) {
        String val = getProperty(name);
        return StrUtil.isNotEmpty(val) ? Integer.valueOf(val) : defaultVal;
    }

    /**
     * 获取对应键的长整形值
     */
    public static Long getLong(String name) {
        String val = getProperty(name);
        return StrUtil.isNotEmpty(val) ? Long.valueOf(val) : null;
    }

    /**
     * 获取对应键的长整形值(没有值，取默认值defaultVal)
     */
    public static Long getLong(String name, Long defaultVal) {
        String val = getProperty(name);
        return StrUtil.isNotEmpty(val) ? Long.valueOf(val) : defaultVal;
    }

    /**
     * 获取对应键的double值
     */
    public static Long getDouble(String name) {
        String val = getProperty(name);
        return StrUtil.isNotEmpty(val) ? Long.valueOf(val) : null;
    }

    /**
     * 获取对应键的double值(没有值，取默认值defaultVal)
     */
    public static Double getDouble(String name, Double defaultVal) {
        String val = getProperty(name);
        return StrUtil.isNotEmpty(val) ? Double.valueOf(val) : defaultVal;
    }

    /**
     * 获取对应键的boolean值
     */
    public static boolean getBoolean(String name) {
        String val = getProperty(name);
        return Boolean.valueOf(val);
    }

    /**
     * 获取对应键的boolean值(没有值，取默认值defaultVal)
     */
    public static boolean getBoolean(String name, Boolean defaultVal) {
        String val = getProperty(name);
        return StrUtil.isNotEmpty(val) ? Boolean.valueOf(val) : defaultVal;
    }

    /**
     * 修改对应键的值
     */
    public static Object setProperty(String name, Object value) {
        return ctxPropertiesMap.put(name, value.toString());
    }

    public static Map<String, String> getContextPropertyMap() {
        return ctxPropertiesMap;
    }
}
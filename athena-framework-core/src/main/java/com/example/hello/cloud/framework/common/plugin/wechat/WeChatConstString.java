package com.example.hello.cloud.framework.common.plugin.wechat;

/**
 * Created by v-hut12 on 2018/9/28.
 * WeChatConstString
 */
public class WeChatConstString {

    /***
     * 获取微信用户信息地址
     */
    public final static String USER_INFO_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=zh_CN";
    public final static String Access_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";
    public final static String QRCODE_CREATE_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s";

    /**
     * 未关注公众号时 获取用户信息的地址
     */
    public final static String USER_INFO_BY_NOT_SIGN = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN";
    public final static String APPLET_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?appid=%s&secret=%s&grant_type=client_credential";
    public final static String APPLET_SESSION_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";
    public final static String APPLET_APPID = "wx8e7bfe0f4e6d7b29";
    public final static String APPLET_SECRET = "e5db9d979467732434f40573f4a6eb2c";
    public final static String WXE_APPLET_APPID = "wxaac38610878d0feb";
    public final static String ZYSQ_APPLET_APPID = "wxe9692d75c9e71da7";
    public final static String ZYSQ_APPLET_SECRET = "e7334a61fb0dc7e8eda974b304ce54ed";
    public final static String ZYSQ_SIGN_APPLET_ONLINE_APPID = "wxce9ed5907a551ef0";
    public final static String ZYSQ_SIGN_APPLET_ONLINE_SECRET = "87cada71937e8bcee9940eedfa567eef";

    public final static String ZYSQ_SIGN_APPLET_APPID = "wx2007d6fefb22bbe9";
    public final static String ZYSQ_SIGN_APPLET_SECRET = "4acc9a9c7fe1af668f4f7fb0d267aeec";
    public final static String ZYSQ_APPLET_ONLINE_APPID = "wx1f6ef77faaff2c59";
    public final static String ZYSQ_APPLET_ONLINE_SECRET = "db28a19a7d2d44ca9582eddac3c3b4e8";
    public final static String ZYSQ_APPLET_AGENT_APPID = "wx1f6ef77faaff2c59";
    public final static String ZYSQ_APPLET_AGENT_SECRET = "db28a19a7d2d44ca9582eddac3c3b4e8";
    public final static String ZYSQ_APPLET_CHAT_APPID = "wxec5088e62d7f2441";
    public final static String ZYSQ_APPLET_CHAT_SECRET = "6c0688d0de2e5209676e36cfc191f0df";
    public final static String ZYSQ_BARGAIN_APPLET_APPID = "wxee3d8a6b78c48524";
    public final static String ZYSQ_BARGAIN_APPLET_SECRET = "016ec63d507b12b3c9d502365150d052";
    public final static String ZYSQ_TIANJIN_APPLET_APPID = "wx9f22cb8ef817791b";
    public final static String ZYSQ_TIANJIN_APPLET_SECRET = "f926c4f8a6509f571fbb2f9feb33be46";
    /**
     * 拓客神器appid
     */
    public final static String TOKER_APPLET_APPID = "wxe25ec68e4c068492";
    /**
     * 拓客神器appsecret
     */
    public final static String TOKER_APPLET_SECRET = "ffd1eee162729fb4dbd267d09fd7d1ce";

    /**
     * 适用于需要的码数量较少的业务场景 接口地址：
     */
    public final static String APPLET_WXACODE_A_URL = "https://api.weixin.qq.com/wxa/getwxacode?access_token=%s";
    public final static String APPLET_WXACODE_B_URL = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s";

    public final static String APPLET_MAP_SEARCH = "https://apis.map.qq.com/ws/place/v1/search?keyword=%s&orderby=_distance&page_size=20&page_index=1&output=json&key=%s&boundary=nearby(%s)";
    public final static String APPLET_MAP_KEY = "TEUBZ-WV2RG-5NKQY-IZL4S-4JW2Q-NJFJF";//-- 测试KEY （MZVBZ-U72WJ-FT6FX-KCBIR-T2IMZ-H5BPE)
    public final static String SECURITYMSGSECCHECK_URL = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token=%s";

}
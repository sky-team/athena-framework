package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author :v-zhousq04
 * @description: 产品映射
 * @create date: 2018/8/29 14:49
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductMappingQueryReq extends BaseYiyunRequestData {

    private static final long serialVersionUID = 8299008589064651446L;

    private Integer categoryId;     //所属类目id
    private String outerApp;        //外部应用
    private String outerId;         //外部编码，可以存放productCode
    private String propCodes;       //需要返回的产品属代码

}

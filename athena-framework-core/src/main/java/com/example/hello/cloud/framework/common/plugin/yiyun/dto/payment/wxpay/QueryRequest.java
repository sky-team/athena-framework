package com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment.wxpay;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;

import java.io.Serializable;

/**
 * 支付信息
 *
 * @author mall
 */
@Data
public class QueryRequest extends BaseYiyunRequestData implements Serializable {

    /*
     * merchantNo	    字符串	商户号	    是
     * paymentOrderNo	字符串	支付订单号	否 两者必填其一或者两者都填
     * tradeOrderNo	    字符串	交易订单号 否
     * */
    private String merchantNo;
    private String paymentOrderNo;
    private String tradeOrderNo;

}
package com.example.hello.cloud.framework.common.plugin.yiyun.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.hello.cloud.framework.common.exception.ArgumentException;
import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunProperties;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.exception.ArgumentException;
import com.example.hello.cloud.framework.common.http.HttpSyncClientUtil;
import com.example.hello.cloud.framework.common.http.common.HttpConfig;
import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.SignUtils;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunServiceType;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequest;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.MidReqResult;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.MiddleTransService;
import com.example.hello.cloud.framework.common.util.ReflectionUtil;
import com.example.hello.cloud.framework.common.util.json.GsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 调用中台交易接口
 *
 * @author hey54
 */
@Service
public class MiddleTransServiceImpl implements MiddleTransService {

    private static final Logger logger = LoggerFactory.getLogger(MiddleTransServiceImpl.class);
    @Autowired
    private YiyunProperties yiyunProperties;

    @Override
    public MidReqResult actionMidReqResult(YiyunServiceType serviceType, YiyunRequestData request) {
        logger.info("调用中台api[{}-{}-{}-{}]",
                serviceType.url(),
                serviceType.desc(),
                yiyunProperties.getAppId(),
                yiyunProperties.getSecret());
        if (serviceType.needTicket() && StringUtils.isEmpty(request.getTicket())) {
            throw new ArgumentException("访问翼云中台api需要传入ticket：" + serviceType.url());
        }
        YiyunRequest yiyunRequest = packRequest(request, yiyunProperties.getAppId());
        MidReqResult result = new MidReqResult();
        try {
            String resultStr = post(serviceType.url(), yiyunRequest, yiyunProperties.getSecret());
            // 把json字符串转换成json对象
            JSONObject jsonResult = JSONObject.parseObject(resultStr);
            result = getReqResult(jsonResult);
        } catch (Exception e) {
            String errMsg = "调用中台api异常" + serviceType.url();
            logger.error(errMsg + "{}", e);
            result.setCode(ApiResponseCodeEnum.EXCEPTION.getCode().toString());
            result.setMessage("调用中台api异常!");
        }
        return result;
    }

    /**
     * 解析返回结果
     *
     * @param resultJSON
     * @return
     */
    public MidReqResult getReqResult(JSONObject resultJSON) {
        String code = resultJSON.getString("code");
        String message = resultJSON.getString("msg");
        String httpStatus = resultJSON.getString("httpStatus");
        MidReqResult reqResult;
        try {
            JSONObject data = resultJSON.getJSONObject("data");
            reqResult = new MidReqResult(code, message, data, httpStatus);
        } catch (Exception e) {
            logger.error("解析异常{}", e);
            JSONArray dataArray = resultJSON.getJSONArray("data");
            reqResult = new MidReqResult(code, message, dataArray, httpStatus);
        }
        return reqResult;
    }

    private String post(String url, YiyunRequest object, String secret) throws IOException {
        Map<String, String> map = objectToMap(object);
        map.put("sign", SignUtils.signTopRequest(map, secret, map.get("sign_method")));
        Map<String, Object> objectMap = strMapToObjMap(map);
        // 发起请求
        HttpConfig config = HttpConfig.custom().contentType(ContentType.APPLICATION_FORM_URLENCODED.getMimeType())
                .url(yiyunProperties.getHost() + url).map(objectMap);
        logger.info("=======交易请求接口地址：{}", yiyunProperties.getHost() + url);
        String result = StringUtils.EMPTY;
        try {
            result = HttpSyncClientUtil.post(config);
        } catch (Exception e) {
            logger.error("解析异常：{}", e);
        }
        return result;
    }

    /**
     * 组装请求数据
     */
    private YiyunRequest packRequest(YiyunRequestData request, String appId) {
        YiyunRequest yiyunRequest = new YiyunRequest();
        yiyunRequest.setAppId(appId);
        yiyunRequest.setTicket(request.getTicket());
        yiyunRequest.setTenantId(request.getTenantId());
        yiyunRequest.setBizParam(GsonUtil.toJson(request, false));
        return yiyunRequest;
    }

    /**
     * 对象转Map（value为空不进行转换）
     */
    private Map<String, String> objectToMap(YiyunRequest obj) {
        List<Field> fields = ReflectionUtil.getAccessibleFields(obj);
        Map<String, String> map = new HashMap<>(16);
        fields.forEach(field -> {
            Object value = ReflectionUtil.invokeGetter(obj, field.getName());
            if (null != value) {
                map.put(field.getName(), value.toString());
            }
        });
        return map;
    }

    /**
     * 将Map<String,String>转成Map<String,Object>，并进行URLEncode
     */
    private Map<String, Object> strMapToObjMap(Map<String, String> map) {
        Iterator<String> it = map.keySet().iterator();
        Map<String, Object> stringObjectMap = new HashMap<>(16);
        while (it.hasNext()) {
            String key = it.next();
            String value = map.get(key);
            stringObjectMap.put(key, value);
        }
        return stringObjectMap;
    }
}

package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 属性组合类型
 *
 * @author v-linxb
 * @create 2018/6/1
 **/
public enum BundleTypeEnum {

    /**
     * 楼盘户型交付标准
     */
    HOUSE_STANDARD(1),
    /**
     * 楼盘装修亮点
     */
    ITEM_ESTATE_DECORATE_LIGHTSPOT(2),
    /**
     * 楼盘亮点（交通、教育、医疗、商业）
     */
    ITEM_ESTATE_LIGHTSPOT(3),
    /**
     * 楼盘分期
     */
    ITEM_ESTATE_PHASE(4),
    /**
     * 楼盘动态
     */
    @Deprecated
    ITEM_ESTATE_NEWS(5);

    BundleTypeEnum(Integer value) {
        this.value = value;
    }

    private Integer value;


    public Integer getValue() {
        return this.value;
    }
}

package com.example.hello.cloud.framework.common.plugin.yiyun.inf;

import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.customer.QuerySpecialTagBizParam;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.customer.QuerySpecialTagResData;

/**
 * @author zhoujj07
 * @ClassName: ${CLASSNAME}
 * @Description: ${DESCRIPTION}
 * @date 2018/8/23
 */
public interface CustomerInfoService {

    /**
     * 查询客户账号标签信息
     *
     * @param bizParam tag入参
     * @return tag查询结果
     */
    ApiResponse<QuerySpecialTagResData> querySpecialTag(QuerySpecialTagBizParam bizParam);

}

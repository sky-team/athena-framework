package com.example.hello.cloud.framework.common.util;

import javax.servlet.http.HttpServletRequest;

import com.example.hello.cloud.framework.common.util.http.HttpUtil;
import com.google.gson.Gson;

import com.example.hello.cloud.framework.common.util.http.HttpUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 摘要：IP工具类
 * @author guohg03
 * @version 1.0
 * @Date 2018年7月27日
 */
@Slf4j
public class IPUtil {

	/**
	 * 功能：获取IP
	 */
	public static String getIP(HttpServletRequest request) {
		String ip = request.getHeader("X-Real-IP");
		if (StringUtil.isNotEmpty(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return ip;
		}
		ip = request.getHeader("X-Forwarded-For");
		if (StringUtil.isNotEmpty(ip) && !"unknown".equalsIgnoreCase(ip)) {
			// 多次反向代理后会有多个IP值，第一个为真实IP。
			int index = ip.indexOf(',');
			if (index != -1) {
				ip = ip.substring(0, index);
			}
			if ("0:0:0:0:0:0:0:1".equals(ip)) {
				ip = "127.0.0.1";
			}
			return ip;
		}
		return request.getRemoteAddr();
	}

	/**
	 * 功能：通过IP返回地理信息
	 */
	public String getCityInfo(String ip, String appKey) {
		String cloudUrl = "http://apicloud.mob.com/ip/query?key=" + appKey + "&ip=";
		if (null != ip) {
			String url = cloudUrl + ip;
			String result = "未知";
			try {
				String json = HttpUtil.doGet(url);
				IpLocate locate = new Gson().fromJson(json, IpLocate.class);
				if (("200").equals(locate.getRetCode())) {
					if (StringUtil.isNotBlank(locate.getResult().getProvince())) {
						result = locate.getResult().getProvince() + " " + locate.getResult().getCity();
					} else {
						result = locate.getResult().getCountry();
					}
				}
			} catch (Exception e) {
				log.error(e.getMessage(),e);
			}
			return result;
		}
		return null;
	}

	@Data
	public class City {
		String country;
		String province;
		String city;
	}
	@Data
	public class IpLocate {
		String retCode;
		City result;
	}
}

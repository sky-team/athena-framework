package com.example.hello.cloud.framework.common.plugin.yiyun.core.request;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by zhuf04 on 2018/6/7.
 */
@Data
public class PropQueryCriteria implements Serializable {

    /**
     *查询条件
     */
    private ProCriteria criteria;
    /**
     *排序条件
     */
    private ProSort sort;


}

package com.example.hello.cloud.framework.common.plugin.yiyun.impl;

import com.alibaba.fastjson.JSON;
import com.example.hello.cloud.framework.common.plugin.openapi.api.YiyunProCusProfileApiEnum;
import com.example.hello.cloud.framework.common.plugin.openapi.core.exampleOpenApiService;
import com.example.hello.cloud.framework.common.plugin.openapi.api.YiyunProCusProfileApiEnum;
import com.example.hello.cloud.framework.common.plugin.openapi.core.exampleOpenApiService;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.ProCusProfileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 涂永康
 * @className
 * @date 2019/8/6 16:30
 * @description
 */
@Slf4j
@Service
public class ProCusProfileServiceImpl implements ProCusProfileService {

    @Autowired
    private com.example.hello.cloud.framework.common.plugin.openapi.core.exampleOpenApiService exampleOpenApiService;

    private static final String ERROR_R_TEMPLATE = "{\"code\":\"-2\",\"msg\":\"接口调用异常\",\"data\":null}";

    @Override
    public String prdCusBasicInfoReport(Map<String, Object> param) {
        log.info("进入ProCusProfileService.prdCusBasicInfoReport()方法，入参param={}", JSON.toJSONString(param));
        String r = null;
        try {
            r = exampleOpenApiService.postAction(YiyunProCusProfileApiEnum.PRD_CUS_BASIC_INFO_REPORT, param);
        } catch (Exception e) {
            log.info(e.getCause().getMessage());
        }
        log.info("退出ProCusProfileService.prdCusBasicInfoReport()方法，结果result={}", r);
        return r;
    }

    @Override
    public String prdCusDepictReport(Map<String, Object> param) {
        log.info("进入ProCusProfileService.prdCusDepictReport()方法，入参param={}", JSON.toJSONString(param));
        String r = null;
        try {
            r = exampleOpenApiService.postAction(YiyunProCusProfileApiEnum.PRD_CUS_DEPICT_REPORT, param);
        } catch (Exception e) {
            log.info(e.getCause().getMessage());
        }
        if (r == null) {
            r = ERROR_R_TEMPLATE;
        }
        log.info("退出ProCusProfileService.prdCusDepictReport()方法，结果result={}", r);
        return r;
    }

    @Override
    public String cusProfilePage(Map<String, Object> param) {
        log.info("进入ProCusProfileService.cusProfilePage()方法，入参param={}", JSON.toJSONString(param));
        String r = null;
        try {
            r = exampleOpenApiService.postAction(YiyunProCusProfileApiEnum.CUS_PROFILE_PAGE, param);
        } catch (Exception e) {
            log.info(e.getCause().getMessage());
        }
        if (r == null) {
            r = ERROR_R_TEMPLATE;
        }
        log.info("退出ProCusProfileService.cusProfilePage()方法，结果result={}", r);
        return r;
    }

    @Override
    public String cusProfileDetails(Map<String, Object> param) {
        log.info("进入ProCusProfileService.cusProfileDetails()方法，入参param={}", JSON.toJSONString(param));
        String r = null;
        try {
            r = exampleOpenApiService.postAction(YiyunProCusProfileApiEnum.CUS_PRO_FILE_DETAILS, param);
        } catch (Exception e) {
            log.info(e.getCause().getMessage());
        }
        if (r == null) {
            r = ERROR_R_TEMPLATE;
        }
        log.info("退出ProCusProfileService.cusProfileDetails()方法，结果result={}", r);
        return r;
    }
}

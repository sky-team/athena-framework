package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import lombok.Data;

import java.io.Serializable;

@Data
public class QueryCustInfoVo implements Serializable {
    /**
     * 业主姓名
     */
    private String name;
    /**
     * 业主姓名
     */
    private String nameHidden;
    /**
     * 业主证件号
     */
    private String certNo;
    /**
     * 业主证件号隐号处理
     */
    private String certNoHidden;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 是否亲属
     */
    private Integer registerIdentity;

    /**
     * 为亲属时本人name
     */
    private String ownerName;
}

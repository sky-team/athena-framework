package com.example.hello.cloud.framework.common.util.excel;

import cn.hutool.core.date.DateUtil;
import com.example.hello.cloud.framework.common.util.excel4j.ProgressService;
import com.example.hello.cloud.framework.common.util.excel4j.ProgressUtil;
import com.example.hello.cloud.framework.common.util.excel4j.ProgressService;
import com.example.hello.cloud.framework.common.util.excel4j.ProgressUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author guohg03
 * @ClassName: POIXSSFExcelBuilder
 * @Description: 模板大标题、小标题、数据列表、统计 都已固定样式
 * <p>
 * 支持$V{NO}是否生成序号，固定写法，并且生成在每行的第一列；
 * 支持$T{xxx}参数，xxx代表List<Bean>集合中Bean对象的类型；
 * 支持$F{xxx}参数，xxx代表List<Bean>集合中Bean对象的属性；
 * 支持$F{xxx.zzz}参数写法，其中xxx代表List<Bean>集合中Bean对象的子对象,zzz代码子对象的属性；
 * 支持$P{xxx}参数，xxx代表POIXSSFExcelBuilder对象属性pMap的Key值；
 * <p>
 * 注意：
 * 1、$V{NO}固定写法只能有一个参数；$T{xxx}、$F{xxx}与$P{xxx}可以有多个参数；
 * 2、$V{NO}与$F{xxx}必须在同一行，且只能有一行；$V{NO}如果有只能在第一列；
 * 3、$P{xxx}只能有一行；
 * 4、对于大数值类型的列，不显示科学计算法E+12等，需设置模板单元格格式：选择F类型参数单元格-右键-设置单元格格式-自定义-类型=0.00；并设置setDataFormatFlag为true
 * 5、避免耗系统资源，设置了最大导出30万条；
 * @date 2018年7月25日
 */
@Slf4j
public class POIXSSFExcelBuilder {


    /**
     * 待导出数据列表转换后
     */
    private List<HashMap<String, Object>> transBeanList = new ArrayList<>();

    /**
     * P类型参数对象-外部传入
     */
    private HashMap<String, Object> pMap = new HashMap<>();

    /**
     * 序号数据, 默认1
     */
    private int no = 1;

    /**
     * 是否需要设置序号数据, 默认false
     */
    private boolean noFlag = false;

    /**
     * Excel中F类型集合-对应的下标
     */
    private int fRowNum;

    /**
     * Excel中P类型集合-对应的下标
     */
    private int pRowNum;

    /**
     * Excel中T类型集合
     */
    private List<String> tKeyList = new ArrayList<>();

    /**
     * 创建SXSSFWorkbook.createRow时的下标
     */
    private int createRowNum = 0;

    /**
     * 定义excel工作簿-只写入
     */
    private Workbook wb = null;

    /**
     * 定义excel工作簿-为模板数据
     */
    private Workbook xswb = null;

    //后置执行的生成进度
    private ProgressUtil progressUtil;

    private ProgressService progressService;

    private String progressId;

    /**
     * 默认日期格式
     */
    private String defaultDateFormat = "yyyy-MM-dd";
    private String defaultTimeFormat = "HH:mm:ss";
    private String defaultDateTimeFormat = defaultDateFormat + " " + defaultTimeFormat;

    /**
     * 创建公共cellStyle，因为每个单元格都通过wb.createCellStyle()创建的话，导出性能会快速下降
     */
    /**
     * 大标题 cellstyle
     */
    private CellStyle cellstyleT1 = null;

    /**
     * 小标题 cellstyle
     */
    private CellStyle cellstyleT2 = null;

    /**
     * P类型 cellstyle
     */
    private CellStyle cellstyleP = null;

    /**
     * F类型  cellstyle
     */
    private CellStyle cellstyleF = null;

    /**
     * P类型 cellstyle,对应每列的cellStyle
     */
    private Map<String, CellStyle> cellstylePMap = null;

    /**
     * F类型 cellstyle,对应每列的cellStyle
     */
    private Map<String, CellStyle> cellstyleFMap = null;

    /**
     * 设置单元格格式-自定义-开关
     * 默认不使用，需要时开启，因为写入大量数据时createCellStyle很占性能
     */
    private boolean setDataFormatFlag = false;

    /**
     * sheet名称
     */
    private String sheetName = null;

    /**
     * 构造方法1
     */
    public POIXSSFExcelBuilder(File excelFile) throws IOException {
        this(new FileInputStream(excelFile));
    }

    /**
     * 构造方法3
     */
    public POIXSSFExcelBuilder(File excelFile, String fileExt) throws IOException {
        /* 默认xlsx*/
        FileInputStream fileInputStream = new FileInputStream(excelFile);
        try {
            readExcel(fileInputStream, fileExt);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            fileInputStream.close();
        }

    }

    /**
     * 构造方法2
     */
    public POIXSSFExcelBuilder(InputStream inputStream, ProgressUtil progressUtil, ProgressService progressService, String progressId) throws IOException {
        this.progressUtil = progressUtil;
        this.progressService = progressService;
        this.progressId = progressId;
        readExcel(inputStream, ".xlsx");
    }

    /**
     * 构造方法3
     */
    public POIXSSFExcelBuilder(InputStream inputStream) throws IOException {
        readExcel(inputStream, ".xlsx");
    }

    public void readExcel(InputStream inputStream, String ext) throws IOException {
        String errorMsg = "模板出錯，請確認EXCEL文件能被正確打開";
        try {
            getXswb(inputStream, ext);
            Sheet sheet = xswb.getSheetAt(0);

            int i = 0;
            int j = 0;
            int size = 0;
            int vRowNum = 0;
            for (Row row : sheet) {
                int k = 0;
                for (Cell cell : row) {
                    if (checkCellTypeNoValid(cell)) {
                        k++;
                        continue;
                    }
                    Pattern p = Pattern.compile("(.*)(\\$)([P,F,T,C,L,V,A,I])(\\{)(.+?)(\\})(.*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
                    Matcher matcher = p.matcher(cell.getStringCellValue());
                    if (!matcher.find()) {
                        k++;
                        continue;
                    }
                    if ("V".equalsIgnoreCase(matcher.group(3))) {
                        // 设置生成序号列-则每行第一列数据为序号
                        noFlag = true;
                        // 设置下标
                        vRowNum = i;
                        // 设置所在列数
                        j = k;
                        // 设置大小
                        size++;
                    } else if ("F".equalsIgnoreCase(matcher.group(3))) {
                        // 设置下标
                        fRowNum = i;
                    } else if ("P".equalsIgnoreCase(matcher.group(3))) {
                        // 设置下标
                        pRowNum = i;
                    }
                    k++;
                }
                i++;
            }

            checkError(size,vRowNum,j);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new IOException(errorMsg + e.getMessage());
        }
    }

    private boolean checkCellTypeNoValid(Cell cell) {
        return cell.getCellType() != Cell.CELL_TYPE_STRING || cell.getStringCellValue().length() <= 0;
    }

    private void getXswb(InputStream inputStream, String ext) throws IOException {
        if (".xls".equals(ext)) {
            xswb = new HSSFWorkbook(inputStream); //2003
        } else {
            xswb = new XSSFWorkbook(inputStream);//2007++
        }
    }
    private void checkError(int size, int vRowNum, int j) throws IOException {
        String errorMsg;

        // 校验模板
        if (noFlag && size > 1) {
            errorMsg = "模板V类型参数设置错误，V类型参数只能有一个";
            throw new IOException(errorMsg);
        }
        if (noFlag && j != 0) {
            errorMsg = "模板V类型参数设置错误，V类型参数只能在第一列";
            throw new IOException(errorMsg);
        }
        if (noFlag && vRowNum != fRowNum) {
            errorMsg = "模板V类型参数设置错误，V类型参数只能与F类型参数在同一行";
            throw new IOException(errorMsg);
        }
    }

    /**
     * 设置导出list
     *
     * @param list
     * @param config
     */
    public <T> void put(List<T> list, POIFormatConfig<T> config) {
        try {
            setList(list, config);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 设置导出list
     *
     * @param list
     * @param config
     */
    public <T> void setList(List<T> list, POIFormatConfig<T> config) {
        if (null != list) {
            if (list.size() > 300000) {
                log.error("导出30万以上的数据直接抛出异常!");
            }

            for (T bean : list) {
                try {
                    transBean(bean, config);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * 把bean对象转成List<HashMap<fieldName,fieldVal>>
     *
     * @param bean
     * @param config
     */
    private <T> void transBean(T bean, POIFormatConfig<T> config) {
        HashMap<String, Object> map = new HashMap<>();
        String fieldName = null;
        Object fieldVal = null;
        try {

            BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor descriptor : propertyDescriptors) {
                fieldName = descriptor.getName();
                if ("class".equals(fieldName) || fieldName.isEmpty()) {
                    continue;
                }
                fieldVal = descriptor.getReadMethod().invoke(bean);
                if (fieldVal == null) {
                    fieldVal = "";
                }

                if (config != null) {// 根据自定义格式，格式对应值
                    Object valueObj = config.formatValue(fieldName, fieldVal, bean);
                    if (valueObj == null) {
                        map.put(fieldName, "");
                    } else {
                        map.put(fieldName, valueObj);
                    }
                } else {
                    map.put(fieldName, fieldVal);
                }
            }
            transBeanList.add(map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            log.error("transBean error:[fieldName:" + fieldName + ",fieldVal:" + fieldVal + "]" + e.getMessage());
        }
    }

    /**
     * 获取子对象属性的值
     */
    private <T> HashMap<String, Object> getSubBeanProperty(T bean) {
        HashMap<String, Object> map = new HashMap<>();
        String fieldName = null;
        Object fieldVal = null;
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor descriptor : propertyDescriptors) {
                fieldName = descriptor.getName();
                if ("class".equals(fieldName) || fieldName.isEmpty()) {
                    continue;
                }
                fieldVal = descriptor.getReadMethod().invoke(bean);
                if (fieldVal == null) {
                    fieldVal = "";
                }
                map.put(fieldName, fieldVal);
            }
        } catch (Exception e) {
            log.error("getSubBeanProperty error:[fieldName:" + fieldName
                    + ",fieldVal:" + fieldVal + "]" + e.getMessage(), e);
        }
        return map;
    }

    /**
     * 设置P类型参数Map
     */
    public void put(String key, Object value) {
        pMap.put(key, value);
    }

    /**
     * 創建SXSSFWorkbook并寫入數據
     */
    public void parse() {
        createSXSSFWb();
    }

    /**
     * 創建SXSSFWorkbook并寫入數據
     */
    public void parse(String ext) {
        if (".xls".equals(ext)) {
            createHSSFWb();
        } else {
            createSXSSFWb();
        }
    }

    /**
     * 創建HSSFWorkbook并寫入數據
     */
    public void createHSSFWb() {
        wb = new HSSFWorkbook();
        createWbContent();
    }

    /**
     * 創建SXSSFWorkbook并寫入數據
     */
    public void createSXSSFWb() {
        if (transBeanList.size() > 50000) {
            wb = new SXSSFWorkbook(1000);
        } else {
            // keep 100 rows in memory, exceeding rows will be flushed to disk
            wb = new SXSSFWorkbook(100);
        }
        createWbContent();
    }


    private void createWbContent() {
        Sheet xssheet = xswb.getSheetAt(0);
        // create sheet
        Sheet sheet = wb.createSheet(xssheet.getSheetName());// sheet名称
        // setColumnWidth
        setColumnWidth(xssheet, sheet);
        // createCellStyle
        createCellStyle(xssheet);
        // copyRow
        int i = 0;
        for (Row xsrow : xssheet) {
            if (i < (fRowNum - 2)) {
                // F类型以上大标题的row, 进行复制
                copyRowT1(sheet, xssheet, xsrow);
            } else if (i == (fRowNum - 2)) {
                // F类型以上小标题的row, 进行复制
                copyRowT2(sheet, xssheet, xsrow);
            } else if (i == (fRowNum - 1)) {
                // F类型以上T类型的row, 获取设置的对象类型
                setObjTypeList(xsrow);
            } else if (i == fRowNum) {
                // F类型的row, 循环list数据创建多行Row
                try {
                    copyRowF(sheet, xsrow);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            } else if (i > fRowNum && i < pRowNum) {
                // F类型与P类型之间的row, 直接进行复制
                copyRowT(sheet, xssheet, xsrow, 0);
            } else if (i == pRowNum) {
                // P类型的row, 根据pMap数据进行填充创建一行
                try {
                    copyRowP(sheet, xssheet, xsrow);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
            i++;
        }
    }

    /**
     * create cellStyle
     */
    private void createCellStyle(Sheet xssheet) {
        // 设置大标题类型
        cellstyleT1 = wb.createCellStyle();
        Font fontT1 = wb.createFont();
        fontT1.setFontHeightInPoints((short) 15);//设置字体大小
        fontT1.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);//粗体
        cellstyleT1.setAlignment(XSSFCellStyle.ALIGN_CENTER);//居中
        cellstyleT1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);//设置垂直居中
        cellstyleT1.setFont(fontT1);
        cellstyleT1.setBorderBottom(CellStyle.BORDER_THIN); //下边框
        cellstyleT1.setBorderLeft(CellStyle.BORDER_THIN);//左边框
        cellstyleT1.setBorderTop(CellStyle.BORDER_THIN);//上边框
        cellstyleT1.setBorderRight(CellStyle.BORDER_THIN);//右边框

        // 设置小标题类型
        cellstyleT2 = wb.createCellStyle();
        Font fontT2 = wb.createFont();
        fontT2.setFontHeightInPoints((short) 13);//设置字体大小
        fontT2.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);//粗体
        cellstyleT2.setAlignment(XSSFCellStyle.ALIGN_LEFT);//左对齐
        cellstyleT2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);//设置垂直居中
        cellstyleT2.setFont(fontT2);
        cellstyleT2.setBorderBottom(CellStyle.BORDER_THIN); //下边框
        cellstyleT2.setBorderLeft(CellStyle.BORDER_THIN);//左边框
        cellstyleT2.setBorderTop(CellStyle.BORDER_THIN);//上边框
        cellstyleT2.setBorderRight(CellStyle.BORDER_THIN);//右边框

        if (!setDataFormatFlag) {
            // 设置F类型
            cellstyleF = wb.createCellStyle();
            Font fontF = wb.createFont();
            fontF.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);//正常显示
            cellstyleF.setAlignment(XSSFCellStyle.ALIGN_LEFT);//左对齐
            cellstyleF.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);//设置垂直居中
            cellstyleF.setFont(fontF);
            cellstyleF.setBorderBottom(CellStyle.BORDER_THIN); //下边框
            cellstyleF.setBorderLeft(CellStyle.BORDER_THIN);//左边框
            cellstyleF.setBorderTop(CellStyle.BORDER_THIN);//上边框
            cellstyleF.setBorderRight(CellStyle.BORDER_THIN);//右边框

            // 设置P类型
            cellstyleP = wb.createCellStyle();
            Font fontP = wb.createFont();
            fontP.setFontHeightInPoints((short) 13);//设置字体大小
            fontP.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);//粗体
            cellstyleP.setAlignment(XSSFCellStyle.ALIGN_RIGHT);//右对齐
            cellstyleP.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);//设置垂直居中
            cellstyleP.setFont(fontP);
        } else {
            // 设置F类型
            cellstyleFMap = new HashMap<>();
            Row row = xssheet.getRow(fRowNum);
            doCellStyle(row);

            // 设置P类型
            cellstylePMap = new HashMap<>();
            Row rowP = xssheet.getRow(pRowNum);
            doCellStyleRowP(rowP);
        }

    }

    private void doCellStyleRowP(Row rowP) {
        if (null != rowP) {
            int i = 0;
            for (Cell cell : rowP) {
                CellStyle cellStyle = wb.createCellStyle();
                Font font = wb.createFont();
                font.setFontHeightInPoints((short) 13);//设置字体大小
                font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);//粗体
                cellStyle.setAlignment(XSSFCellStyle.ALIGN_RIGHT);//右对齐
                cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);//设置垂直居中
                cellStyle.setFont(font);

                short fmt = cell.getCellStyle().getDataFormat();
                if (fmt != 0) {
                    cellStyle.setDataFormat(fmt);
                }

                cellstylePMap.put(i + "", cellStyle);
                i++;
            }
        }
    }

    private void doCellStyle(Row row) {
        if (null != row) {
            int i = 0;
            for (Cell cell : row) {
                CellStyle cellStyle = wb.createCellStyle();
                Font font = wb.createFont();
                font.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);//正常显示
                cellStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);//左对齐
                cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);//设置垂直居中
                cellStyle.setFont(font);
                cellStyle.setBorderBottom(CellStyle.BORDER_THIN); //下边框
                cellStyle.setBorderLeft(CellStyle.BORDER_THIN);//左边框
                cellStyle.setBorderTop(CellStyle.BORDER_THIN);//上边框
                cellStyle.setBorderRight(CellStyle.BORDER_THIN);//右边框

                short fmt = cell.getCellStyle().getDataFormat();
                if (fmt != 0) {
                    cellStyle.setDataFormat(fmt);
                }

                cellstyleFMap.put(i + "", cellStyle);
                i++;
            }
        }
    }

    /**
     * create cellStyle
     */
    private CellStyle getCellStyle(int type, String key) {
        if (type == 1) {
            return cellstyleT1;
        } else if (type == 2) {
            return cellstyleT2;
        } else if (type == 3) {
            if (!setDataFormatFlag) {
                return cellstyleF;
            } else {
                return cellstyleFMap.get(key);
            }
        } else if (type == 4) {
            if (!setDataFormatFlag) {
                return cellstyleP;
            } else {
                return cellstylePMap.get(key);
            }
        } else {
            return null;
        }
    }

    /**
     * 设置每列的列宽
     *
     * @param xssheet
     * @param sheet
     */
    private void setColumnWidth(Sheet xssheet, Sheet sheet) {
        Row row = xssheet.getRow(fRowNum);
        for (int i = 0; i < row.getLastCellNum(); i++) {
            sheet.setColumnWidth(i, xssheet.getColumnWidth(i));// 設置列寬
        }
    }

    /**
     * createRow
     *
     * @param sheet
     */
    private Row createRow(Sheet sheet) {
        Row targetRow = sheet.createRow(createRowNum);
        createRowNum++;
        return targetRow;
    }

    /**
     * copyRow 大标题
     */
    private void copyRowT1(Sheet sheet, Sheet xssheet, Row sourceRow) {
        int type = 1;
        copyRowT(sheet, xssheet, sourceRow, type);
    }

    /**
     * copyRow 小标题
     */
    private void copyRowT2(Sheet sheet, Sheet xssheet, Row sourceRow) {
        int type = 2;
        copyRowT(sheet, xssheet, sourceRow, type);
    }

    /**
     * 获取设置的类型
     */
    private List<String> setObjTypeList(Row sourceRow) {
        for (Cell cell : sourceRow) {
            if (cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().length() > 0) {
                Pattern p = Pattern.compile("(.*)(\\$)([P,F,C,T,L,V,A,I])(\\{)(.+?)(\\})(.*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
                Matcher matcher = p.matcher(cell.getStringCellValue());
                if (matcher.find()) {
                    if (matcher.group(3).equalsIgnoreCase("T")) {
                        String tKey = matcher.group(5);
                        tKeyList.add(tKey);//该列有设置类型，则添加对应类型
                    } else {
                        tKeyList.add("");//该列没有设置类型，则添加空值
                    }
                }
            }
        }
        return tKeyList;
    }

    /**
     * copyRow F类型
     */
    private void copyRowF(Sheet sheet, Row sourceRow) {
        int type = 3;
        // 记录fkey到List,如果cell不是f类型参数则为""
        List<String> fKeyList = new ArrayList<>();

        initfKeyList(sourceRow, fKeyList);

        // F类型的row, 循环list数据创建多行Row
        for (int k = 0; k < transBeanList.size(); k++) {
            Row targetRow = createRow(sheet);
            HashMap<String, Object> map = transBeanList.get(k);

            // 循环fKeyList生成row cell
            for (int i = 0; i < fKeyList.size(); i++) {
                Cell cell = targetRow.createCell(i);
                Cell xscell = sourceRow.getCell(i);
                generateRowCell(type, fKeyList, map, i, cell, xscell);
            }

            if (progressUtil != null && progressService != null && StringUtils.isNotEmpty(progressId)) {
                progressUtil.updateProgress(progressService, transBeanList.size(), progressId, k + 1);
            }
        }
    }

    private void generateRowCell(int type, List<String> fKeyList, HashMap<String, Object> map, int i, Cell cell, Cell xscell) {
        if (i == 0 && noFlag) {
            // 生成序号
            setCellValue(cell, no, type, i);
            no++;
        } else {
            String key = fKeyList.get(i);
            if (StringUtils.isNotBlank(key)) {
                // 判断是否是二级属性
                Object obj = null;
                int index = key.indexOf('.');
                if (index >= 0) {
                    String fix = key.substring(0, index);
                    String pix = key.substring(index + 1);
                    Object objTemp = map.get(fix);
                    // 循环objTemp的属性，匹配pix并获取对应的值
                    HashMap<String, Object> subMap = getSubBeanProperty(objTemp);
                    obj = subMap.get(pix);
                } else {
                    obj = map.get(key);
                }
                if (obj == null) {
                    obj = "";
                }
                // 根据模板设置的类型转换类型
                String objType = tKeyList.get(i);
                setCellValue(cell, converObjectType(obj, objType), type, i);
            } else {
                copyCell(xscell, cell, type, i);
            }
        }
    }

    private void initfKeyList(Row sourceRow, List<String> fKeyList) {
        for (Cell cell : sourceRow) {
            boolean flag = false;
            if (cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().length() > 0) {
                Pattern p = Pattern.compile("(.*)(\\$)([P,F,C,T,L,V,A,I])(\\{)(.+?)(\\})(.*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
                Matcher matcher = p.matcher(cell.getStringCellValue());
                if (matcher.find() && "F".equalsIgnoreCase(matcher.group(3))) {
                    String fKey = matcher.group(5);
                    fKeyList.add(fKey);
                    flag = true;
                }
            }

            if (!flag) {
                fKeyList.add("");
            }
        }
    }

    /**
     * copyRow P类型
     * 根据pMap数据进行填充创建一行
     */
    private void copyRowP(Sheet sheet, Sheet xssheet, Row sourceRow) {
        int type = 4;
        // 替换此行的cellValue
        for (Cell cell : sourceRow) {
            if (cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().length() > 0) {
                String cellValue = new RegexReplace(cell.getStringCellValue(), "(.*)(\\$P\\{)(.+?)(\\})(.*)",
                        Pattern.DOTALL | Pattern.CASE_INSENSITIVE) {
                    @Override
                    public String getReplacement(Matcher matcher) {
                        String key = matcher.group(3);
                        if (StringUtils.isNotBlank(key)) {
                            Object obj = pMap.get(key);
                            if (null != obj) {
                                return matcher.group(1) + obj + matcher.group(5);
                            }
                        }
                        return matcher.group(1) + " " + matcher.group(5);
                    }
                }.replaceAll();
                cell.setCellValue(cellValue);
            }
        }
        copyRowT(sheet, xssheet, sourceRow, type);
    }

    /**
     * copyRow
     */
    private void copyRowT(Sheet sheet, Sheet xssheet, Row sourceRow, int type) {
        Row targetRow = createRow(sheet);

        Set<CellRangeAddress> mergedRegions = new HashSet<>();
        if (targetRow != null) {
            if (sourceRow.getHeight() >= 0) {
                targetRow.setHeight(sourceRow.getHeight());
            }
            int startCellNum = sourceRow.getFirstCellNum();
            int endCellNum = sourceRow.getLastCellNum();

            if (startCellNum < 0 || endCellNum < 0) {
                return;
            }

            doCell(sheet, xssheet, sourceRow, type, targetRow, mergedRegions, startCellNum, endCellNum);
        }
    }

    private void doCell(Sheet sheet, Sheet xssheet, Row sourceRow, int type, Row targetRow, Set<CellRangeAddress> mergedRegions, int startCellNum, int endCellNum) {
        for (int j = startCellNum; j <= endCellNum; j++) {
            Cell oldCell = sourceRow.getCell(j);
            Cell newCell = targetRow.getCell(j);
            if (oldCell != null) {
                if (newCell == null) {
                    newCell = targetRow.createCell(j);
                }
                CellRangeAddress mergedRegion = getMergedRegion(xssheet, sourceRow.getRowNum(),
                        oldCell.getColumnIndex());
                doMergeRegion(sheet, targetRow, mergedRegions, mergedRegion);
                copyCell(oldCell, newCell, type, j);
            }
        }
    }

    private void doMergeRegion(Sheet sheet, Row targetRow, Set<CellRangeAddress> mergedRegions, CellRangeAddress mergedRegion) {
        if (mergedRegion != null) {
            CellRangeAddress newMergedRegion = new CellRangeAddress(targetRow.getRowNum(),
                    targetRow.getRowNum() + mergedRegion.getLastRow() - mergedRegion.getFirstRow(),
                    mergedRegion.getFirstColumn(), mergedRegion.getLastColumn());
            if (isNewMergedRegion(newMergedRegion, mergedRegions)) {
                mergedRegions.add(newMergedRegion);
                sheet.addMergedRegion(newMergedRegion);
            }
        }
    }

    /**
     * copyCell
     * type: 类型
     * cellColNum: cell所在列数
     */
    private void copyCell(Cell sourceCell, Cell targetCell, int type, int cellColNum) {
        CellStyle cellStyle = getCellStyle(type, cellColNum + "");
        if (null != cellStyle) {
            targetCell.setCellStyle(cellStyle);
        }
        switch (sourceCell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                if (type == 1 && sheetName != null) {
                    targetCell.setCellValue(sourceCell.getRichStringCellValue() + "-" + sheetName);
                } else if (type == 2) {
                    RegexReplace regexReplace = new RegexReplace(sourceCell.getStringCellValue(), "(.*)(\\$P\\{)(.+?)(\\})(.*)",
                            Pattern.DOTALL | Pattern.CASE_INSENSITIVE) {
                        @Override
                        public String getReplacement(Matcher matcher) {
                            String key = matcher.group(3);
                            if (StringUtils.isNotBlank(key)) {
                                Object obj = pMap.get(key);
                                if (null != obj) {
                                    return matcher.group(1) + obj + matcher.group(5);
                                }
                            }
                            return matcher.group(1) + " " + matcher.group(5);
                        }
                    };
                    String s = regexReplace.replaceAll();
                    targetCell.setCellValue(s);
                }else{
                    targetCell.setCellValue(sourceCell.getRichStringCellValue());
                }
                break;
            case Cell.CELL_TYPE_NUMERIC:
                targetCell.setCellValue(sourceCell.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_BLANK:
                targetCell.setCellType(Cell.CELL_TYPE_BLANK);
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                targetCell.setCellValue(sourceCell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_ERROR:
                targetCell.setCellErrorValue(sourceCell.getErrorCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA:
                int oldRowNum = sourceCell.getRowIndex() + 1;
                int newRowNum = targetCell.getRowIndex() + 1;
                String oldFormula = sourceCell.getCellFormula();
                if (oldFormula.indexOf("SUM") != -1) {
                    oldFormula = oldFormula.replace(oldRowNum + "", newRowNum + "");
                }
                targetCell.setCellFormula(oldFormula);
                break;
            default:
                break;
        }
    }

    /**
     * setCellValue
     * type: 类型
     * cellColNum: cell所在列数
     */
    private void setCellValue(Cell cell, Object value, int type, int cellColNum) {
        CellStyle cellStyle = getCellStyle(type, cellColNum + "");
        if (null != cellStyle) {
            cell.setCellStyle(cellStyle);
        }
        doSetCellValue(cell, value);
    }

    private void doSetCellValue(Cell cell, Object value) {
        if (value == null) {
            cell.setCellValue("");
        } else if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Long) {
            cell.setCellValue((Long) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
        } else if (value instanceof Date) {
            cell.setCellValue(DateUtil.format((Date) value, defaultDateTimeFormat));
        } else if (value instanceof String) {
            String strValue = ((String) value).trim();
            setCellString(cell, (String) value, strValue);
        } else if (value instanceof BigDecimal) {
            cell.setCellValue(((BigDecimal) value).doubleValue());
        } else {
            log.error("unknown data type:value[" + value + "],class[" + value.getClass().getName() + "]");
        }
    }

    private void setCellString(Cell cell, String value, String strValue) {
        if (!strValue.equals("") && strValue.matches("=[IF|SUM|COUNT|COUNTIF]\\(*.*(\\)$)")) {
            cell.setCellFormula(cell.getCellType() == Cell.CELL_TYPE_FORMULA ? (cell.getCellFormula()) : (strValue.substring(1, strValue.length())));
        } else {
            cell.setCellValue(value);
        }
    }

    /**
     * 根据模板设置的类型转换类型
     */
    private Object converObjectType(Object obj, String objType) {
        if (StringUtils.isNotBlank(objType)) {
            if (ObjectTypeEnum.INT.getLabelKey().equals(objType)) {
                return getInt(obj);
            } else if (ObjectTypeEnum.DOUBLE.getLabelKey().equals(objType)) {
                return getDouble(obj);
            } else if (ObjectTypeEnum.STRING.getLabelKey().equals(objType)) {
                return obj + "";
            } else if (ObjectTypeEnum.DATE.getLabelKey().equals(objType)) {
                return getDate(obj);
            }
        }
        return obj;
    }

    private Object getDate(Object obj) {
        if (null != obj && StringUtils.isNotBlank(obj + "")) {
            return DateUtil.format((Date) obj, defaultDateTimeFormat);
        } else {
            return "";
        }
    }

    private Object getDouble(Object obj) {
        if (null != obj && StringUtils.isNotBlank(obj + "")) {
            return Double.parseDouble(obj + "");
        } else {
            return 0.0D;
        }
    }

    private Object getInt(Object obj) {
        if (null != obj && StringUtils.isNotBlank(obj + "")) {
            return Integer.parseInt(obj + "");
        } else {
            return 0;
        }
    }

    /**
     * 把文件转成流写入工作簿
     *
     * @param saveTo
     * @throws IOException
     */
    public void write(File saveTo) throws IOException {
        this.createFolder(saveTo.getParent(), false);
        FileOutputStream fos = new FileOutputStream(saveTo);
        wb.write(fos);
        fos.flush();
        fos.close();
    }

    public boolean createFolder(String strPath, boolean isAncestorExist) {
        try {
            strPath = getDirPath(strPath);
            File file = new File(strPath);
            boolean success = false;
            if (file.exists()) {
                return true;
            }
            if (isAncestorExist) {
                // create a directory;all ancestor directories must exist
                success = file.mkdir();
                if (!success) {
                    success = true;
                }
            } else {
                // create a directory;all non-ancestor directories are
                success = file.mkdirs();
            }
            return success;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return false;
        }
    }

    public static String getDirPath(String value) {
        if (value.endsWith("\\") || value.endsWith("/")) {
            return value;
        }
        if (0 == value.indexOf('/')) {
            return value + "/";
        }
        return (value + "\\");
    }

    /**
     * 把流写入工作簿
     *
     * @param writer
     * @throws IOException
     */
    public void write(OutputStream writer) throws IOException {
        wb.write(writer);
    }

    private CellRangeAddress getMergedRegion(Sheet sheet, int rowNum, int cellNum) {
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress merged = getMergedRegion(sheet, i);
            if (isRangeContainsCell(merged, rowNum, cellNum)) {
                return merged;
            }
        }
        return null;
    }

    private CellRangeAddress getMergedRegion(Sheet sheet, int i) {
        return sheet.getMergedRegion(i);
    }

    private boolean isRangeContainsCell(CellRangeAddress range, int row, int col) {
        return (range.getFirstRow() <= row) && (range.getLastRow() >= row) && (range.getFirstColumn() <= col)
                && (range.getLastColumn() >= col);
    }

    private boolean isNewMergedRegion(CellRangeAddress region, Collection<?> mergedRegions) {
        for (Iterator<?> iterator = mergedRegions.iterator(); iterator.hasNext(); ) {
            CellRangeAddress cellRangeAddress = (CellRangeAddress) iterator.next();
            if (areRegionsEqual(cellRangeAddress, region)) {
                return false;
            }
        }
        return true;
    }

    private boolean areRegionsEqual(CellRangeAddress region1, CellRangeAddress region2) {
        if (region1 == null && region2 == null) {
            return true;
        } else if (region1 == null || region2 == null) {
            return false;
        } else {
            return (region1.getFirstColumn() == region2.getFirstColumn()
                    && region1.getLastColumn() == region2.getLastColumn() && region1.getFirstRow() == region2.getFirstRow()
                    && region1.getLastRow() == region2.getLastRow());
        }
    }

    public void setSetDataFormatFlag(boolean setDataFormatFlag) {
        this.setDataFormatFlag = setDataFormatFlag;
    }

    public List<HashMap<String, Object>> getTransBeanList() {
        return transBeanList;
    }

    public ProgressUtil getProgressUtil() {
        return progressUtil;
    }

    public void setProgressUtil(ProgressUtil progressUtil) {
        this.progressUtil = progressUtil;
    }

    public ProgressService getProgressService() {
        return progressService;
    }

    public void setProgressService(ProgressService progressService) {
        this.progressService = progressService;
    }

    public String getProgressId() {
        return progressId;
    }

    public void setProgressId(String progressId) {
        this.progressId = progressId;
    }
}

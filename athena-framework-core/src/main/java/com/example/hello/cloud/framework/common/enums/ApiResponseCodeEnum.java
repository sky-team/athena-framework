package com.example.hello.cloud.framework.common.enums;

/**
 * API返回码定义
 * 
 * @author guohg03
 * @date 2018年7月24日
 */
public enum ApiResponseCodeEnum implements BaseResponseCode {
    /**
     * 成功
     */
    SUCCESS(0, "成功", "成功"),
    /**
     * 系统异常
     */
    EXCEPTION(-1, "系统异常", "系统异常"),
    /**
     * 失败
     */
    FAIL(-2, "失败", "失败"),
    /**
     * 连接失败
     */
    RPC_ERROR(-3, "连接失败", "连接失败"),
    /**
     * 连接超时
     */
    RPC_TIMEOUT(-4, "连接超时", "连接超时"),
    /**
     * 调用接口异常
     */
    THIRD_API_ERROR(-5, "调用第三方接口异常", "调用第三方接口异常"),

    /**
     * 鉴权失败
     */
    AUTHENTICATION_FAIL(-6, "鉴权失败", "鉴权失败"),

    /**
     * Token过期
     */
    JWT_TOKEN_EXPIRED(-7, "Token过期", "Token过期"),

    /**
     * 请勿重复提交
     */
    REPEAT_SUBMIT(-8, "请勿重复操作", "请勿重复操作"),

    /**
     * 越权访问
     */
    OVER_AUTHENTICATION(-9, "越权访问", "越权访问"),

    /**
     * 系统繁忙，请稍后重试
     */
    SYSTEM_BUSY(-10, "系统繁忙，请稍后重试", "系统繁忙，请稍后重试"),

    /**
     * 您当前无系统权限(全局黑名单)
     */
    GLOBAL_BLACK_LIST(-11, "您当前无系统权限", "您当前无系统权限"),

    /**
     * http请求方法类型不支持
     */
    HTTP_REQUEST_METHOD_NOT_SUPPORTED(-11, "http请求方法类型不支持", "http请求方法类型不支持"),

    /**
     * host不合法
     */
    HOST_ILLEGAL(-13, "host不合法", "host不合法"),

    /**
     * 业务异常
     */
    BIZ_ERROR(-14, "业务异常", null),

    /**
     * 参数校验失败
     */
    PARAM_VALID_ERROR(-15, "参数校验失败", "参数校验失败"),

    /**
     * 参数类型不合法
     */
    PARAM_TYPE_ERROR(-16, "参数类型不合法", "参数类型不合法"),

    /**
     * 参数绑定错误
     */
    PARAM_BIND_ERROR(-17, "参数绑定错误", "参数绑定错误"),

    /**
     * 找不到此接口
     */
    NOT_FOUND(-18, "找不到此接口", "找不到此接口"),

    /**
     * 消息不可读
     */
    MSG_NOT_READABLE(-19, "消息不可读", "消息不可读"),

    /**
     * 不支持的请求媒体类型
     */
    MEDIA_TYPE_NOT_SUPPORTED(-20, "不支持的请求媒体类型", "不支持的请求媒体类型"),
    
	/**
	 * 无权登录销售家经理版！
	 */
	E10022(10022, "无权登录销售家经理版！", "无权登录销售家经理版！"),
    E2700001(2700001, "冲突提示：当前有城市“%s”存在多条自定义标签设置中，是否保存最新操作设置替换原有设置？", "冲突提示：当前有城市“%s”存在多条自定义标签设置中，是否保存最新操作设置替换原有设置？"),

    /**
     * mall 在线商城 ,错误码从250000开始
     */
    E2500001(2500001,"已购买过该商品","已购买过该商品"),
    E2500002(2500002,"库存不足","库存不足"),
    E2500003(2500003,"支付订单不存在，或存在多条记录"),
    E2500004(2500004,"取消订单失败，%s订单不允许取消"),
    E2500005(2500005,"取消订单成功"),
    E2500006(2500006,"商品已下架"),
    E2500007(2500007,"城市没有开通商户号，请先开通商户号");

    /**
     * 响应码
     */
    private Integer code;

    /**
     * 响应信息
     */
    private String message;

    /**
     * 给前台展示的消息
     */
    private String showMessage;

    /**
     * @param code
     *            响应码
     * @param message
     *            响应信息
     */
    ApiResponseCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
        this.showMessage = message;
    }

    /**
     * @param code
     *            响应码
     * @param message
     *            响应信息
     * @param showMessage
     *            展示信息
     */
    ApiResponseCodeEnum(Integer code, String message, String showMessage) {
        this.code = code;
        this.message = message;
        this.showMessage = showMessage;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    protected void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    protected void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getShowMessage() {
        return showMessage;
    }

    public void setShowMessage(String showMessage) {
        this.showMessage = showMessage;
    }

    /**
     * @param code
     *            响应码
     * @return ApiResponseCodeEnum
     */
    public static ApiResponseCodeEnum match(byte code) {
        for (ApiResponseCodeEnum apiResponseCodeEnum : ApiResponseCodeEnum.values()) {
            if (apiResponseCodeEnum.getCode() == code) {
                return apiResponseCodeEnum;
            }
        }
        return null;
    }
}

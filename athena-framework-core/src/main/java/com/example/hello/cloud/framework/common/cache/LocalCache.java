package com.example.hello.cloud.framework.common.cache;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class LocalCache  {
	

	static ListeningExecutorService backgroundRefreshPools = 
            MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(20));

    /**
     * 构建一个本地缓存
     */
    public static final LoadingCache<String, Object> CACHE = CacheBuilder.newBuilder()
            //初始化100个
            .initialCapacity(100)
            //最大10000
            .maximumSize(10000)
            //2小时定时刷新
            .refreshAfterWrite(2, TimeUnit.HOURS)
            //只有当内存不够的时候才会value才会被回收
            .softValues()
            //开启统计功能
            .recordStats()
            .build(new CacheLoader<String, Object>() {
                //如果get()没有拿到缓存，直接点用load()加载缓存
                @Override
                public Object load(String key) {
                    log.info("key:" + key);
                    return "null";
                }

           
                /**
                 * 调用refresh()的时候调用reload()，一般用于更新缓存
                 * @param key
                 * @param oldValue
                 * @return
                 * @throws Exception
                 */
                @Override
                public ListenableFuture<Object> reload(String key, Object oldValue) throws Exception {
                	log.info("reload:[" + key + ":" + oldValue + "]");
                	return backgroundRefreshPools.submit(new Callable<Object>() {	 
                        @Override
                        public Object call() throws Exception {
                            return "null";

                        }
                    });
                   
                }
            });
}
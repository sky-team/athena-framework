package com.example.hello.cloud.framework.common.plugin.yiyun.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 阿里云消息队列配置信息
 *
 * @author zhoujj07
 * @create 2017/9/7
 */
@Data
@ConfigurationProperties(prefix = "yiyun")
public class YiyunProperties {

    private String host;

    private String secret;

    private String appId;

}

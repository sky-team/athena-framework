package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/9.
 */
@Data
public class SyncCustomerResultDTO extends SyncResultDTO implements Serializable {
    private  String nssCustomerId;
    private  String fusionBackgroundId;
    private  String sNumber;
    private  String salesOrgCode;
    private  String customerId;
    private  String phoneNumber;
    private  String batchId;
    private  String proCode;
    
    @Override
    public String toString() {
        return "SyncCustomerResultDTO{" +
                "nssCustomerId='" + nssCustomerId + '\'' +
                ", fusionBackgroundId='" + fusionBackgroundId + '\'' +
                ", salesOrgCode='" + salesOrgCode + '\'' +
                ", customerId='" + customerId + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", batchId='" + batchId + '\'' +
                ", proCode='" + proCode + '\'' +
                ", sNumber='" + sNumber + '\'' +
                ", errCode= "+ getErrCode()+" ,errMsg=" + getErrMsg() +
                '}';
    }
}

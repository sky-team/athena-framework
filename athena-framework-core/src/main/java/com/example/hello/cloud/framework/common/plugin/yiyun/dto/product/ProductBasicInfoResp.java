package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import lombok.Data;

import java.util.List;

/**
 * 查询产品详细信息
 *
 * @author v-linxb
 * @create 2018/5/7
 **/
@Data
public class ProductBasicInfoResp extends ProductListInfo {
    /**
     * 产品属性组合分组
     */
    private List<ProductPropertyGroupInfo> propGroup;
}

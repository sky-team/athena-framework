package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import lombok.Data;

@Data
public class QueryTradeInfo {
    private String name;
    private String certNo;
    private String mobile;
    private String signDate;
    private String projectName;
    private String projectCode;
    private String roomId;
    private boolean jointPurchase;
    private String projectType;
    private String productTypeId;
}

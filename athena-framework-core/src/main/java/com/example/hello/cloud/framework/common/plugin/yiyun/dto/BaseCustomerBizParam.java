package com.example.hello.cloud.framework.common.plugin.yiyun.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 客户共用请求参数
 *
 * @author zhoujj07
 * @create 2018/5/3
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseCustomerBizParam extends BaseUserDomainBizParam {

    private static final long serialVersionUID = 3366022182465066606L;

    /**
     * 应用系统自行管理的登录账号唯一标记,如agentId
     */
    private String outerId;

    /**
     * 子账号登录名
     */
    private String loginName;

}

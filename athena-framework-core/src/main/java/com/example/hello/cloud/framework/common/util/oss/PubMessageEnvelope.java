package com.example.hello.cloud.framework.common.util.oss;

import cn.hutool.json.JSONUtil;
import lombok.Data;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

/**
 * PubMessageEnvelope
 * 消息生产者生产的消息包装类
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Data
public class PubMessageEnvelope<T extends Serializable> implements Serializable {

    private static final long serialVersionUID = 8081063816090457491L;

    PubMessageEnvelope() {
        this.startDeliverTime = System.currentTimeMillis();
    }

    public PubMessageEnvelope(T message) {
        this.message = message;
        this.startDeliverTime = System.currentTimeMillis();
    }

    /**
     * 消息id
     */
    private String msgId;

    /**
     * 代表消息的业务关键属性，请尽可能全局唯一。
     */
    private String key;

    /**
     * 阿里云topic
     */
    private String topic;

    /**
     * 阿里云pid
     */
    private String producerId;

    /**
     * unix时间戳，消息发送时间
     */
    private Long startDeliverTime;

    /**
     * 生产者同消费者协调一致的tag，可理解为Gmail中的标签，对消息进行再归类，方便Consumer指定过滤条件在ONS服务器过滤
     */
    private String tag;

    /**
     * 消息内容对象序列化成的json字符串
     */
    private String jsonString;

    /**
     * 消息内容对象
     */
    private T message;

    /**
     * 消息类型
     */
    private Integer messageType;

    public byte[] getSerializeMessage() {
        jsonString = JSONUtil.toJsonStr(message);
        return jsonString.getBytes(StandardCharsets.UTF_8);
    }

}

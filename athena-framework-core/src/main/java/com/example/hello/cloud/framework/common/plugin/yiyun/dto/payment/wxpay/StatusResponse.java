package com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment.wxpay;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 退款申请响应
 *
 * @author mall
 */
@Data
public class StatusResponse implements YiyunResponseData {

    @ApiModelProperty("交易结果状态 0待付款1已付款2部分退款3已退款6已撤销")
    private Integer status;

}
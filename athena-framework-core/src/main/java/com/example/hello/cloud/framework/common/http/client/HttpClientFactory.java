package com.example.hello.cloud.framework.common.http.client;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import java.io.InterruptedIOException;
import java.net.UnknownHostException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author zhoujj
 * @ClassName: HttpClientFactory
 * @Description: httpClient工厂类
 * @date 2018年7月24日
 */
@Slf4j
public class HttpClientFactory {
    private static PoolingHttpClientConnectionManager cm = null;
    private static ScheduledExecutorService scheduledExecutorService = null;
    
    /**连接池最大数为500*/
    private static final int POOL_MAX_TOTAL = 500;
    /**将每个路由基础的连接增加*/
    private static final int DEFAULT_MAXPERROUTE = 50;
    
    private static final long TIMEOUT = 30;

    static {
        try {
            ConnectionSocketFactory plainFactory = PlainConnectionSocketFactory.getSocketFactory();
            LayeredConnectionSocketFactory sslFactory = SSLConnectionSocketFactory.getSocketFactory();
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
									                    .register("http", plainFactory)
									                    .register("https", sslFactory)
									                    .build();
            cm = new PoolingHttpClientConnectionManager(registry);
            // 将最大连接数增加
            cm.setMaxTotal(POOL_MAX_TOTAL);
            // 将每个路由基础的连接增加
            cm.setDefaultMaxPerRoute(DEFAULT_MAXPERROUTE);
            Thread thread = new Thread(getCleanConnectionTask());
            thread.setDaemon(true);
            thread.setName("IdleConnectionMonitorThread");
            ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(1);
            scheduledExecutorService.scheduleAtFixedRate(thread,1,30,TimeUnit.SECONDS);
        } catch (Exception e) {
            log.error("初始化http连接池异常"+e.getMessage(), e);
        }
    }

    public static void shutdown() {
        if (null != scheduledExecutorService) {
            scheduledExecutorService.shutdown();
        }
    }

    public static Runnable getCleanConnectionTask() {
        return () -> {
            try {
                cm.closeExpiredConnections();
                cm.closeIdleConnections(TIMEOUT, TimeUnit.SECONDS);
            } catch (Exception e) {
                log.error("清理过期httpClient链接异常"+e.getMessage(), e);
            }
        };
    }

    /**
     * Gets the pooled client.
     *
     * @return the pooled client
     */
    static CloseableHttpClient getPooledClient() {
        int timeout = 10;
        int retry = 3;

        return getPooledClient(timeout, retry);
    }

    /**
     * Gets the pooled client.
     *
     * @param second the second
     * @param retry  the retry
     * @return the pooled client
     */
    private static CloseableHttpClient getPooledClient(int second, int retry) {
        HttpClientBuilder builder = HttpClientBuilder.create();

        // https://hc.apache.org/httpcomponents-client-ga/httpclient/apidocs/org/apache/http/client/config/RequestConfig.html
        // A timeout value of zero is interpreted as an infinite timeout.
        // A negative value is interpreted as undefined (system default).
        if (second > 0) {
            int ms = second * 1000;
            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(ms)
                    .setConnectionRequestTimeout(ms)
                    .setSocketTimeout(ms).build();
            builder.setDefaultRequestConfig(config);
        }

        if (retry > 0) {
            //DefaultHttpRequestRetryHandler handler = new DefaultHttpRequestRetryHandler(retry, true);
            // 请求重试处理
            HttpRequestRetryHandler handler = (exception, executionCount, context) -> {
                // 如果已经重试了次数次，就放弃
                if (executionCount >= retry) {
                    return false;
                }
                // 如果服务器丢掉了连接，那么就重试
                if (exception instanceof NoHttpResponseException) {
                    return true;
                }
                // 不要重试SSL握手异常
                if (exception instanceof SSLHandshakeException) {
                    return false;
                }
                // 超时
                if (exception instanceof InterruptedIOException) {
                    return false;
                }
                // 目标服务器不可达
                if (exception instanceof UnknownHostException) {
                    return false;
                }
                // SSL握手异常
                if (exception instanceof SSLException) {
                    return false;
                }

                HttpClientContext clientContext = HttpClientContext.adapt(context);
                HttpRequest request = clientContext.getRequest();
                // 如果请求是幂等的,比如HttpPut, HttpPost等请求，就再次尝试
                return !(request instanceof HttpEntityEnclosingRequest);
            };
            builder.setRetryHandler(handler);
        }

        return builder.setConnectionManager(cm).build();
    }
}
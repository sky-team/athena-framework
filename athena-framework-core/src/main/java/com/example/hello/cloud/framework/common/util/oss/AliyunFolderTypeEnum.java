package com.example.hello.cloud.framework.common.util.oss;

/**
 * AliyunFolderTypeEnum 阿里云oss服务定义的文件夹类型
 *
 * @author yingc04
 * @create 2019/11/5
 */
public enum AliyunFolderTypeEnum {
    /**
     * 用户相关
     */
    USER("user"), ITEM("item"), TRADE("trade"), PROMOTION("promotion"), SYSTEM("system"), GOODS("goods"),
    SHARE("share"), QRCODE("qrCode"), TEMP("temp"), TEST("test"), SHOP("shop"), AGENT("agent"),
    COMMISSION("commission"), ULTRA("ultra"), ONLINE("online"), CONSULTANT("consultant"), CUSTOMER("customer"),
    APPLET("applet"), AI("ai"), TOKEN("token"), INVOICE("invoice"), CLUES("clues"), COUPON("coupon"),
    TIMEDELAY("timedelay"), HTML("html"), PERIODICAL("periodical"), DEV("dev"), LEFTMOBILE("leftmobile"),
    /**
     * 营销
     */
    MARKETING("marketing"),
    /**
     * 拓客报表
     */
    TOOKEEN("tookeen"),
    /**
     * 顾问台帐报表
     */
    CONSULTANTACCOUNT("consultantAccount"),
    /**
     * 顾问拓客列表
     */
    CONSULTANTTOOKEEN("consultantTookeen"),
    /**
     * 顾问服务列表
     */
    CONSULTANTSERVE("consultantServe"),
    /**
     * 认筹方案客户列表模板
     */
    SAVEMONEYCUSTOMER("saveMoneyCustomer"),
    /**
     * 客户描摹报表
     */
    DEPICT("depict"),
    /**
     * 隐号报备报表
     */
    HIDDEN("hidden"),
    /**
     * 签约统计表
     */
    SIGN("sign"),
    /**
     * 失败用户
     */
    FAILUSER("failuser"),
    /**
     * 顾问动态相关
     */
    DYNAMIC("dynamic"),
    /**
     * 活动相关报表
     */
    ACTIVITY("activity"), CITYPUBLICAPPLY("cityPublicApply"),
    /**
     * 业务看板
     */
    DATABUSINESS("dataBusiness"),
    /**
     * 行销拓客
     */
    TOKER("toker"),

    /**
     * 企业微信H5
     */
    QYH5("qyh5"),
    /**
     * 中台客户导入
     */
    CUSIMPORT("cusimport"),
    /**
     * 中台客户导出
     */
    CUSEXPORT("cusexport"),
    /**
     * 中台线索导出
     */
    CUSCLUEXPORT("cusculexport"),
    /**
     *协议
     */
    AGREEMENT("agreement"),
    /**
     *城市
     */
    CITY("city"),
    /**
     *场次
     */
    SCREEN("screen"),
    /**
     *e选房
     */
    EFANG("efang");

    private String name;

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    AliyunFolderTypeEnum(String name) {
        this.name = name;
    }

    public String getPath() {
        return this.toString() + "/";
    }

    public static AliyunFolderTypeEnum getFolderType(String folderName) {
        for (AliyunFolderTypeEnum typeEnum : AliyunFolderTypeEnum.values()) {
            if (typeEnum.getName().equals(folderName)) {
                return typeEnum;
            }
        }
        return AliyunFolderTypeEnum.TEMP;
    }

    @Override
    public String toString() {
        return name;
    }
}
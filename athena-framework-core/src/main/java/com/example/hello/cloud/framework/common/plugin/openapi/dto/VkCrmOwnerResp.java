package com.example.hello.cloud.framework.common.plugin.openapi.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 万科物业业主身份信息出参参数
 *
 * @author v-linxb
 * @create 2019/9/6
 **/
@Data
public class VkCrmOwnerResp implements Serializable {

    /**
     * 姓名
     */
    private String fullName;

    /**
     * 身份证号
     */
    private String certificateId;

    /**
     * 手机号
     */
    private String mainMobile;

    /**
     * 楼栋
     */
    private String buildingName;

    /**
     * 是否业主
     */
    private Integer isOwner;

    /**
     * 是否二手房
     */
    private Integer isSecondhand;

    /**
     * 是否有小孩
     */
    private Integer isChild;
}

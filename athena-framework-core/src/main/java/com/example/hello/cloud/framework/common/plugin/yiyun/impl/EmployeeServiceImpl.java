package com.example.hello.cloud.framework.common.plugin.yiyun.impl;

import com.google.gson.reflect.TypeToken;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunService;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunCustomerApiEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee.QueryEmployeeListRequest;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee.QueryEmployeeListResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee.QueryEmployeeRequest;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee.QueryEmployeeResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;

/**
 * User: luodf01
 * Date: 2019-7-9
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final YiyunService yiyunService;

    @Autowired
    public EmployeeServiceImpl(YiyunService yiyunService) {
        this.yiyunService = yiyunService;
    }

    @Override
    public ApiResponse<QueryEmployeeResponse> queryEmployeeInfo(QueryEmployeeRequest request) {
        YiyunCustomerApiEnum type = YiyunCustomerApiEnum.QUERY_EMPLOYEE_INFO;
        Type responseType = new TypeToken<YiyunResponse<QueryEmployeeResponse>>() {
        }.getType();
        YiyunResponse<QueryEmployeeResponse> yiyunResponse = yiyunService.action2(type, request, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ApiResponse<QueryEmployeeListResponse> queryEmployeeList(QueryEmployeeListRequest request) {
        YiyunCustomerApiEnum type = YiyunCustomerApiEnum.QUERY_EMPLOYEE_LIST_INFO;
        Type responseType = new TypeToken<YiyunResponse<QueryEmployeeListResponse>>() {
        }.getType();
        YiyunResponse<QueryEmployeeListResponse> yiyunResponse = yiyunService.action2(type, request, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

}

package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 特殊产品枚举
 *
 * @author v-linxb
 * @create 2018/5/9
 **/
public enum BundleProductEnum {

    /**
     * 特殊产品
     */
    IS_BUNDLE(1),
    /**
     * 非特殊产品
     */
    NOT_BUNDLE(0);

    BundleProductEnum(int value) {
        this.value = value;
    }

    private int value;


    public int getValue() {
        return this.value;
    }
}

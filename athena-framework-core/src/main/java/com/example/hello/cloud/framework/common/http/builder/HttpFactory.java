package com.example.hello.cloud.framework.common.http.builder;

/**
 * httpclient工厂
 *
 * @author v-linxb
 * @date 2017/11/30
 */
public final class HttpFactory {

    private HttpFactory() {
    }

    public static HttpFactory getInstance() {
        return httpFactory;
    }

    public HttpAsyncBuilder getHttpAsyncPool() {
        return httpAsyncBuilder;
    }

    public HttpSyncBuilder getHttpSyncPool() {
        return httpSyncBuilder;
    }

    private static HttpFactory httpFactory = new HttpFactory();

    private static HttpAsyncBuilder httpAsyncBuilder = new HttpAsyncBuilder();

    private static HttpSyncBuilder httpSyncBuilder = new HttpSyncBuilder();

}

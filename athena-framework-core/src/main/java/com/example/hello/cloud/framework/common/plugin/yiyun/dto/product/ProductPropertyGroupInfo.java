package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 产品属性组合分组信息
 * Created by v-zhangxj22 on 2018/5/15.
 */
@Data
public class ProductPropertyGroupInfo implements Serializable {

    private Integer groupType;

    private String groupName;

    private Map<String,Object> propMap;

    private List<ProductPropertyGroupInfo> childrenGroup;
}

package com.example.hello.cloud.framework.common.validator;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import com.example.hello.cloud.framework.common.constant.C;
import com.example.hello.cloud.framework.common.exception.CheckedException;

/**
 * hibernate-validator校验工具类
 *
 * @author zhanj04
 * @date 2017/8/1 9:29
 */
public class ValidatorUtil {
    private static Validator validator;

    static {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    /**
     * 校验对象
     *
     * @param object
     *            待校验对象
     * @param groups
     *            待校验的组
     * @throws CheckedException
     *             校验不通过，则报RRException异常
     */
    public static void validateModel(Object object, Class<?>... groups) throws CheckedException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        if (!constraintViolations.isEmpty()) {
            StringBuilder msg = new StringBuilder();
            for (ConstraintViolation<Object> constraint : constraintViolations) {
                msg.append(constraint.getMessage()).append(";");
            }
            throw new CheckedException(C.FAIL, msg.toString());
        }
    }
}

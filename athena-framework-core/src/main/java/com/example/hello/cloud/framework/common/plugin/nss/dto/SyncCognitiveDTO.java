package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/9.
 */
@Data
public class SyncCognitiveDTO implements Serializable {
    private  String salesOrgCode;
    private  String nssChannelId;
    private  String classValue;
    private  String name;
    private  String isEnable;

    @Override
    public String toString() {
        return "SyncCognitiveDTO{" +
                "salesOrgCode='" + salesOrgCode + '\'' +
                ", nssChannelId='" + nssChannelId + '\'' +
                ", classValue='" + classValue + '\'' +
                ", name='" + name + '\'' +
                ", isEnable='" + isEnable + '\'' +
                '}';
    }
}

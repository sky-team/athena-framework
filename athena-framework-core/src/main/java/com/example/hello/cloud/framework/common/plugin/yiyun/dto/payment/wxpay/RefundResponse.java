package com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment.wxpay;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 退款申请响应
 *
 * @author mall
 */
@Data
public class RefundResponse implements YiyunResponseData {

    /*
    paymentOrderNo	字符串	原支付订单号
    refundNo	    字符串	退款订单号
    serialNo	    字符串	中台流水号
    amount          数值	退款金额
    status          数值	状态 0退款失败1退款成功
    * */

    @ApiModelProperty("原支付订单号")
    private String paymentOrderNo;

    @ApiModelProperty("退款订单号")
    private String refundNo;

    @ApiModelProperty("中台流水号")
    private String serialNo;

    @ApiModelProperty("退款金额")
    private BigDecimal amount;

    @ApiModelProperty("支付订单号（中台生成）")
    private Integer status;

}
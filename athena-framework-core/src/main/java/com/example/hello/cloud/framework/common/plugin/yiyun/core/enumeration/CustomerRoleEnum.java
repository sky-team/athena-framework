package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 客户角色枚举
 *
 * @author zhoujj07
 * @create 2018/5/3
 */
public enum CustomerRoleEnum {

    /**
     * 经纪人
     */
    AGENT("00", "经纪人"),
    /**
     * 置业顾问
     */
    COUNSELOR("01", "置业顾问"),
    /**
     * 业主
     */
    OWNER("02", "业主"),
    /**
     * 员工
     */
    EMPLOYEE("03", "员工"),
    /**
     * 同住人
     */
    HOUSEMATE("04", "同住人"),;

    private String name;
    private String index;

    CustomerRoleEnum(String index, String name) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public String getIndex() {
        return index;
    }

}

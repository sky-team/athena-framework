package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by v-hut12 on 2018/9/28.
 */
@Data
public class AppletKeyInfo implements Serializable {

    private static final long serialVersionUID = -4326647322856968609L;
    private String session_key;
    private String openid;
    private String unionid;

}

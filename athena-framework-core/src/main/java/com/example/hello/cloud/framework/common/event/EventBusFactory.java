package com.example.hello.cloud.framework.common.event;

import com.google.common.collect.Maps;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import lombok.extern.slf4j.Slf4j;

import java.util.EventListener;
import java.util.Map;
import java.util.concurrent.Executors;

/**
 * EventBusFactory
 * Guava event事件工厂
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Slf4j
public class EventBusFactory {
    private static volatile EventBusFactory instance;
    /**
     * 保存已经注册的监听器，防止监听器重复注册
     */
    private Map<String, Class<? extends EventListener>> registerListenerContainers = Maps.newConcurrentMap();
    /**
     * 保存已经注册的异步监听器，防止监听器重复注册
     */
    private Map<String, Class<? extends EventListener>> asyncRegisterListenerContainers = Maps.newConcurrentMap();
    private final EventBus eventbus = new EventBus();
    private final AsyncEventBus asyncEventBus = new AsyncEventBus(
            Executors.newFixedThreadPool(2 * Runtime.getRuntime().availableProcessors() + 1));

    private EventBusFactory() {
    }

    public static EventBusFactory build() {
        if (instance == null) {
            synchronized (EventBusFactory.class) {
                if (instance == null) {
                    instance = new EventBusFactory();
                }
            }
        }
        return instance;
    }

    /**
     * 转发事件
     */
    public void postEvent(BaseEvent event) {
        eventbus.post(event);
    }

    /**
     * 转发异步事件
     */
    public void postAsyncEvent(BaseEvent event) {
        asyncEventBus.post(event);
    }

    /**
     * 注册监听器
     */
    public void registerEvent(Class<? extends EventListener> clazz) {
        String clazzName = clazz.getSimpleName();
        if (registerListenerContainers.containsKey(clazzName)) {
            return;
        }
        try {
            registerListenerContainers.put(clazzName, clazz);
            Object obj = registerListenerContainers.get(clazzName).newInstance();
            eventbus.register(obj);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 注册异步监听器
     */
    public void registerAsyncEvent(Class<? extends EventListener> clazz) {
        String clazzName = clazz.getSimpleName();
        if (asyncRegisterListenerContainers.containsKey(clazzName)) {
            return;
        }
        try {
            asyncRegisterListenerContainers.put(clazzName, clazz);
            Object obj = asyncRegisterListenerContainers.get(clazzName).newInstance();
            asyncEventBus.register(obj);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}

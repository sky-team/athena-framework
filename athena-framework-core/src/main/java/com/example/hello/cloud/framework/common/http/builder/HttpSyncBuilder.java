package com.example.hello.cloud.framework.common.http.builder;

import com.example.hello.cloud.framework.common.http.monitor.SyncConnectionMonitorThread;
import com.example.hello.cloud.framework.common.http.common.SSLs;
import com.example.hello.cloud.framework.common.http.common.Utils;
import com.example.hello.cloud.framework.common.http.monitor.SyncConnectionMonitorThread;
import org.apache.http.Consts;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.auth.AuthSchemeProvider;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.Lookup;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.impl.auth.*;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;

import java.nio.charset.CodingErrorAction;

/**
 * 同步client创建
 *
 * @author v-linxb
 */
public final class HttpSyncBuilder {

    /**
     * 请求等待超时
     */
    private static final int CONNECTIONREQUESTTIMEOUT = 5000;

    /**
     * 连接超时
     */
    private static final int CONNECTTIMEOUT = 5000;

    /**
     * 等待数据超时时间
     */
    private static final int SOCKETTIMEOUT = 10000;

    /**
     * 连接池最大连接数
     */
    private static final int POOLSIZE = 300;

    /**
     * 每个主机的并发数
     */
    private static final int MAXPERROUTE = 50;

    private static PoolingHttpClientConnectionManager conMgr = null;

    private static Lookup<AuthSchemeProvider> authSchemeRegistry = null;

    private static RequestConfig requestConfig = null;

    private CloseableHttpClient closeableHttpClient = null;

    private static ConnectionKeepAliveStrategy myStrategy = null;

    /**
     * ssl 协议版本
     */
    private SSLs.SSLProtocolVersion sslpv = SSLs.SSLProtocolVersion.SSLV3;

    /**
     * 用于配置ssl
     */
    private SSLs ssls = SSLs.getInstance();

    protected HttpSyncBuilder() {
        try {
            if (null == conMgr) {
                // 设置超时
                requestConfig = RequestConfig.custom()
                        .setConnectionRequestTimeout(CONNECTIONREQUESTTIMEOUT)
                        .setConnectTimeout(CONNECTTIMEOUT)
                        .setSocketTimeout(SOCKETTIMEOUT).build();
                // 设置协议http和https对应的处理socket链接工厂的对象
                Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
                        .<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.INSTANCE)
                        .register("https", ssls.getSSLCONNSF(sslpv)).build();

                // 设置连接池大小
                conMgr = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
                conMgr.setMaxTotal(POOLSIZE);
                conMgr.setDefaultMaxPerRoute(MAXPERROUTE);

                //keep live
                myStrategy = (response, context) -> {
                    HeaderElementIterator it = new BasicHeaderElementIterator
                            (response.headerIterator(HTTP.CONN_KEEP_ALIVE));
                    while (it.hasNext()) {
                        HeaderElement he = it.nextElement();
                        String param = he.getName();
                        String value = he.getValue();
                        if (value != null && param.equalsIgnoreCase
                                ("timeout")) {
                            return Long.parseLong(value) * 1000;
                        }
                    }
                    return 60 * 1000;//如果没有约定，则默认定义时长为60s
                };

                // 连接配置
                ConnectionConfig connectionConfig = ConnectionConfig.custom()
                        .setMalformedInputAction(CodingErrorAction.IGNORE)
                        .setUnmappableInputAction(CodingErrorAction.IGNORE)
                        .setCharset(Consts.UTF_8).build();

                authSchemeRegistry = RegistryBuilder
                        .<AuthSchemeProvider>create()
                        .register(AuthSchemes.BASIC, new BasicSchemeFactory())
                        .register(AuthSchemes.DIGEST, new DigestSchemeFactory())
                        .register(AuthSchemes.NTLM, new NTLMSchemeFactory())
                        .register(AuthSchemes.SPNEGO, new SPNegoSchemeFactory())
                        .register(AuthSchemes.KERBEROS, new KerberosSchemeFactory())
                        .build();
                conMgr.setDefaultConnectionConfig(connectionConfig);
                //将连接池加入监控，定时删除空闲的链接
                SyncConnectionMonitorThread idleConnMonitor = new SyncConnectionMonitorThread(conMgr);
                idleConnMonitor.setName("SyncConnectionMonitorThread");
                idleConnMonitor.start();
            }
        } catch (Exception e) {
            Utils.errorException("初始化同步连接池配置异常：{}", e);
        }
    }



    public CloseableHttpClient custom() {
        if (null == closeableHttpClient) {
            closeableHttpClient = HttpClients.custom().setConnectionManager(conMgr)
                    .setDefaultAuthSchemeRegistry(authSchemeRegistry)
                    .setDefaultCookieStore(new BasicCookieStore())
                    .setDefaultRequestConfig(requestConfig)
                    .setKeepAliveStrategy(myStrategy)
                    .build();
        }
        return closeableHttpClient;
    }

}
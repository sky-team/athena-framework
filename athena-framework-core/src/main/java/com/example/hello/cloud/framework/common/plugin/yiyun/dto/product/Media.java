package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;

import java.io.Serializable;

/**
 * 多媒体
 * Created by v-zhangxj22 on 2018/5/21.
 */
@Data
public class Media extends BaseYiyunRequestData implements Serializable {

    /**
     * 数值	产品Id	是
     */
    private Long productId;
    /**
     * 数值	业务类型	是	业务类型 MediaBusiTypeEnum.java
     */
    private String busiType;
    /**
     * 数值	文件类型：1，图片；2，视频	是
     */
    private Integer fileType;
    /**
     * 字符串	文件Url	是	图片/视频URL
     */
    private String fileUrl;
    /**
     * 字符串	重定向Url	否
     */
    private String redirectUrl;
    /**
     * 数值	排序（越大排越前）	否
     */
    private Integer sort;
}

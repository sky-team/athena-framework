package com.example.hello.cloud.framework.common.plugin.yiyun.inf;

import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade.*;

import java.util.List;

/**
 * User: ${v-zhongj11}
 * Date: 2018-10-01
 */
public interface TradeInfoService {

    /**
     * 易云-即售地产交易单据查询接口
     *
     * @param param 输入
     * @return 输出
     */
    ApiResponse<QueryEstateOrderRespons> queryEstateOrder(QueryEstateOrderRequest param);

    /**
     * 根据单据流水号查询发票信息
     *
     * @param request
     * @return
     */
    ApiResponse<OrderDataResponse> queryInvoiceInfo(OrderDataRequest request);

    /**
     * 业主证件号查询业主签约交易信息
     *
     * @param request
     * @return
     */
    ApiResponse<QueryTradeInfoResp> queryTradeInfo(QueryTradeInfoReq request);

    /**
     * 手机号查询业主信息接口
     *
     * @param request
     * @return
     */
    ApiResponse<QueryCustInfoResp> queryCustInfoVo(QueryCustInfoReq request);

    /**
     * 交易单重推
     *
     * @param req
     * @return
     */
    ApiResponse<MqReissueResp> tradeMqReissue(MqReissueReq req);

    /**
     * 根据单据流水号查询发票信息
     *
     * @param mobile
     * @return
     */
    ApiResponse<List<OrderDataResponse>> getInvoiceByMobile(String mobile);


}

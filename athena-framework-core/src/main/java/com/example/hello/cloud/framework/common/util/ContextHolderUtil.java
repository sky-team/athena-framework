package com.example.hello.cloud.framework.common.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.hello.cloud.framework.common.constant.AuthConstant;
import com.example.hello.cloud.framework.common.util.json.JsonUtil;
import com.example.hello.cloud.framework.common.constant.AuthConstant;
import com.example.hello.cloud.framework.common.dto.UserAccessInfo;
import com.example.hello.cloud.framework.common.util.json.JsonUtil;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author guohg03
 * @ClassName: ContextHolderUtil
 * @Description: 系统上下文相关工具类
 * @date 2018年9月26日
 */
public class ContextHolderUtil {

    private ContextHolderUtil() {
    }
    
	private static ThreadLocal<UserAccessInfo> accessInfoMap = new ThreadLocal<>();
    
    /**
     * 功能：获取request对象
     */
    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }
    
    /**
     * 功能：获取IP
     */
    public static String getIp() {
		return IPUtil.getIP(getRequest());
	}
    
    /**
     * 功能: 获取response对象
     */
    public static HttpServletResponse getResponse(){
        return((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
    }

    /**
     * 功能：获取session对象
     */
    public static HttpSession getSession() {
        return getRequest().getSession();
    }

    /**
     * 功能：获得请求路径
     */
    public static String getRequestPath() {
        String requestPath = getRequest().getRequestURI();
        //去掉其他参数
        if (requestPath.indexOf('?') > -1) {
            requestPath = requestPath.substring(0, requestPath.indexOf('?'));
        }
        if (requestPath.indexOf('&') > -1) {
            requestPath = requestPath.substring(0, requestPath.indexOf('&'));
        }
        //去掉项目路径
        requestPath = requestPath.substring(getRequest().getContextPath().length() + 1);
        return requestPath;
    }


    /**
     * 存放用户访问信息
     */
    public static void putUserAccessInfo(UserAccessInfo userAccessInfo) {
        accessInfoMap.set(userAccessInfo);
    }

    /**
     * 获取用户访问信息
     */
    public static UserAccessInfo getUserAccessInfo() {
        return accessInfoMap.get();
    }

    /**
     * 获取用户访问信息
     */
    public static UserAccessInfo getUserAccessInfo(HttpServletRequest request) {
        UserAccessInfo userAccessInfo = new UserAccessInfo();
        String accessJson = request.getHeader("UserAccessInfo");
        UserAccessInfo temp = JsonUtil.json2Obj(accessJson, UserAccessInfo.class);
        if (temp != null) {
            userAccessInfo = temp;
        }
        userAccessInfo.setIp(IPUtil.getIP(request));
        return userAccessInfo;
    }
    
    /**
     * 从header中获取authorization
     */
    public static String getAuthorizationFromHeader() {
    	return getRequest().getHeader(AuthConstant.AUTHORIZATION_HEADER);
    }
}
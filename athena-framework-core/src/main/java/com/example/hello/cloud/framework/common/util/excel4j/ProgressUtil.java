package com.example.hello.cloud.framework.common.util.excel4j;

import com.example.hello.cloud.framework.common.redis.RedisSingleCacheTemplate;
import com.example.hello.cloud.framework.common.redis.RedisSingleCacheTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 进度方法
 *
 * @author v-tanhp02
 */
@Component
@Slf4j
public class ProgressUtil {

    @Autowired
    RedisSingleCacheTemplate redisSingleCacheTemplate;

    ReentrantLock reentrantLock = new ReentrantLock();

    private ThreadLocal<Integer> beforeProgress = new ThreadLocal<>();


    public void updateProgress(ProgressService progressService, int total, String progressId, int index) {
        if (StringUtils.isEmpty(progressId) || progressService == null) {
            return;
        }
        try {
            if (total <= 0) {
                updateProgressVal(progressId, 99L, progressService);
                return;
            }
            //避免多线程写入excel 进度错乱
            if (index == 1) {
                beforeProgress.remove();
            }
            //执行完了
            if (index >= total) {
                if (StringUtils.isNotEmpty(progressId)) {
                    //更新进度为99 上传阿里云1
                    updateProgressVal(progressId, 99L, progressService);
                    this.beforeProgress.set(99);
                }
                return;
            }
            double granularity = this.calPercentage(total);
            double result = index / granularity;
            if (result < 1) {
                return;
            }
            int progress = (int) Math.ceil(result * 2);
            int before = beforeProgress.get() == null ? 0 : beforeProgress.get();
            //最多更新100次
            if (progress > before) {
                beforeProgress.set(progress);
                updateProgressVal(progressId, (long) (progress >= 100 ? 99 : progress), progressService);
            }
        } catch (
                Exception e) {
            log.error("修改进度失败", e);
        }

    }

    private void updateProgressVal(String id, Long progress, ProgressService progressService) {
        if (progressService != null) {
            progressService.updateProgress(id, progress);
        } else {
            log.error("未找到进度更新实例不进行更新");
        }
    }

    /**
     * 获取百分之二的个数
     *
     * @param total
     * @return
     */
    private double calPercentage(long total) {
        return total * 0.02;
    }
}

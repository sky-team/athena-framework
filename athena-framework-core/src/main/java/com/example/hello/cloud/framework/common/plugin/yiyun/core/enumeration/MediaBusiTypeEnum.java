package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 产品多媒体业务类型
 *
 * @author v-linxb
 * @create 2018/5/17
 **/
public enum MediaBusiTypeEnum {

    /**
     * 头图
     */
    HEAD("head", "头图"),
    /**
     * 缩略图
     */
    THUMBNAIL("thumbnail", "缩略图"),
    /**
     * 视频
     */
    VIDEO("video", "视频"),
    /**
     * 规划图
     */
    PLAN("plan", "规划图"),
    /**
     * 配套图
     */
    MATCH("match", "配套图"),
    /**
     * 效果图
     */
    EFFECT("effect", "效果图"),
    /**
     * 实景图
     */
    REAL("real", "实景图"),
    /**
     * 样板图
     */
    TEMPLATE("template", "样板图"),
    /**
     * 视频封面图
     */
    VIDEO_COVER("videocover", "视频封面图"),
    /**
     * 鸟瞰图（海报
     */
    POSTER("poster", "鸟瞰图（海报）"),

    AIR_SCAPE("airscape","俯视图"),
    /**
     * 动态主图
     */
    DYNAMICMAIN("dynamicmain", "动态主图"),
    /**
     * 户型-户型图
     */
    ROOM_TYPE("roomtype", "户型图"),
    /**
     * 户型-样板图
     */
    HOUSE_TEMPLATE("template", "样板图"),
    /**
     * 楼盘视频-视频封面图
     */
    ESTATE_VIDEO_COVER("estateVideoCover","视频封面图")
    ;

    MediaBusiTypeEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    private String value;

    private String desc;

    public String getValue() {
        return this.value;
    }

    public String getDesc() {
        return this.desc;
    }

    public static String getDesc(String value) {
        for (MediaBusiTypeEnum c : MediaBusiTypeEnum.values()) {
            if (c.getValue().equals(value)) {
                return c.getDesc();
            }
        }
        return null;
    }
}

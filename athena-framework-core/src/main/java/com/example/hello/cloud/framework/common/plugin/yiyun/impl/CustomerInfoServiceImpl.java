package com.example.hello.cloud.framework.common.plugin.yiyun.impl;

import com.google.gson.reflect.TypeToken;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunService;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunCustomerApiEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunServiceType;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.customer.QuerySpecialTagBizParam;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.customer.QuerySpecialTagResData;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.CustomerInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;

/**
 * @author zhoujj07
 * @ClassName: CustomerInfoServiceImpl
 * @Description: 翼云客户信息相关服务
 * @date 2018/8/23
 */
@Service
public class CustomerInfoServiceImpl implements CustomerInfoService {

    private final YiyunService yiyunService;

    @Autowired
    public CustomerInfoServiceImpl(YiyunService yiyunService) {
        this.yiyunService = yiyunService;
    }

    @Override
    public ApiResponse<QuerySpecialTagResData> querySpecialTag(QuerySpecialTagBizParam bizParam) {
        YiyunServiceType type = YiyunCustomerApiEnum.QUERY_SPECIAL_TAG;
        Type responseType = new TypeToken<YiyunResponse<QuerySpecialTagResData>>() {
        }.getType();
        YiyunResponse<QuerySpecialTagResData> yiyunResponse
                = yiyunService.action2(type, bizParam, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

}

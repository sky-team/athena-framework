package com.example.hello.cloud.framework.common.constant;

/**
 * @author guohg03
 * @ClassName: Constants
 * @Description: 公共的常量类
 * @date 2018年7月25日
 */
public class Constants {
	private Constants() {
	}
	
    /**
     * Redis短信验证码前缀
     */
    public static final String REDIS_SMS_CODE = "s:c:";

    /**
     * Redis登录图片验证码前缀
     */
    public static final String REDIS_LOGIN_CAPATHA_CODE = "c:c:";

    /**
     * 删除标记
     */
    public static final String DELETE_FLAG = "delete_flag";

    /**
     * 默认超级管理员角色id
     */
    public static final String SUPER_ADMIN_ROLE_ID = "1";

    /**
     * 获取seq最大重试次数
     */
    public static final int GAIN_SEQ_FAIL_CNT = 3;


    /**
     * 用户地理位置标识
     */
    public static final String LBS = "lbs";
    /**
     * 用户设备标识
     */
    public static final String DEVICE = "device";

    public static final String SMS_REDIS_KEY = "sms";

    public static final String TEN_SMS_APP_KEY = "APP_KEY";

    public static final String TEN_SMS_APP_ID = "APP_ID";

    public static final String TEN_SMS_URL = "URL";

    public static final String SMS_TEMPLATE_ID = "_TEMPLATE_ID";

    public static final String SMS_TEMPLATE_CONTENT = "_TEMPLATE_CONTENT";

    public static final String TEN_SMS_RSP_CODE = "result";


    /**
     * 默认分割符号
     */
    public static final String DEFAULT_SEG_SYMBOL = ",";


    public static final String DEFAULT_REPLACE_SYMBOL = "%s";

    /**
     * 腾讯短息发送成功返回码
     */
    public static final String TEN_SMS_SUCCESS_CODE = "0";

    public static final long WX_QRCODE_IMG_SIZE = 10000L;

    /**
     * 上报销售系统返回代码
     */
    public static final String NSS_SUCCESS_CODE = "10000";

    public static final int MAX_RETRIES = 3;

    /**
     * 上报销售系统返回信息最大长度
     */
    public static final int MAX_NSS_RESP_MSG = 200;

    /**
     * 产品线
     */
    public static final String PRODUCT_LINE = "productLine";

    /**
     * 自定义产品类型
     */
    public static final String OTHER_PRODUCT_TYPE = "otherProductType";

    /**
     * 客户台账根据产品分配客户
     */
    public static final String DISTRIBUTION_PRO_CUS = "distributionCusByPro";

    /**
     * 最大组织编码
     */
    public static final int MAX_ORG_CODE = 1000;

    /**
     * 验证码redis前缀
     */
    public static final String SMS_CAPTCHA_REDIS = "sms:cap:inf";

    public static final String SMS_CAPTCHA_PIN = "sms:pin:";

    /**
     * redis验证码校验信息失效时间
     */
    public static final long MAX_CAPTCHA_EXPIRE = 28800;

    public static final String SMS_CAPTCHA_FAILS = "fails";



    public static final  String HEAD_ORG_ID ="1";
    /**
     * 验证码校验时间
     */
    public static final String SMS_CAPTCHA_CHECK_TM = "checkTime";

    /**
     * 短息验证码发送时间
     */
    public static final String SMS_CAP_SENT_TM = "sentTime";

    /**
     * 验证码发送成功次数
     */
    public static final String SMS_CAPTCHA_SENT_COUNT = "sentCnt";
    /**
     * 验证码频繁错误限制时间
     */
    public static final String SMS_CAPTCHA_EXPIRE = "sms.captcha.expire";
    /**
     * 验证码频繁错误限制时间缺省值
     */
    public static final int SMS_CAPTCHA_EXPIRE_DEFAULTVAL = 300;
    /**
     * 验证码手机号每日发送次数限制
     */
    public static final String SMS_MAX_SEND = "sms.max.send";
    /**
     * 验证码手机号每日发送次数限制缺省值
     */
    public static final int SMS_MAX_SEND_DEFAULTVAL = 15;
    /**
     * 验证码发送间隔时间限制
     */
    public static final String SMS_MAX_INTERVAL = "sms.max.interval";
    /**
     * 验证码发送间隔时间限制缺省值
     */
    public static final int SMS_MAX_INTERVAL_DEFAULTVAL = 50;
    /**
     * 验证码ip每日发送次数限制
     */
    public static final String SMS_IP_MAX_SEND = "sms.ip.max.send";
    /**
     * 验证码ip每日发送次数限制缺省值
     */
    public static final long SMS_IP_MAX_SEND_DEFAULTVAL = 10000L;
    /**
     * 验证码ip限制开关
     */
    public static final String SMS_IP_SWITCHER = "sms.ip.switcher";
    /**
     * 验证码ip限制开关缺省值
     */
    public static final boolean SMS_IP_SWITCHER_DEFAULTVAL = false;



    /**
     * 交易数据来源(销售系统)
     */
    public static final String YIYUN_TRADE_SOURCE = "yiYun";

    /**
     * 交易数据来源(小泊科技)
     */
    public static final String BOYU_TRADE_SOURCE = "boYu";

    public static final String IMPORT_ORG_ERR= "第%s行%s";


    //异步方法添加traceid
    public static final String X_SPAN_ID = "X-B3-SpanId";
    public static final String X_SPAN_EXPORT = "X-Span-Export";
    public static final String X_TRACE_ID = "X-B3-TraceId";
    public static final String ENCODING = "UTF-8";


}

package com.example.hello.cloud.framework.common.kafka.parse;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.serializer.DateCodec;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Type;

@Slf4j
public class DateExtraProcessor extends DateCodec {
    @Override
    public  <T> T cast(DefaultJSONParser parser, Type clazz, Object fieldName, Object value) {
        try {
            return super.cast(parser,clazz,fieldName,value);
        } catch (Exception e) {
            return null;
        }
    }
}
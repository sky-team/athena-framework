package com.example.hello.cloud.framework.common.util;

import com.example.hello.cloud.framework.common.constant.Constants;
import com.example.hello.cloud.framework.common.constant.Constants;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.util.ReflectionUtils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 常用的操作工具集锦
 *
 * @author Bond(China)
 * @version 1.0.0
 */
@Slf4j
public final class Helper {
    private Helper() {
    }

    /**
     * 判断是否为空
     *
     * @param object 需要被判定的对象
     * @return Boolean
     */
    public static boolean isNull(Object object) {
        return object == null;
    }

    /**
     * 判断是否为空
     *
     * @param object
     * @return
     */
    public static boolean isEmpty(String object) {
        if (isNull(object)) {
            return true;
        }
        return StringUtils.isEmpty(object.trim());
    }

    /**
     * 判断是否为空
     *
     * @param object
     * @return
     */
    public static boolean isEmpty(List object) {
        return CollectionUtils.isEmpty(object);
    }

    /**
     * 判断Map是滞为空
     *
     * @param object
     * @return
     */
    public static boolean isEmpty(Map object) {
        if (isNull(object)) {
            return true;
        }
        return object.size() == 0;
    }

    /**
     * 隐藏手机号中间4位
     *
     * @param phone
     * @return
     */
    public static String hiddenPhone(String phone) {
        if (Helper.isEmpty(phone)) {
            return phone;
        }
        return phone.replaceAll("(\\d{3})(\\d{4})(\\d{1,5})", "$1****$3");
    }

    /**
     * 批量判空，多个对象中只要有一个为空就为true
     *
     * @param objects
     * @return
     */
    public static boolean batchEmpty(Object[] objects) {
        boolean rs = false;
        for (int i = 0; i < objects.length; i++) {
            Object object = objects[i];
            if (isNull(object)) {
                return true;
            }
            if (object instanceof String) {
                rs = isEmpty((String) object);
            }
            if (object instanceof List) {
                rs = isEmpty((List) object);
            }
            if (object instanceof Map) {
                rs = isEmpty((Map) object);
            }
            if (rs) {
                return rs;
            }
        }
        return rs;
    }

    /**
     * 是否相等
     *
     * @param v1 Value1
     * @param v2 Value2
     * @return Boolean
     */
    public static boolean isEqual(Integer v1, Integer v2) {

        if (isNull(v1) || isNull(v2)) {
            return false;
        }
        return v1.equals(v2);
    }

    /**
     * 是否相等
     *
     * @param v1 Value1
     * @param v2 Value2
     * @return Boolean
     */
    public static boolean isEqual(Byte v1, Byte v2) {

        if (isNull(v1) || isNull(v2)) {
            return false;
        }
        return v1.equals(v2);
    }

    /**
     * 是否相等
     *
     * @param v1 Value1
     * @param v2 Value2
     * @return Boolean
     */
    public static boolean isEqual(Long v1, Long v2) {

        if (isNull(v1) || isNull(v2)) {
            return false;
        }
        return v1.equals(v2);
    }

    /**
     * 是否相等
     *
     * @param v1
     * @param v2
     * @return Boolean
     */

    public static boolean isEqual(Boolean v1, Boolean v2) {

        if (isNull(v1) || isNull(v2)) {
            return false;
        }
        return v1.equals(v2);
    }

    /**
     * 是否相等
     *
     * @param v1 Value1
     * @param v2 Value2
     * @return Boolean
     */
    public static boolean isEqual(String v1, String v2) {

        if (isNull(v1) || isNull(v2)) {
            return false;
        }
        return v1.equals(v2);
    }

    public static boolean isTrue(Boolean val) {

        if (isNull(val) || val == false) {
            return false;
        }
        return true;
    }


    /**
     * 非0均为true, 0 为false
     *
     * @param integer
     * @return
     */
    public static Boolean toBool(Integer integer) {
        if (Helper.isNull(integer)) {
            return null;
        }
        if (Helper.isEqual(integer, 0)) {
            return false;
        }
        return true;
    }

    /**
     * "false" To boolean false, "true" to boolean true, other of null
     *
     * @param str
     * @return
     */
    public static Boolean toBool(String str) {
        return Helper.isEqual(str, "true");
    }

    /**
     * 判断数组是否为空
     *
     * @param ignoreFields
     * @return
     */
    public static boolean isEmpty(Object[] ignoreFields) {
        return ArrayUtils.isEmpty(ignoreFields);
    }

    /**
     * 区间判断，开区间
     *
     * @param dest
     * @param low
     * @param high
     * @return
     */
    public static boolean betweenOpen(Integer dest, Integer low, Integer high) {
        return dest > low && dest < high;
    }

    /**
     * 区间判断，闭区间
     *
     * @param dest
     * @param low
     * @param high
     * @return
     */
    public static boolean betweenClose(Integer dest, Integer low, Integer high) {
        return dest >= low && dest <= high;
    }

    /**
     * 区间判断，开区间
     *
     * @param dest
     * @param low
     * @param high
     * @return
     */
    public static boolean betweenOpen(Long dest, Long low, Long high) {
        return dest > low && dest < high;
    }

    /**
     * 区间判断，闭区间
     *
     * @param dest
     * @param low
     * @param high
     * @return
     */
    public static boolean betweenClose(Long dest, Long low, Long high) {
        return dest >= low && dest <= high;
    }

    /**
     * URL 转码
     *
     * @param url
     * @return
     */
    public static String encodeUrl(String url) {
        try {
            return URLEncoder.encode(url, Constants.ENCODING);
        } catch (UnsupportedEncodingException e) {
            log.warn("Url encode", e);
            return URLEncoder.encode(url);
        }
    }

    /**
     * URL 转码
     *
     * @param url
     * @return
     */
    public static String decodeUrl(String url) {
        try {
            return URLDecoder.decode(url, Constants.ENCODING);
        } catch (UnsupportedEncodingException e) {
            log.warn("Url decode", e);
            return URLDecoder.decode(url);
        }
    }


    /**
     * 将数组转换成字符串
     *
     * @param arr
     * @return
     */
    public static String toString(String[] arr) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
        }
        return sb.toString();
    }

    /**
     * 将StringList数组转成传统Array
     *
     * @param list
     * @return
     */
    public static String[] toArray(List<String> list) {
        if (Helper.isEmpty(list)) {
            return new String[0];
        }
        String[] rs = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            rs[i] = list.get(i);
        }
        return rs;
    }

    /**
     * 生成一个无 - 的UUID
     *
     * @return
     */
    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 生成一个nonce
     *
     * @param len
     * @return
     */
    public static String nonce(int len) {
        String rs = UUID.randomUUID().toString();
        rs = SHA1.encrypt(rs);
        int tlen = len;
        if (tlen >= rs.length()) {
            return rs;
        }
        return rs.substring(0, tlen);
    }

    /**
     * 生成一个时间戳
     *
     * @return
     */
    public static String timestamp() {
        return new Date().getTime() + "";
    }

    /**
     * 生成hashCode
     *
     * @param params
     * @return
     */
    public static int generateHashCode(String[] params) {

        if (Helper.isEmpty(params)) {
            return (int) (Math.random() * 10000);
        }

        int code = 10000;
        for (int i = 0; i < params.length; i++) {
            char[] chars = params[i].toCharArray();
            for (int j = 0; j < chars.length; j++) {
                code = code + chars[j] << 3 + chars[j] >> 2;
            }
        }
        return code;
    }

    /**
     * 截取
     *
     * @param str
     * @param len
     * @return
     */
    public static String trim(String str, int len) {

        if (Helper.isEmpty(str)) {
            return str;
        }

        if (str.length() > len) {
            return str.substring(0, len);
        } else {
            return str;
        }
    }

    /**
     * 获取汉字的拼音
     *
     * @param src
     * @return
     */
    public static String toPinYin(String src) {

        if (Helper.isEmpty(src)) {
            return "";
        }

        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        StringBuilder rs = new StringBuilder();
        char[] chs = src.toCharArray();
        try {
            for (int i = 0; i < chs.length; i++) {
                if (!Validator.CHINESE.match(chs[i] + "")) {
                    rs.append(chs[i]);
                    continue;
                }
                String[] tmp = PinyinHelper.toHanyuPinyinStringArray(chs[i], format);
                rs.append(tmp[0]);
            }
        } catch (Exception e) {
            log.error("Convert PinYin Error", e);
        }
        return rs.toString();
    }

    /**
     * 将一串字串转换成Int, 累加字符的ASCII值，按位乘以2的指数
     *
     * @param src
     * @return
     */
    public static int toIntForCompare(String src) {
        if (Helper.isEmpty(src)) {
            return 0;
        }
        String tmp = Helper.toPinYin(src).toUpperCase();
        int limit = 5;
        if (tmp.length() > limit) {
            tmp = tmp.substring(0, limit);
        }
        // 补空
        if (tmp.length() < limit) {
            int needChs = limit - tmp.length();
            for (int i = 0; i < needChs; i++) {
                tmp += "A";
            }
        }
        char[] chs = tmp.toCharArray();
        int rs = 0;
        int size = chs.length;
        for (int i = chs.length - 1; i >= 0; i--) {
            int it = chs[i];
            rs += it * Math.pow(10, size - i);
        }
        return rs;
    }

    /**
     * 将src中属性存在于dest中的属性值复制dest中, 当值为null时，就不复制
     *
     * @param dest 目标对象
     * @param src  源对象
     * @param <T>
     * @param <E>
     * @return
     */
    public static <T, E> T merge(T dest, E src) {
        return merge(dest, src, new String[]{}, true);
    }

    /**
     * 将src中属性存在于dest中的属性值复制dest中, 当值为null时，就不复制
     *
     * @param dest         目标对象
     * @param src          源对象
     * @param ignoreFields 属性忽略集
     * @param <T>
     * @param <E>
     * @return
     */
    public static <T, E> T merge(T dest, E src, String[] ignoreFields) {
        return merge(dest, src, ignoreFields, true);
    }

    /**
     * 将src中属性存在于dest中的属性值复制dest中
     *
     * @param dest         目标对象
     * @param src          源对象
     * @param ignoreFields 属性忽略集
     * @param ignoreNull   true：当值为null时，就不复制, false:当值为null时，也复制
     * @param <T>
     * @param <E>
     * @return
     */
    public static <T, E> T merge(T dest, E src, String[] ignoreFields, boolean ignoreNull) {
        if (isNull(dest) || isNull(src)) {
            return dest;
        }
        Class<?> destClz = dest.getClass();
        Class<?> srcClz = src.getClass();

        Field[] destClzDeclaredFields = destClz.getDeclaredFields();
        Field[] srcClzDeclaredFields = srcClz.getDeclaredFields();

        if (isEmpty(destClzDeclaredFields) || isEmpty(srcClzDeclaredFields)) {
            return dest;
        }

        for (int i = 0; i < destClzDeclaredFields.length; i++) {
            Field field = destClzDeclaredFields[i];
            if (isIgnore(ignoreFields, field)) {
                continue;
            }
            String name = field.getName();
            boolean flag = false;
            Field srcField = null;
            for (int e = 0; e < srcClzDeclaredFields.length; e++) {
                srcField = srcClzDeclaredFields[e];
                if (srcField.getName().equals(name)) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                continue;
            }
            try {
                srcField.setAccessible(true);
                field.setAccessible(true);
                Object value = srcField.get(src);
                if (ignoreNull && value == null) {
                    continue;
                }
                field.set(dest, value);
            } catch (Exception e) {
                log.error("Helper merge setting value has error", e);
            } finally {
                srcField.setAccessible(false);
                field.setAccessible(false);
            }
        }
        return dest;
    }

    private static boolean isIgnore(String[] ignoreFields, Field field) {
        boolean ignore = false;
        if (Helper.isEmpty(ignoreFields)) {
            return ignore;
        }
        for (int e = 0; e < ignoreFields.length; e++) {
            if (Helper.isEqual(field.getName(), ignoreFields[e])) {
                ignore = true;
            }
        }
        return ignore;
    }

    /**
     * 获取分页大小
     *
     * @param size
     * @return
     */
    public static Integer getPerThreadPageSize(int size) {
        Integer pageSize = 10;
        if (size > 10) {
            Integer limitThread = getLimitThread();
            if (limitThread >= size) {
                pageSize = 1;
                return pageSize;
            }
            pageSize = size / limitThread;
            if (size % limitThread != 0) {
                pageSize = pageSize + 1;
            }
        }
        return pageSize;
    }

    /**
     * 获取可用线程数
     *
     * @return
     */
    public static Integer getLimitThread() {
        int limitThread = Runtime.getRuntime().availableProcessors();
        if (limitThread <= 2) {
            return 1;
        }
        if (limitThread % 2 == 0) {
            limitThread = limitThread / 2;
        } else {
            limitThread = limitThread / 3;
        }
        return limitThread;
    }


    /**
     * 获取线程池的大小
     *
     * @return
     */
    public static int getThreadPoolSize() {
        return 2 * Runtime.getRuntime().availableProcessors();
    }

    /**
     * 从map中获取一个值
     *
     * @param map
     * @param key
     * @param <T>
     * @return
     */
    public static <T> T get(Map<String, T> map, String key) {
        if (Helper.isEmpty(map)) {
            return null;
        }
        if (Helper.isEmpty(key)) {
            return null;
        }
        return map.get(key);
    }

    /**
     * 从Map中获取一个值，如果为null，返回默认值
     *
     * @param map
     * @param key
     * @param replace
     * @param <T>
     * @return
     */
    public static <T> T get(Map<String, T> map, String key, T replace) {
        T value = get(map, key);
        if (Helper.isNull(value)) {
            value = replace;
        }
        return value;
    }

    /**
     * 过滤集合
     *
     * @param list
     * @param predicate
     * @param <T>
     * @return
     */
    public static <T> List<T> filter(List<T> list, Predicate<T> predicate) {
        if (Helper.isEmpty(list)) {
            return list;
        }
        return list.stream().filter(predicate).collect(Collectors.toList());
    }

    /**
     * 获取属性值
     *
     * @param clz
     * @param fieldName
     * @param target
     * @param <T>
     * @return
     * @throws Throwable
     */
    public static <T> Object getField(Class<T> clz, String fieldName, T target){
    	try {
    		Field field = clz.getDeclaredField(fieldName);
            field.setAccessible(true);
            Object value = ReflectionUtils.getField(field, target);
            field.setAccessible(false);
            return value;
    	}catch(Exception e) {
    		log.error(e.getMessage(),e);
    		return null;
    	}
    }

    /**
     * 获取属性值
     *
     * @param fieldName
     * @param target
     * @return
     * @throws Throwable
     */
    public static Object getField(String fieldName, Object target) throws NoSuchFieldException {
        Class<?> clz = target.getClass();
        Field field = clz.getDeclaredField(fieldName);
        field.setAccessible(true);
        Object value = ReflectionUtils.getField(field, target);
        field.setAccessible(false);
        return value;
    }

    /**
     * 遍历集合
     *
     * @param list
     * @param consumer
     * @param <T>
     */
    public static <T> void each(List<T> list, Consumer<T> consumer) {
        if (Helper.isEmpty(list)) {
            return;
        }
        list.forEach(consumer);
    }


    /**
     * 隐藏身份证号年月日信息
     *
     * @param value
     * @return
     */
    public static String hiddenIdCard(String value) {
        if (Helper.isEmpty(value)) {
            return value;
        }
        return value.replaceAll("(\\d{6})(\\d{8})(\\d{1,4})", "$1********$3");
    }
    /**
     * 身份证号码只显示最后四位
     *
     * @param value
     * @return
     */
    public static String hiddenIdCardNumber(String value) {
        if (Helper.isEmpty(value)) {
            return value;
        }
        return value.replaceAll("(\\d{0})(\\d{14})(\\d{1,4})", "$1********$3");
    }


    /**
     * 校验
     */
    public static enum Validator {

        ALPHA_CHINESE("([\\u4e00-\\u9fa5]|[A-Za-z])+"),
        EMOJI("[\\ud83c\\udc00-\\ud83c\\udfff]|[\\ud83d\\udc00-\\ud83d\\udfff]|[\\u2600-\\u27ff]"),
        PHONE("[1][123456789][0-9]{9}"),
        EMAIL("(\\w+((-\\w+)|(\\.\\w+))*)\\+\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+"),
        NONCE("\\w{16,64}"),
        CHINESE("[\\u4e00-\\u9fa5]+");

        private String reg;

        private Validator(String reg) {
            this.reg = reg;
        }

        public boolean match(String val) {
            Pattern pattern = Pattern.compile(this.reg);
            return pattern.matcher(val).matches();
        }

        public String match(String val, String replace) {
            if (Helper.isNull(val)) {
                return val;
            }
            Pattern pattern = Pattern.compile(this.reg);
            if (Helper.isEmpty(replace)) {
                pattern.matcher(val).replaceAll("[表情]");
            }
            return pattern.matcher(val).replaceAll(replace);
        }

        public static boolean customMatch(String reg, String val) {
            Pattern pattern = Pattern.compile(reg);
            return pattern.matcher(val).matches();
        }
    }
}
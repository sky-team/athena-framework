package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 客户子账号状态枚举
 *
 * @author zhoujj07
 * @create 2018/5/3
 */
public enum CustomerSubStatusEnum {

    /**
     * 正常
     */
    NORMAL(0),
    /**
     * 已锁定
     */
    LOCKED (1),;

    private Integer index;

    CustomerSubStatusEnum(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

}

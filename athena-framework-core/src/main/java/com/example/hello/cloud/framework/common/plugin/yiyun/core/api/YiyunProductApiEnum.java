package com.example.hello.cloud.framework.common.plugin.yiyun.core.api;

/**
 * 产品中心api地址枚举
 *
 * @author v-linxb
 * @create 2018/5/4
 **/
public enum YiyunProductApiEnum implements YiyunServiceType {

    /**
     * 新增产品信息
     */
    PRODUCT_CREATE("v1/product/create", "新增产品信息", false),

    PRODUCT_BASE_UPDATE("v1/product/basic/update", "修改产品信息", false),

    PRODUCT_BASE_QUERY("v1/product/basic/listQuery", "查询产品信息", false),

    PRODUCT_MAPPING_QUERY("v1/product/basic/queryNew", "产品映射查询", false),

    PRODUCT_MAPPING_CREATE("v1/product/relation/create", "新增产品映射", false),
    /**
     * 查询产品属性信息树
     */
    PRODUCT_PARENT_TREE("v1/product/prop/parentTree", "查询产品属性信息树", false),
    /**
     * 查询产品详细信息
     */
    PRODUCT_BASIC_QUERY("v1/product/basic/query", "查询产品详细信息", false),
    ;


    YiyunProductApiEnum(String url, String desc, boolean needTicket) {
        this.url = url;
        this.desc = desc;
        this.needTicket = needTicket;
    }

    private final static String center = "product/";
    private String url;
    private String desc;
    private boolean needTicket;

    @Override
    public String url() {
        return center + this.url;
    }

    @Override
    public String desc() {
        return this.desc;
    }

    @Override
    public boolean needTicket() {
        return this.needTicket;
    }
}

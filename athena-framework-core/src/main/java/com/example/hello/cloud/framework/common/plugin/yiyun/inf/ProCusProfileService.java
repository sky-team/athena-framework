package com.example.hello.cloud.framework.common.plugin.yiyun.inf;

import java.util.Map;

/**
 * @author 涂永康
 * @className
 * @date 2019/8/6 16:27
 * @description
 */
public interface ProCusProfileService {
    /**
     * 项目营销画像/首页/客户基础信息
     * @param param 请求JSON
     * @return
     */
    String prdCusBasicInfoReport(Map<String, Object> param);

    /**
     * 项目营销画像/首页/客户购房意向画像
     * @param param 请求JSON
     * @return
     */
    String prdCusDepictReport(Map<String, Object> param);

    /**
     * 项目营销画像/客户画像列表
     * @param param 请求JSON
     * @return
     */
    String cusProfilePage(Map<String, Object> param);

    /**
     * 项目营销画像/客户画像详情
     * @param param 请求JSON
     * @return
     */
    String cusProfileDetails(Map<String, Object> param);

}

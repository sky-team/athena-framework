package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 翼云-交易单据是否关闭状态枚举
 *
 * @author zhoujj07
 * @create 2018/6/6
 */
public enum TransactionCloseStateEnum {

    /**
     * 未关闭
     */
    NOT_CLOSE("0"),
    /**
     * 已关闭
     */
    CLOSED("1"),;

    private String index;

    TransactionCloseStateEnum(String index) {
        this.index = index;
    }

    public String getIndex() {
        return index;
    }
}

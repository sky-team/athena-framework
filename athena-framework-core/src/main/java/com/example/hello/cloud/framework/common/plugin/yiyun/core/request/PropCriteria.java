package com.example.hello.cloud.framework.common.plugin.yiyun.core.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 查询条件
 *
 * @author v-linxb
 * @create 2018/5/15
 **/
@Data
public class PropCriteria implements Serializable {

    /**
     * 需要匹配的值列表。满足此列表中的任意一个值则适配属性匹配。类似sql 中 where条件的in
     */
    private String[] criteriaValues;

    /**
     * 需要模糊匹配的值  criteriaWildcardValue和criteriaValues 二选一必填
     */
    private String criteriaWildcardValue;

    /**
     * 需要匹配的扩展属性
     */
    private String extAttr;
}

package com.example.hello.cloud.framework.common.http.client;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.util.concurrent.TimeUnit;

/**
 * @author zhoujj
 * @ClassName: IdleConnectionMonitorThread
 * @Description: 监控http connection的空闲线程
 * @date 2018年7月24日
 */
@Slf4j
public class IdleConnectionMonitorThread extends Thread {

    /**
     * The conn mgr.
     * PoolingHttpClientConnectionManager is a more complex implementation that manages a pool
     * of client connections and is able to service connection requests from multiple execution threads.
     */
    private final PoolingHttpClientConnectionManager connMgr;

    /**
     * The shutdown.
     */
    private volatile boolean shutdown;

    /**
     * The timeout.
     */
    private int timeout = 30;

    /**
     * The interval.
     */
    private long interval = 6000L;

    /**
     * Instantiates a new idle connection monitor thread.
     *
     * @param connMgr the conn mgr
     */
    IdleConnectionMonitorThread(PoolingHttpClientConnectionManager connMgr) {
        super();
        this.connMgr = connMgr;
    }

    /**
     * Sets the timeout(the maximum time to wait in milliseconds).
     *
     * @param timeout the new timeout
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * Sets the interval.
     *
     * @param interval the new interval
     */
    public void setInterval(long interval) {
        this.interval = interval;
    }

    /**
     * run
     */
    @Override
    public void run() {
        try {
            while (!shutdown) {
                synchronized (this) {
                    wait(this.interval);
                    // Close expired connections
                    connMgr.closeExpiredConnections();
                    // Optionally, close connections that have been idle longer than 30 sec
                    connMgr.closeIdleConnections(this.timeout, TimeUnit.SECONDS);
                }
            }
        } catch (Exception ex) {
        	log.error(ex.getMessage(),ex);
        }
    }

    /**
     * Shutdown.
     */
    public void shutdown() {
        shutdown = true;
        synchronized (this) {
            notifyAll();
        }
    }
}

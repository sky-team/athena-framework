package com.example.hello.cloud.framework.common.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 非空正则匹配校验器
 *
 * @author zhanj04
 * @date 2017/8/1 9:29
 */
public class NotEmptyPatternValidator implements ConstraintValidator<NotEmptyPattern, String> {

    private String regexp;

    /**
     * @see ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(NotEmptyPattern constraintAnnotation) {
        this.regexp = constraintAnnotation.regexp();

    }

    /**
     * @see ConstraintValidator#isValid(Object, ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(String arg0, ConstraintValidatorContext arg1) {
        if (arg0 == null || "".equals(arg0)) {
            return true;
        }
        return Pattern.matches(regexp, arg0);
    }

    public static void main(String[] args) {
        String regexp = "1\\d{10}$|^(\\+\\d+)?(\\d{3,4}\\-?)?\\d{7,8}?";
        System.out.println(Pattern.matches(Regexp.PHONE_REGEXP, "18871278765"));
    }
}

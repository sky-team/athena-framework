package com.example.hello.cloud.framework.common.plugin.yiyun.enums;

/**
 * 员工状态枚举
 *
 * @author guoc16
 * @date 2020/09/03
 */
public enum EmployeeStatusEnum {
    /**
     * 在职
     */
    ZAIZHI1("1", "在职"),
    /**
     * 在职不在岗
     */
    ZAIZHI2("2", "在职不在岗"),
    /**
     * 离职
     */
    LIZHI("3", "离职"),
    /**
     * 退休
     */
    TUIXIU("4", "退休");

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    EmployeeStatusEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String code) {
        for (EmployeeStatusEnum agentTypeEnum : EmployeeStatusEnum.values()) {
            if (agentTypeEnum.getCode().equals(code)) {
                return agentTypeEnum.getName();
            }
        }
        return null;
    }
}

package com.example.hello.cloud.framework.common.plugin.openapi.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.reflect.TypeToken;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.plugin.openapi.api.DataOpenApiEnum;
import com.example.hello.cloud.framework.common.plugin.openapi.core.exampleOpenApiService;
import com.example.hello.cloud.framework.common.plugin.openapi.dto.VkCrmOwnerReq;
import com.example.hello.cloud.framework.common.plugin.openapi.dto.VkCrmOwnerResp;
import com.example.hello.cloud.framework.common.plugin.openapi.inf.VkCrmApiService;
import com.example.hello.cloud.framework.common.util.StringUtil;
import com.example.hello.cloud.framework.common.util.json.GsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 万科物业业主身份信息service
 *
 * @author v-linxb
 * @create 2019/9/5
 **/
@Slf4j
@Service
public class VkCrmApiServiceImpl implements VkCrmApiService {

    @Autowired
    private exampleOpenApiService exampleOpenApiService;

    /**
     * 根据手机号/身份证号查询物业业主信息
     *
     * @param vkCrmOwnerReq
     * @return
     */
    @Override
    public List<VkCrmOwnerResp> crmOwnerQuery(VkCrmOwnerReq vkCrmOwnerReq) {
        try {
            String resp = exampleOpenApiService.postAction(DataOpenApiEnum.CUSQUERY_VKCRM_OWNER, vkCrmOwnerReq);
//            String resp = "{\n" +
//                    "    \"success\": true,\n" +
//                    "    \"message\": \"ok\",\n" +
//                    "    \"data\": [\n" +
//                    "        {\n" +
//                    "            \"fullName\": \"陈*\",\n" +
//                    "            \"certificateId\": \"2101****5335\",\n" +
//                    "            \"mainMobile\": \"180****3933\",\n" +
//                    "            \"buildingName\": \"沈阳香湖盛景苑南区16号楼\",\n" +
//                    "            \"isOwner\": 1,\n" +
//                    "            \"isSecondhand\": 0,\n" +
//                    "            \"isChild\": 0\n" +
//                    "        },\n" +
//                    "        {\n" +
//                    "            \"fullName\": \"林*\",\n" +
//                    "            \"certificateId\": \"2101****5335\",\n" +
//                    "            \"mainMobile\": \"180****3933\",\n" +
//                    "            \"buildingName\": \"沈阳香湖盛景苑南区166666666号楼\",\n" +
//                    "            \"isOwner\": 1,\n" +
//                    "            \"isSecondhand\": 1,\n" +
//                    "            \"isChild\": 0\n" +
//                    "        }\n" +
//                    "    ]\n" +
//                    "}";
            if (StringUtil.isNotEmpty(resp)) {
                JSONObject json = JSONObject.parseObject(resp);
                boolean successFlag = json.containsKey("success") && json.getBoolean("success");
                boolean codeFlag = json.containsKey("code") && Objects.equals(ApiResponseCodeEnum.SUCCESS.getCode(), json.getInteger("code"));
                boolean dataFlag = successFlag || codeFlag;
                if (dataFlag && json.containsKey("data")) {
                    return GsonUtil.fromJson(json.getString("data"), new TypeToken<List<VkCrmOwnerResp>>() {
                    }.getType());
                }
            }
        } catch (Exception e) {
            log.error("根据手机号/身份证号查询物业业主信息 error {}", e);
        }
        return new ArrayList<>();
    }
}

package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.report.vo.Prop;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.report.vo.Prop;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

/**
 * @author :v-zhousq04
 * @description: 中台返回产品映射关系对象
 * @create date: 2018/8/29 15:22
 */
@Data
@EqualsAndHashCode
public class ProductMappingQueryResp implements YiyunResponseData {
    private static final long serialVersionUID = -2653047022209480919L;

    private String productId;
    private String name;
    private String parentProductId;
    private String status;
    private String categoryId;
    private String outerApp;
    private String outerId;
    private String bundle;
    private String createPerson;
    private String share;
    private Map propMap;
    private List<Prop> propList;
    private String propMapWithExtAttr;
    private String extAttrGroupPropMap;
    private String propGroup;
}

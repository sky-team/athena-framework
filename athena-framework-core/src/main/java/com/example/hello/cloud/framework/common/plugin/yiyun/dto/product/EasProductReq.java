package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.PropQueryCriteria;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.PropQueryCriteria;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author v-linxb
 * @create 2019/6/20
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class EasProductReq extends BaseYiyunRequestData implements Serializable {

    private String categoryId;

    private PropQueryCriteria queryCriteria;

    private List<String> returnPubProps;
}

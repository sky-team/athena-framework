package com.example.hello.cloud.framework.common.util.oss;

import cn.hutool.core.util.StrUtil;
import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.util.*;

/**
 * AliyunOssUtil
 * 阿里云oss上传帮助类
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Slf4j
@Component
public class AliyunOssUtil {

    /**
     * WEB直传TOKEN超时时间
     */
    private static final int TOKEN_EXPIRE_SECONDS = 120;

    /**
     * 图片最大允许上传6M
     */
    private static final int MAX_UPLOAD_IMAGE_SIZE = 6144000;
    /**
     * 视频最大允许上传51M
     */
    private static final int MAX_UPLOAD_MP4_SIZE = 53477376;
    @Value("${oss.key:}")
    String ossKey;
    @Value("${oss.secret:}")
    String ossSecret;
    @Value("${oss.domain:}")
    String ossDomain;
    @Value("${oss.bucket:}")
    String ossBucket;
    @Value("${oss.enabledOpenCDN:}")
    String ossEnabledOpenCdn;
    @Value("${oss.cdnBaseUrl:}")
    String ossCdnBaseUrl;

    /**
     * 获取临时图片的url开头
     *
     * @return
     */
    public String getTempUrlHead() {
        return getUrlHead("temp");
    }

    /**
     * 获取指定目录图片的url开头
     *
     * @param folderName
     * @return
     */
    public String getUrlHead(String folderName) {
        return HTTPS + ossBucket + "." + ossDomain + "/" + AliyunClassificationEnum.IMAGE + "/" + folderName + "/";
    }

    /**
     * 获取http域名
     *
     * @return
     */
    public String getHttpDomain() {
        return HTTPS + ossBucket + "." + ossDomain;
    }

    private OSSClient ossClient;

    private static final String HTTPS = "https://";

    @PostConstruct
    public void init() {
        if (StrUtil.isBlank(ossKey) || StrUtil.isBlank(ossKey)) {
            log.warn("aliyun oss工具未配置,不启动");
            return;
        }
        try {

            ClientConfiguration conf = new ClientConfiguration();
            conf.setMaxConnections(2048);
            // 设置TCP连接超时为5000毫秒
            conf.setConnectionTimeout(5000);
            //连接空闲超时时间，超时则关闭连接
            conf.setIdleConnectionTime(2000);
            // 设置最大的重试次数为3
            conf.setMaxErrorRetry(3);
            // 设置Socket传输数据超时的时间为2000毫秒
            conf.setSocketTimeout(2000);
            ossClient = new OSSClient(ossDomain,
                    ossKey,
                    ossSecret, conf);
            log.info("aliyun oss工具启动成功");
        } catch (Exception e) {
            log.warn("aliyun oss工具启动失败！", e);
        }

    }

    /**
     * 上传文件到 OSS
     *
     * @param classificationEnum 分类
     * @param folderEnum         文件夹
     * @param fileName           文件名称
     * @param fileLength         文件长度
     * @param inputStream        文件流
     * @return 文件名
     * @throws IOException
     */
    public String putObject(AliyunClassificationEnum classificationEnum, AliyunFolderTypeEnum folderEnum, String fileName, InputStream inputStream, long fileLength) throws IOException {
        try {
            createFolder(classificationEnum, folderEnum);
            //创建上传 Object 的 Metadata
            ObjectMetadata objectMetadata = new ObjectMetadata();
            //必须设置
            objectMetadata.setContentLength(fileLength);
            //上传 Object
            ossClient.putObject(ossBucket, StringUtils.join(classificationEnum.getPath(), folderEnum.getPath(), fileName), inputStream, objectMetadata);

        } catch (Exception e) {
            log.error("putObject error={}", e);
        } finally {
            close(inputStream);
        }
        return fileName;
    }

    /**
     * 上传文件到 OSS（指定目录 目录必须已经存在）
     *
     * @param classificationEnum 分类
     * @param folderEnum         文件夹
     * @param fileName           文件名称
     * @param fileLength         文件长度
     * @param inputStream        文件流
     * @return 文件名
     * @throws IOException
     */
    public String putObject(String path, String fileName, InputStream inputStream, long fileLength) throws IOException {
        try {
            //创建上传 Object 的 Metadata
            ObjectMetadata objectMetadata = new ObjectMetadata();
            //必须设置
            objectMetadata.setContentLength(fileLength);
            //上传 Object
            log.info("oss上传key为 {}", StringUtils.join(path, "/", fileName));
            ossClient.putObject(ossBucket, StringUtils.join(path, "/", fileName), inputStream, objectMetadata);

        } catch (Exception e) {
            log.error("putObject error={}", e);
        } finally {
            close(inputStream);
        }
        return fileName;
    }

    /**
     * 删除单个文件
     *
     * @param classificationEnum 分类
     * @param folderEnum         文件夹
     * @param fileName           文件名称
     * @throws IOException
     */
    public void deleteObject(AliyunClassificationEnum classificationEnum, AliyunFolderTypeEnum folderEnum, String fileName) throws IOException {
        ossClient.deleteObject(ossBucket, StringUtils.join(classificationEnum.getPath(), folderEnum.getPath(), fileName));
    }

    /**
     * 删除单个文件
     *
     * @param fileKey 文件key
     */
    public void deleteObject(String fileKey) {
        ossClient.deleteObject(ossBucket, fileKey);
    }

    /**
     * 删除多个文件
     *
     * @param fileKeys 文件key
     */
    public void deleteObjects(List<String> fileKeys) {
        DeleteObjectsRequest deleteObjectsRequest = new DeleteObjectsRequest(ossBucket);
        deleteObjectsRequest.setKeys(fileKeys);
        ossClient.deleteObjects(deleteObjectsRequest);
    }

    /**
     * 列举目录下所有文件
     *
     * @param classificationEnum 分类
     * @param folderEnum         文件夹
     * @throws IOException
     */
    public List<String> showObject(AliyunClassificationEnum classificationEnum, AliyunFolderTypeEnum folderEnum) throws IOException {
        List<String> keyList = new ArrayList<>();
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest(ossBucket);
        listObjectsRequest.setPrefix(StringUtils.join(classificationEnum.getPath(), folderEnum.getPath()));
        ObjectListing listing = ossClient.listObjects(listObjectsRequest);
        for (OSSObjectSummary objectSummary : listing.getObjectSummaries()) {
            if (objectSummary.getSize() != 0) {
                keyList.add(objectSummary.getKey());
            }
        }
        return keyList;
    }

    /**
     * 拷贝文件到别的目录
     *
     * @param classificationEnum       分类
     * @param fileName                 文件名
     * @param srcAliyunFolderTypeEnum  源目录
     * @param destAliyunFolderTypeEnum 目标目录
     * @throws IOException
     */
    public String copyObject(AliyunClassificationEnum classificationEnum, String fileName, AliyunFolderTypeEnum srcAliyunFolderTypeEnum, AliyunFolderTypeEnum destAliyunFolderTypeEnum) throws IOException {
        createFolder(classificationEnum, destAliyunFolderTypeEnum);
        ossClient.copyObject(ossBucket, StringUtils.join(classificationEnum.getPath(), srcAliyunFolderTypeEnum.getPath(), fileName), ossBucket, StringUtils.join(classificationEnum.getPath(), destAliyunFolderTypeEnum.getPath(), fileName));
        return fileName;
    }

    /**
     * 拷贝文件到别的目录（源目录为根目录）
     *
     * @param classificationEnum       分类
     * @param fileName                 文件名
     * @param destAliyunFolderTypeEnum 目标目录
     * @param sourceBucket             源bucket
     * @throws IOException
     */
    public String copyObjectFromDiffBucket(AliyunClassificationEnum classificationEnum, String fileName, AliyunFolderTypeEnum destAliyunFolderTypeEnum, String sourceBucket) throws IOException {
        createFolder(classificationEnum, destAliyunFolderTypeEnum);
        ossClient.copyObject(sourceBucket, fileName, ossBucket, StringUtils.join(classificationEnum.getPath(), destAliyunFolderTypeEnum.getPath(), fileName));
        return fileName;
    }

    /**
     * 拷贝文件到别的目录（源目录为子目录）
     *
     * @param classificationEnum         分类
     * @param fileName                   文件名
     * @param destAliyunFolderTypeEnum   目标目录
     * @param sourceBucket               源bucket
     * @param sourceAliyunFolderTypeEnum 源目录
     * @throws IOException
     */
    public String copyObjectFromDiffBucket(AliyunClassificationEnum classificationEnum, String fileName, AliyunFolderTypeEnum destAliyunFolderTypeEnum, String sourceBucket, AliyunFolderTypeEnum sourceAliyunFolderTypeEnum) throws IOException {
        createFolder(classificationEnum, destAliyunFolderTypeEnum);
        ossClient.copyObject(sourceBucket, StringUtils.join(classificationEnum.getPath(), sourceAliyunFolderTypeEnum.getPath(), fileName), ossBucket, StringUtils.join(classificationEnum.getPath(), destAliyunFolderTypeEnum.getPath(), fileName));
        return fileName;
    }


    /**
     * @param classificationEnum
     * @param fileName
     * @param destBucket
     * @param destAliyunFolderTypeEnum
     * @param sourceBucket
     * @param sourceAliyunFolderTypeEnum
     * @return
     * @throws IOException
     */
    public String copyObjectFromDiffBucket(AliyunClassificationEnum classificationEnum, String fileName, String destBucket, AliyunFolderTypeEnum destAliyunFolderTypeEnum, String sourceBucket, AliyunFolderTypeEnum sourceAliyunFolderTypeEnum) throws IOException {
        createFolder(classificationEnum, destAliyunFolderTypeEnum);
        ossClient.copyObject(sourceBucket, StringUtils.join(classificationEnum.getPath(), sourceAliyunFolderTypeEnum.getPath(), fileName), destBucket, StringUtils.join(classificationEnum.getPath(), destAliyunFolderTypeEnum.getPath(), fileName));
        return fileName;
    }

    /**
     * 创建文件夹
     *
     * @param classificationEnum 分类
     * @param folderEnum         文件夹
     * @throws IOException
     */
    private void createFolder(AliyunClassificationEnum classificationEnum, AliyunFolderTypeEnum folderEnum) throws IOException {
        if (!ossClient.doesObjectExist(ossBucket, classificationEnum.getPath())) {
            /*这里的size为0,注意OSS本身没有文件夹的概念,这里创建的文件夹本质上是一个size为0的Object,dataStream仍然可以有数据*/
            ObjectMetadata objectMeta = new ObjectMetadata();
            byte[] buffer = new byte[0];
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buffer);
            objectMeta.setContentLength(0);
            try {
                ossClient.putObject(ossBucket, folderEnum.getPath(), byteArrayInputStream, objectMeta);
            } finally {
                close(byteArrayInputStream);
            }
        }
        if (!ossClient.doesObjectExist(ossBucket, classificationEnum.getPath() + folderEnum.getPath())) {
            /*这里的size为0,注意OSS本身没有文件夹的概念,这里创建的文件夹本质上是一个size为0的Object,dataStream仍然可以有数据*/
            ObjectMetadata objectMeta = new ObjectMetadata();
            byte[] buffer = new byte[0];
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buffer);
            objectMeta.setContentLength(0);
            try {
                ossClient.putObject(ossBucket, classificationEnum.getPath() + folderEnum.getPath(), byteArrayInputStream, objectMeta);
            } finally {
                close(byteArrayInputStream);
            }
        }
    }

    /**
     * 获取图片
     *
     * @param classificationEnum
     * @param fileName
     * @param folderEnum
     * @return
     * @throws IOException
     */
    public BufferedImage getImage(AliyunClassificationEnum classificationEnum, String fileName, AliyunFolderTypeEnum folderEnum) throws IOException {
        OSSObject object = null;
        try {
            object = ossClient.getObject(ossBucket, StringUtils.join(classificationEnum.getPath(), folderEnum.getPath(), fileName));
            return ImageIO.read(object.getObjectContent());
        } catch (Exception e) {
            log.error("getTokens error={}", e);
        } finally {
            close(object);
        }
        return null;
    }

    public Boolean doesImageExist(AliyunClassificationEnum classificationEnum, String fileName, AliyunFolderTypeEnum folderEnum) {
        return ossClient.doesObjectExist(ossBucket, StringUtils.join(classificationEnum.getPath(), folderEnum.getPath(), fileName));
    }


    /**
     * 获得文件夹枚举.
     *
     * @param folderName 文件名
     * @return 文件夹枚举
     * @throws Exception
     */
    public AliyunFolderTypeEnum getFolder(String folderName) {
        AliyunFolderTypeEnum folderEnum = null;
        switch (folderName) {
            case "agent":
                folderEnum = AliyunFolderTypeEnum.AGENT;
                break;
            case "token":
                folderEnum = AliyunFolderTypeEnum.TOKEN;
                break;
            case "user":
                folderEnum = AliyunFolderTypeEnum.USER;
                break;
            case "item":
                folderEnum = AliyunFolderTypeEnum.ITEM;
                break;
            case "trade":
                folderEnum = AliyunFolderTypeEnum.TRADE;
                break;
            case "promotion":
                folderEnum = AliyunFolderTypeEnum.PROMOTION;
                break;
            case "system":
                folderEnum = AliyunFolderTypeEnum.SYSTEM;
                break;
            case "goods":
                folderEnum = AliyunFolderTypeEnum.GOODS;
                break;
            case "shop":
                folderEnum = AliyunFolderTypeEnum.SHOP;
                break;
            case "share":
                folderEnum = AliyunFolderTypeEnum.SHARE;
                break;
            case "qrCode":
                folderEnum = AliyunFolderTypeEnum.QRCODE;
                break;
            case "temp":
                folderEnum = AliyunFolderTypeEnum.TEMP;
                break;
            case "test":
                folderEnum = AliyunFolderTypeEnum.TEST;
                break;
            case "consultant":
                folderEnum = AliyunFolderTypeEnum.CONSULTANT;
                break;
            case "online":
                folderEnum = AliyunFolderTypeEnum.ONLINE;
                break;
            case "commission":
                folderEnum = AliyunFolderTypeEnum.COMMISSION;
                break;
            case "ultra":
                folderEnum = AliyunFolderTypeEnum.ULTRA;
                break;
            case "applet":
                folderEnum = AliyunFolderTypeEnum.APPLET;
                break;
            case "ai":
                folderEnum = AliyunFolderTypeEnum.AI;
                break;
            case "invoice":
                folderEnum = AliyunFolderTypeEnum.INVOICE;
                break;
            default:
                throw new IllegalArgumentException("文件夹不存在！");
        }
        return folderEnum;
    }

    public String getUrl(AliyunClassificationEnum classificationEnum, AliyunFolderTypeEnum folderEnum, String fileName) {
        String enabledCDN = ossEnabledOpenCdn;
        if ("true".equals(enabledCDN)) {
            return StringUtils.join(HTTPS, ossCdnBaseUrl, "/", classificationEnum.getPath(), folderEnum.getPath(), fileName);
        } else {
            return StringUtils.join(HTTPS, ossBucket, ".", ossDomain, "/", classificationEnum.getPath(), folderEnum.getPath(), fileName);
        }
    }

    public String getUrl(String cBucket, AliyunClassificationEnum classificationEnum, AliyunFolderTypeEnum folderEnum, String fileName) {
        String enabledCDN = ossEnabledOpenCdn;
        if ("true".equals(enabledCDN)) {
            return StringUtils.join(HTTPS, ossCdnBaseUrl, "/", classificationEnum.getPath(), folderEnum.getPath(), fileName);
        } else {
            return StringUtils.join(HTTPS, cBucket, ".", ossDomain, "/", classificationEnum.getPath(), folderEnum.getPath(), fileName);
        }
    }

    public String getFileKey(AliyunClassificationEnum classificationEnum, AliyunFolderTypeEnum folderEnum, String fileName) {
        return StringUtils.join(classificationEnum.getPath(), folderEnum.getPath(), fileName);
    }

    public String getFileNameByUrl(String url) {
        String[] urls = url.split("/");
        return urls[urls.length - 1];
    }

    public String getFolderNameByUrl(String url) {
        String[] urls = url.split("/");
        return urls[urls.length - 2];
    }

    /**
     * 获取WEB直传阿里云图片的Policy
     *
     * @param dir
     * @return
     * @throws UnsupportedEncodingException
     */
    public Map<String, String> getUploadImagePolicy(String dir) throws UnsupportedEncodingException {
        long expireEndTime = System.currentTimeMillis() + TOKEN_EXPIRE_SECONDS * 1000;
        Date expiration = new Date(expireEndTime);
        PolicyConditions policyConds = new PolicyConditions();
        policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, MAX_UPLOAD_IMAGE_SIZE);
        policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dir);

        String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
        byte[] binaryData = postPolicy.getBytes("utf-8");
        String encodedPolicy = BinaryUtil.toBase64String(binaryData);
        String postSignature = ossClient.calculatePostSignature(postPolicy);

        Map<String, String> respMap = new LinkedHashMap<>();
        respMap.put("accessid", ossKey);
        respMap.put("policy", encodedPolicy);
        respMap.put("signature", postSignature);
        respMap.put("dir", dir);
        respMap.put("host", getHttpDomain());
        respMap.put("cdnHost", getCdnHost());
        respMap.put("expire", String.valueOf(expireEndTime / 1000));

        return respMap;
    }

    public String getCdnHost() {
        boolean enabledCdn = Boolean.parseBoolean(ossEnabledOpenCdn);
        String cdnHost = "";
        if (enabledCdn) {
            cdnHost = HTTPS + ossCdnBaseUrl;
        }
        return cdnHost;
    }

    /***
     * 获得上传tokens
     * @param fileName 图名
     * @return
     */
    public String getTokens(AliyunClassificationEnum classificationEnum, AliyunFolderTypeEnum folderEnum, String fileName) {
        OSSObject ossObject = null;
        String tokens = "";
        try {
            String name = StringUtils.join(classificationEnum.getPath(), folderEnum.getPath(), fileName);
            ossObject = ossClient.getObject(ossBucket, name);
            ObjectMetadata objectMetadata = ossObject.getObjectMetadata();
            Map<String, String> userMetadata = objectMetadata.getUserMetadata();
            tokens = userMetadata.get("tokens");
        } catch (Exception e) {
            log.error("getTokens error={}", e);
        } finally {
            close(ossObject);
        }
        return tokens;
    }

    /**
     * 流关闭
     */
    private static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                log.error("AliyunOssUtil.close has an error", e);
            }
        }
    }


    /**
     * 上传tokens到 OSS
     *
     * @param classificationEnum 分类
     * @param folderEnum         文件夹
     * @param fileName           文件名称
     * @param inputStream        文件流
     * @return 文件名
     * @throws IOException
     */
    public void putTokens(AliyunClassificationEnum classificationEnum, AliyunFolderTypeEnum folderEnum, String fileName, InputStream inputStream, String tokens) throws IOException {
        try {
            createFolder(classificationEnum, folderEnum);
            //创建上传 Object 的 Metadata
            ObjectMetadata objectMetadata = new ObjectMetadata();
            //必须设置
            objectMetadata.setContentLength(100);
            objectMetadata.setCacheControl("no-cache");
            objectMetadata.setHeader("Pragma", "no-cache");
            objectMetadata.setContentType(getcontentType(fileName.substring(fileName.lastIndexOf('.'))));
            objectMetadata.setContentDisposition("inline;filename=" + fileName);
            Map<String, String> map = new HashMap<>();
            map.put("tokens", tokens);
            objectMetadata.setUserMetadata(map);
            //上传 Object
            ossClient.putObject(ossBucket, StringUtils.join(classificationEnum.getPath(), folderEnum.getPath(), fileName), inputStream, objectMetadata);
        } catch (Exception e) {
            log.error("putTokens error={}", e);
        } finally {
            close(inputStream);
        }
    }

    /**
     * @param dir
     * @return
     */
    public Map<String, String> getUploadMp4Policy(String dir) {
        long expireEndTime = System.currentTimeMillis() + TOKEN_EXPIRE_SECONDS * 1000;
        Date expiration = new Date(expireEndTime);
        PolicyConditions policyConds = new PolicyConditions();
        policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, MAX_UPLOAD_MP4_SIZE);
        policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dir);

        String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
        byte[] binaryData = postPolicy.getBytes(StandardCharsets.UTF_8);
        String encodedPolicy = BinaryUtil.toBase64String(binaryData);
        String postSignature = ossClient.calculatePostSignature(postPolicy);

        Map<String, String> respMap = new LinkedHashMap<>();
        respMap.put("accessid", ossKey);
        respMap.put("policy", encodedPolicy);
        respMap.put("signature", postSignature);
        respMap.put("dir", dir);
        respMap.put("host", getHttpDomain());
        respMap.put("cdnHost", getCdnHost());
        respMap.put("expire", String.valueOf(expireEndTime / 1000));
        return respMap;
    }

    /**
     * Description: 判断OSS服务文件上传时文件的contentType
     *
     * @param FilenameExtension 文件后缀
     * @return String
     */
    public static String getcontentType(String filenameExtension) {
        if (filenameExtension.equalsIgnoreCase("bmp")) {
            return "image/bmp";
        }
        if (filenameExtension.equalsIgnoreCase("gif")) {
            return "image/gif";
        }
        if (filenameExtension.equalsIgnoreCase("jpeg") ||
                filenameExtension.equalsIgnoreCase("jpg") ||
                filenameExtension.equalsIgnoreCase("png")) {
            return "image/png";
        }
        if (filenameExtension.equalsIgnoreCase("html")) {
            return "text/html";
        }
        if (filenameExtension.equalsIgnoreCase("txt")) {
            return "text/plain";
        }
        if (filenameExtension.equalsIgnoreCase("vsd")) {
            return "application/vnd.visio";
        }
        if (filenameExtension.equalsIgnoreCase("pptx") ||
                filenameExtension.equalsIgnoreCase("ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (filenameExtension.equalsIgnoreCase("docx") ||
                filenameExtension.equalsIgnoreCase("doc")) {
            return "application/msword";
        }
        if (filenameExtension.equalsIgnoreCase("xml")) {
            return "text/xml";
        }
        return "image/png";
    }

}
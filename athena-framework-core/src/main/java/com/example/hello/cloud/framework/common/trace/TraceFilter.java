package com.example.hello.cloud.framework.common.trace;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * TraceFilter
 *
 * @author tongf01
 * @date 2019/10/12 17:56
 */

@Slf4j
public class TraceFilter extends GenericFilterBean {
    private static final String NULL = "null";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        String traceId = String.valueOf(httpRequest.getHeader(TraceHttpHeader.HTTP_HEADER_TRACE_ID.getCode()));

        if (StringUtils.isNotEmpty(traceId) && !NULL.equals(traceId)) {
            TraceContext.setCurrentTrace(traceId);
        } else {
            TraceContext.getCurrentTrace();
        }
        filterChain.doFilter(httpRequest, servletResponse);
        TraceContext.clearCurrentTrace();
    }
}

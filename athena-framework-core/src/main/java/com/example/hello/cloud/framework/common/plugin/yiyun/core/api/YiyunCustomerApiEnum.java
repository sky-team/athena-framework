package com.example.hello.cloud.framework.common.plugin.yiyun.core.api;

/**
 * 客户中心api地址枚举
 *
 * @author v-linxb
 * @create 2018/5/4
 **/
public enum YiyunCustomerApiEnum implements YiyunServiceType {

    /**
     * 查询客户账号标签信息
     */
    QUERY_SPECIAL_TAG("v2/customer/tag/querySpecialTag", "查询客户账号标签信息", false),
    /**
     * 手机号查询员工信息接口
     */
    QUERY_EMPLOYEE_TAG("v2/customer/queryEmployee", "手机号查询员工信息", false),
    /**
     * 业主证件号查询业主签约交易信息接口
     */
    QUERY_TRADE_INFO("v2/customer/queryTradeInfo", "业主证件号查询业主签约交易信息接口", false),

    /**
     * 手机号查询业主信息接口
     */
    QUERY_CUST_INFO("v2/customer/queryCustInfo", "手机号查询业主信息接口", false),
    /**
     * 手机号查询员工信息接口
     */
    QUERY_EMPLOYEE_INFO("v2/customer/queryEmployee", "查询员工信息", false),
    /**
     * 手机号查询员工信息接口(批量)
     */
    QUERY_EMPLOYEE_LIST_INFO("v2/customer/queryEmployeeList", "批量查询员工信息", false)
    ;

    YiyunCustomerApiEnum(String url, String desc, boolean needTicket) {
        this.url = url;
        this.desc = desc;
        this.needTicket = needTicket;
    }

    private final static String CENTER = "customer/";
    private String url;
    private String desc;
    private boolean needTicket;

    @Override
    public String url() {
        return CENTER + this.url;
    }

    @Override
    public String desc() {
        return this.desc;
    }

    @Override
    public boolean needTicket() {
        return this.needTicket;
    }
}

package com.example.hello.cloud.framework.common.plugin.nss.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/5.
 */
@Data
public class SyncCustomerDTO implements Serializable {
    private static final long serialVersionUID = -4912941924875570169L;

    private String salesOrgCode;
    private String consultantId;
    private String age;
    private String sex;
    private String name;
    private String phoneNumber;
    private String familyNmuber;
    private String intentionLevelId;
    private String sourceWayId;
    private String certificateType;
    private String licenseNumber;
    private String contactAddress;
    private String salesHomeId;
    private String marketWayId;
    private String customerTypeId;
    private String agentType;
    /**
     * 机构经纪人所在机构id
     */
    private String agentThirdOrgId;
    /**
     * 机构经纪人所在机构名称
     */
    private String agentThirdOrgName;
    private String agentDate;
    private String agentName;
    private String agentPhone;
    private String businessLicense;
    private String legalPerson;
    private String companyName;
    private String fusionBackgroundId;
    private String sNumber;
    private String followTime;

    @ApiModelProperty(value = "户籍省份")
    private String birthplacProvince;

    @ApiModelProperty(value = "户籍城市")
    private String birthplacCity;

    @ApiModelProperty(value = "是否有买房资格(1:有 0：无)")
    private Integer isHouseQualify;

    @ApiModelProperty(value = "有买房资格时间")
    private Long houseQualifyDate;

    @ApiModelProperty(value = "第一联系人手机号")
    private String firstContactMobile;

    @ApiModelProperty(value = "第一联系人姓名")
    private String firstContactName;

    @ApiModelProperty(value = "生日")
    private String birthday;

    @ApiModelProperty(value = "是否是城市下面黑名单用户(0:否,1:是)")
    private String blackFlag;

    @Override
    public String toString() {
        return "SyncCustomerDTO{" + "salesOrgCode='" + salesOrgCode + '\'' + ", consultantId='" + consultantId + '\''
            + ", age='" + age + '\'' + ", sex='" + sex + '\'' + ", name='" + name + '\'' + ", phoneNumber='"
            + phoneNumber + '\'' + ", familyNmuber='" + familyNmuber + '\'' + ", intentionLevelId='" + intentionLevelId
            + '\'' + ", sourceWayId='" + sourceWayId + '\'' + ", certificateType='" + certificateType + '\''
            + ", licenseNumber='" + licenseNumber + '\'' + ", contactAddress='" + contactAddress + '\''
            + ", salesHomeId='" + salesHomeId + '\'' + ", marketWayId='" + marketWayId + '\'' + ", customerTypeId='"
            + customerTypeId + '\'' + ", agentType='" + agentType + '\'' + ", agentThirdOrgId='" + agentThirdOrgId + '\''
                + ", agentThirdOrgName='" + agentThirdOrgName + '\'' + ", agentDate='" + agentDate + '\''
            + ", agentName='" + agentName + '\'' + ", agentPhone='" + agentPhone + '\'' + ", businessLicense='"
            + businessLicense + '\'' + ", legalPerson='" + legalPerson + '\'' + ", companyName='" + companyName + '\''
            + ", fusionBackgroundId='" + fusionBackgroundId + '\'' + ", sNumber='" + sNumber + '\'' + ", followTime='"
            + followTime + '\'' + ", birthplacProvince='" + birthplacProvince + '\'' + ", birthplacCity='"
            + birthplacCity + '\'' + ", isHouseQualify=" + isHouseQualify + ", houseQualifyDate=" + houseQualifyDate
            + ", firstContactMobile='" + firstContactMobile + '\'' + ", firstContactName='" + firstContactName + '\''
            + ", birthday='" + birthday + '\'' + ", blackFlag='" + blackFlag + '\'' + '}';
    }
}

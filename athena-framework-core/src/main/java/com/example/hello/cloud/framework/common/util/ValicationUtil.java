package com.example.hello.cloud.framework.common.util;

import lombok.extern.slf4j.Slf4j;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class ValicationUtil {
    /**
     * 校验手机号码
     *
     * @param mobile
     * @return
     */
    public static boolean checkMobile(String mobile) {
        if (StringUtil.isNullOrEmpty(mobile)) {
            return false;
        }
        if (mobile.startsWith("+")) {
            return true;
        }
        String regex = "^\\d{11}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(mobile);
        boolean isMatch = m.matches();
        if (isMatch) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 校验邮箱
     *
     * @param email
     * @return
     */
    public static boolean checkEmail(String email) {
        if (email.length() > 50) {
            return false;
        }
        String regex = "^[a-z0-9A-Z]+[-|a-z0-9A-Z._]+@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-z]{2,}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        boolean isMatch = m.matches();
        if (isMatch) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 排序号校验 1-100
     *
     * @param orderNo
     * @return
     */
    public static boolean checkOrderNo(Integer orderNo) {
        if (orderNo.intValue() >= 1 && orderNo.intValue() <= 100) {
            return true;
        }

        return false;
    }

    /**
     * 校验权限Code 长度为：1-255
     *
     * @param permissionCode
     * @return
     */
    public static boolean checkPermissionCode(String permissionCode) {
        int len = permissionCode.length();
        if (len >= 1 && len <= 255) {
            return true;
        }

        return false;
    }

    /**
     * 校验密码
     *
     * @param pwd
     * @return
     */
    public static boolean checkPwd(String pwd) {
        String regex = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(pwd);
        boolean isMatch = m.matches();
        if (isMatch) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 校验密码
     *
     * @param pwd
     * @return
     */
    public static boolean checkPwdWithAdmin(String pwd) {
        String regex = "^(?![A-Za-z0-9]+$)(?![a-z0-9\\W]+$)(?![A-Za-z\\W]+$)(?![A-Z0-9\\W]+$)[a-zA-Z0-9\\W]{8,16}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(pwd);
        boolean isMatch = m.matches();
        if (isMatch) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 校验入参是否有效的前3后4隐号手机号码
     *
     * @param telephone 手机号码
     * @return true/false
     */
    public static boolean isHideTelephone(String telephone) {
        Pattern pattern = Pattern.compile("^1(3|4|5|6|7|8|9)[0-9]\\*{4}\\d{4}$");
        Matcher matcher = pattern.matcher(telephone);
        return matcher.matches();
    }

    public static void main(String[] args) {
        // System.out.println(checkEmail("v-yis06@example.com"));
//		System.out.println(checkMobile("a1111111111"));
        // System.out.println(checkPwd("aaa333"));
        System.out.println(isIDNumber("43072419991146073X"));
    }

    /**
     * 校验身份证
     *
     * @param IDNumber
     * @return
     */
    public static boolean isIDNumber(String IDNumber) {
        if (IDNumber == null || "".equalsIgnoreCase(IDNumber)) {
            return false;
        }
        // 定义判别用户身份证号的正则表达式（15位或者18位，最后一位可以为字母）
        String regularExpression = "(^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|" +
                "(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}$)";
        boolean matches = IDNumber.matches(regularExpression);

        //判断第18位校验值
        if (matches) {

            if (IDNumber.length() == 18) {
                try {
                    char[] charArray = IDNumber.toCharArray();
                    //前十七位加权因子
                    int[] idCardWi = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
                    //这是除以11后，可能产生的11位余数对应的验证码
                    String[] idCardY = {"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};
                    int sum = 0;
                    for (int i = 0; i < idCardWi.length; i++) {
                        int current = Integer.parseInt(String.valueOf(charArray[i]));
                        int count = current * idCardWi[i];
                        sum += count;
                    }
                    char idCardLast = charArray[17];
                    int idCardMod = sum % 11;
                    if (idCardY[idCardMod].equalsIgnoreCase(String.valueOf(idCardLast))) {
                        return true;
                    } else {
                        System.out.println("身份证最后一位:" + String.valueOf(idCardLast).toUpperCase() +
                                "错误,正确的应该是:" + idCardY[idCardMod].toUpperCase());
                        return false;
                    }

                } catch (Exception e) {
                    log.error("异常 {}", e);
                    System.out.println("异常:" + IDNumber);
                    return false;
                }
            }

        }
        return matches;
    }

}

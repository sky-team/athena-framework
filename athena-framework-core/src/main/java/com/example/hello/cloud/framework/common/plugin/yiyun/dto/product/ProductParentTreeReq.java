package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;

/**
 * @author v-zhongj11
 * @create 2018/6/14
 */
@Data
public class ProductParentTreeReq extends BaseYiyunRequestData {

    /**
     * 产品id
     */
    private Long productId;
}
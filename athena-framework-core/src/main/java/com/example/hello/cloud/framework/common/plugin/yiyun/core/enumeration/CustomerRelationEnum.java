package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 普通客户关系枚举
 *
 * @author zhoujj07
 * @create 2018/5/3
 */
public enum CustomerRelationEnum {

    /**
     * 经纪人
     */
    AGENT("00", "经纪人"),
    /**
     * 置业顾问
     */
    COUNSELOR("01", "置业顾问"),
    /**
     * 权益人
     */
    RIGHTS_HOLDER("02", "权益人"),
    /**
     * 直系父辈
     */
    DIRECT_PARENT("03", "直系父辈"),
    /**
     * 直系子辈
     */
    IMMEDIATE_CHILDREN("04", "直系子辈"),
    /**
     * 配偶
     */
    SPOUSE("05", "配偶"),
    /**
     * 同住
     */
    HOUSEMATE("06", "同住"),
    /**
     * 上级经纪人（经纪人发展经纪人）
     */
    SUPERIOR_AGENT("07", "上级经纪人"),
    /**
     * 发展经纪人(顾问发展经纪人)
     */
    DEVELOP_AGENT("08", "发展经纪人"),
    /**
     * 企业购买决策人
     */
    BUSINESS_DECISION_MAKER("101", "企业购买决策人"),;

    private String name;
    private String index;

    CustomerRelationEnum(String index, String name) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public String getIndex() {
        return index;
    }

}

package com.example.hello.cloud.framework.common.plugin.yiyun.impl;

import com.google.gson.reflect.TypeToken;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunService;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunProductApiEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunServiceType;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.ProCriteria;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.ProbaseCriteria;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.PropQueryCriteria;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.product.*;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.ProductInfoReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ProductInfoReportServiceImpl implements ProductInfoReportService {

    private final YiyunService yiyunService;

    @Autowired
    public ProductInfoReportServiceImpl(YiyunService yiyunService) {
        this.yiyunService = yiyunService;
    }

    /**
     * 根据产品属性查询中台数据
     *
     * @param reportDTO
     * @return
     */
    @Override
    public ApiResponse<ProductReportQueryResp> findByProperties(ProductReportReq reportDTO) {
        YiyunServiceType type = YiyunProductApiEnum.PRODUCT_BASE_QUERY;
        Type responseType = new TypeToken<YiyunResponse<ProductReportQueryResp>>() {
        }.getType();
        YiyunResponse<ProductReportQueryResp> yiyunResponse
                = yiyunService.action(type, reportDTO, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ProductListInfoResp findProdByProperties(String outerId, String categoryId, String... props) {
        EasProductReq req = new EasProductReq();
        req.setCategoryId(categoryId);
        List<String> reqProps = new ArrayList<>();
        Arrays.stream(props).forEach(prop -> reqProps.add(prop));
        req.setReturnPubProps(reqProps);
        PropQueryCriteria queryCriteria = new PropQueryCriteria();
        List<ProbaseCriteria> andCriteria = new ArrayList<>();
        ProbaseCriteria criteria = new ProbaseCriteria();
        criteria.setFieldCode("outerId");
        criteria.setValueType("string");
        criteria.setValue(outerId);
        andCriteria.add(criteria);
        ProCriteria proCriteria = new ProCriteria();
        proCriteria.setAndCriteria(andCriteria);
        queryCriteria.setCriteria(proCriteria);
        req.setQueryCriteria(queryCriteria);

        YiyunServiceType type = YiyunProductApiEnum.PRODUCT_BASE_QUERY;
        Type responseType = new TypeToken<YiyunResponse<ProductListInfoResp>>() {
        }.getType();
        YiyunResponse<ProductListInfoResp> yiyunResponse
                = yiyunService.action(type, req, responseType);
        if (yiyunResponse.isSuccess()) {
            return yiyunResponse.getData();
        } else {
            return null;
        }
    }

    /**
     * 查询产品映射关系
     *
     * @param reportDTO
     * @return
     */
    @Override
    public ApiResponse<ProductMappingQueryResp> findProductMapping(ProductReportReq reportDTO) {
        YiyunServiceType type = YiyunProductApiEnum.PRODUCT_MAPPING_QUERY;
        Type responseType = new TypeToken<YiyunResponse<ProductMappingQueryResp>>() {
        }.getType();
        YiyunResponse<ProductMappingQueryResp> yiyunResponse
                = yiyunService.action(type, reportDTO, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    /**
     * 创建产品映射信息
     *
     * @param mappingReq
     * @return
     */
    @Override
    public ApiResponse<ProductMappingReq> saveMappingInfo(ProductMappingReq mappingReq) {
        YiyunServiceType type = YiyunProductApiEnum.PRODUCT_MAPPING_CREATE;
        Type responseType = new TypeToken<YiyunResponse<ProductMappingReq>>() {
        }.getType();
        YiyunResponse<ProductMappingReq> yiyunResponse
                = yiyunService.action(type, mappingReq, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    /**
     * 上报产品信息
     *
     * @param reportDTO
     * @return
     */
    @Override
    public ApiResponse<ProductReportResp> saveInfo(ProductReportReq reportDTO) {
        YiyunServiceType type = YiyunProductApiEnum.PRODUCT_CREATE;
        Type responseType = new TypeToken<YiyunResponse<ProductReportResp>>() {
        }.getType();
        YiyunResponse<ProductReportResp> yiyunResponse
                = yiyunService.action(type, reportDTO, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    /**
     * 修改产品上报信息
     *
     * @param reportDTO
     * @return
     */
    @Override
    public ApiResponse<ProductReportResp> updateInfo(ProductReportReq reportDTO) {
        YiyunServiceType type = YiyunProductApiEnum.PRODUCT_BASE_UPDATE;
        Type responseType = new TypeToken<YiyunResponse<ProductReportResp>>() {
        }.getType();
        YiyunResponse<ProductReportResp> yiyunResponse
                = yiyunService.action(type, reportDTO, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ApiResponse<ProductParentTreeResp> queryParentTree(ProductParentTreeReq reportDTO) {
        YiyunProductApiEnum type = YiyunProductApiEnum.PRODUCT_PARENT_TREE;
        Type responseType = new TypeToken<YiyunResponse<ProductParentTreeResp>>() {
        }.getType();
        YiyunResponse<ProductParentTreeResp> yiyunResponse = yiyunService.action(type, reportDTO, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ApiResponse<ProductBasicInfoResp> query(ProductBasicInfoReq reportDTO) {
        YiyunProductApiEnum type = YiyunProductApiEnum.PRODUCT_BASIC_QUERY;
        Type responseType = new TypeToken<YiyunResponse<ProductBasicInfoResp>>() {
        }.getType();
        YiyunResponse<ProductBasicInfoResp> yiyunResponse = yiyunService.action(type, reportDTO, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

}
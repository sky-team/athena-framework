package com.example.hello.cloud.framework.common.util.json;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * @author zhoujj07
 * @ClassName: GsonUtil
 * @Description: gson帮助类
 * @date 2018/8/22
 */
@Slf4j
public class GsonUtil {
    private static final Gson GSON = new Gson();

    private static final Gson GSON_SERIALIZE_NULLS = new GsonBuilder()
            .serializeNulls()
            .create();

    /**
     * 将 Java 对象转为 JSON 字符串，含值为null的对象属性
     */
    public static String toJson(Object obj) {
        return toJson(obj, true);
    }


    /**
     * 将 Java 对象转为 JSON 字符串
     *
     * @param obj            需要序列化的对象
     * @param serializeNulls 是否序列化null字段
     * @return json
     */
    public static String toJson(Object obj, boolean serializeNulls) {
        String jsonStr;
        try {
            if (serializeNulls) {
                jsonStr = GSON_SERIALIZE_NULLS.toJson(obj);
            } else {
                jsonStr = GSON.toJson(obj);
            }
            return jsonStr;
        } catch (Exception e) {
            log.error("Java 转 JSON 出错！", e);
            return null;
        }
    }

    /**
     * 将 JSON 字符串转为 Java 对象
     */
    public static <T> T fromJson(String json, Class<T> clazz) {
        T obj;
        try {
            obj = GSON.fromJson(json, clazz);
            return obj;
        } catch (Exception e) {
            log.error("JSON 转 Java对象 出错！", e);
            return null;
        }
    }

    /**
     * 将 JSON 字符串转为 Java 对象
     */
    public static <T> T fromJson(String json, Type typeOfT) {
        T obj;
        try {
            obj = GSON.fromJson(json, typeOfT);
            return obj;
        } catch (Exception e) {
            log.error("JSON 转 Java对象 出错！", e);
            return null;
        }
    }

    public static <T> ArrayList<T> jsonToList(String json, Class<T> classOfT) {
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        try {
            Type type = new TypeToken<ArrayList<JsonObject>>() {
            }.getType();
            ArrayList<JsonObject> jsonObjList = GSON.fromJson(json, type);
            ArrayList<T> listOfT = new ArrayList<>();
            for (JsonObject jsonObj : jsonObjList) {
                listOfT.add(GSON.fromJson(jsonObj, classOfT));
            }
            return listOfT;
        } catch (Exception e) {
            log.error("JSON 转 Java对象 出错！", e);
            return null;
        }
    }

}

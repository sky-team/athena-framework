package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 证件类型枚举
 * 1  身份证 
 * 2  军官证 
 * 3  护照  
 * 4  户口簿  
 * 5  台湾通行证 
 * 6  台湾身份证
*  7  港澳身份证
*  8  营业执照
*  9  组织机构代码
*  10  税务登记证号
*  11  其他
 * @author zhoujj07
 * @create 2018/5/3
 */
public enum CertificateTypeEnum {

    /**
     * 身份证
     */
    SFZ(1, "身份证"),
    /**
     * 军官证
     */
    JGZ(2, "军官证"),
    /**
     * 护照
     */
    HZ(3, "护照"),
    /**
     * 户口簿
     */
    HKB(4, "户口簿"),
    /**
     * 台湾通行证
     */
    TWTXZ(5, "台湾通行证"),
    /**
     * 台湾身份证
     */
    TWSFZ(6, "台湾身份证"),
    /**
     * 港澳身份证
     */
    GASFZ(7, "港澳身份证"),
    /**
     * 营业执照
     */
    YYZZ(8, "营业执照"),
    /**
     * 组织机构代码
     */
    ZZJGDM(9, "组织机构代码"),
    /**
     * 税务登记证号
     */
    SWDJZH(10, "税务登记证号"),
    /**
     * 其他
     */
    OTHER(11, "其他");

    private String name;
    private Integer index;

    CertificateTypeEnum(Integer index, String name) {
        this.name = name;
        this.index = index;
    }

    public static String getName(Integer index) {
        for (CertificateTypeEnum c : CertificateTypeEnum.values()) {
            if (c.getIndex().equals(index)) {
                return c.getName();
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public Integer getIndex() {
        return index;
    }

}

package com.example.hello.cloud.framework.common.plugin.yiyun.dto.customer;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.CustomerIdentification;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * 查询客户账号标签信息请求响应
 *
 * @author zhoujj07
 * @create 2018/5/7
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class QuerySpecialTagResData extends CustomerIdentification implements YiyunResponseData {

    private static final long serialVersionUID = -7998038091998383441L;

    /**
     * 是否包含的tag清单
     */
    private Map<String, Boolean> tagResultMap;

}

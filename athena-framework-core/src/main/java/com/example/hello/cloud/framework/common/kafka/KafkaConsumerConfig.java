//package com.example.hello.cloud.framework.common.kafka;
//
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.common.serialization.StringDeserializer;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.annotation.EnableKafka;
//import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
//import org.springframework.kafka.config.KafkaListenerContainerFactory;
//import org.springframework.kafka.core.ConsumerFactory;
//import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
//import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
//
//import lombok.extern.slf4j.Slf4j;
//
///**  
// * 
// * @Title:  KafkaConsumerConfig.java   
// * @Package com.example.hello.cloud.framework.common.kafka
// * @Description:    描述   
// * @author: yuanxm01     
// * @date:   2019年11月20日 下午4:02:09   
// * @version V1.0 
// * @Copyright: 
// */
//@Configuration
//@EnableKafka
//@Slf4j
//public class KafkaConsumerConfig
//{
//	@Value("${kafka.consumer.servers:null}")
//    private String servers;
//	@Value("${kafka.consumer.enable.auto.commit:true}")
//    private boolean enableAutoCommit;
//	@Value("${kafka.consumer.session.timeout:60000}")
//    private String sessionTimeout;
//	@Value("${kafka.consumer.request.timeout:30000}")
//    private String requestTimeout;
//	@Value("${kafka.consumer.heartbeat.interval:3000}")
//    private String heartbeat;
//	@Value("${kafka.consumer.auto.commit.interval:1000}")
//    private String autoCommitInterval;
//	@Value("${kafka.consumer.auto.offset.reset:latest}")
//    private String autoOffsetReset;
//	@Value("${kafka.consumer.concurrency:1}")
//    private int concurrency;
//	@Value("${kafka.consumer.maxPollRecords:500}")
//    private int maxPollRecords;
//    
//    @Bean
//    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() 
//    {
//        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<String, String>();
//        factory.setConsumerFactory(consumerFactory());
//        factory.setConcurrency(concurrency);
//        factory.setAutoStartup(false);
//        factory.getContainerProperties().setPollTimeout(1500);
//        return factory;
//    }
//
//    public ConsumerFactory<String, String> consumerFactory()
//    {
//        return new DefaultKafkaConsumerFactory<String, String>(consumerConfigs());
//    }
//
//    public Map<String, Object> consumerConfigs() 
//    {
//    	log.info("server------------------   "+servers);
//        Map<String, Object> propsMap = new HashMap<String, Object>();
//        propsMap.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
//        propsMap.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, requestTimeout);
//        propsMap.put(ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG, heartbeat);
//        propsMap.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, enableAutoCommit);
//        propsMap.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, autoCommitInterval);
//        propsMap.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, sessionTimeout);
//        propsMap.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        propsMap.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        propsMap.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
//        return propsMap;
//    }
//    
//    @Bean
//    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> batchKafkaListenerContainerFactory() 
//    {
//        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<String, String>();
//        factory.setConsumerFactory(batchFactory());
//        factory.setConcurrency(concurrency);
//        factory.setBatchListener(true);
//        factory.setAutoStartup(false);
//        factory.getContainerProperties().setPollTimeout(1500);
//        return factory;
//    }
//
//    public ConsumerFactory<String, String> batchFactory()
//    {
//        return new DefaultKafkaConsumerFactory<String, String>(batchConfigs());
//    }
//
//    public Map<String, Object> batchConfigs() 
//    {
//        Map<String, Object> propsMap = new HashMap<String, Object>();
//        propsMap.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
//        propsMap.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, requestTimeout);
//        propsMap.put(ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG, heartbeat);
//        propsMap.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, enableAutoCommit);
//        propsMap.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, autoCommitInterval);
//        propsMap.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, sessionTimeout);
//        propsMap.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        propsMap.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        propsMap.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
//        propsMap.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxPollRecords);
//        return propsMap;
//    }
//}

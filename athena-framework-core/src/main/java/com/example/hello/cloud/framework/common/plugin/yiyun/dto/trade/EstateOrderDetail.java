package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author v-zhongj11
 * @create 2018/6/1
 */
@Data
public class EstateOrderDetail implements Serializable{
    /**
     * 推广项目code
     */
    private String projCode;
    /**
     * 客户当前所在分期code，
     */
    private String oriProjCode;
    /**
     * 客户当前所在分期名称
     */
    private String oriProjName;
    /**
     * 客户号码
     */
    private String phoneNumber;
    /**
     * 客户姓名，
     */
    private String name;
    /**
     * 线索id
     */
    private String leadID;
    /**
     * 顾问姓名(暂计划不提供)
     */
    private String consultantName;
    /**
     * 顾问id
     */
    private String consultantId;
    /**
     * 顾问号码
     */
    private String consultantPhone;
    /**
     * 代理公司名称(暂计划不提供)
     */
    private String consultantCompanyName;
    /**
     * 代理公司id
     */
    private String consultantCompanyID;
    /**
     * 交易id
     */
    private String tradeId;
    /**
     * 房间id
     */
    private String houseGUID;
    /**
     * 交易类型  5 认筹 6 认购 7 签约 8 回款
     */
    private String transType;
    /**
     * 订单类型 退房、换房、撤销
     */
    private String orderType;
    /**
     * 交易是否关闭 0 未关闭 1 已关闭
     */
    private String closeState;
    /**
     * 交易时间
     */
    private String transTime;
    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 金额
     */
    private String amount;
    /**
     * 装修款
     */
    private String decorationTotal;
    /**
     * 是否并入装修款 0 未并入 1 已并入
     */
    private String isMerge;
    /**
     * 交易dealId
     */
    private String dealId;
    private String unionId;
    private String openId;
    /**
     * 成交人
     */
    private List<CustomerInfo> dealInfo;
    /**
     *
     private String //暂计划不提供 "groupID": " 城市花园一期", //客户成交房源分期ID  可通过productId反查
     private String //暂计划不提供 "groupName": " 城市花园一期", //客户成交房源分期名称  可通过productId反查
     private String //暂计划不提供 "buildingID": " 09C201D6-7DE5-4069-B867-1A8D9A55E5FC", //客户成交房源楼栋id  可通过productId反查
     private String //暂计划不提供 "buildingName": "1#", //客户成交房源楼栋名称  可通过productId反查
     private String //暂计划不提供 "houseClass": "商铺", //房间业态  可通过productId反查
     private String //暂计划不提供 "houseName": "1001", //房间名称 可通过productId反查
     private String //暂计划不提供 "houseArea": "100", //房间面积  可通过productId反查
     private String //暂计划不提供 "houseType": "三室两厅", //户型  可通过productId反查

     */
}
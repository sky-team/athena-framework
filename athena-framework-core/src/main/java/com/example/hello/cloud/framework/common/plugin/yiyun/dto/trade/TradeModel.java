package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import lombok.Data;

import java.io.Serializable;

/**
 * 翼云交易消息
 *
 * @author zhoujj07
 * @create 2018/4/26
 */
@Data
public class TradeModel implements Serializable {

	private static final long serialVersionUID = 2241749012160998552L;

	private String orderNo;

}

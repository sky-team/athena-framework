package com.example.hello.cloud.framework.common.plugin.wechat;

import com.alibaba.fastjson.JSON;
import com.example.hello.cloud.framework.common.constant.RedisKey;
import com.example.hello.cloud.framework.common.exception.ArgumentException;
import com.example.hello.cloud.framework.common.exception.BusinessException;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.Actioninfo;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.JsonPuth;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.Scene;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.exception.ArgumentException;
import com.example.hello.cloud.framework.common.exception.BusinessException;
import com.example.hello.cloud.framework.common.http.client.HttpUtil;


import com.example.hello.cloud.framework.common.plugin.wechat.vo.Actioninfo;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.JsonPuth;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.Scene;
import com.example.hello.cloud.framework.common.util.HTTPConnector;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

import static com.example.hello.cloud.framework.common.constant.RedisKey.System.WeChat.accessToken;
import static com.example.hello.cloud.framework.common.plugin.wechat.WeChatConstString.*;


/**
 * User: ${v-zhongj11}
 * Date: 2018-10-03
 */
@Service
@Slf4j
public class WeChatTokenServiceImpl implements WeChatTokenService{
//
    //总号appId
    @Value("${weChat.zxjAppId}")
    private String appId;
    //总号 appSecret
    @Value("${weChat.zxjAppSecret}")
    private String appSercet;
//
//    private final String infoUrl = "https://api.weixin.qq.com/cgi-bin/user/info";
//    private final String weChatUrl = "https://api.weixin.qq.com/cgi-bin/qrcode/create";
//    private final String tokenUrl = "https://api.weixin.qq.com/cgi-bin/token";

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public ApiResponse<String> getUnionid(String openId) {
        if (StringUtils.isEmpty(openId)) {
            return ApiResponse.error("入参不正确！");
        }
        try {
            String accessToken = this.initAccessToken();
            String url = String.format(USER_INFO_URL,accessToken,openId);
            String infoJSONStr = HttpUtil.doGet(url, null);
            if (StringUtils.isEmpty(infoJSONStr)) {
                log.error("获取unionid失败,原因:" + infoJSONStr);
                //微信接口调用失败会返回空字符串
                return ApiResponse.error("获取unionid失败，请检查appId和appSecret是否正确！");
            } else {
                JsonObject tokenJsonObject = new JsonParser().parse(infoJSONStr).getAsJsonObject();
                if (tokenJsonObject.get("errcode") != null) {
                    log.error("调用微信接口获取unionid异常,原因:" + infoJSONStr);
                    return ApiResponse.error(infoJSONStr);
                }
                log.info("获取unionid成功,unionid:" + tokenJsonObject.get("unionid").getAsString());
                return ApiResponse.success(tokenJsonObject.get("unionid").getAsString());
            }
        } catch (Exception e) {
            log.error("获取unionid失败,原因 {}" + e);
            return ApiResponse.error(e.getMessage());
        }
    }

    @Override
    public ApiResponse<String> weChatUrl() {

        Scene scene = new Scene();
        scene.setScene_id(121);

        Actioninfo actioninfo = new Actioninfo();
        actioninfo.setScene(scene);

        JsonPuth jsonPuth = new JsonPuth();
        jsonPuth.setExpire_seconds(604800);
        jsonPuth.setAction_name("QR_SCENE");
        jsonPuth.setAction_info(actioninfo);

        String jsonString = JSON.toJSONString(jsonPuth);
        try {
            String accessToken = initAccessToken();
            String url = String.format(QRCODE_CREATE_URL,accessToken);
            String doPost = HTTPConnector.sendJsonPost(url, jsonString);
            if (StringUtils.isEmpty(doPost)) {
                log.error("获取ticet失败,原因:" + doPost);
                //微信接口调用失败会返回空字符串
                return ApiResponse.error("获取ticet失败，请检查appId和appSecret是否正确！");
            } else {
                JsonObject tokenJsonObject = new JsonParser().parse(doPost).getAsJsonObject();
                if (tokenJsonObject.get("errcode") != null && "40001".equals(tokenJsonObject.get("errcode").getAsString())) {
                    log.error("accessToken无效，重试获取,原因:" + doPost);
                    accessToken = getAccessToken(appId, appSercet);
                    url = String.format(QRCODE_CREATE_URL,accessToken);
                    doPost = HTTPConnector.sendJsonPost(url, jsonString);
                    tokenJsonObject = new JsonParser().parse(doPost).getAsJsonObject();
                }
                if (tokenJsonObject.get("errcode") != null) {
                    log.error("调用微信接口获取ticket异常,原因:" + doPost);
                    return ApiResponse.error(doPost);
                }
                log.info("获取ticket成功,ticket:" + tokenJsonObject.get("ticket").getAsString());
                String ticket = tokenJsonObject.get("ticket").getAsString();
                return ApiResponse.success(ticket);
            }
        } catch (Exception e) {
            log.error("调用微信接口获取ticket异常,原因 {}" ,e);
            return ApiResponse.error("调用微信接口获取ticket异常");
        }
    }

    /**
     * 初始化token
     *
     * @return
     * @Autor cuichenyu
     */
    private String initAccessToken() {
        String token;
        //校验token
        token = stringRedisTemplate.opsForValue().get(StringUtils.join(RedisKey.System.WeChat.accessToken,appId + appSercet));
        if (!StringUtils.isEmpty(token)) {
            return token;
        } else {
            //调用微信接口获取token
            token = getAccessToken(appId, appSercet);
            if (StringUtils.isEmpty(token)) {
                throw new BusinessException("获取token为空");
            }
            log.info("调用接口获取token成功:" + token);
            return token;
        }
    }

    /**
     * 获取access_token
     *
     * @param appId
     * @param appSecret
     * @return
     */
    private String getAccessToken(String appId, String appSecret) {
        if (StringUtils.isEmpty(appId) || StringUtils.isEmpty(appSecret)) {
            throw new ArgumentException("入参不正确！");
        }
        String url = String.format(APPLET_TOKEN_URL,appId,appSecret);
        String tokenJSONStr = HttpUtil.doGet(url, null);
        if (StringUtils.isEmpty(tokenJSONStr)) {
            log.error("获取token失败,原因:" + tokenJSONStr);
            //微信接口调用失败会返回空字符串
            throw new BusinessException("获取token失败，请检查appId和appSecret是否正确！");
        } else {
            JsonObject tokenJsonObject = new JsonParser().parse(tokenJSONStr).getAsJsonObject();
            if (tokenJsonObject.get("errcode") != null) {
                log.error("调用微信接口获取token异常,原因:" + tokenJSONStr);
                throw new BusinessException(tokenJSONStr);
            }
            //写入redis
            log.info("获取token成功,token:" + tokenJsonObject.get("access_token").getAsString());
            stringRedisTemplate.opsForValue().set(StringUtils.join(RedisKey.System.WeChat.accessToken, appId + appSecret), tokenJsonObject.get("access_token").getAsString(), 7200, TimeUnit.SECONDS);
            return tokenJsonObject.get("access_token").getAsString();
        }
    }

    @Override
    public ApiResponse getAccessToken() {
    	 return ApiResponse.success(initAccessToken());
    }
}

package com.example.hello.cloud.framework.common.plugin.yiyun.core;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品中心键值对
 *
 * @author v-linxb
 * @create 2018/5/15
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class YiyunProductPropertyInfo extends YiyunPropertyInfo {

}

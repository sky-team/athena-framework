package com.example.hello.cloud.framework.common.plugin.yiyun.inf;

import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.product.*;

/**
 * @author :v-zhousq04
 * @description: 产品上报服务类
 * @create date: 2018/8/24 15:20
 */
public interface ProductInfoReportService {

    /**
     * 根据产品属性查询中台数据
     *
     * @param reportDTO
     * @return
     */
    ApiResponse<ProductReportQueryResp> findByProperties(ProductReportReq reportDTO);


    ProductListInfoResp findProdByProperties(String outerId, String categoryId, String... props);


    /**
     * 查询产品映射关系
     *
     * @param reportDTO
     * @return
     */
    ApiResponse<ProductMappingQueryResp> findProductMapping(ProductReportReq reportDTO);

    /**
     * 创建产品映射信息
     *
     * @param mappingReq
     * @return
     */
    ApiResponse<ProductMappingReq> saveMappingInfo(ProductMappingReq mappingReq);

    /**
     * 上报产品信息
     *
     * @param reportDTO
     * @return
     */
    ApiResponse<ProductReportResp> saveInfo(ProductReportReq reportDTO);

    /**
     * 修改产品上报信息
     *
     * @param reportDTO
     * @return
     */
    ApiResponse<ProductReportResp> updateInfo(ProductReportReq reportDTO);

    /**
     * 查询产品及父产品属性信息树
     *
     * @param req
     * @return
     */
    ApiResponse<ProductParentTreeResp> queryParentTree(ProductParentTreeReq req);

    /**
     * 查询产品详细信息
     */
    ApiResponse<ProductBasicInfoResp> query(ProductBasicInfoReq req);

}

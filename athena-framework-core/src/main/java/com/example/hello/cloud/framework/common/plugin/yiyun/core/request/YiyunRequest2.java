package com.example.hello.cloud.framework.common.plugin.yiyun.core.request;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.Constants;
import com.example.hello.cloud.framework.common.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Calendar;
import java.util.TimeZone;

@Data
public class YiyunRequest2 implements Serializable {

    private String appId;

    private String ticket;

    private String v = "2.0";

    private String bizParam;

    private String domain = "hello";

    private String userDomain = "helloBiz";

    private String reqId = "sSpFcLXzRGtgaDWP";

    private final String timestamp = DateUtil.formatDateTime(
            Calendar.getInstance(TimeZone.getTimeZone("GMT+8:00")).getTime(),
            "yyyy-MM-dd HH:mm:ss");

    private final String signMethod = Constants.SIGN_METHOD_MD5;

    private String tenantId;
}

package com.example.hello.cloud.framework.common.constant;

/**
 * @author guohg03
 * @ClassName: RedisKey
 * @Description: Redis key的定义
 * @date 2018年10月9日
 */
public class RedisKey {

	public class System {

		public class Login {
			private Login() {
			}
			
			/**
			 * 微信登陆的ticket
			 */
			public static final String weChatLogin = "string:system:login:wechat:ticket:";

			/**
			 * 密码已过期未重置
			 */
			public static final String expireMobile = "string:system:expirePwd:mobile:";
		}

		public class WeChat {
			private WeChat() {
			}
			
			/**
			 * 经纪人永久二维码token，接口调用凭证
			 */
			public static final String accessToken = "string:system:wechat:accessToken:";

		}

		/**
		 * 系统角色权限hash key
		 */
		public static final String ROLE_PERMISSION_HASH_KEY = "hello:hash:rolePermissionHashKey";

		public class Holiday {
			/**
			 * 所有节假日
			 */
			public static final String ALL_HOLIDAY = "list:system:holiday:allHoliday";
		}

		public static final String ENV_NAME = "string:systen:envName:";

	}

	public class Login {

		/**
		 * 存储登录信息，需补充：userType:accountId
		 */
		public static final String LOGIN_CACHE_KEY = "hello:string:loginCacheKey:%s:%s";

		/**
		 * 存储登录的小程序信息，需补充：userType:accountId
		 */
		public static final String LOGIN_APPLET_SESSION_CACHE_KEY = "hello:string:appletSessionCacheKey:%s:%s";
		/**
		 * 后台登录唯一码
		 */
		public static final String ADMIN_LOGIN_CODE = "admin:login:code:";
		/**
		 * 后台登录的在线sessionId
		 */
		public static final String ADMIN_LOGIN_SESSION = "admin:login:session:";
		/**
		 * 极速秀登录验证用户信息
		 */
		public static final String ISHOW_LOGIN_CODE = "ishow:login:code:";
		public static final String ISHOW_LOGIN_TIMEOUT = "ishow:login:timeout:";
	}

	public class Plugin {

		public class Geetest {

			/**
			 * 极验服务器状态
			 */
			public static final String GT_SERVER_STATUS = "string:sso:plugin:geetest:status";

			/**
			 * 极验的用户id
			 */
			public static final String GT_USERID = "string:sso:plugin:geetest:userId";

		}

	}

	public class Wechat {
		/**
		 * 微信公众号的的access_token
		 */
		public static final String WECHAT_ACCESS_TOKEN = "string:sys:wechat:accessToken:";
	}

	public class Product {
		/**
		 * 审核通过的产品列表
		 */
		public static final String productList = "string:product:list:showCityCode:";
		/**
		 * 审核通过的产品详情
		 */
		public static final String productDetail = "string:product:detail:proCode:";

		/**
		 * 户型详情
		 */
		public static final String roomDetail = "string:room:detail:id";

		/**
		 * 户型详情列表
		 */
		public static final String roomDetailList = "string:room:detail:list:proCode:";

		/**
		 * 户型交互标准
		 */
		public static final String standard = "string:standard:handleStdCode:";

		/**
		 * 微楼书
		 */
		public static final String buildingBookJson = "string:buildingBookJson:proCode:";
		/**
		 * 推荐数
		 */
		public static final String recommend = "string:recommend:proCode:";
		/**
		 * 围观数
		 */
		public static final String watch = "string:watch:proCode:";
		/**
		 * 浏览数
		 */
		public static final String browse = "string:browse:proCode:";
		/**
		 * 转发数
		 */
		public static final String forward = "string:forward:proCode:";

		public static final String chatRule = "string:chatRule:proCode:";
	}

	public class Agent {

		/**
		 * 新增经纪人信息，锁定前缀（按手机号锁定）
		 */
		public static final String CREATE_AGENT_LOCK_PREFIX = "string:agent:createAgent:";
		public static final String TODAY_POP_REMINDER = "string:agent:agentInfo:todayPopReminder:";
		public static final String CREATE_CERTIFICATION_LOCK_PREFIX = "string:agent:certification:";
		public static final String DEVELOPMENT_OFFLINE = "string:agent:developmentOffline:";
		public static final String UPDATE_AGENTTYPE = "string:agent:updateAgentType:";
		public static final String CHECK_OWNER_RELATIVES = "string:agent:checkOwnerRelatives:";
		public static final String ADD_OWNER_RELATIVES = "string:agent:addOwnerRelatives:";

		/**
		 * 添加经纪人业主信息
		 */
		public static final String ADD_OWNER = "string:agent:owner:add:";
		/**
		 * 删除经纪人业主信息
		 */
		public static final String DEL_OWNER = "string:agent:owner:del:";

		/**
		 * 分享家APP h5热更新的包路径
		 */
		public static final String APP_H5_URL = "string:agent:appH5Url:%s";
		/**
		 * 签约单自动注册经纪人
		 */
		public static final String ADD_YZ_AGENT_INFO = "string:agent:addYzAgentInfo:%s";

		/**
		 * 添加经纪人帽子关系
		 */
		public static final String ADD_AGT_HAT = "string:agent:addAgtHat:";

		/**
		 * 保存经纪人万客会会员关系
		 */
		public static final String SAVE_AGT_WKH_RELATION = "string:agent:wkh:relation:save:";

		/**
		 * 保存经纪人万客会会员等级房产
		 */
		public static final String SAVE_AGT_WKH_LEVEL = "string:agent:wkh:level:save:";

		public class Clue {
			/**
			 * 置为有效/无效操作日志
			 */
			public static final String VALID_INFO = "string:agent:clue:valid:%s";
			/**
			 * 存储保持的最大score,以便下次进行追加
			 */
			public static final String VALID_MAX_SCORE = "string:agent:clue:valid:%s";

		}

		public class rules {
			public static final String delayRule = "string:agent:delayRule:estateRuleRelease:estateId:";// 楼盘延迟推荐key
			public static final String recommendRule = "string:agent:recommendRule:estateRuleRelease:estateId:";// 楼盘推荐规则key
			public static final String filterRule = "string:agent:filterRule:estateRuleRelease:estateId:";// 楼盘过滤规则key
			public static final String commissionRule = "string:agent:commissionRule:estateRuleRelease:estateId:";// 楼盘延迟推荐key
			public static final String setComonyLook = "string:agent:filterRule:agentComonyId:estateId:";// 楼盘是否有设置机构部分可见
		}

		public class SalesSystem {
			public static final String getSalesSystemTokenUrl = "string:Agent:SalesSystem:salesSystemToken";
			public static final String syncSalesDataTime = "string:Agent:SalesSystem:syncSalesDataTime";
		}

		public class Applet {
			/**
			 * 小程序banner图redis缓存key
			 */
			public static final String BANNER_LIST = "string:agent:applet:banner:cityId:%s";
		}

		public class Qrcode {
			/**
			 * 新增团队二维码
			 */
			public static final String TEAM_QRCODE_ADD = "string:agent:team:qrcode:add:";
		}

	}

	public class Commission {

		/**
		 * 生成K2审批单访问URL
		 */
		public static final String GENERATE_K2_URL = "string:commission:generateK2Url:";

		/**
		 * K2审批单创建结果回调
		 */
		public static final String K2_CREATE_RESULT = "string:commission:k2CreateResult:";

		/**
		 * K2审批单关闭回调
		 */
		public static final String K2_CLOSE = "string:commission:k2Close:";

		/**
		 * K2审批单审批回调
		 */
		public static final String K2_AUDIT = "string:commission:k2Audit:";

		/**
		 * 增值税发票
		 */
		public static final String DISTRIBUTED_LOCK = "string:item:thematic:distributedLock:";

		/**
		 * 用户钱包余额更新锁
		 */
		public static final String INDIVIDUAL_BALANCE_LOCK_KEY = "string:commission:individualBalanceLockKey:";

		/**
		 * 总账户余额更新锁
		 */
		public static final String ACCOUNT_BALANCE_LOCK_KEY = "string:commission:accountBalanceLockKey:";

		/**
		 * 城市佣金授信额度更新锁相关
		 */
		public static final String CREDIT_LINE_BALANCE_LOCK_KEY = "string:commission:creditLineBalanceLock:";

		/**
		 * 单号
		 */
		public static final String APPLY_NO = "string:commission:applyNo:";

		/**
		 * 佣金发放申请：同意
		 */
		public static final String APPLY_AGREE = "string:commission:applyAgree:";

		/**
		 * 佣金发放 :垫付申请
		 */
		public static final String APPLY_ADVANCE = "string:commission:applyAdvance:";

		/**
		 * 佣金发放 :普通发放申请
		 */
		public static final String APPLY_PROVIDE = "string:commission:applyProvide:";
		/**
		 * 还款申请
		 */
		public static final String REPAYMENT_PROVIDE = "string:commission:repaymentProvide:";

		/**
		 * 佣金发放申请：驳回
		 */
		public static final String APPLY_REJECT = "string:commission:applyReject:";

		/**
		 * 佣金还款申请：同意
		 */
		public static final String RETURN_AGREE = "string:commission:returnAgree:";

		/**
		 * 佣金还款申请：驳回
		 */
		public static final String RETURN_REJECT = "string:commission:returnReject:";

		/**
		 * 活动奖励发放申请：同意
		 */
		public static final String ACTIVITY_APPLY_AGREE = "string:commission:activityApplyAgree:";

		/**
		 * 活动奖励发放申请：驳回
		 */
		public static final String ACTIVITY_APPLY_REJECT = "string:commission:activityApplyReject:";

		/**
		 * 活动奖励还款申请：同意
		 */
		public static final String ACTIVITY_RETURN_AGREE = "string:commission:activityReturnAgree:";

		/**
		 * 活动奖励还款申请：驳回
		 */
		public static final String ACTIVITY_RETURN_REJECT = "string:commission:activityReturnReject:";

		/**
		 * 快钱提现操作
		 */
		public static final String BILL99_WITHDRAWAL = "string:commission:bill99:withdrawal";

		/**
		 * 快钱提现结果查询操作
		 */
		public static final String BILL99_QUERY = "string:commission:bill99:query";

		/**
		 * 用户申请提现
		 */
		public static final String INDIVIDUAL_WITHDRAWAL = "string:commission:individual:withdrawal:";

		/**
		 * 用户提现次数（每年每月），用户计算是否扣取提现手续费
		 */
		public static final String WITHDRAWAL_COUNT = "string:commission:withdrawalCount:%s:%s:%s";

		/**
		 * 申请生成发票锁
		 */
		public static final String getInvoiceApplyNo = "string:Agent:Commission:invoiceApplyNo";

		/**
		 * 自动驳回申请锁
		 */
		public static final String AUTO_REJECT_KEY = "string:commission:autoReject";

		/**
		 * 生成佣金单锁
		 */
		public static final String GENERATE_COMMISSION = "string:commission:generateCommission:";
	}

	public class Toker {

		/**
		 * 新增编辑拓客用户
		 */
		public static final String SAVE_TOKER_USER = "string:toker:user:save:";

		/**
		 * 新增编辑签到详情
		 */
		public static final String SAVE_TOKER_SIGN = "string:toker:sign:save:";

		/**
		 * 延迟判客后更新toker日志
		 */
		public static final String UPDATE_JUDGE_RESULT = "string:toker:judge:save:";

		/**
		 * 小蜜蜂手动添加客户
		 */
		public static final String BEE_ADD_CUSTOMER = "string:toker:customer:add:";

		/**
		 * 生成签到二维码
		 */
		public static final String GENERATE_SIGNIN_QR_CODE = "string:toker:task:signin:";

		/**
		 * 生成拓客二维码
		 */
		public static final String GENERATE_TOKER_QR_CODE = "string:toker:task:toker:";
	}

	public class Trade {
		/**
		 * 系统生成佣金
		 */
		public static final String CALC_COMMISSION = "string:trade:calc:commission:";
	}

	public class Shop {
		/**
		 * 添加线下活动
		 */
		public static final String ACTIVITY_ADD = "string:shop:offlineActivity:add:";

		/**
		 * 线下活动扫码
		 */
		public static final String ACTIVITY_SCAN = "string:shop:offlineActivity:scan:";
	}

	public class Customer {
		/**
		 * 交易历史数据批量处理
		 */
		public static final String TRADE_HISTORY = "string:customer:tradeServiceImpl:handelTradeCore";
	}

	public class InvoiceMessage {
		public static final String invoiceMessage = "string:invoicemessage:phone:";
	}

	public class Ai {
		private Ai() {
		}
		
		/**
		 * 验证token
		 */
		public static final String TOEKN = "string:ai:Token:";
		public static final String REDIS_KEY_AI_PESON_IMAGE_RECEIVE = "string:ai:peson:image:receive";
	}

	public class Activity {
		private Activity() {
		}
		
		/**
		 * 优惠券最近领取记录
		 */
		public static final String COUPON_RECEIVE_RECENT = "string:activity:coupon:receive:recent:%s:%s";
		/**
		 * 砍价信息
		 */
		public static final String BARGAIN_INFO = "string:activity:bargain:%s:%s";
		/**
		 * 砍价信息详细
		 */
		public static final String BARGAIN_DETAIL_INFO = "string:activity:bargain:detail:%s";
		/**
		 * 砍价成功列表
		 */
		public static final String BARGAIN_SUCCESS_LIST = "list:activity:bargain:success:%s";
		/**
		 * 砍价活动限流
		 */
		public static final String BARGAIN_LIMIT = "string:activity:bargain:limit:%s";
	}

	public class ShopDoDraw {
		private ShopDoDraw() {
		}
		
		/**
		 * 积分抽奖-每日抽奖次数
		 */
		public static final String SHOP_DO_DRAW_ID = "string:shopdodraw:dodraw:dodrawid";
		/**
		 * 积分抽奖-每天次数
		 */
		public static final String SHOP_DO_DRAW_EVERYDAY = SHOP_DO_DRAW_ID + ":everyday:";
		/**
		 * 积分抽奖-个人总计
		 */
		public static final String SHOP_DO_DRAW_COUNT = SHOP_DO_DRAW_ID + ":count:";
		/**
		 * 查看奖品区间
		 */
		public static final String SHOP_DO_DRAW_SECTION = "string:shopdodraw:dodraw:section:";
		/**
		 * 查看抽奖信息
		 */
		public static final String SHOP_DO_DRAW = "string:shopdodraw:dodraw:city:";
	}

	public class ProjectStatiscs {
		private ProjectStatiscs() {
		}
		
		/**
		 * proCode 当日
		 */
		public static final String PROJECT_STATISTICS_DAY = "string:project:statistics:day:proCode:";
		/**
		 * proCode 本周
		 */
		public static final String PROJECT_STATISTICS_WEEK = "string:project:statistics:week:proCode:";

		/**
		 * proCode 本月
		 */
		public static final String PROJECT_STATISTICS_MONTH = "string:project:statistics:month:proCode:";

		/**
		 * proCode 本年
		 */
		public static final String PROJECT_STATISTICS_YEAR = "string:project:statistics:year:proCode:";

		/**
		 * 城市 天
		 */
		public static final String PROJECT_STATISTICS_CITY_DAY = "string:project:statistics:day:city:";

		/**
		 * 城市 本周
		 */
		public static final String PROJECT_STATISTICS_CITY_WEEK = "string:project:statistics:week:city";
		/**
		 * 城市 本月
		 */
		public static final String PROJECT_STATISTICS_CITY_MONTH = "string:project:statistics:month:city:";
		/**
		 * 本年
		 */
		public static final String PROJECT_STATISTICS_CITY_YEAR = "string:project:statistics:month:year:";
	}

	public class Marketing {
		private Marketing() {
		}
		
		public static final String MARKETING_COUPON_CARD = "string:marketing:coupon:couponCard:_";

		/**
		 * mkt_activity_vote表中进行中的投票活动
		 */
		public static final String MARKETING_VOTE = "list:marketing:vote";

		/**
		 * 每天0点重置投票上限的投票记录
		 */
		public static final String MARKETING_VOTE_LOG_RESET = "list:marketing:vote:log:reset:%s";

		/**
		 * 不重置投票上限的投票记录
		 */
		public static final String MARKETING_VOTE_LOG_NORESET = "list:marketing:vote:log:noreset:%s";

		/**
		 * 待持久化到数据库的已投票记录
		 */
		public static final String MARKETING_VOTE_DATA = "list:marketing:vote:data";

		/**
		 * 持久化操作进行时的redis锁
		 */
		public static final String PERSISTENCE_VOTE_DATA = "string:marketing:vote:persistence:data";

		/**
		 * 同步MARKETING_VOTE、MARKETING_VOTE_LOG_RESET、MARKETING_VOTE_LOG_NORESET
		 * 三个redis时的redis锁
		 */
		public static final String INIT_VOTE_DATA = "string:marketing:vote:init:data";

		public static final String COUPON_INFO = "coupon:info:%s";

		public static final String COUPON_CAN_RECEIVE_NUM = "coupo:num:can:%s";
	}

	public class RyMessage {
		private RyMessage() {
		}
		
		public static final String RY_EXPIRE_MESSAGE = "string:ry:msg:";

		public static final String RY_EXPIRE_MESSAGE_DATA = "string:ry:msg:data";

		public static final String USER_FROM_ID = "string:byUser:formId";
	}

	public class Mall {
		private Mall() {
		}

		public static final String PAYMENT_APPLY_SIGN = "hello:mall:payment:paymentApplySign:";
	}

	public class Reservation {
		private Reservation() {
		}

		public static final String RESERVATION_IP_MESSAGE_COUNT = "reservation:project:ip";
	}

	public class Report {
		private Report() {
		}

		public static final String HOT_PROJECT_RANKING = "string:report:ranking:hot:project";
	}

	public class RpCusAnalyseStatistics {
		private RpCusAnalyseStatistics() {
		}

		/**
		 * 项目 天
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_PROJECT_DAY = "string:rp:cus:analyse:statistics:project:day:";
		/**
		 * 项目 本周
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_PROJECT_WEEK = "string:rp:cus:analyse:statistics:project:week:";
		/**
		 * 项目 本月
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_PROJECT_MONTH = "string:rp:cus:analyse:statistics:project:month:";
		/**
		 * 项目 季
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_PROJECT_QUARTER = "string:rp:cus:analyse:statistics:project:quarter:";
		/**
		 * 项目 本年
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_PROJECT_YEAR = "string:rp:cus:analyse:statistics:project:year:";
		/**
		 * 项目 总览
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_PROJECT_ALL = "string:rp:cus:analyse:statistics:project:all:";

		/**
		 * 城市 天
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_CITY_DAY = "string:rp:cus:analyse:statistics:city:day:";
		/**
		 * 城市 本周
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_CITY_WEEK = "string:rp:cus:analyse:statistics:city:week:";
		/**
		 * 城市 本月
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_CITY_MONTH = "string:rp:cus:analyse:statistics:city:month:";
		/**
		 * 城市 季
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_CITY_QUARTER = "string:rp:cus:analyse:statistics:city:quarter:";
		/**
		 * 城市 本年
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_CITY_YEAR = "string:rp:cus:analyse:statistics:city:year:";
		/**
		 * 城市 总览
		 */
		public static final String RP_CUS_ANALYSE_STATISTICS_CITY_ALL = "string:rp:cus:analyse:statistics:city:all:";
	}

	public class RpZYSQBusinessBoard {
		private RpZYSQBusinessBoard() {
		}

		/**
		 * 项目 天
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_PROJECT_DAY = "string:rp:zysq:business:board:project:day:";
		/**
		 * 项目 本周
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_PROJECT_WEEK = "string:rp:zysq:business:board:project:week:";
		/**
		 * 项目 本月
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_PROJECT_MONTH = "string:rp:zysq:business:board:project:month:";
		/**
		 * 项目 季
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_PROJECT_QUARTER = "string:rp:zysq:business:board:project:quarter:";
		/**
		 * 项目 本年
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_PROJECT_YEAR = "string:rp:zysq:business:board:project:year:";
		/**
		 * 项目 总览
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_PROJECT_ALL = "string:rp:zysq:business:board:project:all:";

		/**
		 * 城市 天
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_CITY_DAY = "string:rp:zysq:business:board:city:day:";
		/**
		 * 城市 本周
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_CITY_WEEK = "string:rp:zysq:business:board:city:week:";
		/**
		 * 城市 本月
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_CITY_MONTH = "string:rp:zysq:business:board:city:month:";
		/**
		 * 城市 季
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_CITY_QUARTER = "string:rp:zysq:business:board:city:quarter:";
		/**
		 * 城市 本年
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_CITY_YEAR = "string:rp:zysq:business:board:city:year:";
		/**
		 * 城市 总览
		 */
		public static final String RP_ZYSQ_BUSINESS_BOARD_CITY_ALL = "string:rp:zysq:business:board:city:all:";
	}

	public class CusCityPublic {
		private CusCityPublic() {
		}

		public static final String APPLY_REDIS_KEY = "list:detail:cus:public";
		public static final String APPLY_COUNT_PRO_WEEK = "string:apply:pro:count";
	}

	public class ExportProgress {
		private ExportProgress() {
		}

		public static final String EXPORT_PROGRESS = "string:ex:progress:";
	}

	public class HiddenPhoneReport {
		private HiddenPhoneReport() {

		}

		public static final String DISTRIBUTION = "string:hidden:dtb:";
	}

	public class Overdue {
		private Overdue() {
		}

		/**
		 * 逾期未执行的key
		 */
		public static final String OVER_DUE_FAIL_KEY = "over:due:fail:";
	}

	/**
	 * 销售e
	 */
	public class SalesY {
		private SalesY() {
		}

		/**
		 * 推荐关系id
		 */
		public static final String RELATION_ID = "relation:id:";
	}

	/**
	 * 唯家看板缓存
	 */
	public class RpVigasStatistics {
		private RpVigasStatistics() {
		}

		/**
		 * 项目 天
		 */
		public static final String RP_VIGAS_STATISTICS = "string:rp:vigas:statistics:";
		public static final String RP_VIGAS_STATISTICS_PRODUCT_LIST = "string:rp:vigas:statistics:product:list:";
		public static final String RP_VIGAS_CST_ANALYZE = "string:rp:vigas:statistics:cst:analyze";
		public static final String RP_VIGAS_CST_RANK = "string:rp:vigas:statistics:cst:rank";

	}

	public class RpDataBusiness {
		private RpDataBusiness() {
		}

		public static final String RP_CUS_ANALYZE = ":analyze:";
		public static final String RP_CUS_TREND = ":custrend:";
		public static final String RP_CITY_TREND = ":citytrend:";
	}

	public class SysAdmRegion {
		private SysAdmRegion() {
		}

		public static final String ADM_REGION_LIST = "sys:a:r:list:";
	}

	public class Banner {
		private Banner() {
		}

		public static final String BANNER_LIST = "mkt:banner:list:";
	}

	public class SysArea {
		private SysArea() {
		}

		public static final String SYS_AREA_LIST = "sys:area:list:";
	}
	
	public class TempZbStatistics {
		private TempZbStatistics() {
		}
	
		public static final String TEMPZBSTATISTICS_LIST = "tza:list:";
	}
	
	public class CusDynamic {
		private CusDynamic() {
		}
	
		public static final String CUSDYNAMIC_LIST = "cus:dynamic:list";
	}
	
	public class UserInfoReport {
		private UserInfoReport() {
		}
	
		public static final String USERINFO_REPORT_LIST = "userinfo:report:list";
	}
}
package com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment.wxpay;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 退款支付入参
 *
 * @author mall
 */
@Data
public class RefundRequest extends BaseYiyunRequestData implements Serializable {

    @ApiModelProperty("商户号")
    private String merchantNo;

    @ApiModelProperty("原支付订单号（中台生成）")
    private String paymentOrderNo;

    @ApiModelProperty("退款订单号")
    private String refundNo;

    @ApiModelProperty("退款金额")
    private BigDecimal amount;


}
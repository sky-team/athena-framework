package com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment.wxpay;

import com.alibaba.fastjson.JSONObject;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 微信支付响应体
 *
 * @author mall
 */
@Data
public class ApplyPayWxResponse implements YiyunResponseData {

    @ApiModelProperty("交易订单号")
    private String paymentOrderNo;

    @ApiModelProperty("支付订单号（中台生成）")
    private String tradeOrderNo;

    @ApiModelProperty("唤醒公众号支付的信息")
    /**
     "appPayInfo" : {
     "appId": "wxf784d28e5aab3b2a",
     "nonceStr": "8808eda0dd3dec4e4df50499f2fc75e8",
     "package": "prepay_id=wx20170216160925ff9d7f0eba0312669250",
     "paySign": "5E752793D49CFDA3E1714C4F0B913C51",
     "signType": "MD5",
     "timeStamp": "1487232619"
     }
     * */
    private JSONObject appPayInfo;

}
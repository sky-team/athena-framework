package com.example.hello.cloud.framework.common.plugin.yiyun.core.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhuf04 on 2018/6/7.
 */
@Data
public class ProCriteria implements Serializable {
    /**
     * 用等号连接的查询条件
     */
    private List<ProbaseCriteria> andCriteria;
    /**
     * 用or连接
     */
    private List<ProbaseCriteria> notCriteria;
    /**
     * Not条件
     */
    private List<ProbaseCriteria> orCriteria;
}

package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by v-zhongj11 on 2017/11/8.
 */
@Data
public class JsonPuth implements Serializable {
    private long expire_seconds;

    private String action_name;

    private Actioninfo action_info;
}

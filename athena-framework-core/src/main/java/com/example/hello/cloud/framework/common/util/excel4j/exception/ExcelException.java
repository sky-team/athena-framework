package com.example.hello.cloud.framework.common.util.excel4j.exception;

/**
 * excel操作异常.
 *
 * @author will
 */
public class ExcelException extends RuntimeException {
    public ExcelException(String msg) {
        super(msg);
    }
}

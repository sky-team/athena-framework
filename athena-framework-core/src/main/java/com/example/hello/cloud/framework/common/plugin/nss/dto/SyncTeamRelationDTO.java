package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author v-yangym10
 * @date 2019/9/21
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SyncTeamRelationDTO implements Serializable {

    /**
     * 销售系统团队id
     */
    private String nssTeamId;

    /**
     * 知客团队id
     */
    private String teamId;

    /** 以及团队id */
    private String nssParentTeamId;

    /** 项目编码 */
    private String proCode;
}

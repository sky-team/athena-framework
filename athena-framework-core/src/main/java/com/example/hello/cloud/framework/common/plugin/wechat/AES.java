package com.example.hello.cloud.framework.common.plugin.wechat;

import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidParameterSpecException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.alibaba.fastjson.JSON;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.AppletUserVO;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.AppletUserVO;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by v-hut12 on 2018/9/28.
 */
@Slf4j
public class AES {
	private AES() {
	}
	
    public static boolean initialized = false;

    /**
     * AES解密
     *
     * @param content 密文
     * @return
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchProviderException
     */
    private static byte[] decrypt(byte[] content, byte[] keyByte, byte[] ivByte) throws InvalidAlgorithmParameterException {
        initialize();
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            Key sKeySpec = new SecretKeySpec(keyByte, "AES");
            // 初始化
            cipher.init(Cipher.DECRYPT_MODE, sKeySpec, generateIV(ivByte));
            return cipher.doFinal(content);
        } catch (Exception e) {
            log.error("解密信息报错{}" + e.getMessage(),e);
            return null;
        }
    }

    public static void initialize() {
        if (initialized) {
            return;
        }
        Security.addProvider(new BouncyCastleProvider());
        initialized = true;
    }

    //生成iv
    public static AlgorithmParameters generateIV(byte[] iv) {
        AlgorithmParameters params = null;
        try {
            params = AlgorithmParameters.getInstance("AES");
        } catch (NoSuchAlgorithmException e) {
            log.error("生成Iv报错{}"+e);
        }
        try {
            if(params!=null) {
                params.init(new IvParameterSpec(iv));
            }
        } catch (InvalidParameterSpecException e) {
            log.error("生成Iv报错{}"+e);
        }
        return params;
    }

    public static AppletUserVO getUserInfo(String encryptedData, String sessionKey, String iv) {
        try {

            byte[] resultByte = decrypt(Base64.decodeBase64(encryptedData), Base64.decodeBase64(sessionKey), Base64.decodeBase64(iv));
            if (null != resultByte && resultByte.length > 0) {
                String userInfoJson = new String(resultByte, "UTF-8");
                AppletUserVO userInfo = JSON.parseObject(userInfoJson, AppletUserVO.class);
                log.warn("用户基本信息密文：解密后长度" + resultByte.length);
                return userInfo;
            }else{
                log.warn("用户基本信息密文：解密后长度0");
            }
        } catch (Exception e) {
            log.error("解密微信用户信息报错{}" + e);
        }
        return null;
    }
}
package com.example.hello.cloud.framework.common.plugin.yiyun.core.api;

/**
 * 交易中心api地址枚举
 *
 * @author v-linxb
 * @create 2018/5/4
 **/
public enum YiyunTradeApiEnum implements YiyunServiceType {
    /**
     * 交易单据查询接口
     */
    QUERY_ORDER("v1/trade/queryOrder", "交易单据查询接口", false),

    /**
     * 即售地产交易单据查询接口
     */
    QUERY_ESTATE_ORDER("v1/trade/queryEstateOrder", "即售地产交易单据查询接口", false),

    /**
     * 交易单据变关联数据查询接口
     */
    ORDER_DATA("v1/trade/orderData", "交易单据变关联数据查询接口", false),

    /**
     * 交易单据重新推送接口
     */
    ORDER_MQ_REISSUE("v1/trade/order/mq/reissue", "交易单据重新推送接口", false),

    /**
     * 查询电子发票
     */
    ORDER_INVOICE("v1/trade/orderInvoice", "查询电子发票", false),
    ;

    YiyunTradeApiEnum(String url, String desc, boolean needTicket) {
        this.url = url;
        this.desc = desc;
        this.needTicket = needTicket;
    }

    private final static String API_CENTER = "trade/";
    private String url;
    private String desc;
    private boolean needTicket;

    @Override
    public String url() {
        return API_CENTER + this.url;
    }

    @Override
    public String desc() {
        return this.desc;
    }

    @Override
    public boolean needTicket() {
        return this.needTicket;
    }

}
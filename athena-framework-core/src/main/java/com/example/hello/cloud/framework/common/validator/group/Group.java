package com.example.hello.cloud.framework.common.validator.group;

import javax.validation.GroupSequence;

/**
 * 定义校验顺序，如果AddGroup组失败，则UpdateGroup组不会再校验
 *
 * @author zhanj04
 * @date 2017/8/1 9:29
 */
@GroupSequence({AddGroup.class, UpdateGroup.class})
public interface Group {

}

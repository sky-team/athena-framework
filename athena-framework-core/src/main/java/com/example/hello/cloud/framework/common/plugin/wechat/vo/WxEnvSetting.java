package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author lium44
 * @ClassName: WxEnvSetting
 * @Description: 微信配置参数
 * @date 2018年10月01日
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WxEnvSetting implements Serializable {
    /**
     * 微信appId
     */
    private String appId;
    /**
     * 获取accessToken URL
     */
    private String accessTokenUrl;
    /**
     * 获取二维码 URL
     */
    private String qrcodeUrl;
    /**
     * 微信appSecret
     */
    private String appSecret;

    /**
     * 重试次数
     */
    private Integer retries;
    
    /**方形码接口*/
    private String qrcodeUrlSquare;

}

package com.example.hello.cloud.framework.common.enums;

import com.example.hello.cloud.framework.common.exception.BusinessException;
import com.example.hello.cloud.framework.common.exception.BusinessException;

/**
 * @author guohg03
 * @ClassName: DomainEnum
 * @Description: 子系统标识
 * @date 2018年7月28日
 */
public enum DomainEnum {
	/**
     * 知客外部系统
     */
    hello_EXTERNAL_SYSTEM("hello-external-system", "知客外部系统", DomainEnum.OWNER_hello, UserTypeEnum.EXTERNAL_SYSTEM),
    
    /**
     * 知客管理后台
     */
    hello_ADMIN("hello-admin", "知客管理后台", DomainEnum.OWNER_hello, UserTypeEnum.ADMINISTRATOR),

    /**
     * 在线家web站点
     */
    ZXJ_WEB("zxj-web", "在线家web站点", DomainEnum.OWNER_ZXJ, UserTypeEnum.LIFE_CUSTOMER),
    /**
     * 在线家wap站点
     */
    ZXJ_WAP("zxj-wap", "在线家wap站点", DomainEnum.OWNER_ZXJ, UserTypeEnum.LIFE_CUSTOMER),
    /**
     * 在线家商城
     */
    ZXJ_MALL("zxj-mall", "在线家商城", DomainEnum.OWNER_ZXJ, UserTypeEnum.LIFE_CUSTOMER),

    /**
     * 分享家app
     */
    FXJ_APP("fxj-app", "分享家app", DomainEnum.OWNER_FXJ, UserTypeEnum.AGENT),

    /**
     * 分享家app ios版
     */
    FXJ_APP_IOS("fxj-app-ios", "分享家app ios版", DomainEnum.OWNER_FXJ, UserTypeEnum.AGENT),
    /**
     * 分享家app安卓版
     */
    FXJ_APP_ANDROID("fxj-app-android", "分享家app安卓版", DomainEnum.OWNER_FXJ, UserTypeEnum.AGENT),
    /**
     * 分享家app h5页面
     */
    FXJ_APP_H5("fxj-app-h5", "分享家app h5页面", DomainEnum.OWNER_FXJ, UserTypeEnum.AGENT),
    /**
     * 分享家小程序
     */
    FXJ_APPLET("fxj-applet", "分享家小程序", DomainEnum.OWNER_FXJ, UserTypeEnum.AGENT),
    /**
     * 分享家企业合作平台
     */
    ORG_ADMIN("org-admin", "分享家企业合作平台", DomainEnum.OWNER_FXJ, UserTypeEnum.ORG),

    /**
     * 置业神器顾问客户端
     */
    ZYSQ_APPLET("zysq-applet", "置业神器顾问客户端", DomainEnum.OWNER_ZYSQ, UserTypeEnum.APPLET_CONSULTANT),
    /**
     * 置业神器客户签到端
     */
    ZYSQ_SIGN_APPLET("zysq-sign-applet", "置业神器客户签到端", DomainEnum.OWNER_ZYSQ, UserTypeEnum.APPLET_CONSULTANT),
    /**
     * 置业神器客户签到端（线上）
     */
    ZYSQ_SIGN_APPLET_ONLINE("zysq-sign-applet-online", "置业神器客户签到端（线上）", DomainEnum.OWNER_ZYSQ, UserTypeEnum.APPLET_CONSULTANT),
    /**
     * 置业神器-顾问客户端-预生产
     */
    ZYSQ_APPLET_ONLINE("zysq-applet-online", "置业神器-顾问客户端-预生产", DomainEnum.OWNER_ZYSQ, UserTypeEnum.APPLET_CONSULTANT),
    /**
     * 置业神器-签到应用（ipad）
     */
    ZYSQ_SIGN_IOS("zysq-sign-ios", "签到应用（ipad）", DomainEnum.OWNER_ZYSQ, UserTypeEnum.APPLET_CONSULTANT),
    /**
     * 置业神器-经纪人端
     */
    ZYSQ_APPLET_AGENT("zysq-applet-agent", "置业神器-经纪人端", DomainEnum.OWNER_ZYSQ, UserTypeEnum.APPLET_CONSULTANT),
    /**
     * 置业神器-经理端
     */
    ZYSQ_APPLET_MANAGER("zysq-applet-manager", "置业神器-经理端", DomainEnum.OWNER_ZYSQ, UserTypeEnum.APPLET_MANAGER),
    /**
     * 置业神器-聊聊端
     */
    ZYSQ_CHAT_APPLET("zysq-chat-applet", "置业神器-聊聊端", DomainEnum.OWNER_ZYSQ, UserTypeEnum.APPLET_CONSULTANT),
    /**
     * 销售家顾问app ios版
     */
    XSJ_CONSULTANT_IOS("xsj-consultant-ios", "销售家顾问app ios版", DomainEnum.OWNER_XSJ, UserTypeEnum.CONSULTANT),
    /**
     * 销售家顾问app安卓版
     */
    xsj_consultant_android("xsj-consultant-android", "销售家顾问app安卓版", DomainEnum.OWNER_XSJ, UserTypeEnum.CONSULTANT),
    /**
     * 销售家经理app ios版
     */
    xsj_manager_ios("xsj-manager-ios", "销售家经理app ios版", DomainEnum.OWNER_XSJ, UserTypeEnum.MANAGER),
    /**
     * 销售家经理app安卓版
     */
    xsj_manager_android("xsj-manager-android", "销售家经理app安卓版", DomainEnum.OWNER_XSJ, UserTypeEnum.MANAGER),

    ZYSQ_BARGAIN_APPLET("zysq-bargain-applet", "置业神器砍价活动", DomainEnum.OWNER_ZYSQ_BARGAIN, UserTypeEnum.APPLET_CUSTOMER),
	external("external", "对外模拟", DomainEnum.OWNER_EXTERNAL, UserTypeEnum.AGENT),

    ZYSQ_TIANJIN("zysq-tianjin", "置业神器天津万科", DomainEnum.OWNER_ZYSQ_TIANJIN, UserTypeEnum.APPLET_CUSTOMER),

    TOKER_APPLET("toker-applet", "拓客神器", DomainEnum.OWNER_TOKER, UserTypeEnum.APPLET_TOKER),
	
    /**
     * e选房-头条小程序
     */
    EFANG_TOUTIAO("efang-toutiao", "e选房-头条小程序", DomainEnum.OWNER_EFANG, UserTypeEnum.EFANG_TOUTIAO),
            /**
     * 易选房H5
     */
    MKT_H5("mkt-h5", "易选房H5", DomainEnum.OWNER_ZYSQ, UserTypeEnum.MKT_H5),
    
	 /**
     * 万科万小二小程序
     */
    WXE_NANFANG_APPLET("wanx2", "万科万小二小程序", DomainEnum.OWNER_WXE_NANFANG, UserTypeEnum.WXE_NANFANG_APPLET_TOKER);

    private String code;
    private String name;
    private String owner;
    private UserTypeEnum userType;

    /** 分享家 */
    public static final String OWNER_FXJ = "fxj";
    /** 销售家 */
    public static final String OWNER_XSJ = "xsj";
    /** 在线家 */
    public static final String OWNER_ZXJ = "zxj";
    /**在线商城*/
    public static final String OWNER_ONMALL="onlineMall";
    /** 置业神器 */
    public static final String OWNER_ZYSQ = "zysq";
    /** 置业神器砍价*/
    public static final String OWNER_ZYSQ_BARGAIN = "zysq_bargain";
    /** 知客 */
    public static final String OWNER_hello = "hello";
    /**对外*/
    public static final String OWNER_EXTERNAL = "external";
    public static final String OWNER_ZYSQ_TIANJIN = "zysq-tianjin";
    /** 拓客神器*/
    public static final String OWNER_TOKER = "toker";
    /** 万科万小二小程序*/
    public static final String OWNER_WXE_NANFANG = "wanx2";
    /** e选房 */
    public static final String OWNER_EFANG = "efang";



    DomainEnum(String code, String name, String owner, UserTypeEnum userType) {
        this.code = code;
        this.name = name;
        this.owner = owner;
        this.userType = userType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public UserTypeEnum getUserType() {
        return userType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static DomainEnum getDomainEnum(String domain) {
        DomainEnum domainEnum = null;
        for (DomainEnum tempEnum : DomainEnum.values()) {
            if (tempEnum.getCode().equals(domain)) {
                domainEnum = tempEnum;
            }
        }
        if (domainEnum == null) {
            throw new BusinessException("不支持的domain：" + domain);
        }
        return domainEnum;
    }

    public String getOwner() {
        return this.owner;
    }


    public static DomainEnum match(String domain) {
        for (DomainEnum domainEnum : DomainEnum.values()) {
            if (domainEnum.getCode().equals(domain)) {
                return domainEnum;
            }
        }
        return null;
    }


    private static boolean isDomain(DomainEnum domainEnum, String name) {
        if(domainEnum == null) {
            return false;
        }
        return domainEnum.getOwner().equals(name);
    }

    public static boolean isFxjDomain(DomainEnum domainEnum) {
        return isDomain(domainEnum, OWNER_FXJ);
    }

    public static boolean isXsjDomain(DomainEnum domainEnum) {
        return isDomain(domainEnum, OWNER_XSJ);
    }

    public static boolean isZysqDomain(DomainEnum domainEnum) {
        return isDomain(domainEnum, OWNER_ZYSQ);
    }
    public static boolean isWxeNanFangDomain(DomainEnum domainEnum) {
        return isDomain(domainEnum, OWNER_WXE_NANFANG);
    }

    public static boolean isZxjDomain(DomainEnum domainEnum) {
        return isDomain(domainEnum, OWNER_ZXJ);
    }
    
    public static boolean isOnlineMallDomain(DomainEnum domainEnum) {
        return isDomain(domainEnum, OWNER_ONMALL);
    }

    public static boolean ishelloDomain(DomainEnum domainEnum) {
        return isDomain(domainEnum, OWNER_hello);
    }

    public static boolean isZysqBargainDomain(DomainEnum domainEnum) {
        return isDomain(domainEnum, OWNER_ZYSQ_BARGAIN);
    }

    public static boolean isTokerDomain(DomainEnum domainEnum){
        return isDomain(domainEnum, OWNER_TOKER);
    }
}

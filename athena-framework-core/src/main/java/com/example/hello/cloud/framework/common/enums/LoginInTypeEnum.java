package com.example.hello.cloud.framework.common.enums;

/**
 * @ClassName: LoginInTypeEnum
 * @Description:登录Id类型
 * @author guohg03
 * @date 2018年10月9日
 */
public enum LoginInTypeEnum {
	/**
	 * 微信表Id
	 */
	WX("1", "微信表Id"),

	/**
	 * 账户表Id
	 */
	ACCOUNTID("2", "账户表Id"),;

	private String type;
	private String name;

	LoginInTypeEnum(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}

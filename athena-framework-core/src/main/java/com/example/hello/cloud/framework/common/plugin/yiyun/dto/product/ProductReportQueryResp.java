package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * author :v-zhousq04 description: 产品上报实体 create date: 2018/8/22 10:28
 */
@Data
@EqualsAndHashCode
public class ProductReportQueryResp implements YiyunResponseData {
    private static final long serialVersionUID = 6719690618252186098L;
    private Integer total;
    private Integer pageSize;
    private Integer pageNo;
    private Integer pageCount;
    private List<ProductReportReq> list;
}

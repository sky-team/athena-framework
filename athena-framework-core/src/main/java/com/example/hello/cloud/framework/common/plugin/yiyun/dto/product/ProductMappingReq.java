package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;

/**
 * @author :v-zhousq04
 * @description:产品映射请求参数
 * @create date: 2018/8/29 14:51
 */
@Data
public class ProductMappingReq extends BaseYiyunRequestData implements YiyunResponseData {
    private String id;
    private String productId;
    private String productId2;
    private String refType;
    private String refDetail;
    private String validDate;
    private String createPerson;
    private String updatePerson;
}

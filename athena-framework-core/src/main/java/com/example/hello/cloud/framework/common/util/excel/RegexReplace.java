package com.example.hello.cloud.framework.common.util.excel;

import lombok.extern.slf4j.Slf4j;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * RegexReplace
 * 正则匹配类
 *
 * @author yingc04
 * @create 2019/11/4
 */
@Slf4j
public abstract class RegexReplace {
    private String regex;
    private String source;
    //Pattern.CASE_INSENSITIVE|Pattern.DOTALL
    private int flags = 0;

    public RegexReplace(String source, String regex) {
        this.regex = regex;
        this.source = source;
    }

    public RegexReplace(String source, String regex, int flags) {
        this(source, regex);
        this.flags = flags;
    }

    public abstract String getReplacement(Matcher matcher);

    public String replaceAll() {
        try {
            Pattern p = Pattern.compile(regex, flags);
            Matcher m = p.matcher(source);
            StringBuffer result = new StringBuffer();
            while (m.find()) {
                String replacement = getReplacement(m);
                if (replacement != null) {
                    if (replacement.indexOf('$') >= 0) {
                        replacement = replacement.replace("$", "\\$");
                    }
                    m.appendReplacement(result, replacement);
                }
            }
            m.appendTail(result);
            return result.toString();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public String replaceFirst() {
        try {
            Pattern p = Pattern.compile(regex, flags);
            Matcher m = p.matcher(source);
            StringBuffer result = new StringBuffer();
            if (m.find()) {
                String replacement = getReplacement(m);
                if (replacement != null) {
                    if (replacement.indexOf('$') >= 0) {
                        replacement = replacement.replace("$", "\\$");
                    }
                    m.appendReplacement(result, replacement);
                }
            }
            m.appendTail(result);
            return result.toString();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }
}

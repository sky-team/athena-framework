package com.example.hello.cloud.framework.common.threadpool;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @ClassName: CustomThreadPoolFactory
 * @Description: 创建自定义的线程池工厂
 * @author guohg03
 * @date 2019年12月22日
 */
public class CustomThreadPoolFactory {

	private CustomThreadPoolFactory() {
	}
	
	/**
	 * 创建固定线程的线程池
	 */
	public static ThreadPoolTaskExecutor newFixedThreadPool(Integer nThreads) {
		return new ThreadPoolCreate().getTaskExecutor(nThreads);
	}

	public static ThreadPoolTaskExecutor newFixedThreadPool(ThreadPoolCreate.ThreadPoolProperties properties, Integer nThreads) {
		ThreadPoolTaskExecutor taskExecutor = null;
		try {
			taskExecutor = new ThreadPoolCreate(properties).getTaskExecutor(nThreads);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return taskExecutor;
	}
}

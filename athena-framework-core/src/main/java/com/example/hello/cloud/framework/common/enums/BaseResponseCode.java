package com.example.hello.cloud.framework.common.enums;

/**
 * 接口相应码的父接口
 *
 * @author zhanj04
 * @date 2019/10/22 17:18
 */
public interface BaseResponseCode {

    public Integer getCode();

    public String getMessage();

    public String getShowMessage();
}

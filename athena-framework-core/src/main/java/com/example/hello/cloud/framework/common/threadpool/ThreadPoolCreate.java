package com.example.hello.cloud.framework.common.threadpool;

import com.example.hello.cloud.framework.common.trace.KeyPropertyEnum;
import com.example.hello.cloud.framework.common.trace.TraceContext;
import org.slf4j.MDC;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.Assert;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @ClassName: ThreadpoolCreate
 * @Description: 主线程池创建
 * @author guohg03
 * @date 2019年12月22日
 */
public class ThreadPoolCreate {

    private ThreadPoolProperties properties;

    public ThreadPoolCreate() {
        properties = new ThreadPoolProperties();
        properties.setMaxPoolSize(20);
        properties.setCorePoolSize(20);
        properties.setQueueCapacity(65535);
        properties.setWaitForTasksToCompleteOnShutdown(true);
        properties.setAwaitTerminationSeconds(60);
    }

    public ThreadPoolCreate(ThreadPoolProperties properties) {
        Assert.notNull(properties, "threadPoolProperties 不能为空");
        Assert.notNull(properties.getCorePoolSize(), "CorePoolSize 不能为空");
        Assert.notNull(properties.getMaxPoolSize(), "MaxPoolSize 不能为空");
        Assert.notNull(properties.getQueueCapacity(), "QueueCapacity 不能为空");
        Assert.notNull(properties.getAwaitTerminationSeconds(), "AwaitTerminationSeconds 不能为空");
        Assert.notNull(properties.getWaitForTasksToCompleteOnShutdown(), "WaitForTasksToCompleteOnShutdown 不能为空");
        this.properties = properties;
    }
    public void setProperties(ThreadPoolProperties properties) {
        this.properties = properties;
    }
    public static class ThreadPoolProperties {
        /**默认 200*/
        private Integer corePoolSize;
        /**默认 200*/
        private Integer maxPoolSize;
        /**默认 20000*/
        private Integer queueCapacity;
        /**默认 true*/
        private Boolean waitForTasksToCompleteOnShutdown;
        /**默认 60*/
        private Integer awaitTerminationSeconds;

        public Integer getCorePoolSize() {
            return corePoolSize;
        }

        public void setCorePoolSize(Integer corePoolSize) {
            this.corePoolSize = corePoolSize;
        }

        public Integer getMaxPoolSize() {
            return maxPoolSize;
        }

        public void setMaxPoolSize(Integer maxPoolSize) {
            this.maxPoolSize = maxPoolSize;
        }

        public Integer getQueueCapacity() {
            return queueCapacity;
        }

        public void setQueueCapacity(Integer queueCapacity) {
            this.queueCapacity = queueCapacity;
        }

        public Boolean getWaitForTasksToCompleteOnShutdown() {
            return waitForTasksToCompleteOnShutdown;
        }

        public void setWaitForTasksToCompleteOnShutdown(Boolean waitForTasksToCompleteOnShutdown) {
            this.waitForTasksToCompleteOnShutdown = waitForTasksToCompleteOnShutdown;
        }

        public Integer getAwaitTerminationSeconds() {
            return awaitTerminationSeconds;
        }

        public void setAwaitTerminationSeconds(Integer awaitTerminationSeconds) {
            this.awaitTerminationSeconds = awaitTerminationSeconds;
        }
    }


    /**
	 * 对线程池进行包装，使之支持traceId透传
	 */
    public ThreadPoolTaskExecutor getTaskExecutor(Integer nThreads) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor() {
			private static final long serialVersionUID = 1L;

			@Override
            public void execute(Runnable task) {
                super.execute(wrap(task, MDC.get(KeyPropertyEnum.TRACE.getName())));
            }

            @Override
            public void execute(Runnable task, long startTimeout) {
                super.execute(wrap(task, MDC.get(KeyPropertyEnum.TRACE.getName())), startTimeout);
            }

            @Override
            public <T> Future<T> submit(Callable<T> task) {
                return super.submit(wrap(task, MDC.get(KeyPropertyEnum.TRACE.getName())));
            }

            @Override
            public Future<?> submit(Runnable task) {
                return super.submit(wrap(task, MDC.get(KeyPropertyEnum.TRACE.getName())));
            }

            @Override
            public ListenableFuture<?> submitListenable(Runnable task) {
                return super.submitListenable(wrap(task, MDC.get(KeyPropertyEnum.TRACE.getName())));
            }

            @Override
            public <T> ListenableFuture<T> submitListenable(Callable<T> task) {
                return super.submitListenable(wrap(task, MDC.get(KeyPropertyEnum.TRACE.getName())));
            }
        };
        setProperties(nThreads, executor);
        executor.initialize();
        return executor;
    }

    private void setProperties(Integer nThreads, ThreadPoolTaskExecutor executor) {
        if(null == nThreads) {
        	// 设置核心线程数
            executor.setCorePoolSize(properties.getCorePoolSize());
            // 设置最大线程数
            executor.setMaxPoolSize(properties.getMaxPoolSize());
        } else {
        	// 设置核心线程数
            executor.setCorePoolSize(nThreads);
            // 设置最大线程数
            executor.setMaxPoolSize(nThreads);
        }
        // 设置队列容量
        executor.setQueueCapacity(properties.getQueueCapacity());
        // 设置拒绝策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(properties.getWaitForTasksToCompleteOnShutdown());
        // 等待指定时间没关闭自动关闭
        executor.setAwaitTerminationSeconds(properties.getAwaitTerminationSeconds());
    }

    /**
     * 线城重新包装-带返回值
     */
    public static <T> Callable<T> wrap(final Callable<T> callable, final String traceId) {
        return () -> {
            if (traceId != null) {
                TraceContext.setCurrentTrace(traceId);
            }
            try {
                return callable.call();
            } finally {
                TraceContext.clearCurrentTrace();
            }
        };
    }


    /**
     * 线城重新包装
     */
    public static Runnable wrap(final Runnable runnable, final String traceId) {
        return () -> {
            if (traceId != null) {
                TraceContext.setCurrentTrace(traceId);
            }
            try {
                runnable.run();
            } finally {
                TraceContext.clearCurrentTrace();
            }
        };
    }
}

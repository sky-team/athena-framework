package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/9.
 */
@Data
public class SyncConsultantDTO implements Serializable {

    private  String salesOrgCode;
    private  String nssParentSalesTeamId;
    private  String nssParentSalesTeamName;
    private  String nssSalesTeamId;
    private  String nssSalesTeamName;
    private  String jobId;
    private  String jobName;
    private  String salesSysId;
    private  String salesSysexampleid;
    private  String salesSysPhone;
    private  String salesSysEmail;
    private  String salesSysName;
    private  String userType;
    private  String proCode;
    private  String agentCode;
    private  String fenqiCode;
    private  String sNumber;

    @Override
    public String toString() {
        return "SyncConsultantDTO{" +
                "salesOrgCode='" + salesOrgCode + '\'' +
                ", nssParentSalesTeamId='" + nssParentSalesTeamId + '\'' +
                ", nssParentSalesTeamName='" + nssParentSalesTeamName + '\'' +
                ", nssSalesTeamId='" + nssSalesTeamId + '\'' +
                ", nssSalesTeamName='" + nssSalesTeamName + '\'' +
                ", jobId='" + jobId + '\'' +
                ", jobName='" + jobName + '\'' +
                ", salesSysId='" + salesSysId + '\'' +
                ", salesSysexampleid='" + salesSysexampleid + '\'' +
                ", salesSysPhone='" + salesSysPhone + '\'' +
                ", salesSysEmail='" + salesSysEmail + '\'' +
                ", salesSysName='" + salesSysName + '\'' +
                ", userType='" + userType + '\'' +
                ", proCode='" + proCode + '\'' +
                ", agentCode='" + agentCode + '\'' +
                ", fenqiCode='" + fenqiCode + '\'' +
                ", sNumber='" + sNumber + '\'' +
                '}';
    }
}

package com.example.hello.cloud.framework.common.util;

import cn.hutool.core.collection.CollectionUtil;
import com.google.common.base.Joiner;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author guohg03
 * @ClassName: SensitiveInfoUtils
 * @Description: 敏感信息隐藏处理
 * @date 2018年11月19日
 */
public class SensitiveInfoUtils {
    private SensitiveInfoUtils() {
    }

    /**
     * [手机号码] 前三位，后四位，其他隐藏<例子:138******1234>
     *
     * @param num
     * @return
     */
    public static String mobilePhone(String num) {
        if (StringUtils.isBlank(num)) {
            return "";
        }
        return StringUtils.left(num, 3).concat(StringUtils
                .removeStart(StringUtils.leftPad(StringUtils.right(num, 4), StringUtils.length(num), "*"), "***"));
    }

    /**
     * [手机号码] 前三位，后四位，其他隐藏<例子:138******1234>(包含联名)
     *
     * @param num
     * @return
     */
    public static String mobilePhoneArr(String num) {
        if (StringUtils.isBlank(num)) {
            return "";
        }
        String[] numArr = num.split("，");
        return dealMultiMobile(numArr);
    }

    /**
     * [手机号码] 前三位，后四位，其他隐藏<例子:138******1234>(包含联名)
     *
     * @param num   手机号
     * @param split 分割符
     * @return
     */
    public static String mobilePhoneBySplit(String num, String split) {
        if (StringUtils.isBlank(num)) {
            return "";
        }
        String[] numArr = num.split(split);
        return dealMultiMobile(numArr);
    }

    /**
     * [固定电话] 后四位，其他隐藏<例子：****1234>
     */
    public static String fixedPhone(final String num) {
        if (StringUtils.isBlank(num)) {
            return "";
        }
        return StringUtils.leftPad(StringUtils.right(num, 4), StringUtils.length(num), "*");
    }

    /**
     * [身份证号 银行卡] 显示最后四位，其他隐藏。共计18位或者15位。<例子：*************5762>
     *
     * @param id
     * @return
     */
    public static String idCardNum(String id) {
        if (StringUtils.isBlank(id)) {
            return "";
        }
        String num = StringUtils.right(id, 4);
        return StringUtils.leftPad(num, StringUtils.length(id), "*");

    }

    /**
     * [身份证号 银行卡] 显示前二后四，其他隐藏。共计18位或者15位。<例子：*************5762>
     *
     * @param idcard
     * @return
     */
    public static String idCardNumPreTwoSufFour(String idcard) {
        if (StringUtils.isBlank(idcard)) {
            return "";
        }
        return StringUtils.left(idcard, 2).concat(StringUtils
                .removeStart(StringUtils.leftPad(StringUtils.right(idcard, 4), StringUtils.length(idcard), "*"), "**"));
    }

    /**
     * [银行卡号] 前六位，后四位，其他用星号隐藏每位1个星号<例子:6222600**********1234>
     *
     * @param cardNum
     * @return
     */
    public static String bankCard(String cardNum) {
        if (StringUtils.isBlank(cardNum)) {
            return "";
        }
        return StringUtils.left(cardNum, 6).concat(StringUtils.removeStart(
                StringUtils.leftPad(StringUtils.right(cardNum, 4), StringUtils.length(cardNum), "*"), "******"));
    }

    /**
     * [公司开户银行联号] 公司开户银行联行号,显示前两位，其他用星号隐藏，每位1个星号<例子:12********>
     *
     * @param code
     * @return
     */
    public static String cnapsCode(String code) {
        if (StringUtils.isBlank(code)) {
            return "";
        }
        return StringUtils.rightPad(StringUtils.left(code, 2), StringUtils.length(code), "*");
    }

    public static String mobilePhoneList(String phoneList) {
        List<String> strings = Arrays.asList(phoneList.split(","));
        ArrayList<String> nowList = new ArrayList<>();
        for (String string : strings) {
            String s = mobilePhone(string);
            nowList.add(s);
        }
        if (CollectionUtil.isNotEmpty(nowList)) {
            return Joiner.on(",").join(nowList);
        }
        return "";
    }

    /**
     * [地址] 只显示到地区，不显示详细地址；我们要对个人信息增强保护<例子：北京市海淀区****>
     *
     * @param sensitiveSize 敏感信息长度
     */
    public static String address(final String address, final int sensitiveSize) {
        if (StringUtils.isBlank(address)) {
            return "";
        }
        final int length = StringUtils.length(address);
        return StringUtils.rightPad(StringUtils.left(address, length - sensitiveSize), length, "*");
    }

    /**
     * [电子邮箱] 邮箱前缀仅显示第一个字母，前缀其他隐藏，用星号代替，@及后面的地址显示<例子:g**@163.com>
     */
    public static String email(final String email) {
        if (StringUtils.isBlank(email)) {
            return "";
        }
        final int index = StringUtils.indexOf(email, "@");
        if (index <= 1) {
            return email;
        } else {
            return StringUtils.rightPad(StringUtils.left(email, 1), index, "*")
                    .concat(StringUtils.mid(email, index, StringUtils.length(email)));
        }
    }

    /**
     * [中文姓名] 只显示第一个汉字，其他隐藏为2个星号<例子：李**>
     */
    public static String chineseName(final String fullName) {
        if (StringUtils.isBlank(fullName)) {
            return "";
        }
        final String name = StringUtils.left(fullName, 1);
        return StringUtils.rightPad(name, StringUtils.length(fullName), "*");
    }

    /**
     * [中文姓名] 只显示第一个汉字，其他隐藏为2个星号<例子：李**>
     */
    public static String chineseName(final String familyName, final String givenName) {
        if (StringUtils.isBlank(familyName) || StringUtils.isBlank(givenName)) {
            return "";
        }
        return chineseName(familyName + givenName);
    }

    /**
     * 多个手机号码处理
     */
    private static String dealMultiMobile(String[] numArr) {
        StringBuilder buffer = new StringBuilder();
        for (String phone : numArr) {
            buffer.append(StringUtils.left(phone, 3).concat(StringUtils
                    .removeStart(StringUtils.leftPad(StringUtils.right(phone, 4), StringUtils.length(phone), "*"), "***")));
            buffer.append(",");
        }
        return buffer.toString().substring(0, buffer.length() - 1);
    }
}
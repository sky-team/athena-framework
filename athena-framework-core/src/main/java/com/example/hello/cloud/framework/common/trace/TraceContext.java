package com.example.hello.cloud.framework.common.trace;

import org.slf4j.MDC;
import org.springframework.core.NamedThreadLocal;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * TraceContext
 *
 * @author tongf01
 * @date 2019/10/12 17:56
 */
@Slf4j
public final class TraceContext {

    private static final ThreadLocal<Trace> CURRENT_SPAN = new NamedThreadLocal("Trace Context");

    /**
     * Get current Trace info
     *
     * @return
     */

    public static Trace getCurrentTrace() {
        Trace trace = CURRENT_SPAN.get();
        if (trace == null) {
            trace = new Trace();
            String traceId = getRandomTraceId();
            String spanId = getRandomSpanId();
            trace.setTraceId(traceId);
            trace.setSpanId(spanId);
            MDC.put(KeyPropertyEnum.TRACE.getName(), trace.getTraceId());
            MDC.put(KeyPropertyEnum.PARENT_SPAN.getName(), trace.getSpanId());
            CURRENT_SPAN.set(trace);
            return trace;
        }
        return trace;
    }

    /**
     * Setting trace by parent' traceId
     *
     * @param traceId
     */

    public static void setCurrentTrace(String traceId) {
        if (StringUtils.isBlank(traceId) || traceId.equals("null")) {
            // log.warn("TraceContext setting trace info but traceId is empty - {}", traceId);
            traceId = getRandomTraceId();
        }
        Trace trace = new Trace();
        trace.setTraceId(traceId);
        trace.setSpanId(getRandomSpanId());
        MDC.put(KeyPropertyEnum.TRACE.getName(), trace.getTraceId());
        MDC.put(KeyPropertyEnum.PARENT_SPAN.getName(), trace.getSpanId());
        CURRENT_SPAN.set(trace);
    }

    /**
     * Generate a new random id;
     *
     * @return
     */
    private static String getRandomTraceId() {
        return RandomUtil.randomString(16);
    }

    /**
     * Generate a new random id;
     *
     * @return
     */

    @org.apache.skywalking.apm.toolkit.trace.Trace
    private static String getRandomSpanId() {
        String spanId = RandomUtil.randomString(16);
        String skyTraceId = org.apache.skywalking.apm.toolkit.trace.TraceContext.traceId();
        if (StrUtil.isNotBlank(skyTraceId) && !"[Ignored Trace]".equals(skyTraceId)) {
            spanId = skyTraceId;
        }
        return spanId;
    }

    /**
     * Clear current trace info in container
     */

    public static void clearCurrentTrace() {
        CURRENT_SPAN.set(null);
    }
}

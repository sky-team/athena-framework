package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/1.
 */
@Data
public class SyncTeamResultDTO extends SyncResultDTO implements Serializable {
       private  String salesTeamPId;
       private  String salesTeamPName;
       private  String salesTeamId;
       private  String salesTeamName;
       private  String jobId;
       private  String jobName;
       private  String syncMsg;
       private  int syncType;

    @Override
    public String toString() {
        return "SyncTeamResultDTO{" +
                "projectId='" + salesTeamPId + '\'' +
                ", projectName='" + salesTeamPName + '\'' +
                ", salesTeamId='" + salesTeamId + '\'' +
                ", salesTeamName='" + salesTeamName + '\'' +
                ", jobId='" + jobId + '\'' +
                ", jobName='" + jobName + '\'' +
                ", syncMsg='" + syncMsg + '\'' +
                ", syncType='" + syncType + '\'' +
                ", errCode='" + getErrCode() + '\'' +
                ", errMsg=" + getErrMsg() +

                '}';
    }
}

package com.example.hello.cloud.framework.common.plugin.yiyun.impl;

import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunPayProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.enums.PaymentStatusEnum;
import com.google.gson.reflect.TypeToken;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunPayProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunService;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunPaymentApiEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment.wxpay.*;
import com.example.hello.cloud.framework.common.plugin.yiyun.enums.PaymentStatusEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.YiyunPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;

/**
 * User: mall
 * Date: 2019-04-02
 */
@Slf4j
@Service
public class YiyunPaymentServiceImpl implements YiyunPaymentService {

    private final YiyunService yiyunService;


    @Autowired
    public YiyunPaymentServiceImpl(YiyunService yiyunService) {
        this.yiyunService = yiyunService;
    }

    @Override
    public ApiResponse<ApplyPayWxResponse> payApplyForWx(ApplyPayWxRequest applyPayWxRequest, YiyunPayProperties payProperties) {
        YiyunPaymentApiEnum type = YiyunPaymentApiEnum.APPLY_UNIFIED_PAY;
        Type responseType = new TypeToken<YiyunResponse<ApplyPayWxResponse>>() {
        }.getType();
        YiyunResponse<ApplyPayWxResponse> yiyunResponse = yiyunService.zxjShopPaymentAction(type, applyPayWxRequest, responseType, payProperties);
        log.info("YiyunPaymentServiceImpl payApplyForWx:{}", applyPayWxRequest);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ApiResponse<RefundResponse> payRefundApply(RefundRequest refundRequest, YiyunPayProperties payProperties) {
        YiyunPaymentApiEnum type = YiyunPaymentApiEnum.REFUND_APPLY;
        Type responseType = new TypeToken<YiyunResponse<RefundResponse>>() {
        }.getType();
        YiyunResponse<RefundResponse> yiyunResponse = yiyunService.zxjShopPaymentAction(type, refundRequest, responseType, payProperties);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ApiResponse<StatusResponse> queryStatus(StatusRequest statusRequest, YiyunPayProperties payProperties) {
        YiyunPaymentApiEnum type = YiyunPaymentApiEnum.QUERY_STATUS;
        Type responseType = new TypeToken<YiyunResponse<StatusResponse>>() {
        }.getType();
        YiyunResponse<StatusResponse> yiyunResponse = yiyunService.zxjShopPaymentAction(type, statusRequest, responseType, payProperties);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ApiResponse<QueryResponse> queryOrderPayment(QueryRequest queryRequest, YiyunPayProperties payProperties) {
        YiyunPaymentApiEnum type = YiyunPaymentApiEnum.QUERY;
        Type responseType = new TypeToken<YiyunResponse<QueryResponse>>() {
        }.getType();
        YiyunResponse<QueryResponse> yiyunResponse = yiyunService.zxjShopPaymentAction(type, queryRequest, responseType, payProperties);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            if (yiyunResponse.getCode().equals(PaymentStatusEnum.NOT_EXIST.getCredit())) {
                return ApiResponse.error(ApiResponseCodeEnum.E2500003);
            }
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }
}

package com.example.hello.cloud.framework.common.plugin.wechat.pay.result;

import java.io.Serializable;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName: MiniprogramCorpPayResult.java
 * @Description: 企业到付零钱结果
 * @author guohg03
 * @date 2020年2月29日
 */
@Data
@XStreamAlias("xml")
@Accessors(chain = true)
public class MiniprogramCorpPayResult implements Serializable {
	private static final long serialVersionUID = -2606104334485310471L;
	/**
	 * 返回状态码.
	 */
	@XStreamAlias("return_code")
	protected String returnCode;

	/**
	 * 返回信息.
	 */
	@XStreamAlias("return_msg")
	protected String returnMsg;

	/**
	 * 业务结果.
	 */
	@XStreamAlias("result_code")
	private String resultCode;

	/**
	 * 错误代码.
	 */
	@XStreamAlias("err_code")
	private String errCode;
	/**
	 * 错误代码描述.
	 */
	@XStreamAlias("err_code_des")
	private String errCodeDes;
	
	/**
	 * 商户号.
	 */
	@XStreamAlias("mchid")
	private String mchId;

	/**
	 * 商户appid.
	 */
	@XStreamAlias("mch_appid")
	private String mchAppid;

	/**
	 * 设备号.
	 */
	@XStreamAlias("device_info")
	private String deviceInfo;
	
	/**
	 * 随机字符串
	 */
	@XStreamAlias("nonce_str")
	private String nonceStr;

	/**############以下字段在return_code 和result_code都为SUCCESS的时候有返回##############*/
	/**
	 * 商户订单号.
	 */
	@XStreamAlias("partner_trade_no")
	private String partnerTradeNo;
	
	/**
	 * 微信订单号.
	 */
	@XStreamAlias("payment_no")
	private String paymentNo;

	/**
	 * 微信支付成功时间.
	 */
	@XStreamAlias("payment_time")
	private String paymentTime;

	@SuppressWarnings("unchecked")
	public static <T> T fromXML(String xmlString, Class<T> clz) {
		XStream xstream = new XStream();
		xstream.processAnnotations(clz);
		return (T) xstream.fromXML(xmlString);
	}
}
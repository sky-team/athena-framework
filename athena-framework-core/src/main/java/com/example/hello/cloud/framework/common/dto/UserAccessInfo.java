package com.example.hello.cloud.framework.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author lium44
 * @ClassName UserAccessInfo
 * @Description 用户访问信息
 * @date 2018/10/3
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserAccessInfo implements Serializable {

    private static final long serialVersionUID = 4633640053033737271L;

    /**
     * 用户登录id（微信id或者accountId）
     */
    @ApiModelProperty(value="用户登录id(微信id或者accountId)",name="loginId")
    private String loginId;

    /**
     * 请求流水号
     */
    @ApiModelProperty(value="请求流水号",name="traceId")
    private String traceId;

    /**
     * 应用类型 详见DomainEnum
     */
    @ApiModelProperty(value="应用类型",name="domain")
    private String domain;

    /**
     * 访问令牌
     */
    @ApiModelProperty(value="访问令牌",name="accessToken")
    private String accessToken;

    /**
     * App端请求时间
     */
    @ApiModelProperty(value="App端请求时间",name="accessTime")
    private String accessTime;

    /**
     * 网关接收app端请求时间
     */
    @ApiModelProperty(value="网关接收app端请求时间",name="receiveTime")
    private Date receiveTime;
    
    /**
     * 小程序appId
     */
    @ApiModelProperty(value="小程序appId",name="appletAppId")
    private String appletAppId;

    /**
     * app请求ip地址
     */
    @ApiModelProperty(value="app请求ip地址",name="ip")
    private String ip;

    /**
     * 扩展请求参数(经纬度,手机型号,品牌,手机系统版本)
     * {"lbs":{"longitude":""},"device":{"os":""}}
     */
    @ApiModelProperty(value="扩展请求参数",name="extParam")
    private String extParam;

    /**
     * http请求中body字符串（业务请求入参）
     */
    @ApiModelProperty(value="http请求中body字符串",name="bizParam")
    private String bizParam;

    /**
     * 用户设备信息
     */
    @ApiModelProperty(value="用户设备信息",name="userDevice")
    private UserDevice userDevice;
    
    /**
     * 用户地理位置
     */
    @ApiModelProperty(value="用户地理位置",name="userLbs")
    private UserLbs userLbs;

    @ApiModelProperty(value="版本号，用于前端接口控制",name="versionId")
    private String versionId;
}

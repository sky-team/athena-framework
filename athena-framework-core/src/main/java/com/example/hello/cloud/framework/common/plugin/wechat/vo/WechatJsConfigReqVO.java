package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author susw
 * @ClassName: WechatJsConfigReqVO
 * @Description: 
 * @date 2020/3/24、15:06
 */
@Data
public class WechatJsConfigReqVO implements Serializable {

    private static final long serialVersionUID = 48839699000023127L;


    /**
     * url
     * */
    @ApiModelProperty(value = "url",required = true)
    String url;

    /**
     * appId
     * */
    @ApiModelProperty(value = "appId",required = true)
    String appId;
}
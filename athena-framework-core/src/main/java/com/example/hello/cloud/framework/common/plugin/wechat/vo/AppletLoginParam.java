package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 小程序登录入参
 *
 * @author v-linxb
 * @create 2018/10/10
 **/
@Data
@ApiModel("小程序登录入参")
public class AppletLoginParam implements Serializable{

    @ApiModelProperty("小程序前端登录获取的code")
    private String code;

    @ApiModelProperty("前端不需要传递")
    private String domain;

}

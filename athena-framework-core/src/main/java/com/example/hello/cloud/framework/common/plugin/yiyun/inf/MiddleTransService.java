package com.example.hello.cloud.framework.common.plugin.yiyun.inf;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunServiceType;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.MidReqResult;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunServiceType;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.MidReqResult;

/**
 * 调用中台交易接口
 *
 * @author hey54
 */
public interface MiddleTransService {
    /**
     * 根据单据流水号获取交易记录
     *
     * @param serviceType
     * @param request
     * @return
     */
    MidReqResult actionMidReqResult(YiyunServiceType serviceType, YiyunRequestData request);
}

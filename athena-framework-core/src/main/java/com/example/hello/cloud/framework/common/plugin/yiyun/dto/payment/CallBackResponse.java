package com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class CallBackResponse implements Serializable {

    @ApiModelProperty("响应码")
    private String retCode;

    @ApiModelProperty("响应消息")
    private String retMsg;

}
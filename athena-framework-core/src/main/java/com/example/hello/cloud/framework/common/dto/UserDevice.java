package com.example.hello.cloud.framework.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author lium44
 * @ClassName UserDevice
 * @Description 用户设备信息
 * @date 2018/10/3
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserDevice implements Serializable {

    private static final long serialVersionUID = -4451100667616706861L;

    /**
     * 手机型号
     */
    @ApiModelProperty(value="手机型号",name="model")
    private String model;
    
    /**
     * 手机品牌
     */
    @ApiModelProperty(value="手机型号",name="brand")
    private String brand;
    
    /**
     * 应用类型 ios,android
     */
    @ApiModelProperty(value="应用类型 ios,android",name="os")
    private String os;
    
    /**
     * 系统版本
     */
    @ApiModelProperty(value="系统版本",name="version")
    private String version;
    
    /**
     * 系统使用语言
     */
    @ApiModelProperty(value="系统使用语言",name="language")
    private String language;
}

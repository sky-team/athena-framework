package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 楼盘价格
 * Created by v-zhangxj22 on 2018/5/21.
 */
@Data
public class EstatePrice extends BaseYiyunRequestData implements Serializable {

    /**
     * 数值	产品Id	是
     */
    private Long productId;

    /**
     * 数值	价格类型	是(已沟通，改为字符串类型，由业务自定义类型)
     */
    private String priceType;

    /**
     * 数值	价格	是
     */
    private BigDecimal price;

    /**
     * 字符串	生效时间	否
     */
    private String bizFromDate;

    /**
     * 字符串	失效时间	否
     */
    private String bizThruDate;
}

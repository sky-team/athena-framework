package com.example.hello.cloud.framework.common.plugin.wechat.pay.request;

import java.io.Serializable;
import java.math.BigDecimal;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;


import com.example.hello.cloud.framework.common.plugin.wechat.pay.util.XStreamInitializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName: MiniprogramCorpPayRequest.java
 * @Description: 小程序-企业到付零钱请求
 * @author guohg03
 * @date 2020年2月28日
 */
@Data
@Accessors(chain = true)
@XStreamAlias("xml")
public class MiniprogramCorpPayRequest implements Serializable {
	private static final long serialVersionUID = 476074960486845432L;

	/**
	 * 商户账号appid-申请商户号的appid或商户号绑定的appid
	 */
	@XStreamAlias("mch_appid")
	protected String mchAppid;
	
	/**
	 * 商户号-微信支付分配的商户号
	 */
	@XStreamAlias("mchid")
	protected String mchid;

	/**
	 * 设备号
	 */
	@XStreamAlias("device_info")
	protected String deviceInfo;

	/**
	 * 随机字符串
	 */
	@XStreamAlias("nonce_str")
	protected String nonceStr;

	/**
	 * 签名
	 */
	@XStreamAlias("sign")
	protected String sign;

	/**
	 * 微信支付分配的商户号.
	 */
	@XStreamAlias("partner_trade_no")
	protected String partnerTradeNo;

	/**
	 * <pre>
	 * 字段名：需保持唯一性 用户openid.
	 * 变量名：openid
	 * 是否必填：是
	 * 示例值：oxTWIuGaIt6gTKsQRLau2M0yL16E
	 * 类型：String
	 * 描述：商户appid下，某用户的openid
	 * </pre>
	 */
	@XStreamAlias("openid")
	private String openid;

	/**
	 * <pre>
	 * 字段名：校验用户姓名选项.
	 * 变量名：check_name
	 * 是否必填：是
	 * 示例值：OPTION_CHECK
	 * 类型：String
	 * 描述：NO_CHECK：不校验真实姓名 
	 * FORCE_CHECK：强校验真实姓名（未实名认证的用户会校验失败，无法转账） 
	 * OPTION_CHECK：针对已实名认证的用户才校验真实姓名（未实名认证用户不校验，可以转账成功）
	 * </pre>
	 */
	@XStreamAlias("check_name")
	private String checkName;

	/**
	 * <pre>
	 * 字段名：收款用户姓名.
	 * 变量名：re_user_name
	 * 是否必填：可选
	 * 示例值：马花花
	 * 类型：String
	 * 描述：收款用户真实姓名。
	 * 如果check_name设置为FORCE_CHECK或OPTION_CHECK，  则必填用户真实姓名
	 * </pre>
	 */
	@XStreamAlias("re_user_name")
	private String reUserName;

	/**
	 * <pre>
	 * 字段名：金额.
	 * 变量名：amount
	 * 是否必填：是
	 * 示例值：10099
	 * 类型：int
	 * 描述：企业付款金额， 单位为分
	 * </pre>
	 */
	@XStreamAlias("amount")
	private Integer amount;

	/**
	 * <pre>
	 * 字段名：企业付款描述信息.
	 * 变量名：desc
	 * 是否必填：是
	 * 示例值：理赔
	 * 类型：String
	 * 描述：企业付款操作说明信息。必填。
	 * </pre>
	 */
	@XStreamAlias("desc")
	private String description;

	/**
	 * Ip地址
	 */
	@XStreamAlias("spbill_create_ip")
	private String spbillCreateIp;

	/**
	 * 将单位为元转换为单位为分.
	 *
	 * @param yuan 将要转换的元的数值字符串
	 * @return the integer
	 */
	public static Integer yuanToFen(String yuan) {
		return new BigDecimal(yuan).setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue();
	}

	/**
	 * To xml string.
	 */
	public String toXML() {
		XStream xstream = XStreamInitializer.getInstance();
		xstream.processAnnotations(this.getClass());
		return xstream.toXML(this);
	}
}
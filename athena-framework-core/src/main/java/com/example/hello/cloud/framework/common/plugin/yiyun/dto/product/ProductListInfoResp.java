package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;

import java.util.List;

/**
 * 产品列表信息
 *
 * @author v-linxb
 * @create 2018/5/4
 **/
@Data
public class ProductListInfoResp extends YiyunBasePage implements YiyunResponseData {

    private List<ProductListInfo> list;
}

package com.example.hello.cloud.framework.common.plugin.openapi.dto;

import com.example.hello.cloud.framework.common.plugin.openapi.core.OpenApiBaseReqData;
import com.example.hello.cloud.framework.common.plugin.openapi.core.OpenApiBaseReqData;
import lombok.Data;

/**
 * 万科物业业主身份信息请求入参
 *
 * @author v-linxb
 * @create 2019/9/5
 **/
@Data
public class VkCrmOwnerReq extends OpenApiBaseReqData {

    /**
     * 身份证号 非必填（不能与手机号同时为空）
     */
    private String certificateId;

    /**
     * 手机号 非必填（不能与身份证号同时为空）
     */
    private String mainMobile;
}

package com.example.hello.cloud.framework.common.util.excel4j;


import com.example.hello.cloud.framework.common.annotation.Column;
import com.example.hello.cloud.framework.common.annotation.Excel;
import com.example.hello.cloud.framework.common.annotation.Sheet;
import com.example.hello.cloud.framework.common.annotation.Column;
import com.example.hello.cloud.framework.common.annotation.Excel;
import com.example.hello.cloud.framework.common.annotation.Sheet;
import lombok.Data;

import java.io.Serializable;

/**
 * ExportErrorResponse
 * pengdm
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Data
@Excel
@Sheet(sheetname = "导出错误信息")
public class ExportErrorResponse implements Serializable {
    private static final long serialVersionUID = 6552229920614095170L;

    public ExportErrorResponse(String error) {
        this.error = error;
    }

    @Column(title = "错误信息")
    private String error;
}

package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 登录设备类型
 *
 * @author zhoujj07
 * @create 2018/5/10
 */
public enum LoginDeviceTypeEnum {

    /**
     * 基于web应用产生的设备唯一标识
     */
    COOKIES_UUID("cookiesUuid"),
    /**
     * 基于app应用产生的设备唯一标识
     */
    MOBILE_UUID("mobileUuid"),;

    private String index;

    LoginDeviceTypeEnum(String index) {
        this.index = index;
    }

    public String getIndex() {
        return index;
    }
}

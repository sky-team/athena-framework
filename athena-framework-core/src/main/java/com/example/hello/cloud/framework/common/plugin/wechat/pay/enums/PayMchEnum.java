package com.example.hello.cloud.framework.common.plugin.wechat.pay.enums;

/**
 * @ClassName: PayMchEnum.java
 * @Description: 支付商户枚举
 * @author guohg03
 * @date 2020年3月1日
 */
public enum PayMchEnum {
	/**
     * 东莞
     */
    DONG_GUAN("1493581312", "b9e5ND7JZsUC4c6gTiwuX382daBhpSHv", "东莞公司");
    
    private String mchNo;
    private String mchKey;
    private String mchName;

    PayMchEnum(String mchNo,String mchKey,String mchName) {
        this.mchNo = mchNo;
        this.mchKey = mchKey;
        this.mchName = mchName;
    }

	public String getMchNo() {
		return mchNo;
	}

	protected void setMchNo(String mchNo) {
		this.mchNo = mchNo;
	}

	public String getMchKey() {
		return mchKey;
	}

	protected void setMchKey(String mchKey) {
		this.mchKey = mchKey;
	}

	public String getMchName() {
		return mchName;
	}

	protected void setMchName(String mchName) {
		this.mchName = mchName;
	}
}
package com.example.hello.cloud.framework.common.config;

import com.example.hello.cloud.framework.common.interceptor.FeignRequestInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.hello.cloud.framework.common.interceptor.FeignRequestInterceptor;

import feign.RequestInterceptor;

/**
 * Feiclient相关配置
 *
 * @author zhanj04
 * @date 2019/10/24 16:36
 */
@Configuration
public class FeignConfig {
    /**
     * 日志
     */
    private static final Logger log = LoggerFactory.getLogger(FeignConfig.class);

    @Bean
    public RequestInterceptor feignRequestInterceptor() {
        return new FeignRequestInterceptor();
    }
}

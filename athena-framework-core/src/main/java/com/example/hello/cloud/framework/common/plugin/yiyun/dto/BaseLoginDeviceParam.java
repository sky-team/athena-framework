package com.example.hello.cloud.framework.common.plugin.yiyun.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 登录设备相关入参
 *
 * @author zhoujj07
 * @create 2018/5/15
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseLoginDeviceParam extends BaseUserDomainBizParam {

    private static final long serialVersionUID = -756063057016562074L;

    /**
     * 登录设备类型
     */
    private String loginDeviceType;

    /**
     * 登录设备id
     */
    private String loginDeviceId;

}

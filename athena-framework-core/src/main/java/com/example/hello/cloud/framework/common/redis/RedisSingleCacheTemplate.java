package com.example.hello.cloud.framework.common.redis;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author guohg03
 * @ClassName: RedisSingleCacheTemplate
 * @Description: Redis缓存工具类(单机)
 * @date 2018年7月26日
 */
@Component
@SuppressWarnings("unchecked")
public class RedisSingleCacheTemplate {

    @Autowired
    @SuppressWarnings("rawtypes")
    private RedisTemplate redisTemplate;

    @PostConstruct
    public void init() {
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(getSerializer());
        redisTemplate.setHashValueSerializer(getSerializer());
        redisTemplate.afterPropertiesSet();
    }

    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

    @SuppressWarnings("rawtypes")
    private static Jackson2JsonRedisSerializer getSerializer() {
        Jackson2JsonRedisSerializer serializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        serializer.setObjectMapper(om);
        return serializer;
    }

    /**
     * 功能：写入 描述：仅当key不存在时才可写入成功，用于分布式锁。升级spring-date-redis后，可支持与expire为同一事务，避免写入后无法过期，形成死锁
     */
    public boolean setNx(final String key, Object value) {
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        return operations.setIfAbsent(key, value);
    }

    /**
     * 功能：写入
     */
    public boolean set(final String key, Object value) {
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        operations.set(key, value);
        return true;
    }

    /**
     * 功能：写入(设置时效时间，单位为秒)
     */
    public boolean set(final String key, Object value, Long expireTime) {
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        operations.set(key, value);
        redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
        return true;
    }

    /**
     * 功能：写入(设置时效时间)
     */
    public boolean set(final String key, Object value, Long expireTime, TimeUnit unit) {
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        operations.set(key, value);
        redisTemplate.expire(key, expireTime, unit);
        return true;
    }

    /**
     * 功能：写入(设置时效时间)
     */
    public boolean setNx(final String key, Object value, Long expireTime, TimeUnit unit) {
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        operations.setIfAbsent(key, value);
        redisTemplate.expire(key, expireTime, unit);
        return true;
    }

    /**
     * 功能：设置过期(单位为秒,设置为<=0 key立即失效)
     */
    public boolean expire(final String key, Long expireTime) {
        return redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
    }

    /**
     * 功能：设置过期(设置为<=0 key立即失效)
     */
    public boolean expire(final String key, Long expireTime, TimeUnit unit) {
        return redisTemplate.expire(key, expireTime, unit);
    }

    /**
     * 功能: 获取对应key的过期时间(默认为秒)
     */
    public long getExpire(final Object key) {
        return redisTemplate.getExpire(key);
    }

    /**
     * 功能: 获取对应key的过期时间(指定单位)
     */
    public long getExpire(final String key, TimeUnit unit) {
        return redisTemplate.getExpire(key, unit);
    }

    /**
     * 功能：批量删除key
     */
    public void remove(final String... keys) {
        for (String key : keys) {
            remove(key);
        }
    }

    /**
     * 功能：批量删除key(模糊)
     */
    public void removePattern(final String pattern) {
        Set<Serializable> keys = redisTemplate.keys(pattern);
        if (keys.size() > 0) {
            redisTemplate.delete(keys);
        }
    }

    /**
     * 功能：删除对应key
     */
    public void remove(final String key) {
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }

    /**
     * 功能：判断缓存中是否有对应的key
     */
    public boolean exists(final String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 功能：获取包含keyPrefix的key列表
     */
    public Set<String> getKeyWithPrefix(final String keyPrefix) {
        return redisTemplate.keys(keyPrefix + "*");
    }

    /**
     * 功能：批量删除对应前缀的key
     */
    public void batchRemove(final String keyPrefix) {
        Set<String> keySet = redisTemplate.keys(keyPrefix + "*");
        if (keySet != null && keySet.size() > 0) {
            for (String key : keySet) {
                remove(key);
            }
        }
    }

    /**
     * 功能：批量过期对应前缀的key
     */
    public void batchExpire(final String keyPrefix) {
        Set<String> keySet = redisTemplate.keys(keyPrefix + "*");
        if (keySet != null && keySet.size() > 0) {
            for (String key : keySet) {
                expire(key, -1L);
            }
        }
    }

    /**
     * 功能：读取缓存
     */
    public Object get(final String key) {
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        return operations.get(key);
    }

    /**
     * 缓存Map(批量)
     *
     * @param key     缓存键值
     * @param dataMap map集合
     * @param <HK>    map的key类型
     * @param <HV>    map的value类型
     */
    @SuppressWarnings("rawtypes")
    public <HK, HV> void setMap(String key, Map<HK, HV> dataMap) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.putAll(key, dataMap);
    }

    /**
     * 缓存Map(批量)-带过期时间
     *
     * @param key        缓存键值
     * @param dataMap    map集合
     * @param expireTime 过期时间
     * @param <HK>       map的key类型
     * @param <HV>       map的value类型
     */
    @SuppressWarnings("rawtypes")
    public <HK, HV> void setMap(String key, Map<HK, HV> dataMap, Long expireTime) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.putAll(key, dataMap);
        expire(key, expireTime);
    }

    /**
     * 缓存Map(批量)-带过期时间
     *
     * @param key        缓存键值
     * @param dataMap    map集合
     * @param expireTime 过期时间
     * @param unit       过期时间的单位
     * @param <HK>       map的key类型
     * @param <HV>       map的value类型
     */
    @SuppressWarnings("rawtypes")
    public <HK, HV> void setMap(String key, Map<HK, HV> dataMap, Long expireTime, TimeUnit unit) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.putAll(key, dataMap);
        expire(key, expireTime, unit);
    }

    /**
     * 设置map集合中指定键的对象值
     *
     * @param key       缓存键值
     * @param hashKey   map的键值
     * @param hashValue map的对象数据
     * @param <HK>      map的key类型
     * @param <HV>      map的value类型
     */
    @SuppressWarnings("rawtypes")
    public <HK, HV> void setMap(String key, HK hashKey, HV hashValue) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.put(key, hashKey, hashValue);
    }

    /**
     * 设置map集合中指定键的对象值(带过期时间)
     *
     * @param key        缓存键值
     * @param hashKey    map的键值
     * @param hashValue  map的对象数据
     * @param expireTime 过期时间
     * @param <HK>       map的key类型
     * @param <HV>       map的value类型
     */
    public <HK, HV> void setMap(String key, HK hashKey, HV hashValue, Long expireTime) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.put(key, hashKey, hashValue);
        expire(key, expireTime);
    }

    /**
     * 设置map集合中指定键的对象值(带单位的过期时间)
     *
     * @param key        缓存键值
     * @param hashKey    map的键值
     * @param hashValue  map的对象数据
     * @param expireTime 过期时间
     * @param unit       过期时间的单位
     * @param <HK>       map的key类型
     * @param <HV>       map的value类型
     */
    public <HK, HV> void setMap(String key, HK hashKey, HV hashValue, Long expireTime, TimeUnit unit) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.put(key, hashKey, hashValue);
        expire(key, expireTime, unit);
    }

    /**
     * 获得缓存的Map
     *
     * @param key  缓存键值
     * @param <HK> map的key类型
     * @param <HV> map的value类型
     * @return map集合
     */
    public <HK, HV> Map<HK, HV> getMap(String key) {
        return (Map<HK, HV>) redisTemplate.opsForHash().entries(key);
    }

    /**
     * 获取map集合指定键的对象信息
     *
     * @param key     缓存键值
     * @param hashKey map的键值
     * @param <HK>    map的key类ZZA/ZA?C  X   ZX .X ,   /?键的对象信息
     */
    @SuppressWarnings("rawtypes")
    public <HK, HV> HV getMapValue(String key, HK hashKey) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        return (HV) hashOperations.get(key, hashKey);
    }

    /**
     * 获取map的键值集合对应的value集合
     *
     * @param key         缓存键值
     * @param hashKeyList map键值集合
     * @param <HK>        map的key类型
     * @param <HV>        map的value类型
     * @return map的键值集合对应的value集合
     */
    @SuppressWarnings("rawtypes")
    public <HK, HV> List<HV> getMapValueList(String key, List<HK> hashKeyList) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        return hashOperations.multiGet(key, hashKeyList);
    }

    /**az,q.ws an.
     * 获取map集合对的value集合
     *
     * @param key  缓存键值
     * @param <HV> map的value类型
     * @return map集合对的value集合
     */
    @SuppressWarnings("rawtypes")
    public <HV> List<HV> getMapValueList(String key) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        return hashOperations.values(key);
    }

    /**
     * 从hashmap中删除一个值
     *
     * @param key     map名
     * @param hashKey 成员名称
     * @param <HK>    map的key类型
     */
    @SuppressWarnings("rawtypes")
    public <HK> void delFromMap(String key, HK hashKey) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.delete(key, hashKey);
    }

    /**
     * 判断hash集合中是否缓存了数据
     *
     * @param key     缓存键值
     * @param hashKey 数据KEY
     * @return 判断是否缓存了
     */
    @SuppressWarnings("rawtypes")
    public boolean isMapCached(String key, String hashKey) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        return hashOperations.hasKey(key, hashKey);
    }

    // endregion

    /**
     * 功能：列表添加
     */
    public void lPush(String key, Object value) {
        ListOperations<String, Object> list = redisTemplate.opsForList();
        list.rightPush(key, value);
    }

    /**
     * 功能：获取指定范围的列表
     */
    public List<Object> lRange(String key, long start, long end) {
        ListOperations<String, Object> list = redisTemplate.opsForList();
        return list.range(key, start, end);
    }

    /**
     * 功能: 获取所有的列表
     */
    public List<Object> lRange(String key) {
        ListOperations<String, Object> list = redisTemplate.opsForList();
        return list.range(key, 0, -1);
    }

    /**
     * 功能：弹出并清空指定的列表
     */
    public List<Object> popAndClear(String key, long start, long end) {
        ListOperations<String, Object> list = redisTemplate.opsForList();
        List<Object> returnList = lRange(key, start, end);
        list.trim(key, end+1, -1);
        return returnList;
    }

    /**
     * 功能：保留list中部分元素
     */
    public void trimList(String key, long start, long end) {
        ListOperations<String, Object> list = redisTemplate.opsForList();
        list.trim(key, start, end);
    }

    /**
     * 功能：删除list中所有的元素
     */
    public void trimListAll(String key) {
        trimList(key, 1, 0);
    }

    /**
     * 功能：获取列表长度
     */
    public Long listSize(String key) {
        ListOperations<String, Object> list = redisTemplate.opsForList();
        return list.size(key);
    }

    /**
     * 功能: 对key增加value(整形)
     */
    public long incrByLong(Object key, long value) {
        return redisTemplate.opsForValue().increment(key, value);
    }

    /**
     * 功能: 对key增加value(double)
     */
    public double incrByDouble(Object key, double value) {
        return redisTemplate.opsForValue().increment(key, value);
    }

    /**
     * 功能: 添加集合
     */
    public void lset(String key, Object value) {
        SetOperations<String, Object> set = redisTemplate.opsForSet();
        set.add(key, value);
    }

    /**
     * 功能: 获取所有集合
     */
    public Set<Object> lset(String key) {
        SetOperations<String, Object> set = redisTemplate.opsForSet();
        return set.members(key);
    }

    /**
     * 功能：获取集合长度
     */
    public Long llen(String key) {
        SetOperations<String, Object> set = redisTemplate.opsForSet();
        return set.size(key);
    }

    /**
     * 功能：批量删除集合
     */
    public Long lremove(String key, Object... values) {
        SetOperations<String, Object> set = redisTemplate.opsForSet();
        return set.remove(key, values);
    }

    /**
     * 功能：添加有序集合
     */
    public void zAdd(String key, Object value, double score) {
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        zset.add(key, value, score);
    }

    /**
     * 功能：删除有序集合
     */
    public Long zremove(String key, Object... values) {
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        return zset.remove(key, values);
    }

    /**
     * 功能：获取有序集合长度
     */
    public Long zsize(String key, Object... values) {
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        return zset.size(key);
    }

    /**
     * 功能：获取指定的有序集合长度
     */
    public Set<Object> zrange(String key, long start, long end) {
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        return zset.range(key, start, end);
    }

    /**
     * 功能：获取所有的有序集合长度
     */
    public Set<Object> zrange(String key) {
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        return zset.range(key, 0, -1);
    }

    /**
     * 功能：返回有序集中，成员的分数值
     */
    public Set<Object> zByScore(String key, double min, double max) {
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        return zset.rangeByScore(key, min, max);
    }
}

package com.example.hello.cloud.framework.common.plugin.yiyun.dto;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 客户唯一标识, accountId和mobile二选一
 *
 * @author zhoujj07
 * @create 2018/5/3
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerIdentification extends BaseYiyunRequestData {

    private static final long serialVersionUID = 4024978757108789810L;

    /**
     * 中台账号ID
     */
    private Long accountId;

    /**
     * 用户主手机号
     */
    private String phoneNumber;

}

package com.example.hello.cloud.framework.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.alibaba.cloud.sentinel.annotation.SentinelRestTemplate;

/**
 * RestTemplate
 *
 * @author zhanj04
 * @date 2017/8/4 10:40
 */
@Configuration
public class RestTemplateConfig {

    @Bean
    @SentinelRestTemplate
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}

package com.example.hello.cloud.framework.common.plugin.wechat.pay.enums;

/**
 * @ClassName: PayAppEnum.java
 * @Description: 支付应用枚举
 * @author guohg03
 * @date 2020年3月1日
 */
public enum PayAppEnum {
	/**
     * 万小二
     */
    WAN_XIAO_ER("wanx2", "万小二", "wxaac38610878d0feb"),
    
    /**
     * 分享家小程序
     */
    FXJ_APPLET("fxj-applet", "分享家小程序", "wx8e7bfe0f4e6d7b29"),
    
    /**
     * 置业神器小程序
     */
    ZYSQ_APPLET("zysq-applet", "置业神器小程序", "wxe9692d75c9e71da7");
    
    private String code;
    private String name;
    private String appId;

    PayAppEnum(String code, String name, String appId) {
        this.code = code;
        this.name = name;
        this.appId = appId;
    }

    public String getCode() {
        return code;
    }

    protected void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

	public String getAppId() {
		return appId;
	}

	protected void setAppId(String appId) {
		this.appId = appId;
	}
}
package com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
public class CallBackRequest implements Serializable {

    /*
        paymentOrderNo	            字符串	支付订单号	是
        tradeOrderNo	            字符串	交易订单号	是
        channelCode	                字符串	渠道编码	是
        amount	                    数值	支付金额	是
        status	                    数值	状态	    是	0待付款1已付款 2部分退款 3已退款 4 支付失败 5 待确认
        sign	                    字符串	签名串	    是
        signType	                字符串	签名方式	是	MD5, RSA
    */

    @ApiModelProperty("支付订单号")
    private String paymentOrderNo;

    @ApiModelProperty("交易订单号")
    private String tradeOrderNo;

    @ApiModelProperty("渠道编码")
    private String channelCode;

    @ApiModelProperty("支付金额")
    private BigDecimal amount;

    @ApiModelProperty("状态")
    private Integer status;

    @ApiModelProperty("签名串")
    private String sign;

    @ApiModelProperty("签名方式")
    private String signType;
}
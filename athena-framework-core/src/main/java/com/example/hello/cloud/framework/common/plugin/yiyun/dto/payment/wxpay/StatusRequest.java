package com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment.wxpay;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 支付状态入参
 *
 * @author mall
 */
@Data
public class StatusRequest extends BaseYiyunRequestData implements Serializable {

    @ApiModelProperty("商户号")
    private String merchantNo;

    @ApiModelProperty("支付订单号")
    private String orderNo;

}
package com.example.hello.cloud.framework.common.plugin.yiyun.core.response;

import lombok.Data;

import java.io.Serializable;

/**
 * 翼云接口响应泛型类
 *
 * @author v-linxb
 * @create 2018/5/3
 **/
@Data
public class YiyunResponse<T> implements Serializable {

    private String code;

    private String msg;

    private T data;

    /**
     * 成功返回码
     */
    public static final String RES_SUCC_CODE = "000000";

    /**
     * 判断返回是否成功
     *
     * @return 访问是否成功
     */
    public boolean isSuccess() {
        return RES_SUCC_CODE.equals(code);
    }
}

package com.example.hello.cloud.framework.common.enums;

/**
 * 人员角色
 *
 * @author guoc16
 * @date 2020/3/3
 */
public enum RoleTypeEnum {

    /**
     * 客户
     */
    CUSTOMER(1, "客户"),
    /**
     * 顾问
     */
    CONSULTANT(2, "顾问"),
    /**
     * 经纪人
     */
    AGENT(3, "经纪人"),
    /**
     * 经纪人
     */
    MANAGER(4, "经理"),
    /**
     * 机构
     */
    ORG(5, "机构"),
    /**
     * 客服
     */
    SERVER(6, "客服"),
    ;

    RoleTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private Integer code;

    public Integer getCode() {
        return code;
    }

    private String desc;

    public String getDesc() {
        return desc;
    }

    public static RoleTypeEnum match(Integer code) {
        if (code == null) {
            return null;
        }
        for (RoleTypeEnum roleTypeEnum : RoleTypeEnum.values()) {
            if (roleTypeEnum.getCode().equals(code)) {
                return roleTypeEnum;
            }
        }
        return null;
    }
}

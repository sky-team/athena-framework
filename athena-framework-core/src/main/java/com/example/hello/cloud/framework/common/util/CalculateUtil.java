package com.example.hello.cloud.framework.common.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * 精度计算工具类，提供加减乘除的计算(默认保留两位小数)
 * 注意double有效长度16位
 */
public class CalculateUtil {
	
	private CalculateUtil() {
	}

    /**
     * 默认小数点后保留的位数
     */
    public static final int SCALE = 2;
    /**
     * 0
     */
    public static final int ZERO = 0;

    /**
     * 金额格式化格式
     */
    private static final String FORMAT_NUMBER = "###,##0.00";

    /**
     * 大写金额单位
     */
    private static final String UNIT = "万千佰拾亿千佰拾万千佰拾元角分";

    /**
     * 大写金额数值
     */
    private static final String DIGIT = "零壹贰叁肆伍陆柒捌玖";

    /**
     * 最大有效数值
     */
    private static final double MAX_VALUE = 9999999999999.99D;



    /**
     * BigDecimal大小比较
     * @param a
     * @param b
     * @return 返回1， 表示a大于b
     *         返回0 ，表示a等于b
     *         返回-1，表示a小于b
     */
    public static int compareTo(BigDecimal a, BigDecimal b) {
        return a.compareTo(b);
    }

    /**
     * double大小比较
     * @param a
     * @param b
     * @return 返回1， 表示a大于b
     *         返回0 ，表示a等于b
     *         返回-1，表示a小于b
     */
    public static int compareTo(double a, double b) {
        return compareTo(String.valueOf(a), String.valueOf(b));
    }

    /**
     * float大小比较
     * @param a
     * @param b
     * @return 返回1， 表示a大于b
     *         返回0 ，表示a等于b
     *         返回-1，表示a小于b
     */
    public static int compareTo(float a, float b) {
        return compareTo(String.valueOf(a), String.valueOf(b));
    }

    /**
     * String数值大小比较
     * @param a
     * @param b
     * @return 返回1， 表示a大于b
     *         返回0 ，表示a等于b
     *         返回-1，表示a小于b
     */
    public static int compareTo(String a, String b) {
        return compareTo(new BigDecimal(a), new BigDecimal(b));
    }



    /**
     * 累加运算
     * @param vals 数值
     * @return
     */
    public static BigDecimal add(BigDecimal... vals) {
        if (vals == null || vals.length <= ZERO) {
            return null;
        }
        BigDecimal sum = new BigDecimal("0");
        for (BigDecimal val : vals) {
            sum = sum.add(val);
        }
        return sum;
    }

    /**
     * 累加运算
     * @param vals 数值
     * @return
     */
    public static double add(double... vals) {
        if (vals == null || vals.length <= ZERO) {
            return ZERO;
        }
        BigDecimal sum = new BigDecimal("0");
        for (double val : vals) {
            sum = sum.add(new BigDecimal(Double.toString(val)));
        }
        return sum.doubleValue();
    }



    /**
     * 减法运算
     * @param a 被减数
     * @param b 减数
     * @return
     */
    public static BigDecimal sub(BigDecimal a, BigDecimal b) {
        return a.subtract(b);
    }

    /**
     * 减法运算
     * @param a 被减数
     * @param b 减数
     * @return
     */
    public static double sub(double a, double b) {
        return sub(new BigDecimal(Double.toString(a)), new BigDecimal(Double.toString(b))).doubleValue();
    }



    /**
     * 乘法运算
     * @param a 被乘数
     * @param b 乘数
     * @return
     */
    public static BigDecimal mul(BigDecimal a, BigDecimal b) {
        return a.multiply(b);
    }

    /**
     * 乘法运算
     * @param a 被乘数
     * @param b 乘数
     * @return
     */
    public static double mul(double a, double b) {
        return mul(new BigDecimal(Double.toString(a)), new BigDecimal(Double.toString(b))).doubleValue();
    }



    /**
     * 除法运算(默认保留2位小数)
     * @param a 被除数
     * @param b 除数
     * @return 四舍五入后的结果
     */
    public static BigDecimal div(BigDecimal a, BigDecimal b) {
        return div(a, b, SCALE);
    }

    /**
     * 除法运算
     * @param a 被除数
     * @param b 除数
     * @param scale 保留小数位数
     * @return 四舍五入后的结果
     */
    public static BigDecimal div(BigDecimal a, BigDecimal b, int scale) {
        return a.divide(b, scale, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 除法运算(默认保留两位小数)
     * @param a 被除数
     * @param b 除数
     * @return 四舍五入后的结果
     */
    public static double div(double a, double b) {
        return div(a, b, SCALE);
    }

    /**
     * 除法运算
     * @param a 被除数
     * @param b 除数
     * @param scale 保留小数位数
     * @return 四舍五入后的结果
     */
    public static double div(double a, double b, int scale) {
        return div(new BigDecimal(Double.toString(a)), new BigDecimal(Double.toString(b)), scale).doubleValue();
    }



    /**
     * 四舍五入保留小数(默认两位)
     * @param v 数值
     * @return
     */
    public static double formatDecimals(double v){
        BigDecimal decimal = new BigDecimal(Double.toString(v)).setScale(2, BigDecimal.ROUND_HALF_UP);
        return decimal.doubleValue();
    }

    /**
     * 四舍五入保留小数(默认两位)
     * @param v 数值
     * @param scale 保留小数位数
     * @return
     */
    public static double formatDecimals(double v, int scale){
        if(scale < ZERO){
            throw new IllegalArgumentException("请输入有效保留位数!");
        }
        BigDecimal decimal = new BigDecimal(Double.toString(v)).setScale(scale, BigDecimal.ROUND_HALF_UP);
        return decimal.doubleValue();
    }

    /**
     * 四舍五入保留小数(默认两位)
     * @param v 数值
     * @return
     */
    public static BigDecimal formatDecimals(BigDecimal v){
        return v.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 四舍五入保留小数(默认两位)
     * @param v 数值
     * @param scale 保留小数位数
     * @return
     */
    public static BigDecimal formatDecimals(BigDecimal v, int scale){
        if(scale < ZERO){
            throw new IllegalArgumentException("请输入有效保留位数!");
        }
        return v.setScale(scale, BigDecimal.ROUND_HALF_UP);
    }


    /**
     * 格式化金额(金额前加￥,千分位逗号分隔,保留两位小数)
     * @param v 数值
     * @return
     */
    public static String formatMoney(double v){
        NumberFormat nf = DecimalFormat.getCurrencyInstance();
        return nf.format(v);
    }

    /**
     * 格式化金额(金额前加￥,千分位逗号分隔,保留两位小数)
     * @param v 数值
     * @return
     */
    public static String formatMoney(BigDecimal v){
        NumberFormat nf = DecimalFormat.getCurrencyInstance();
        return nf.format(v);
    }

    /**
     * 格式化金额(千分位逗号分隔,保留两位小数)
     * @param v 数值
     * @return
     */
    public static String formatNumber(double v){
        DecimalFormat df = new DecimalFormat(FORMAT_NUMBER);
        return df.format(v);
    }

    /**
     * 格式化金额(千分位逗号分隔,保留两位小数)
     * @param v 数值
     * @return
     */
    public static String formatNumber(BigDecimal v){
        DecimalFormat df = new DecimalFormat(FORMAT_NUMBER);
        return df.format(v);
    }

    /**
     * 转大写金额
     * @param v
     * @return
     */
    public static String toUpper(double v) {
        if (v < 0 || v > MAX_VALUE) {
            return "参数非法!";
        }
        long l = Math.round(v * 100);
        if (l == 0) {
            return "零元整";
        }
        String strValue = l + "";
        // i用来控制数
        int i = 0;
        // j用来控制单位
        int j = UNIT.length() - strValue.length();
        StringBuilder rs = new StringBuilder();
        boolean isZero = false;
        for (; i < strValue.length(); i++, j++) {
            char ch = strValue.charAt(i);
            if (ch == '0') {
                isZero = true;
                if (UNIT.charAt(j) == '亿' || UNIT.charAt(j) == '万' || UNIT.charAt(j) == '元') {
                    rs.append(UNIT.charAt(j));
                    isZero = false;
                }
            } else {
                if (isZero) {
                    rs.append("零");
                    isZero = false;
                }
                rs.append(DIGIT.charAt(ch - '0')).append(UNIT.charAt(j));
            }
        }
        String rsN = rs.toString();
        if (!rsN.endsWith("分")) {
        	rsN = rsN + "整";
        }
        rsN = rsN.replaceAll("亿万", "亿");
        return rsN;
    }
}
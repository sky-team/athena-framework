package com.example.hello.cloud.framework.common.datascope;

import java.util.HashMap;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 数据权限查询参数
 *
 * @author zhanj04
 * @date 2017/8/4 10:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DataScope extends HashMap {

    private static final long serialVersionUID = 9052335203133492956L;
    /**
     * 限制范围的字段名称
     */
    private String scopeName = "deptId";

    /**
     * 具体的数据范围
     */
    private List<Integer> deptIds;

    /**
     * 是否只查询本部门
     */
    private Boolean isOnly = false;
}

package com.example.hello.cloud.framework.common.plugin.yiyun.inf;

import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunPayProperties;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunPayProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment.wxpay.*;

/**
 * User: mall
 * Date: 2019-04-02
 */
public interface YiyunPaymentService {

    /**
     * 申请微信订单支付
     *
     * @param applyPayWxRequest
     * @return
     */
    public ApiResponse<ApplyPayWxResponse> payApplyForWx(ApplyPayWxRequest applyPayWxRequest, YiyunPayProperties payProperties);

    /**
     * 申请退款
     *
     * @param refundRequest
     * @return
     */
    public ApiResponse<RefundResponse> payRefundApply(RefundRequest refundRequest, YiyunPayProperties payProperties);

    /**
     * 支付状态查询
     *
     * @param statusRequest
     * @return
     */
    public ApiResponse<StatusResponse> queryStatus(StatusRequest statusRequest, YiyunPayProperties payProperties);

    /**
     * 订单支付信息查询
     *
     * @param queryRequest
     * @return
     */
    public ApiResponse<QueryResponse> queryOrderPayment(QueryRequest queryRequest, YiyunPayProperties payProperties);

}

package com.example.hello.cloud.framework.common.plugin.openapi.core;

/**
 * 万科API服务市场接口服务
 *
 * @author v-linxb
 * @create 2019/9/4
 **/
public interface OpenApiServiceType {

    /**
     * api地址
     *
     * @return api地址
     */
    String url();

    /**
     * serviceId
     * @return
     */
    String serviceId();

    /**
     * api描述
     *
     * @return api描述
     */
    String desc();

}

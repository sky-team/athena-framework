package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 交易单重推响应参数
 *
 * @author v-linxb
 * @create 2019/10/15
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class MqReissueResp extends YiyunBasePage implements YiyunResponseData {


    /**
     * 重推交易的单据流水号
     */
    private String orderNo;

    /**
     * mq消息ID
     */
    private String messageId;
}

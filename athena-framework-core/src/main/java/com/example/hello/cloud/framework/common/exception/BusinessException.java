package com.example.hello.cloud.framework.common.exception;

/**
 * @author zhoujj07
 * @ClassName: ArgumentException
 * @Description: 业务异常类
 * @date 2018/7/28
 */
public class BusinessException extends RuntimeException {
	private static final long serialVersionUID = 8443556069512063019L;

	public BusinessException(String msg) {
        super(msg);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

}

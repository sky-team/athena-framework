package com.example.hello.cloud.framework.common.enums;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * 应用Domain
 *
 * @author c-linxb
 * @create 2020/3/24
 **/
public enum AppDomainEnum {

    FXJ_APP(RoleDomainEnum.AGENT, "fxj-app", "分享家APP", WechatBindingEnum.APP),

    FXJ_APPLET(RoleDomainEnum.AGENT, "fxj-applet", "分享家小程序", WechatBindingEnum.APPLET),

    XSJ_CONSULTANT_APP(RoleDomainEnum.CONSULTANT, "xsj-app", "销售家顾问端APP", WechatBindingEnum.APP),

    XSJ_CONSULTANT_APPLET(RoleDomainEnum.CONSULTANT, "xsj-applet", "销售家顾问端小程序", WechatBindingEnum.APPLET),

    EFANG_H5(RoleDomainEnum.C_SIDE_USER, "efang-h5", "e选房小程序", null),

    EFANG_APPLET(RoleDomainEnum.C_SIDE_USER, "efang-applet", "e选房小程序", WechatBindingEnum.APPLET),

    EFANG_TOUTIAO(RoleDomainEnum.C_SIDE_USER, "efang-toutiao", "e选房头条", WechatBindingEnum.APPLET),

    CMOP_SERVER(RoleDomainEnum.SERVER, "cmop-server", "企业微信客服", WechatBindingEnum.APPLET),

    XSJ_MANAGER(RoleDomainEnum.MANAGER, "xsj-manager", "销售家经理端", WechatBindingEnum.APP),

    ORG_MANAGER(RoleDomainEnum.ORG, "org-manager", "分享家企业合作平台", null),

    hello_MANAGER(RoleDomainEnum.MANAGER, "hello-manager", "知客管理后台", null);

    /**
     * 角色domain
     */
    private RoleDomainEnum roleDomain;

    /**
     * 应用domain
     */
    private String code;

    /**
     * 应用domain名称
     */
    private String name;

    /**
     * 应用绑定类型
     */
    private WechatBindingEnum bindingType;

    AppDomainEnum(RoleDomainEnum roleDomain, String code, String name, WechatBindingEnum bindingType) {
        this.roleDomain = roleDomain;
        this.code = code;
        this.name = name;
        this.bindingType = bindingType;
    }

    public RoleDomainEnum getRoleDomain() {
        return roleDomain;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public WechatBindingEnum getBindingType() {
        return bindingType;
    }

    public static AppDomainEnum getByCode(String code) {
        Optional<AppDomainEnum> opt = Arrays.stream(AppDomainEnum.values()).filter(item -> Objects.equals(item.getCode(), code)).findFirst();
        return opt.orElse(null);
    }
}

package com.example.hello.cloud.framework.common.enums;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * domain映射枚举
 */
public enum DomainMappingEnum {

    /**
     * 分享家app ios版
     */
    FXJ_APP_IOS("fxj-app-ios", "分享家app ios版", AppDomainEnum.FXJ_APP),
    /**
     * 分享家app安卓版
     */
    FXJ_APP_ANDROID("fxj-app-android", "分享家app安卓版", AppDomainEnum.FXJ_APP),
    /**
     * 分享家app h5页面
     */
    FXJ_APP_H5("fxj-app-h5", "分享家app h5页面", AppDomainEnum.FXJ_APP),
    /**
     * 分享家小程序
     */
    FXJ_APPLET("fxj-applet", "分享家小程序", AppDomainEnum.FXJ_APPLET),

    /**
     * 销售家顾问app ios版
     */
    XSJ_CONSULTANT_IOS("xsj-consultant-ios", "销售家顾问app ios版", AppDomainEnum.XSJ_CONSULTANT_APP),
    /**
     * 销售家顾问app安卓版
     */
    XSJ_CONSULTANT_ANDROID("xsj-consultant-android", "销售家顾问app安卓版", AppDomainEnum.XSJ_CONSULTANT_APP),
    /**
     * 销售家顾问PC
     */
    XSJ_CONSULTANT_ADMIN("xsj-consultant-admin", "销售家顾问", AppDomainEnum.XSJ_CONSULTANT_APP),

    /**
     * 销售家顾问app安卓版
     */
    XSJ_CONSULTANT_APPLET("xsj-applet", "销售家顾问小程序", AppDomainEnum.XSJ_CONSULTANT_APPLET),


    /**
     * 在线家web
     */
    ZXJ_WEB("zxj-web", "在线家web", AppDomainEnum.EFANG_APPLET),

    /**
     * 在线家wap
     */
    ZXJ_WAP("zxj-wap", "在线家wap", AppDomainEnum.EFANG_APPLET),

    /**
     * 置业神器顾问客户端
     */
    ZYSQ_APPLET("zysq-applet", "置业神器顾问客户端", AppDomainEnum.EFANG_APPLET),

    /**
     * 易选房H5
     */
    MKT_H5("mkt-h5", "易选房H5", AppDomainEnum.EFANG_H5),

    /**
     * 置业神器-房屋管家
     */
    ZYSQ_APPLET_ONINE("zysq-applet-online", "置业神器-房屋管家", AppDomainEnum.EFANG_APPLET),

    /**
     * 万科万小二小程序
     */
    WXE_NANFANG_APPLET("wanx2", "万科万小二小程序", AppDomainEnum.EFANG_APPLET),

    /**
     * 企微应用h5
     */
    CMOP_SERVER_H5("cmop-server-h5", "企微应用h5", AppDomainEnum.CMOP_SERVER),

    /**
     * 企微应用pc
     */
    CMOP_SERVER_PC("cmop-server-pc", "企微应用pc", AppDomainEnum.CMOP_SERVER),

    /**
     * 销售家经理app ios版
     */
    XSJ_MANAGER_IOS("xsj-manager-ios", "销售家经理app ios版", AppDomainEnum.XSJ_MANAGER),
    /**
     * 销售家经理app安卓版
     */
    XSJ_MANAGER_ANDROID("xsj-manager-android", "销售家经理app安卓版", AppDomainEnum.XSJ_MANAGER),
    /**
     * 知客管理后台
     */
    hello_ADMIN("hello-admin", "知客管理后台", AppDomainEnum.hello_MANAGER),
    /**
     * 知客管理后台
     */
    EFANG_TOUTIAO("efang-toutiao", "头条c端小程序", AppDomainEnum.EFANG_TOUTIAO),
    /**
     * 机构管理员
     */
    ORG_ADMIN("org-admin", "分享家企业合作平台", AppDomainEnum.ORG_MANAGER),
	
	  /**
     * 置业神器客户签到端
     */
    ZYSQ_SIGN_IOS("zysq-sign-ios", "签到应用（ipad）", AppDomainEnum.EFANG_APPLET);


    private String code;
    private String name;
    private AppDomainEnum appDomain;


    DomainMappingEnum(String code, String name, AppDomainEnum appDomain) {
        this.code = code;
        this.name = name;
        this.appDomain = appDomain;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public AppDomainEnum getAppDomain() {
        return appDomain;
    }

    public static DomainMappingEnum getByCode(String code) {
        Optional<DomainMappingEnum> opt = Arrays.stream(DomainMappingEnum.values()).filter(item ->
                Objects.equals(item.getCode(), code)).findFirst();
        return opt.orElse(null);
    }

    public static List<DomainMappingEnum> getByAppDomainCode(String code) {
        return Arrays.stream(DomainMappingEnum.values()).filter(item -> Objects.equals(item.getAppDomain().getCode(), code)).collect(Collectors.toList());
    }
}

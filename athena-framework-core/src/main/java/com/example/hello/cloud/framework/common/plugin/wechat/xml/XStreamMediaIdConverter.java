package com.example.hello.cloud.framework.common.plugin.wechat.xml;

/**
 * MediaId转换
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 * @date 2019-08-22
 */
public class XStreamMediaIdConverter extends XStreamCDataConverter {
	@Override
	public String toString(Object obj) {
		return "<MediaId>" + super.toString(obj) + "</MediaId>";
	}
}

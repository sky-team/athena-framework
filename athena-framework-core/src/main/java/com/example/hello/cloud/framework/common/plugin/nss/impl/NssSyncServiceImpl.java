package com.example.hello.cloud.framework.common.plugin.nss.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.hello.cloud.framework.common.plugin.nss.NssSyncService;
import com.example.hello.cloud.framework.common.plugin.nss.dto.*;
import com.example.hello.cloud.framework.common.redis.RedisSingleCacheTemplate;
import com.example.hello.cloud.framework.common.util.http.HttpUtil;
import com.example.hello.cloud.framework.common.util.json.JsonUtil;
import io.netty.util.internal.ThrowableUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Alikes on 2018/10/1.
 */
@Service
@Slf4j
@RefreshScope
public class NssSyncServiceImpl implements NssSyncService {

    @Value("${nss.url:https://sales_test.example.com}")
    private String prefix_url;
    @Value("${nss.userName:SD00004}")
    private String userName;
    @Value("${nss.userPwd:123456}")
    private String credit;

    RedisSingleCacheTemplate redisSingleCacheTemplate;

    @Value("${sync.nss.lostTime:86400}")
    private Long lostTime;

    public NssSyncServiceImpl(RedisSingleCacheTemplate redisSingleCacheTemplate) {
        this.redisSingleCacheTemplate = redisSingleCacheTemplate;
//        userName = ResourcesUtil.getProperty("nss.userName");
//        credit = ResourcesUtil.getProperty("nss.userPwd");
//        prefix_url = ResourcesUtil.getProperty("nss.url");
    }

    @Override
    public String syncToken() {
        // RedisClusterCacheTemplate redisClusterCacheTemplate = new RedisClusterCacheTemplate();
        /**
         * 獲取redis過期時間和token
         **/
        String tokendate = redisSingleCacheTemplate.get("nss:tokendate") == null ? ""
                : redisSingleCacheTemplate.get("nss:tokendate").toString();
        String nsstoken = redisSingleCacheTemplate.get("nss:nsstoken") == null ? ""
                : redisSingleCacheTemplate.get("nss:nsstoken").toString();

        log.info("prefix_url -> "+prefix_url);
        if (StrUtil.isEmpty(userName) || StrUtil.isEmpty(credit) || StrUtil.isEmpty(prefix_url)) {
            log.info("用户名或密码或域名为空，使用redis");

            prefix_url = redisSingleCacheTemplate.get("nss.url") == null ? ""
                    : redisSingleCacheTemplate.get("nss.url").toString();
            userName = redisSingleCacheTemplate.get("nss.nssUserName") == null ? ""
                    : redisSingleCacheTemplate.get("nss.nssUserName").toString();
            credit = redisSingleCacheTemplate.get("nss.nssPassword") == null ? ""
                    : redisSingleCacheTemplate.get("nss.nssPassword").toString();

            log.info("syncToken prefix_url -> "+prefix_url);
            log.info("syncToken credit -> "+credit);
            log.info("syncToken userName -> "+userName);
        }

        if (!StrUtil.isEmpty(tokendate) && !StrUtil.isEmpty(nsstoken)) {
            long tokendateLong = Long.parseLong(tokendate);
            long nowdateLong = System.currentTimeMillis() / 1000;

            long days = (nowdateLong - tokendateLong) / 86400;

            if (days < 2) {
                return nsstoken;
            }
        }

        String token = "XXXXX";

        String url = prefix_url + "/api/auth";
        log.info("NssSyncServiceImpl syncToken url={}", url);
        Map<String, Object> map = new HashMap<>();
        // String userName = redisClusterCacheTemplate.get("nssUserName");
        // String password = redisClusterCacheTemplate.get("nssPassword");
        if (StrUtil.isEmpty(userName) || StrUtil.isEmpty(credit)) {
            log.info("用户名或密码为空，请检查redis");
            return token;
        }
        map.put("username", userName);
        map.put("password", DigestUtils.md5Hex(credit));
        String jsonstr = JSONObject.toJSONString(map);
        log.info("NssSyncServiceImpl syncToken jsonstr={}", jsonstr);
        Map header = new HashMap();
        header.put("Authorization", token);
        String response = null;
        try {
            response = HttpUtil.doPostWithJson(url, jsonstr, header);

            log.info("NssSyncServiceImpl syncToken response={}", response);
        } catch (Exception e) {
            log.error("syncToken error!", e);
            return token;
        }
        JSONObject json = JSON.parseObject(response);
        JSONObject body = (JSONObject) json.get("body");
        if (!StrUtil.isEmpty(body.getString("token"))) {
            token = body.getString("token");
            redisSingleCacheTemplate.set("nss:nsstoken", token);
            redisSingleCacheTemplate.set("nss:tokendate", System.currentTimeMillis() / 1000 + "");
        }
        return token;
        // return
        // "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJTRDAwMDA0IiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTM5MzA5NTMyNzkzLCJleHAiOjE1Mzk5MTQzMzJ9.ZAlcoGqBVGRDWEYyvUvc6FUemwY31WMohmpiRgn18cxkw66fNuWWbSHFS228P4cXZ2zpNzDoi4nxiYo7En3Qmg";
    }

    private Map<String, String> setHeader() {
        Map<String, String> header = new HashMap();

        header.put("Authorization", syncToken()); // 封装获取token方法

        return header;
    }

    @Override
    public SyncTeamResultDTO syncTeam(SyncTeamDTO syncTeamDTO) {
        log.info("NssSyncServiceImpl syncTeam syncTeamDTO={}", syncTeamDTO);
        SyncTeamResultDTO syncTeamResultDTO = new SyncTeamResultDTO();

        String snumber = System.currentTimeMillis() / 1000 + "" + (int) (Math.random() * 9000 + 1000);
        syncTeamDTO.setSNumber(snumber);
        try {
            syncTeamResultDTO = syncPTeam(syncTeamDTO);
        } catch (Exception e) {
            syncTeamResultDTO.setErrCode("10001");
            syncTeamResultDTO.setErrMsg(e.getMessage());
            log.error("syncTeam error!", e);
        }

        log.info("NssSyncServiceImpl syncTeam syncTeamResultDTO={}", syncTeamResultDTO);
        return syncTeamResultDTO;
    }

    public SyncTeamResultDTO syncPTeam(SyncTeamDTO syncTeamDTO) throws Exception {
        log.info("NssSyncServiceImpl syncPTeam syncTeamDTO={}", syncTeamDTO);
        SyncTeamResultDTO syncTeamResultDTO = new SyncTeamResultDTO();

        // log.info("放入token参数到请求头");
        String url = "";
        String response = "";
        String s = syncTeamDTO.getConsultantOrgId();

        url = prefix_url + "/api/object/salesTeam/lookup/salesOrgId/" + s;
        log.info("NssSyncServiceImpl syncPTeam sNumber={}, url={}", syncTeamDTO.getSNumber(), url);
        // log.info("顶级团队检查发送信息：" + snumber1 + url);
        response = HttpUtil.doGet(url, setHeader());
        log.info("NssSyncServiceImpl syncPTeam sNumber={}, response={}", syncTeamDTO.getSNumber(), response);

        syncTeamResultDTO = returnRsponse(response, syncTeamDTO);

        log.info("NssSyncServiceImpl syncPTeam syncTeamResultDTO={}", syncTeamResultDTO);
        return syncTeamResultDTO;
    }

    /*
     *  处理团队检查返回后的结果
     * */
    private SyncTeamResultDTO returnRsponse(String resp, SyncTeamDTO syncTeamDTO) {
        log.info("NssSyncServiceImpl returnRsponse resp={} syncTeamDTO={}", resp, syncTeamDTO);

        SyncTeamResultDTO syncTeamResultDTO = new SyncTeamResultDTO();
        // log.info("获取返回的信息");
        JSONObject jsonObject = JSON.parseObject(resp);
        JSONObject body = (JSONObject) jsonObject.get("body");
        // log.info("对返回的信息进行判断，当取得的list为空发送请求销售系统新建，不为空插入信息");
        if (body.getJSONArray("list").size() == 0) {
            // 顶级团队新建请求`

            syncTeamResultDTO = registerFirstTeam(syncTeamDTO);

        } else {
            // 处理顶级团队检查返回对象结果;
            syncTeamResultDTO = dealFirstTeamResponse(resp, new SyncTeamResultDTO(), syncTeamDTO);
        }
        log.info("NssSyncServiceImpl returnRsponse syncTeamResultDTO={}", syncTeamResultDTO);

        return syncTeamResultDTO;
    }

    // 顶级团队新建
    private SyncTeamResultDTO registerFirstTeam(SyncTeamDTO syncTeamDTO) {
        log.info("NssSyncServiceImpl registerFirstTeam syncTeamDTO={}", syncTeamDTO);

        SyncTeamResultDTO syncTeamResultDTO = new SyncTeamResultDTO();

        try {
            String response = "";
            String jsonstr = setBodyUrl(syncTeamDTO, "");
            String url = prefix_url + "/api/salesTeamSvc/save";
            log.info("NssSyncServiceImpl registerFirstTeam sNumber={},url={}", syncTeamDTO.getSNumber(), url);
            response = HttpUtil.doPutWithJson(url, jsonstr, setHeader());
            log.info("NssSyncServiceImpl registerFirstTeam sNumber={},response={}", syncTeamDTO.getSNumber(), response);
            syncTeamResultDTO = dealFirstTeamResponse(response, new SyncTeamResultDTO(), syncTeamDTO);
        } catch (Exception e) {
            log.error("registerFirstTeam error!", e);
        }
        log.info("NssSyncServiceImpl registerFirstTeam syncTeamResultDTO={}", syncTeamResultDTO);

        return syncTeamResultDTO;
    }

    /*
     * 设置请求信息数据包裹
     * */
    public String setBodyUrl(SyncTeamDTO oldNewSalesteam, String type) {
        String salesOrgId = oldNewSalesteam.getConsultantOrgId();
        String agentname = oldNewSalesteam.getAgentName();
        String parentSalesTeamId = oldNewSalesteam.getSalesTeamPid();
        String parentSalesTeamName = oldNewSalesteam.getSalesTeamPName();
        String salesteamid = oldNewSalesteam.getSalesTeamId();
        String salesteamname = oldNewSalesteam.getSalesTeamName();
        Map<String, Object> map = new HashMap();
        Map<String, Object> map1 = new HashMap<>();
        Map<String, Object> mapMap = new HashMap<>();
        if (null == parentSalesTeamId || ("").equals(parentSalesTeamId)) {
            // log.info("顶级团队请求部分参数");
            map.put("parentSalesTeamId", "");
            map.put("parentSalesTeamNo", "");
            map.put("parentSalesTeamName", "");
            map.put("salesTeamName", "销售家");
            map.put("salesTeamTypeId", "SD2032001");
        } else if (salesteamid == null || salesteamid.equals("")) {
            // log.info("二级团队信息请求部分参数");
            map.put("parentSalesTeamId", parentSalesTeamId);
            map.put("parentSalesTeamNo", "");
            map.put("parentSalesTeamName", parentSalesTeamName);
            map.put("salesTeamName", agentname);
            map.put("salesTeamTypeId", "SD2032001");
        } else {
            // 岗位名称请求部分参数
            if (type.equals("1")) { // 置业顾问
                map.put("parentSalesTeamId", parentSalesTeamId);
                map.put("parentSalesTeamNo", "");
                map.put("parentSalesTeamName", parentSalesTeamName);
                map.put("id", salesteamid);
                map.put("salesTeamTypeId", "");
                map.put("salesTeamName", salesteamname);
                mapMap.put("jobCode", "SD2030001");
                mapMap.put("jobName", "置业顾问");
                mapMap.put("jobNo", "");
                mapMap.put("salesTeamId", salesteamid);
                mapMap.put("salesTeamName", salesteamname);
                List<Map> list = new ArrayList<Map>();
                list.add(mapMap);
                map1.put("jobList", list);
            } else if (type.equals("2")) { // 项目经理
                map.put("parentSalesTeamId", "");
                map.put("parentSalesTeamNo", "");
                map.put("parentSalesTeamName", "");
                map.put("id", parentSalesTeamId);
                map.put("salesTeamTypeId", "");
                map.put("salesTeamName", parentSalesTeamName);
                mapMap.put("jobCode", "SD2030003");
                mapMap.put("jobName", "项目经理");
                mapMap.put("jobNo", "");
                mapMap.put("salesTeamId", parentSalesTeamId);
                mapMap.put("salesTeamName", parentSalesTeamName);
                List<Map> list = new ArrayList<Map>();
                list.add(mapMap);
                map1.put("jobList", list);

                // map.put("parentSalesTeamId", parentSalesTeamId);
                // map.put("parentSalesTeamNo", "");
                // map.put("parentSalesTeamName", parentSalesTeamName);
                // map.put("id", parentSalesTeamId);
                // map.put("salesTeamTypeId", "");
                // mapMap.put("jobCode", "SD2030003");
                // mapMap.put("jobName", "项目经理");
                // mapMap.put("jobNo", "");
                // mapMap.put("salesTeamId", parentSalesTeamId);
                // mapMap.put("salesTeamName", parentSalesTeamName);
                // List<Map> list = new ArrayList<Map>();
                // list.add(mapMap);
                // map1.put("jobList", list);
            } else if (type.equals("3")) { // 代理经理
                map.put("parentSalesTeamId", parentSalesTeamId);
                map.put("parentSalesTeamNo", "");
                map.put("parentSalesTeamName", parentSalesTeamName);
                map.put("id", salesteamid);
                map.put("salesTeamName", salesteamname);
                map.put("salesTeamTypeId", "");
                mapMap.put("jobCode", "SD2030002");
                mapMap.put("jobName", "代理经理");
                mapMap.put("jobNo", "");
                mapMap.put("salesTeamId", salesteamid);
                mapMap.put("salesTeamName", salesteamname);
                List<Map> list = new ArrayList<Map>();
                list.add(mapMap);
                map1.put("jobList", list);
            }

        }
        map.put("salesTeamNo", "");
        map.put("salesOrgId", salesOrgId);
        map.put("salesOrgNo", "");
        map.put("salesOrgName", "");
        map.put("projectId", "");
        map.put("projectName", "");
        map.put("stageId", "");
        map.put("stageName", "");
        map.put("status", "1");
        map1.put("salesTeam", map);

        String jsonstr = JSONObject.toJSONString(map1);

        return jsonstr;

    }

    /*
     *处理销售系统顶级团队新建返回的信息
     * */
    // @Transactional
    public SyncTeamResultDTO dealFirstTeamResponse(String response, SyncTeamResultDTO syncTeamResultDTO,
                                                   SyncTeamDTO syncTeamDTO) {
        log.info("NssSyncServiceImpl dealFirstTeamResponse response={} syncTeamResultDTO={}", response,
                syncTeamResultDTO);
        log.info("NssSyncServiceImpl dealFirstTeamResponse syncTeamDTO={}", syncTeamDTO);
        // List<OldNewSalesteam> oldNewSalesteams = oldNewSalesteamExtendRepository.findBySalesOrgCode(s);
        // log.info("处理返回的结果");
        JSONObject jsonObject = JSON.parseObject(response);
        JSONObject body = (JSONObject) jsonObject.get("body");
        JSONObject state = (JSONObject) jsonObject.get("state");
        String errmsg = state.getString("errMsg");
        String errCode = state.getString("errCode");
        if (errCode.equals("10000")) {
            JSONArray listarray = body.getJSONArray("list");
            String projectId = null;
            String projectName = null;
            String ParentSalesTeamId = null;
            String ParentSalesTeamName = null;
            if (listarray != null && listarray.size() > 0) { // 顶级团队检查处理结果
                for (int i = 0; i < listarray.size(); i++) {
                    JSONObject object = (JSONObject) listarray.get(i);
                    // 得到指定销售组织id对应的顶级团队的id和名称
                    ParentSalesTeamId = object.getString("parentSalesTeamId");
                    ParentSalesTeamName = object.getString("parentSalesTeamName");
                    if (ParentSalesTeamId != null && !ParentSalesTeamId.equals("")) {
                        projectId = ParentSalesTeamId;
                        syncTeamResultDTO.setSalesTeamPId(projectId);
                        if (ParentSalesTeamName != null && !ParentSalesTeamName.equals("")) {
                            projectName = ParentSalesTeamName;
                            syncTeamResultDTO.setSalesTeamPName(projectName);
                        } else {
                            projectName = "销售家";
                            syncTeamResultDTO.setSalesTeamPName(projectName);
                        }
                        break;
                    } else {
                        projectId = object.getString("id");
                        projectName = object.getString("salesTeamName");
                        if (projectId != null && !projectId.equals("")) {
                            syncTeamResultDTO.setSalesTeamPId(projectId);
                            if (projectName != null && !projectName.equals("")) {
                                syncTeamResultDTO.setSalesTeamPName(projectName);
                            } else {
                                projectName = "销售家";
                                syncTeamResultDTO.setSalesTeamPName(projectName);
                            }
                            break;
                        }
                    }
                }
                // 如果返回的所有对象中没有顶级团队则进行顶级团队新建上报
                if (projectId == null || projectId.equals("")) {
                    registerFirstTeam(syncTeamDTO);
                }
            } else { // 顶级团队新建上报结果处理
                JSONObject salesTeam = body.getJSONObject("salesTeam");
                if (salesTeam != null && !salesTeam.isEmpty()) {
                    ParentSalesTeamId = salesTeam.getString("parentSalesTeamId");
                    ParentSalesTeamName = salesTeam.getString("parentSalesTeamName");
                    if (ParentSalesTeamId != null && !ParentSalesTeamId.equals("")) {
                        syncTeamResultDTO.setSalesTeamPId(ParentSalesTeamId);
                        if (ParentSalesTeamName != null && !ParentSalesTeamName.equals("")) {
                            syncTeamResultDTO.setSalesTeamPName(ParentSalesTeamName);
                        } else {
                            syncTeamResultDTO.setSalesTeamPName("销售家");
                        }
                    } else {
                        projectId = salesTeam.getString("id");
                        projectName = salesTeam.getString("salesTeamName");
                        if (projectId != null && !projectId.equals("")) {
                            syncTeamResultDTO.setSalesTeamPId(projectId);
                            if (projectName != null && projectName.equals("")) {
                                syncTeamResultDTO.setSalesTeamPName(projectName);
                            } else {
                                projectName = "销售家";
                                syncTeamResultDTO.setSalesTeamPName(projectName);
                            }

                        }
                    }
                } else {
                    syncTeamResultDTO.setSyncMsg("顶级团队新建上报返回结果失败");
                }
            }

            // log.info("上报二级团队");
            // flag = syncSecondTeam(oldNewSalesteam);
        } else {

            // String salesOrgCode = oldNewSalesteam.getSalesOrgCode();
            // List<OldNewSalesteam> oldNewSalesteamList = oldNewSalesteamService.findByProperty("salesOrgCode",
            // salesOrgCode);
            // for (int i = 0; i < oldNewSalesteamList.size(); i++) {
            // oldNewSalesteamList.get(i).setSNumber1(errmsg);
            // }
            // 设置当前数据的返回结果日志信息
            syncTeamResultDTO.setSyncMsg("顶级团队上报失败");
            // oldNewSalesteamService.saveOrUpdate(oldNewSalesteam);

        }
        syncTeamResultDTO.setErrCode(errCode);
        syncTeamResultDTO.setErrMsg(errmsg);

        log.info("NssSyncServiceImpl dealFirstTeamResponse syncTeamResultDTO={}", syncTeamResultDTO);
        return syncTeamResultDTO;
    }

    /*
     *二级团队上报
     * */
    @Override
    public SyncTeamResultDTO syncSecondTeam(SyncTeamDTO syncTeamDTO) {
        log.info("NssSyncServiceImpl syncSecondTeam syncTeamDTO={}", syncTeamDTO);

        SyncTeamResultDTO syncTeamResultDTO = new SyncTeamResultDTO();

        String snumber = System.currentTimeMillis() / 1000 + "" + (int) (Math.random() * 9000 + 1000);
        syncTeamDTO.setSNumber(snumber);

        // 根据id值判断是否上报过一级，二级团队
        String salesTeamPid = syncTeamDTO.getSalesTeamPid(); // 顶级团队id
        if (salesTeamPid != null && !salesTeamPid.equals("")) {

            // log.info("放入token参数到请求头");
            String url = "";
            String response = "";
            String strjson = setBodyUrl(syncTeamDTO, "");
            try {

                url = prefix_url + "/api/salesTeamSvc/save";
                log.info("NssSyncServiceImpl syncSecondTeam sNumber={},url={}", syncTeamDTO.getSNumber(), url);
                response = HttpUtil.doPutWithJson(url, strjson, setHeader());
                log.info("NssSyncServiceImpl syncSecondTeam sNumber={},response={}", syncTeamDTO.getSNumber(),
                        response);

                syncTeamResultDTO = dealSecondTeam(response, syncTeamDTO);
            } catch (Exception e) {
                syncTeamResultDTO.setErrCode("10001");
                syncTeamResultDTO.setErrMsg(e.getMessage());
                log.error("syncSecondTeam error!", e);
            }
        } else {
            syncTeamResultDTO.setErrCode("10001");
            syncTeamResultDTO.setErrMsg("顶级团队id为空");
        }
        log.info("NssSyncServiceImpl syncSecondTeam syncTeamResultDTO={}", syncTeamResultDTO);

        return syncTeamResultDTO;
    }

    /*
     * 处理二级团队上报返回结果
     * */
    // @Transactional
    public SyncTeamResultDTO dealSecondTeam(String response, SyncTeamDTO syncTeamDTO) {
        log.info("NssSyncServiceImpl dealSecondTeam response={} syncTeamDTO={}", response, syncTeamDTO);

        SyncTeamResultDTO syncTeamResultDTO = new SyncTeamResultDTO();
        // log.info("处理二级团队上报返回的结果");
        JSONObject jsonObject = JSON.parseObject(response);
        JSONObject body = (JSONObject) jsonObject.get("body");
        JSONObject state = (JSONObject) jsonObject.get("state");
        String salesteamid = "";
        String salesteamname = "";
        JSONObject object = body.getJSONObject("salesTeam");
        if (object != null) {
            salesteamid = object.getString("id");
            salesteamname = object.getString("salesTeamName");
        } else {
            String string = body.getString("object");
            if (string == null) {
                syncTeamResultDTO.setSyncMsg("二级团队上报返回结果为空，上报失败");
                syncTeamResultDTO.setErrMsg("二级团队上报返回结果为空，上报失败");
                syncTeamResultDTO.setErrCode("10001");

                return syncTeamResultDTO;
            } else {
                String s = string.substring(1, string.length() - 1);
                String[] strings = s.split("=|,| ");
                for (int i = 0; i < strings.length; i++) {
                    if (("id").equals(strings[i])) {
                        salesteamid = strings[i + 1];
                    } else if (("name").equals(strings[i])) {
                        salesteamname = strings[i + 1];
                    }
                    if (salesteamid != null && !salesteamid.equals("") && salesteamname != null
                            && !salesteamname.equals("")) {
                        break;
                    }
                }
            }

        }

        syncTeamResultDTO.setSalesTeamId(salesteamid);
        syncTeamResultDTO.setSalesTeamName(salesteamname);
        syncTeamResultDTO.setErrMsg("二级团队上报成功");
        syncTeamResultDTO.setErrCode("10000");

        log.info("NssSyncServiceImpl dealSecondTeam syncTeamResultDTO={}", syncTeamResultDTO);
        return syncTeamResultDTO;
    }

    /*
     * 岗位名称上报
     * */
    // 岗位名称上报
    @Override
    public SyncTeamResultDTO syncJobTeam(SyncTeamDTO syncTeamDTO) {
        log.info("NssSyncServiceImpl syncJobTeam syncTeamDTO={}", syncTeamDTO);
        SyncTeamResultDTO syncTeamResultDTO = new SyncTeamResultDTO();

        String snumber = System.currentTimeMillis() / 1000 + "" + (int) (Math.random() * 9000 + 1000);
        syncTeamDTO.setSNumber(snumber);

        String url = "";
        String response = "";

        try {
            String strjson = setBodyUrl(syncTeamDTO, syncTeamDTO.getJobType()); // 置业顾问
            // 岗位名称日志代号
            // String snumber3 = System.currentTimeMillis() / 1000 + "" + (int) (Math.random() * 9000 + 1000);
            url = prefix_url + "/api/salesTeamSvc/save";
            log.info("NssSyncServiceImpl syncJobTeam sNumber={},url={}", syncTeamDTO.getSNumber(), url);
            response = HttpUtil.doPutWithJson(url, strjson, setHeader()); // 置业顾问
            log.info("NssSyncServiceImpl syncJobTeam response={}", response);
            // 将发送数据和返回数据保存到日志表中

            syncTeamResultDTO = dealJobTeam(response, syncTeamDTO, Integer.parseInt(syncTeamDTO.getJobType()));
        } catch (Exception e) {
            log.error("syncJobTeam error!", e);

        }
        log.info("NssSyncServiceImpl syncJobTeam syncTeamResultDTO={}", syncTeamResultDTO);
        return syncTeamResultDTO;

    }

    /*
     * 处理岗位上报返回结果
     * */
    // @Transactional
    public SyncTeamResultDTO dealJobTeam(String response, SyncTeamDTO syncTeamDTO, Integer type) {
        log.info("NssSyncServiceImpl dealJobTeam response={} syncTeamDTO={}", response, syncTeamDTO);
        log.info("NssSyncServiceImpl dealJobTeam type={}", type);
        SyncTeamResultDTO syncTeamResultDTO = new SyncTeamResultDTO();
        // log.info("处理岗位上报返回的结果");
        JSONObject jsonObject = JSON.parseObject(response);
        JSONObject body = (JSONObject) jsonObject.get("body");
        JSONObject state = (JSONObject) jsonObject.get("state");
        String errmsg = state.getString("errMsg");
        String errCode = state.getString("errCode");

        if (errCode.equals("10000")) {
            JSONArray listarray = body.getJSONArray("jobList");
            // 销售系统里团队下所有角色id相同，所以只需要一条就可以
            if (listarray == null || listarray.size() == 0) {
                syncTeamResultDTO.setSyncMsg("岗位上报返回数据为空");
                syncTeamResultDTO.setErrMsg("岗位上报返回数据为空");
                syncTeamResultDTO.setErrCode("10001");
                ;
            } else {
                JSONObject object = (JSONObject) listarray.get(0);
                String jobid = object.getString("id");
                if (jobid != null && !jobid.equals("")) {
                    syncTeamResultDTO.setJobId(jobid);
                    if (type == 1) {
                        syncTeamResultDTO.setJobName("置业顾问");
                    } else if (type == 2) {
                        syncTeamResultDTO.setJobName("项目经理");
                    } else if (type == 3) {
                        syncTeamResultDTO.setJobName("代理经理");
                    }
                    syncTeamResultDTO.setSyncType(9);
                    syncTeamResultDTO.setSyncMsg("上报成功");

                } else {
                    syncTeamResultDTO.setSyncMsg("岗位名称id为空，上报失败");
                    syncTeamResultDTO.setErrMsg("岗位名称id为空，上报失败");
                    syncTeamResultDTO.setErrCode("10001");
                }

            }
        } else {
            String string = body.getString("object");
            if (string == null) {
                syncTeamResultDTO.setSyncMsg("岗位名称返回结果为空，上报失败");
                syncTeamResultDTO.setErrMsg("岗位名称返回结果为空，上报失败");
                syncTeamResultDTO.setErrCode("10001");
                log.info("NssSyncServiceImpl dealJobTeam syncTeamResultDTO={}", syncTeamResultDTO);
                return syncTeamResultDTO;
            }
            String s = string.substring(1, string.length() - 1);
            String[] strings = s.split("=|,| ");
            String id = null;
            for (int i = 0; i < strings.length; i++) {
                if (("id").equals(strings[i])) {
                    id = strings[i + 1];
                }
                if (id != null && !id.equals("")) {
                    break;
                }
            }
            if (id != null && !id.equals("")) {
                syncTeamResultDTO.setJobId(id);
                if (type == 1) {
                    syncTeamResultDTO.setJobName("置业顾问");
                } else if (type == 2) {
                    syncTeamResultDTO.setJobName("项目经理");
                } else if (type == 3) {
                    syncTeamResultDTO.setJobName("代理经理");
                }
                syncTeamResultDTO.setSyncType(9);
                syncTeamResultDTO.setSyncMsg("上报成功");
            }

        }
        syncTeamResultDTO.setErrMsg("岗位上报成功");
        syncTeamResultDTO.setErrCode("10000");
        log.info("NssSyncServiceImpl dealJobTeam syncTeamResultDTO={}", syncTeamResultDTO);
        return syncTeamResultDTO;
    }

    // 顾问信息检查上报
    @Override
    public SyncCosultantResultDTO syncSalers(SyncConsultantDTO syncConsultantDTO) {
        log.info("NssSyncServiceImpl syncSalers syncConsultantDTO={}", syncConsultantDTO);

        SyncCosultantResultDTO syncCosultantResultDTO = new SyncCosultantResultDTO();
        try {
            // log.info("放入token参数到请求头");
            // header.put("Authorization", token);
            String url = "";
            String response = "";
            // 日志代号

            String json = setSalesBodyUrl(syncConsultantDTO, 1);
            log.info("NssSyncServiceImpl syncSalers json={}", json);
            url = prefix_url + "/api/saleMemberSvc/search/view/SaleMemberVIew";
            log.info("NssSyncServiceImpl syncSalers sNumber={},url={}", syncConsultantDTO.getSNumber(), url);
            response = HttpUtil.doPostWithJson(url, json, setHeader());
            log.info("NssSyncServiceImpl syncSalers sNumber={},response={}", syncConsultantDTO.getSNumber(), response);
            syncCosultantResultDTO = dealSales(response, syncConsultantDTO);
        } catch (Exception e) {
            syncCosultantResultDTO.setErrCode("10001");
            syncCosultantResultDTO.setErrMsg(e.getMessage());
            log.error("syncSalers error!", e);
        }
        return syncCosultantResultDTO;
    }

    // 顾问上报请求参数封装
    public String setSalesBodyUrl(SyncConsultantDTO syncConsultantDTO, int type) {
        String jsonstr = "";
        String salesSysId = syncConsultantDTO.getSalesSysId();
        String salesOrgCode = syncConsultantDTO.getSalesOrgCode();
        String jobId = syncConsultantDTO.getJobId();
        String jobName = syncConsultantDTO.getJobName();
        String salesTeamPid = syncConsultantDTO.getNssParentSalesTeamId();
        String salesTeamPname = syncConsultantDTO.getNssParentSalesTeamName();
        String salesTeamId = syncConsultantDTO.getNssSalesTeamId();
        String salesTeamName = syncConsultantDTO.getNssSalesTeamName();
        String userType = syncConsultantDTO.getUserType();
        String salesSysPhone = syncConsultantDTO.getSalesSysPhone();
        String salesSysEmail = syncConsultantDTO.getSalesSysEmail();
        String salesSysName = syncConsultantDTO.getSalesSysName();
        String salesSysexampleid = syncConsultantDTO.getSalesSysexampleid();

        Map<String, Object> map = new HashMap();
        Map<String, Object> map1 = new HashMap<>();
        Map<String, Object> map2 = new HashMap<>();
        Map<String, Object> map3 = new HashMap<>();
        // 置业顾问
        if (type == 1) { // type=1 是顾问上报检查
            map.put("SaleMemberSvc_data_SaleMember_accountId", salesSysId);
            map.put("SaleMemberSvc_data_SaleMember_jobId", jobId);
            if ("2".equals(userType)) {
                map.put("SaleMemberSvc_data_SaleMember_saleTeamId", salesTeamPid);
            } else {
                map.put("SaleMemberSvc_data_SaleMember_saleTeamId", salesTeamId);
            }
            map.put("SaleMemberSvc_data_SaleMember_state", 1);
            map.put("SaleMemberSvc_data_SalesTeam_salesOrgId", salesOrgCode);
            jsonstr = JSONObject.toJSONString(map);
            return jsonstr;
        }

        map1.put("id", jobId);
        map1.put("jobName", jobName);
        map2.put("state", "1");
        map2.put("accountId", salesSysId);
        map2.put("telphone", salesSysPhone);
        map2.put("email", salesSysEmail);
        map2.put("name", salesSysName);
        map2.put("userNo", salesSysexampleid);

        if ("1".equals(userType)) {
            map1.put("salesTeamId", salesTeamId);
            map1.put("salesTeamName", salesTeamName);
            map1.put("jobCodeName", "置业顾问");
            map1.put("jobCode", "SD2030001");
            map2.put("salesTeamName", salesTeamName);
            map2.put("saleTeamId", salesTeamId);
            map2.put("jobCode", "SD2030001");
        } else if ("2".equals(userType)) {
            map1.put("salesTeamId", salesTeamPid);
            map1.put("salesTeamName", salesTeamPname);
            map1.put("jobCodeName", "项目经理");
            map1.put("jobCode", "SD2030003");
            map2.put("salesTeamName", salesTeamPname);
            map2.put("saleTeamId", salesTeamPid);
            map2.put("jobCode", "SD2030003");
        } else if (userType.equals("3")) {
            map1.put("salesTeamId", salesTeamId);
            map1.put("salesTeamName", salesTeamName);
            map1.put("jobCodeName", "代理经理");
            map1.put("jobCode", "SD2030002");
            map2.put("salesTeamName", salesTeamName);
            map2.put("saleTeamId", salesTeamId);
            map2.put("jobCode", "SD2030002");
        }

        map3.put("job", map1);
        map3.put("saleMember", map2);
        jsonstr = JSONObject.toJSONString(map3);
        return jsonstr;
    }

    // 处理顾问检查返回结果
    // @Transactional
    public SyncCosultantResultDTO dealSales(String response, SyncConsultantDTO syncConsultantDTO) {
        log.info("NssSyncServiceImpl dealSales response={} syncConsultantDTO={}", response, syncConsultantDTO);
        SyncCosultantResultDTO syncCosultantResultDTO = new SyncCosultantResultDTO();
        JSONObject jsonObject = JSON.parseObject(response);
        JSONObject body = (JSONObject) jsonObject.get("body");
        JSONObject state = (JSONObject) jsonObject.get("state");
        String errmsg = state.getString("errMsg");
        String errCode = state.getString("errCode");
        String userType = syncConsultantDTO.getUserType();
        if (errCode.equals("10000")) {
            Integer totalCount = body.getInteger("totalCount");
            // totalCount为0表示顾问没有挂团队，进行顾问上报
            if (totalCount == 0) {
                // compareTeam(oldNewSales);
                syncCosultantResultDTO = dealSalesTeam(syncConsultantDTO);
                /*syncCosultantResultDTO.setErrMsg(errmsg);
                syncCosultantResultDTO.setErrCode(errCode)*/
                ;
            } else {
                // totalCount不为0表示已经所属团队，不进行上报
                syncCosultantResultDTO.setErrCode("10000");
                syncCosultantResultDTO.setErrMsg("已存在,不进行上报");

            }
        } else {
            if (userType.equals("1")) {
                syncCosultantResultDTO.setErrCode("100001");
                syncCosultantResultDTO.setErrMsg("顾问上报检查返回结果失败");
            } else if (userType.equals("2")) {
                syncCosultantResultDTO.setErrCode("100001");
                syncCosultantResultDTO.setErrMsg("项目经理检查返回结果失败");
            } else if (userType.equals("3")) {
                syncCosultantResultDTO.setErrCode("100001");
                syncCosultantResultDTO.setErrMsg("代理经理检查返回结果失败");
            }
        }
        log.info("NssSyncServiceImpl dealSales syncCosultantResultDTO={}", syncCosultantResultDTO);
        return syncCosultantResultDTO;
    }

    // 上报顾问
    public SyncCosultantResultDTO dealSalesTeam(SyncConsultantDTO syncConsultantDTO) {
        log.info("NssSyncServiceImpl dealSalesTeam syncConsultantDTO={}", syncConsultantDTO);
        SyncCosultantResultDTO syncCosultantResultDTO = new SyncCosultantResultDTO();

        String url = "";
        String response = "";
        String userType = syncConsultantDTO.getUserType();
        String jsonString = "";
        try {
            if (userType.equals("1")) {
                jsonString = setSalesBodyUrl(syncConsultantDTO, 2);
            } else if (userType.equals("2")) {
                jsonString = setSalesBodyUrl(syncConsultantDTO, 2);
            } else if (userType.equals("3")) {
                jsonString = setSalesBodyUrl(syncConsultantDTO, 2);
            }

            url = prefix_url + "/api/saleMemberSvc/save";
            log.info("NssSyncServiceImpl dealSalesTeam sNumber={} url={} postData={}", syncConsultantDTO.getSNumber(),
                    url, jsonString);
            response = HttpUtil.doPutWithJson(url, jsonString, setHeader());
            log.info("NssSyncServiceImpl dealSalesTeam sNumber={} response={}", syncConsultantDTO.getSNumber(),
                    response);
            // 发送信息
            syncCosultantResultDTO = dealSalesResponse(response, syncConsultantDTO);

        } catch (Exception e) {
            syncCosultantResultDTO.setErrCode("10001");
            syncCosultantResultDTO.setErrMsg(e.getMessage());
            log.error("dealSalesTeam error!", e);

        }
        return syncCosultantResultDTO;
    }

    // 处理顾问上报返回结果
    // @Transactional
    public SyncCosultantResultDTO dealSalesResponse(String response, SyncConsultantDTO syncConsultantDTO) {
        log.info("NssSyncServiceImpl dealSalesResponse response={}", response);

        SyncCosultantResultDTO syncCosultantResultDTO = new SyncCosultantResultDTO();
        JSONObject jsonObject = JSON.parseObject(response);
        JSONObject state = (JSONObject) jsonObject.get("state");
        JSONObject body = (JSONObject) jsonObject.get("body");
        String errMsg = "";
        if (null != body) {
            if (null != body.get("errMsg")) {
                errMsg = body.getString("errMsg");
            } else {
                errMsg = state.getString("errMsg");
            }

        } else {
            errMsg = state.getString("errMsg");
        }

        String errCode = state.getString("errCode");
        // String errMsg = state.getString("errMsg");
        if (errMsg.contains("存在")) {
            log.info("NssSyncSales dealSalesResponse errMsg->{}", errMsg);
            errCode = "10000";
        }
        syncCosultantResultDTO.setErrCode(errCode);
        syncCosultantResultDTO.setErrMsg(errMsg);

        log.info("NssSyncServiceImpl dealSalesResponse syncCosultantResultDTO={}", syncCosultantResultDTO);
        return syncCosultantResultDTO;
    }

    @Override
    public SyncCustomerResultDTO syncCustomer(String postData) {
        SyncCustomerResultDTO syncCustomerResultDTO = new SyncCustomerResultDTO();
        String url = prefix_url + "/api/customerSvc/batch/save";
        String code = "";
        String msg = "";
        String id = "";
        String leadsId = "";
        String batch = "";
        String fusionBackgroundId = "";
        String response = null;
        try {
            response = HttpUtil.doPutWithJson(url, postData, setHeader());
        } catch (Exception e) {
            log.error("syncCustomer error", e);
            syncCustomerResultDTO.setErrCode("10001");
            syncCustomerResultDTO.setErrMsg(e.getMessage());
            return syncCustomerResultDTO;
        }

        try {
            log.info("response-------->{}", response);
            // JSONObject jsonStr = JSONObject.parseObject(response);
            // JSONObject state = (JSONObject) jsonStr.get("state");
            // JSONObject body = (JSONObject) jsonStr.get("body");
            // JSONObject customer = (JSONObject) body.get("customer");
            // if (null != state) {
            // code = state.getString("errCode");
            // msg = state.getString("errMsg");
            // if (customer != null) {
            // String newId = (String) customer.get("id");
            // if (newId != null && !"".equals(newId)) {
            // id = newId;
            // code = "10000";
            //
            // fusionBackgroundId = (String) customer.get("fusionBackgroundId");//判断线索客户
            // } else {
            // fusionBackgroundId = "";
            // }
            // } else {
            // if (null != body && body.get("errMsg") != null) {
            // msg = body.get("errMsg").toString();
            // }
            // }
            // } else {
            // code = "99999";
            // msg = "返回json格式不正确";
            // }
            // //上报数据
            // if ("10000".equals(code)) {
            // syncCustomerResultDTO.setNssCustomerId(id);
            // syncCustomerResultDTO.setFusionBackgroundId(fusionBackgroundId);
            // syncCustomerResultDTO.setErrCode(code);
            // syncCustomerResultDTO.setErrMsg(msg);
            // } else {
            // syncCustomerResultDTO.setErrCode(code);
            // syncCustomerResultDTO.setErrMsg(msg);
            // }
        } catch (Exception e) {
            log.error("syncCustomer error", e);
            syncCustomerResultDTO.setErrCode(code);
            syncCustomerResultDTO.setErrMsg(msg);
        }
        return syncCustomerResultDTO;
    }

    @Override
    public SyncCustomerResultDTO syncCustomer(SyncCustomerDTO syncCustomerDTO) {
        log.info("NssSyncServiceImpl syncCustomer syncCustomerDTO={}", syncCustomerDTO);
        SyncCustomerResultDTO syncCustomerResultDTO = new SyncCustomerResultDTO();
        String url = prefix_url + "/api/customerSvc/save";
        log.info("NssSyncServiceImpl syncCustomer sNumber={},url={}", syncCustomerDTO.getSNumber(), url);
        String code = "";
        String msg = "";
        String id = "";
        String leadsId = "";
        String batch = "";
        String fusionBackgroundId = "";
        try {
            // 客户没有姓名不上报
            if (StrUtil.isBlank(syncCustomerDTO.getName())) {
                log.error("NssSyncServiceImpl syncCustomer 客户没有姓名不上报 客户信息->{}", syncCustomerDTO);
                syncCustomerResultDTO.setErrCode("10001");
                syncCustomerResultDTO.setErrMsg("客户没有姓名");
                return syncCustomerResultDTO;
            }
            // 检查客户 code phone 客户电话 salesOrgId 销售组织ID token
            String resCheck = checkUser(syncCustomerDTO);
            JSONObject jsonObj = JSON.parseObject(resCheck);
            if (jsonObj == null || StrUtil.isBlank(jsonObj.getString("body"))) {
                syncCustomerResultDTO.setErrCode("10001");
                syncCustomerResultDTO.setErrMsg("销售系统检查接口无返回信息");
                return syncCustomerResultDTO;
            }
            JSONObject bodyCheck = (JSONObject) jsonObj.get("body");
            JSONObject customerCheck = (JSONObject) bodyCheck.get("customer");
            if (!bodyCheck.get("isExist").toString().equals("false")) {
                if (bodyCheck.get("customer") != null && !"".equals(bodyCheck.get("customer'"))) {
                    // 客户已存在
                    code = "10002";
                    msg = (String) bodyCheck.get("errMsg");
                    id = (String) customerCheck.get("id");
                    syncCustomerResultDTO.setNssCustomerId(id);
                    syncCustomerResultDTO.setErrCode(code);
                    syncCustomerResultDTO.setErrMsg(msg);
                    return syncCustomerResultDTO;
                } else if (bodyCheck.get("leads") != null) {
                    JSONObject leads = (JSONObject) bodyCheck.get("leads");
                    leadsId = (String) leads.get("id");
                    batch = (String) leads.get("batch");
                } else if (StrUtil.isNotBlank(bodyCheck.getString("errMsg"))
                        && "-1".equals(bodyCheck.getString("type"))) {
                    syncCustomerResultDTO.setErrCode("10001");
                    syncCustomerResultDTO.setErrMsg(bodyCheck.getString("errMsg"));
                    return syncCustomerResultDTO;
                }
            }
        } catch (Exception e) {
            log.error("syncCustomer error", e);
            syncCustomerResultDTO.setErrCode("10001");
            syncCustomerResultDTO.setErrMsg("查询客户信息异常,上报失败");
            return syncCustomerResultDTO;
        }
        // 封装参数
        String post_data = syncUserParameter(batch, syncCustomerDTO.getConsultantId(),
                syncCustomerDTO.getSalesOrgCode(), leadsId, syncCustomerDTO, syncCustomerDTO.getCustomerTypeId());

        log.info("NssSyncServiceImpl syncCustomer sNumber={},postData={}", syncCustomerDTO.getSNumber(), post_data);

        String response = null;
        try {
            // 发请求前，查询redis中是否存贮了该请求
            String s = checkPostRepeat(post_data);
            if ("10005".equals(s)) {
                syncCustomerResultDTO.setErrCode(s);
                syncCustomerResultDTO.setErrMsg("redis获取异常");
                return syncCustomerResultDTO;
            } else if ("10006".equals(s)) {
                syncCustomerResultDTO.setErrCode(s);
                syncCustomerResultDTO.setErrMsg("重复请求，已拦截");
                return syncCustomerResultDTO;
            }

            long startTime = System.currentTimeMillis();
            response = HttpUtil.doPutWithJson(url, post_data, setHeader());
            long endTime = System.currentTimeMillis();
            log.info("SyncVisitCustomerJob syncNssVisitCustomer 客户上报 调用接口 手机号->{} 耗时->{} ",
                    syncCustomerDTO.getPhoneNumber(), (endTime - startTime) / 1000 + "秒");

            log.info("NssSyncServiceImpl syncCustomer sNumber={},response={}", syncCustomerDTO.getSNumber(), response);
        } catch (Exception e) {
            log.error("syncCustomer error", e);
            syncCustomerResultDTO.setErrCode("10001");
            syncCustomerResultDTO.setErrMsg(e.getMessage());
            return syncCustomerResultDTO;
        }

        try {
            JSONObject jsonStr = JSONObject.parseObject(response);
            String tooManyRequest = jsonStr.getString("msg");
            if (tooManyRequest != null && "Too Many Requests".equals(tooManyRequest)) {
                log.error("NssSyncServiceImpl syncCustome Too Many Requests phoneNumber postData={}", post_data);
                syncCustomerResultDTO.setErrCode("10005");
                syncCustomerResultDTO.setErrMsg("Too Many Requests");
                return syncCustomerResultDTO;
            }
            JSONObject state = (JSONObject) jsonStr.get("state");
            JSONObject body = (JSONObject) jsonStr.get("body");

            JSONObject customer = (JSONObject) body.get("customer");
            if (null != state && !body.isEmpty()) {
                code = state.getString("errCode");
                msg = state.getString("errMsg");
                if (customer != null) {
                    String newId = (String) customer.get("id");
                    if (newId != null && !"".equals(newId)) {
                        id = newId;
                        code = "10000";
                        // 请求成功，将请求参数，放redis
                        // 过去时间：24小时
                        log.info("上报拦截功能 上报写redis 1037 ：" + DigestUtils.md5Hex(post_data));

                        log.info("saveUserRedis中设置值 postData->{} key->{} value->{} lostTime->{}", post_data,
                                "nssPost:" + DigestUtils.md5Hex(post_data), "nss", lostTime);
                        redisSingleCacheTemplate.set("nssPost:" + DigestUtils.md5Hex(post_data), "nss", lostTime);
                        // 判断线索客户
                        fusionBackgroundId = (String) customer.get("fusionBackgroundId");
                    } else {
                        fusionBackgroundId = "";
                    }
                } else {
                    if (!body.isEmpty() && body.get("errMsg") != null) {
                        msg = body.get("errMsg").toString();
                    }
                }
            } else {
                code = "99999";
                msg = "返回json格式不正确";
            }
            // 上报数据
            if ("10000".equals(code)) {
                syncCustomerResultDTO.setNssCustomerId(id);
                syncCustomerResultDTO.setFusionBackgroundId(fusionBackgroundId);
                syncCustomerResultDTO.setErrCode(code);
                syncCustomerResultDTO.setErrMsg(msg);
            } else {
                syncCustomerResultDTO.setErrCode(code);
                syncCustomerResultDTO.setErrMsg(msg);
            }
        } catch (Exception e) {
            log.error("syncCustomer error", e);
            syncCustomerResultDTO.setErrCode(code);
            syncCustomerResultDTO.setErrMsg(msg);
        }
        return syncCustomerResultDTO;
    }

    /**
     * 检查用户
     *
     * @return
     * @throws Exception
     */
    public String checkUser(SyncCustomerDTO syncCustomerDTO) throws Exception {
        /* //封装头
        Map header = new HashMap();
        header.put("Authorization", s);*/
        // 封装参数
        Map map = new HashMap();
        map.put("consultantId", ""); // 顾问ID
        map.put("salesOrgId", syncCustomerDTO.getSalesOrgCode());// 销售组织id
        map.put("phoneNumber", syncCustomerDTO.getPhoneNumber());// 客户电话
        map.put("sourceWayId", "SD1003014");// 来源途径
        // 接口地址
        String url = prefix_url + "/api/leadsSvc/insertCheck";
        String post_data = JSONObject.toJSONString(map);

        log.info("NssSyncServiceImpl checkUser sNumber={},url={} postData ->{}", syncCustomerDTO.getSNumber(), url,
                post_data);
        // log.info("检查参数=="+post_data);
        String response = HttpUtil.doPostWithJson(url, post_data, setHeader());
        log.info("NssSyncServiceImpl checkUser sNumber={},response={}", syncCustomerDTO.getSNumber(), response);
        return response;
    }

    /**
     * 封装同步数据 参数
     *
     * @param batch
     * @param consultantId
     * @param salesOrgId
     * @param leadsId
     * @param syncCustomerDTO
     * @return
     */
    public String syncUserParameter(String batch, String consultantId, String salesOrgId, String leadsId,
                                    SyncCustomerDTO syncCustomerDTO, String type) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        List<Map> followUpInfoList = new ArrayList<>();
        List<Map> opportunityList = new ArrayList<>();
        Map customerMap = new HashMap();
        customerMap.put("projectId", "");// 项目id
        customerMap.put("consultantId", consultantId);// 顾问ID
        customerMap.put("salesOrgId", salesOrgId); // 销售组织id
        customerMap.put("age", getTurnValue("age", syncCustomerDTO.getAge()));// 年龄
        customerMap.put("sex", getTurnValue("sex", syncCustomerDTO.getSex()));// 性别
        customerMap.put("name", syncCustomerDTO.getName() == null ? "" : syncCustomerDTO.getName());// 姓名
        customerMap.put("phoneNumber", syncCustomerDTO.getPhoneNumber()); // 客户电话
        customerMap.put("familyNmuber", syncCustomerDTO.getFamilyNmuber()); // 家庭电话
        customerMap.put("intentionLevelId", getTurnValue("m_13", syncCustomerDTO.getIntentionLevelId())); // 意向等级
        customerMap.put("sourceWayId", getTurnValue("addSource", syncCustomerDTO.getSourceWayId())); // 来源途径
        customerMap.put("customerClass", "0"); // 客户类型
        customerMap.put("customerTypeId", syncCustomerDTO.getCustomerTypeId());
        String cardId = syncCustomerDTO.getLicenseNumber() + "," + syncCustomerDTO.getCertificateType();
        customerMap.put("certificateType", getTurnValue("cardId", cardId)); // 证件类型
        customerMap.put("licenseNumber", syncCustomerDTO.getLicenseNumber()); // 证件号码
        customerMap.put("contactAddress", syncCustomerDTO.getContactAddress()); // 通信地址
        customerMap.put("salesHomeId", syncCustomerDTO.getSalesHomeId()); // 销售家客户Id
        customerMap.put("fusionBackgroundId", syncCustomerDTO.getFusionBackgroundId()); // 线索Id
        customerMap.put("marketWayId", syncCustomerDTO.getMarketWayId()); // 营销途径
        customerMap.put("agentType", getTurnValue("tuijian_type", syncCustomerDTO.getAgentType()));
        if (StrUtil.isNotEmpty(syncCustomerDTO.getAgentThirdOrgId())) {
            customerMap.put("agentThirdOrgId", syncCustomerDTO.getAgentThirdOrgId());
        }
        if (StrUtil.isNotEmpty(syncCustomerDTO.getAgentThirdOrgName())) {
            customerMap.put("agentThirdOrgName", syncCustomerDTO.getAgentThirdOrgName());
        }
        customerMap.put("agentDate", syncCustomerDTO.getAgentDate());
        customerMap.put("agentName", syncCustomerDTO.getAgentName());
        customerMap.put("agentPhone", syncCustomerDTO.getAgentPhone());
        customerMap.put("isBlack", syncCustomerDTO.getBlackFlag());

        // 2019-04-09新加字段：户籍省份，户籍市，有无购房资格，预计有购房资格日期，第一联系人，第一联系人电话

        // 判断是否是国外户籍
        if ("-1".equals(syncCustomerDTO.getBirthplacProvince())) {
            customerMap.put("province", syncCustomerDTO.getBirthplacCity());
        } else {
            // 户籍省份
            customerMap.put("province", syncCustomerDTO.getBirthplacProvince());
            // 户籍城市
            customerMap.put("city", syncCustomerDTO.getBirthplacCity());
        }

        // 购房资格，1有 0无 int类型（销售系统为：是否无购房资格：0、否，1、是，即0为有购房资格，1为无购房资格）
        if (0 == syncCustomerDTO.getIsHouseQualify()) {
            customerMap.put("purchaseQualicationsFlag", 1);
            // 无购房资格，预计获取资格日期,字符串 yyyy-MM-dd
            if (syncCustomerDTO.getHouseQualifyDate() != null && syncCustomerDTO.getHouseQualifyDate() != 0) {
                Date date = new Date(syncCustomerDTO.getHouseQualifyDate() * 1000L);
                customerMap.put("planGetQualicationsDate", format.format(date));
            }
        } else {
            customerMap.put("purchaseQualicationsFlag", 0);
        }
        // 第一联系人姓名
        customerMap.put("masterContacts", syncCustomerDTO.getFirstContactName());
        // 第一联系人手机号
        customerMap.put("masterContactsPhoneNumber", syncCustomerDTO.getFirstContactMobile());

        // 生日 2019-07-19加
        customerMap.put("birthday", syncCustomerDTO.getBirthday());

        Map followUpInfoListMap = new HashMap();
        followUpInfoListMap.put("consultantId", consultantId); // 顾问ID
        followUpInfoListMap.put("salesOrgId", salesOrgId);// 销售组织id
        followUpInfoListMap.put("pathClass", "SD1007001");// 跟进类型 到访:SD1007001 跟进:SD1007002
        followUpInfoListMap.put("followContent", "第一次到访");
        followUpInfoListMap.put("followTime", syncCustomerDTO.getFollowTime());// 首次到访时间
        followUpInfoList.add(followUpInfoListMap);

        Map opportunityListMap = new HashMap();
        opportunityListMap.put("intentionLevelId", getTurnValue("m_13", syncCustomerDTO.getIntentionLevelId()));// 意向等级
        opportunityList.add(opportunityListMap);
        Map postData = new HashMap();
        Map opportunityRelationMap = new HashMap();
        opportunityRelationMap.put("description", "");
        if ("3".equals(type)) {
            opportunityRelationMap.put("leadsId", "");
        } else {
            opportunityRelationMap.put("leadsId", leadsId);
        }
        opportunityRelationMap.put("id", batch);
        postData.put("opportunityRelation", opportunityRelationMap);

        Map customerEnterpriseMap = new HashMap();
        customerEnterpriseMap.put("businessLicense", syncCustomerDTO.getBusinessLicense());
        customerEnterpriseMap.put("legalPerson", syncCustomerDTO.getLegalPerson());
        customerEnterpriseMap.put("companyName", syncCustomerDTO.getCompanyName());
        postData.put("opportunityList", opportunityList);
        postData.put("followUpInfoList", followUpInfoList);
        postData.put("customer", customerMap);
        if (syncCustomerDTO.getCustomerTypeId().equals("1")) { // 企业客户
            postData.put("customerEnterprise", customerEnterpriseMap);
        }

        String post_data = JSONObject.toJSONString(postData);
        return post_data;
    }

    /**
     * 销售家转销售系统
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public String getTurnValue(String key, String value) {
        String result = "";
        // 性别 男:SD1001001 女:SD1001002
        Map<String, String> sexMap = new HashMap<String, String>();
        sexMap.put("1", "SD1001001");
        sexMap.put("2", "SD1001002");
        sexMap.put("3", "SD1001003");
        // 年龄 销售家 60以上: 6 50-60:5 40-50:4 30-40:3 20-30:2 10-20:1
        // （销售系统 对应） 60以上: SD1002006 50-60:SD1002005 40-50:SD1002004 30-40:SD1002003 20-30:SD1002002 10-20:SD1002001
        Map<String, String> ageMap = new HashMap<String, String>();
        ageMap.put("1", "SD1002001");
        ageMap.put("2", "SD1002002");
        ageMap.put("3", "SD1002003");
        ageMap.put("4", "SD1002004");
        ageMap.put("5", "SD1002005");
        ageMap.put("6", "SD1002006");
        // 意向等级（销售家 对应）无效:-1 D:1 C:2 B:3 A:4
        // （销售系统 对应） 无效:SD1005005 D:SD1005004 C:SD1005003 B:SD1005002 A:SD1005001
        Map<String, String> intentionLevelIdMap = new HashMap<String, String>();
        intentionLevelIdMap.put("-2", "SD1005005");
        intentionLevelIdMap.put("1", "SD1005004");
        intentionLevelIdMap.put("2", "SD1005003");
        intentionLevelIdMap.put("3", "SD1005002");
        intentionLevelIdMap.put("4", "SD1005001");
        // 来源途径 来电:SD1003003 来访:SD1003001 call客:SD1003002 外拓:SD1003004
        // 案场 13:自然到访 2:分享家-个人 3:来电 4:call客 5:微信 6:带客通 7:外拓 8:批量导入 9:通知导入 10:分享家-机构 11:在线家 12:销售家 13:企业团购 15:链家 16 云眼 17
        // 分享拓客-顾问 18 分享拓客-经理 19 分享拓客-经纪人 20 万科好房自流量
        // 20 分享家-业主 21 分享家-员工 22 分享家-独立经纪人 23 置业神器 24 极速秀 25 万小二 26 摇号预登记 27 万动联盟

//        YXF("9", "易选房"),
//                CHANNEL("70", "自渠"),
//                XSJ("12", "销售家"),
//                EXTERNAL("71", "外部接口"),
//                CALL("15", "Call客"),
//                OTHER("21", "其他"),
//                FXJ("59", "分享家");

        //增加 销售家、易选房、自渠、外部接口

        Map<String, String> sourceWayIdMap = new HashMap<String, String>();
        //20200817新增4个来源途径
        sourceWayIdMap.put("12", "SD1003014");
        sourceWayIdMap.put("9", "SD1003035");
        sourceWayIdMap.put("70", "SD1003036");
        sourceWayIdMap.put("71", "SD1003037");

        sourceWayIdMap.put("13", "SD1003001");
        sourceWayIdMap.put("15", "SD1003002");
        sourceWayIdMap.put("14", "SD1003003");
        sourceWayIdMap.put("17", "SD1003004");
        sourceWayIdMap.put("16", "SD1003005");
        sourceWayIdMap.put("23", "SD1003006");
        sourceWayIdMap.put("10", "SD1003007");
        sourceWayIdMap.put("33", "SD1003008");
        sourceWayIdMap.put("34", "SD1003009");
        sourceWayIdMap.put("19", "SD1003010");
        sourceWayIdMap.put("24", "SD1003011");
        sourceWayIdMap.put("25", "SD1003012");
        sourceWayIdMap.put("21", "SD1003013");

        sourceWayIdMap.put("26", "SD1003015");
        sourceWayIdMap.put("27", "SD1003016");
        sourceWayIdMap.put("36", "SD1003017");
        sourceWayIdMap.put("35", "SD1003018");
        sourceWayIdMap.put("37", "SD1003019");
        // sourceWayIdMap.put("28", "SD1003020");
        sourceWayIdMap.put("38", "SD1003021");
        sourceWayIdMap.put("39", "SD1003022");
        sourceWayIdMap.put("40", "SD1003023");
        sourceWayIdMap.put("9", "SD1003024");
        sourceWayIdMap.put("11", "SD1003025");
        sourceWayIdMap.put("18", "SD1003026");
        // sourceWayIdMap.put("23", "SD1003027");
        sourceWayIdMap.put("20", "SD1003028");
        sourceWayIdMap.put("29", "SD1003029");
        sourceWayIdMap.put("30", "SD1003030");
        sourceWayIdMap.put("31", "SD1003031");
        sourceWayIdMap.put("32", "SD1003032");

        // 2019-07-16 新增 59 分享家。、10099009987655555 长沙聚客、60 贝壳
        sourceWayIdMap.put("59", "SD1003025");
        sourceWayIdMap.put("10099009987655555", "SD1003033");
        sourceWayIdMap.put("60", "SD1003034");

        // 销售系统证件类型 SD1010001 身份证 SD1010002 军官证 SD1010003 护照 SD1010004 户口簿 SD1010005 台湾通行证 SD1010006 台湾身份证
        // SD1010007 港澳身份证 SD1010008 营业执照 SD1010009 组织机构代码 SD1010010 税务登记证号 SD1010011 其他
        Map<String, String> cardMap = new HashMap<String, String>();
        cardMap.put("1", "SD1010001");
        cardMap.put("2", "SD1010002");
        cardMap.put("3", "SD1010003");
        cardMap.put("4", "SD1010004");
        cardMap.put("5", "SD1010005");
        cardMap.put("6", "SD1010006");
        cardMap.put("7", "SD1010007");
        cardMap.put("8", "SD1010008");
        cardMap.put("9", "SD1010009");
        cardMap.put("10", "SD1010010");
        cardMap.put("11", "SD1010011");

        // 销售家字典 1 万科合作方 2 中介公司 3 万科业主 4 万科员工 5 机构 6 独立经纪人 7 企业用户 8 客户 9顾问
        // 销售系统字典 SD1004001 万科合作方 SD1004002 中介公司 SD1004003 万科业主 SD1004004 万科员工 SD1004005 独立经纪人 SD1004006 企业用户 SD1004007
        // 合作机构 SD1004008 在线家
        Map<String, String> agentMap = new HashMap<String, String>();
        agentMap.put("1", "SD1004003");
        agentMap.put("2", "SD1004004");
        agentMap.put("3", "SD1004007");
        agentMap.put("4", "SD1004005");
        agentMap.put("5", "SD1004009");
        agentMap.put("6", "SD1004001");
        agentMap.put("7", "SD1004010");
        // 年龄--数据转换
        if ("age".equals(key)) {
            if (null != value && !"0".equals(value)) {
                result = ageMap.get(value);
            } else if ("0".equals(value)) {
                result = ageMap.get("1");// 默认
            } else {
                result = ageMap.get("1");// 默认
            }
        }
        // 性别--数据转换
        if ("sex".equals(key)) {
            if (null != value && !"".equals(value)) {
                result = sexMap.get(value);
            } else {
                result = sexMap.get("1");// 默认
            }
        }
        // 意向等级--数据转换
        if ("m_13".equals(key)) {
            if (null != value && !"".equals(value)) {
                result = intentionLevelIdMap.get(value);
            } else {
                result = intentionLevelIdMap.get("1");// 默认
            }
            // 如果客户为无效,上报默认为c
            if ("-2".equals(value) || "-1".equals(value)) {
                result = "SD1005003";
            }
        }
        // 来源途径--数据转换
        if ("addSource".equals(key)) {
            if (null != value && !"".equals(value)) {
                result = sourceWayIdMap.get(value);
            } else {
                result = sourceWayIdMap.get("1");// 默认
            }
        }

        // 推荐人类型--数据转换
        if ("tuijian_type".equals(key)) {
            if (null != value && !"".equals(value)) {
                result = agentMap.get(value);
            } else {
                result = "";
            }
        }

        // 证件类型
        if ("cardId".equals(key)) {
            // str index 0 cardId 1 cardType
            String[] str = value.split(",");
            if (str.length > 1) {
                if (null != str[0] && !"".equals(str[0])) {
                    if (!"0".equals(str[1]) && !"".equals(str[1])) {
                        result = cardMap.get(str[1]);
                    } else {
                        result = cardMap.get("1");// 默认
                    }
                } else {
                    result = "";
                }
            } else {
                result = "";
            }

        }
        return result;
    }

    @Override
    public SyncUpdateCustomerResultDTO syncUpdateCustomer(SyncUpdateCustomerDTO syncUpdateCustomerDTO) {
        log.info("NssSyncServiceImpl syncUpdateCustomer syncUpdateCustomerDTO={}", syncUpdateCustomerDTO);
        String code = "";
        String msg = "";

        String url = prefix_url + "/api/customerSvc/update";
        log.info("NssSyncServiceImpl syncUpdateCustomer sNumber={}, url={}", syncUpdateCustomerDTO.getSNumber(), url);
        String post_data = editUserParameter(syncUpdateCustomerDTO);
        log.info("NssSyncServiceImpl syncUpdateCustomer editUserParameter={}", post_data);
        // 客户没有姓名不上报
        if (StrUtil.isBlank(syncUpdateCustomerDTO.getName())) {
            log.error("NssSyncServiceImpl syncUpdateCustomer 客户没有姓名不上报 客户信息->{}", post_data);
            SyncUpdateCustomerResultDTO syncCustomerResultDTO = new SyncUpdateCustomerResultDTO();
            syncCustomerResultDTO.setErrCode("10001");
            syncCustomerResultDTO.setErrMsg("客户没有姓名");
            return syncCustomerResultDTO;
        }
        SyncUpdateCustomerResultDTO syncUpdateCustomerResultDTO = dealEditUser(url, post_data, syncUpdateCustomerDTO);
        log.info("NssSyncServiceImpl syncUpdateCustomer SyncUpdateCustomerResultDTO={}", syncUpdateCustomerResultDTO);
        return syncUpdateCustomerResultDTO;
    }

    /**
     * 同步修改数据 参数
     *
     * @param syncUpdateCustomerDTO
     * @return
     */
    public String editUserParameter(SyncUpdateCustomerDTO syncUpdateCustomerDTO) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String cardId = syncUpdateCustomerDTO.getLicenseNumber() + "," + syncUpdateCustomerDTO.getCertificateType();
        String cardType = getTurnValue("cardId", cardId); // 证件类型
        // 同步数据参数
        // List<Map> followUpInfoList = new ArrayList<>();
        List<Map> opportunityList = new ArrayList<>();
        Map customerMap = new HashMap();
        customerMap.put("projectId", "");// 项目id
        customerMap.put("id", syncUpdateCustomerDTO.getNssCustomerId());// 机会客户id
        customerMap.put("consultantId", syncUpdateCustomerDTO.getConsultantId());// 顾问ID
        customerMap.put("salesOrgId", syncUpdateCustomerDTO.getSalesOrgCode()); // 销售组织id
        customerMap.put("age", getTurnValue("age", syncUpdateCustomerDTO.getAge()));// 年龄
        customerMap.put("sex", getTurnValue("sex", syncUpdateCustomerDTO.getSex()));// 性别
        customerMap.put("name", syncUpdateCustomerDTO.getName());// 姓名
        customerMap.put("phoneNumber", syncUpdateCustomerDTO.getPhoneNumber()); // 客户电话
        customerMap.put("intentionLevelId", getTurnValue("m_13", syncUpdateCustomerDTO.getIntentionLevelId())); // 意向等级
        customerMap.put("sourceWayId", getTurnValue("addSource", syncUpdateCustomerDTO.getSourceWayId())); // 来源途径
        customerMap.put("customerClass", "0"); // 客户类型
        customerMap.put("marketWayId", syncUpdateCustomerDTO.getMarketWayId()); // 营销途径
        customerMap.put("certificateType", cardType); // 证件类型
        customerMap.put("licenseNumber", syncUpdateCustomerDTO.getLicenseNumber());
        customerMap.put("contactAddress", syncUpdateCustomerDTO.getContactAddress());

        customerMap.put("familyNmuber", syncUpdateCustomerDTO.getFamilyNmuber()); // 家庭电话
        customerMap.put("salesHomeId", syncUpdateCustomerDTO.getSalesHomeId()); // 销售家客户id
        customerMap.put("fusionBackgroundId", syncUpdateCustomerDTO.getFusionBackgroundId()); // 线索id
        customerMap.put("customerTypeId", syncUpdateCustomerDTO.getCustomerTypeId()); // 企业客户
        customerMap.put("agentType", getTurnValue("tuijian_type", syncUpdateCustomerDTO.getAgentType()));
        if (StrUtil.isNotEmpty(syncUpdateCustomerDTO.getAgentThirdOrgId())) {
            customerMap.put("agentThirdOrgId", syncUpdateCustomerDTO.getAgentThirdOrgId());
        } else {
            customerMap.put("agentThirdOrgId", "");
        }
        if (StrUtil.isNotEmpty(syncUpdateCustomerDTO.getAgentThirdOrgName())) {
            customerMap.put("agentThirdOrgName", syncUpdateCustomerDTO.getAgentThirdOrgName());
        } else {
            customerMap.put("agentThirdOrgName", "");
        }
        customerMap.put("agentDate", syncUpdateCustomerDTO.getAgentDate());
        customerMap.put("agentName", syncUpdateCustomerDTO.getAgentName());
        customerMap.put("agentPhone", syncUpdateCustomerDTO.getAgentPhone());
        customerMap.put("isBlack", syncUpdateCustomerDTO.getBlackFlag());

        // 2019-04-09新加字段：户籍省份，户籍市，有无购房资格，预计有购房资格日期，第一联系人，第一联系人电话
        // 判断是否是国外户籍
        if ("-1".equals(syncUpdateCustomerDTO.getBirthplacProvince())) {
            customerMap.put("province", syncUpdateCustomerDTO.getBirthplacCity());
        } else {
            // 户籍省份
            customerMap.put("province", syncUpdateCustomerDTO.getBirthplacProvince());
            // 户籍城市
            customerMap.put("city", syncUpdateCustomerDTO.getBirthplacCity());
        }
        // 购房资格，1有 0无 int类型（销售系统为：是否无购房资格：0、否，1、是，即0为有购房资格，1为无购房资格）
        if (0 == syncUpdateCustomerDTO.getIsHouseQualify()) {
            customerMap.put("purchaseQualicationsFlag", 1);
            // 无购房资格，预计获取资格日期,字符串 yyyy-MM-dd
            if (syncUpdateCustomerDTO.getHouseQualifyDate() != null
                    && syncUpdateCustomerDTO.getHouseQualifyDate() != 0) {
                Date date = new Date(syncUpdateCustomerDTO.getHouseQualifyDate() * 1000L);
                customerMap.put("planGetQualicationsDate", format.format(date));
            } else {
                customerMap.put("planGetQualicationsDate", "1900-01-01");
            }
        } else {
            customerMap.put("purchaseQualicationsFlag", 0);
            customerMap.put("planGetQualicationsDate", "1900-01-01");
        }

        // 生日 2019-07-19加
        customerMap.put("birthday", syncUpdateCustomerDTO.getBirthday());

        // 第一联系人姓名
        customerMap.put("masterContacts", syncUpdateCustomerDTO.getFirstContactName());
        // 第一联系人手机号
        customerMap.put("masterContactsPhoneNumber", syncUpdateCustomerDTO.getFirstContactMobile());

        // Map followUpInfoListMap = new HashMap();
        // followUpInfoListMap.put("consultantId", syncUpdateCustomerDTO.getConsultantId()); //顾问ID
        // followUpInfoListMap.put("salesOrgId", syncUpdateCustomerDTO.getSalesOrgCode());//销售组织id
        // followUpInfoListMap.put("pathClass", "SD1007001");//跟进类型 到访:SD1007001 跟进:SD1007002
        // followUpInfoListMap.put("followContent", "第一次到访");
        // followUpInfoList.add(followUpInfoListMap);

        Map opportunityListMap = new HashMap();
        opportunityListMap.put("intentionLevelId", getTurnValue("m_13", syncUpdateCustomerDTO.getIntentionLevelId()));// 意向等级
        opportunityList.add(opportunityListMap);
        Map postData = new HashMap();
        if (syncUpdateCustomerDTO.getCustomerTypeId().equals("1")) {
            Map customerEnterpriseMap = new HashMap();
            customerEnterpriseMap.put("businessLicense", syncUpdateCustomerDTO.getBusinessLicense());
            customerEnterpriseMap.put("legalPerson", syncUpdateCustomerDTO.getLegalPerson());
            customerEnterpriseMap.put("companyName", syncUpdateCustomerDTO.getCompanyName());
            customerEnterpriseMap.put("id", syncUpdateCustomerDTO.getNssCustomerId());
            postData.put("customerEnterprise", customerEnterpriseMap);
        }
        postData.put("opportunityList", opportunityList);
        // postData.put("followUpInfoList", followUpInfoList);
        postData.put("customer", customerMap);

        String post_data = JSONObject.toJSONString(postData);
        log.info("NssSyncServiceImpl editUserParameter post_data={}", post_data);
        return post_data;
    }

    /**
     * @param url
     * @param post_data
     * @param syncUpdateCustomerDTO
     * @return
     */
    public SyncUpdateCustomerResultDTO dealEditUser(String url, String post_data,
                                                    SyncUpdateCustomerDTO syncUpdateCustomerDTO) {

        log.info("NssSyncServiceImpl dealEditUser url={} post_data={}", url, post_data);
        SyncUpdateCustomerResultDTO syncUpdateCustomerResultDTO = new SyncUpdateCustomerResultDTO();
        String response = "";
        try {
            // 发请求前，查询redis中是否存贮了该请求
            String s = checkPostRepeat(post_data);
            if ("10005".equals(s)) {
                syncUpdateCustomerResultDTO.setErrCode(s);
                syncUpdateCustomerResultDTO.setErrMsg("redis获取异常");
                return syncUpdateCustomerResultDTO;
            } else if ("10006".equals(s)) {
                syncUpdateCustomerResultDTO.setErrCode(s);
                syncUpdateCustomerResultDTO.setErrMsg("重复请求，已拦截");
                return syncUpdateCustomerResultDTO;
            }

            response = HttpUtil.doPutWithJson(url, post_data, setHeader());
            log.info("NssSyncServiceImpl dealEditUser sNumber={},response={}", syncUpdateCustomerDTO.getSNumber(),
                    response);

            JSONObject jsonStr = JSONObject.parseObject(response);
            String tooManyRequest = jsonStr.getString("msg");
            if (tooManyRequest != null && "Too Many Requests".equals(tooManyRequest)) {
                log.error("NssSyncServiceImpl dealEditUser Too Many Requests phoneNumber postData={}", post_data);
                syncUpdateCustomerResultDTO.setErrCode("10005");
                syncUpdateCustomerResultDTO.setErrMsg("Too Many Requests");
                return syncUpdateCustomerResultDTO;
            }
            // 正确json
            JSONObject state = (JSONObject) jsonStr.get("state");
            Integer errCode = (Integer) state.get("errCode");
            String code = errCode.toString();
            String msg = (String) state.get("errMsg");

            if (!"10000".equals(code)) {
                JSONObject body = jsonStr.getJSONObject("body");
                if (body != null) {
                    String errMsg = body.getString("errMsg");
                    if (StrUtil.isNotBlank(errMsg)) {
                        msg = errMsg;
                    }
                }
            }

            syncUpdateCustomerResultDTO.setErrCode(code);
            syncUpdateCustomerResultDTO.setErrMsg(msg);
            log.info("editCustomerRedisErrcode ->{} ,判断结果 ->{}", code, "10000".equals(code));
            if ("10000".equals(code)) {
                log.info("上报拦截功能 上报写redis 1564 ：" + DigestUtils.md5Hex(post_data));
                // 过去时间：24小时
                log.info("editCustomerRedis中设置值 postData->{} key->{} value->{} lostTime->{}", post_data,
                        "nssPost:" + DigestUtils.md5Hex(post_data), "nss", lostTime);
                redisSingleCacheTemplate.set("nssPost:" + DigestUtils.md5Hex(post_data), "nss", lostTime);
            }
            log.info("NssSyncServiceImpl dealEditUser syncUpdateCustomerResultDTO={}", syncUpdateCustomerResultDTO);
            return syncUpdateCustomerResultDTO;
        } catch (Exception e) {
            syncUpdateCustomerResultDTO.setErrCode("10001");
            syncUpdateCustomerResultDTO.setErrMsg(e.getMessage());

            log.error("NssSyncServiceImpl dealEditUser 编辑客户上报异常 客户->{} 异常原因->{}", e);
            return syncUpdateCustomerResultDTO;
        }

    }

    private String checkPostRepeat(String postData) {
        try {
            String postDataMd5 = DigestUtils.md5Hex(postData);
            log.info("上报拦截功能 上报读redis 1585->{}", postDataMd5);
            Object redisCatch = redisSingleCacheTemplate.get("nssPost:" + postDataMd5);
            log.info("销售系统请求从redis中取值 ->{}", redisCatch);
            if (redisCatch != null && "nss".equals(redisCatch.toString())) {
                // 重复请求，拦截下来
                log.info("重复请求已拦截 ->{}", postData);
                return "10006";
            }
        } catch (Exception e) {
            log.info("redis获取异常 ->{}", e);
            return "10005";
        }
        return "10000";
    }

    @Override
    public List<SyncPullCustomerResultDTO> syncPullCustomer(SyncPullCustomerDTO syncPullCustomerDTO) {
        log.info("NssSyncServiceImpl syncPullCustomer syncPullCustomerDTO={}", syncPullCustomerDTO);
        List<SyncPullCustomerResultDTO> syncPullCustomerResultDTOs = new ArrayList<SyncPullCustomerResultDTO>();
        String url = prefix_url + "/api/relationLeadsCustomerSvc/search/list/CusDetailList";
        log.info("NssSyncServiceImpl syncPullCustomer url={}", url);
        Map<String, Object> pramaMap = new HashMap<>();
        pramaMap.put("size", 2000);
        pramaMap.put("RelationLeadsCustomerSvc_data_OpportunityRelation_updateDate_start",
                syncPullCustomerDTO.getUpdateTime());
        pramaMap.put("RelationLeadsCustomerSvc_data_OpportunityRelation_customerClass", 0);
        String postData = JSONObject.toJSONString(pramaMap);
        log.info("NssSyncServiceImpl syncPullCustomer postData={}" + postData);
        String response = "";
        try {
            response = HttpUtil.doPostWithJson(url, postData, setHeader());
            log.info("NssSyncServiceImpl syncPullCustomer response={}", response);

            JSONObject jsonObject = JSONObject.parseObject(response);
            JSONObject body = (JSONObject) jsonObject.get("body");
            if (body.get("totalCount") != null && (int) body.get("totalCount") > 0) {

                JSONArray list = (JSONArray) body.get("list");

                for (int j = 0; j < list.size(); j++) {

                    SyncPullCustomerResultDTO syncPullCustomerResultDTO = new SyncPullCustomerResultDTO();
                    JSONObject customer = (JSONObject) ((JSONObject) list.get(j)).get("customer");
                    JSONObject opportunityRelation = (JSONObject) ((JSONObject) list.get(j)).get("opportunityRelation");
                    syncPullCustomerResultDTO.setId(customer.getString("id"));
                    syncPullCustomerResultDTO.setName(customer.getString("name"));
                    syncPullCustomerResultDTO.setPhoneNumber(customer.getString("phoneNumber"));
                    syncPullCustomerResultDTO.setLicenseNumber(customer.getString("licenseNumber"));
                    syncPullCustomerResultDTO
                            .setCertificateType(getToValue("cardType", customer.getString("certificateType")));
                    syncPullCustomerResultDTO.setContactAddress(customer.getString("contactAddress"));
                    syncPullCustomerResultDTO.setSex(getToValue("sex", customer.getString("sex")));
                    syncPullCustomerResultDTO.setAge(getToValue("age", customer.getString("age")));
                    syncPullCustomerResultDTO.setMarketWayId(customer.getString("marketWayId"));
                    syncPullCustomerResultDTO.setUpdateDate(opportunityRelation.getString("updateDate"));

                    // 2019-04-09 新增字段：户籍省份，户籍市，
                    syncPullCustomerResultDTO.setBirthplacProvince(customer.getString("province"));
                    syncPullCustomerResultDTO.setBirthplacCity(customer.getString("city"));
                    syncPullCustomerResultDTOs.add(syncPullCustomerResultDTO);
                }
            }
        } catch (Exception e) {
            log.info("NssSyncServiceImpl syncPullCustomer 拉取客户资料异常" + e);
            return syncPullCustomerResultDTOs;
        }

        log.info("NssSyncServiceImpl syncPullCustomer syncPullCustomerResultDTOs={}", syncPullCustomerResultDTOs);

        return syncPullCustomerResultDTOs;
    }

    /**
     * 销售系统数据转成转销售家数据
     *
     * @param key
     * @param value
     * @return
     */
    public String getToValue(String key, String value) {
        String result = "";
        // 性别 男:SD1001001 女:SD1001002
        Map<String, String> sexMap = new HashMap<String, String>();
        sexMap.put("SD1001001", "1");
        sexMap.put("SD1001002", "2");
        sexMap.put("SD1001003", "3");
        // 销售系统证件类型 SD1010001 身份证 SD1010002 军官证 SD1010003 护照 SD1010004 户口簿 SD1010005 台湾通行证 SD1010006 台湾身份证
        // SD1010007 港澳身份证 SD1010008 营业执照 SD1010009 组织机构代码 SD1010010 税务登记证号 SD1010011 其他
        Map<String, String> cardMap = new HashMap<String, String>();
        cardMap.put("SD1010001", "1");
        cardMap.put("SD1010002", "2");
        cardMap.put("SD1010003", "3");
        cardMap.put("SD1010004", "4");
        cardMap.put("SD1010005", "5");
        cardMap.put("SD1010006", "6");
        cardMap.put("SD1010007", "7");
        cardMap.put("SD1010008", "8");
        cardMap.put("SD1010009", "9");
        cardMap.put("SD1010010", "10");
        cardMap.put("SD1010011", "11");
        // 年龄 销售家 60以上: 6 50-60:5 40-50:4 30-40:3 20-30:2 10-20:1
        // （销售系统 对应） 60以上: SD1002006 50-60:SD1002005 40-50:SD1002004 30-40:SD1002003 20-30:SD1002002 10-20:SD1002001
        Map<String, String> ageMap = new HashMap<String, String>();
        ageMap.put("SD1002001", "1");
        ageMap.put("SD1002002", "2");
        ageMap.put("SD1002003", "3");
        ageMap.put("SD1002004", "4");
        ageMap.put("SD1002005", "5");
        ageMap.put("SD1002006", "6");
        // 年龄--数据转换
        if ("age".equals(key)) {
            if (null != value && !"".equals(value)) {
                result = ageMap.get(value);
            } else {
                result = ageMap.get("SD1002001");// 默认
            }
        }
        // 性别--数据转换
        if ("sex".equals(key)) {
            if (null != value && !"".equals(value)) {
                result = sexMap.get(value);
            } else {
                result = "3";// 默认
            }
        }
        if ("cardType".equals(key)) {
            if (null != value && !"".equals(value)) {
                result = cardMap.get(value);
            } else {
                result = cardMap.get("SD1010001");// 默认
            }
        }
        return result;
    }

    @Override
    public SyncTurnCustomerResultDTO syncTurnCustomer(SyncTurnCustomerDTO syncTurnCustomerDTO) {

        log.info("NssSyncServiceImpl syncTurnCustomer syncTurnCustomerDTO={}", syncTurnCustomerDTO);
        SyncTurnCustomerResultDTO syncTurnCustomerResultDTO = new SyncTurnCustomerResultDTO();
        String url = prefix_url + "/api/customerSvc/dispatchCustomer";
        log.info("NssSyncServiceImpl syncTurnCustomer sNumber={},url={}", syncTurnCustomerDTO.getSNumber(), url);

        List listNewUserId = new ArrayList();
        listNewUserId.add(syncTurnCustomerDTO.getNssCustomerId());
        Map post_data = new HashMap();
        post_data.put("customerIds", listNewUserId); // 客户id
        post_data.put("consultantId", syncTurnCustomerDTO.getNssConsultantId());// 顾问ID
        post_data.put("oType", "0");
        post_data.put("salesOrgId", syncTurnCustomerDTO.getSalesOrgCode());
        post_data.put("toConsultantId", syncTurnCustomerDTO.getNssConsultantId());
        String jsonstr = JSONObject.toJSONString(post_data);
        log.info("NssSyncServiceImpl syncTurnCustomer jsonstr={}", jsonstr);

        // 发请求前，查询redis中是否存贮了该请求
        // String s = checkPostRepeat(jsonstr);
        // if ("10005".equals(s)){
        // syncTurnCustomerResultDTO.setErrCode(s);
        // syncTurnCustomerResultDTO.setErrMsg("redis获取异常");
        // return syncTurnCustomerResultDTO;
        // }else if ("10006".equals(s)){
        // syncTurnCustomerResultDTO.setErrCode(s);
        // syncTurnCustomerResultDTO.setErrMsg("重复请求，已拦截");
        // return syncTurnCustomerResultDTO;
        // }

        // 转客操作接口
        String response = HttpUtil.doPostWithJson(url, jsonstr, setHeader());
        log.info("NssSyncServiceImpl syncTurnCustomer sNumber={},response={}", syncTurnCustomerDTO.getSNumber(),
                response);

        JSONObject jsonStr = JSONObject.parseObject(response);

        String tooManyRequest = jsonStr.getString("msg");
        if (tooManyRequest != null && "Too Many Requests".equals(tooManyRequest)) {
            log.error("NssSyncServiceImpl syncCustome Too Many Requests phoneNumber postData={}", post_data);
            syncTurnCustomerResultDTO.setErrCode("10005");
            syncTurnCustomerResultDTO.setErrMsg("Too Many Requests");
            return syncTurnCustomerResultDTO;
        }
        JSONObject state = (JSONObject) jsonStr.get("state");
        if (state != null) {
            String errCode = state.getString("errCode");
            String errMsg = state.getString("errMsg");
            syncTurnCustomerResultDTO.setErrCode(errCode);
            syncTurnCustomerResultDTO.setErrMsg(errMsg);
            // if ("10000".equals(errCode)){
            // //过去时间：24小时
            // log.info("turnUserRedis中设置值 postData->{} key->{} value->{} lostTime->{}", post_data,"nssPost:" +
            // DigestUtils.md5Hex(jsonstr),"nss" ,lostTime);
            // redisSingleCacheTemplate.set("nssPost:" + DigestUtils.md5Hex(jsonstr) , "nss", lostTime);
            // }
        }
        log.info("NssSyncServiceImpl syncTurnCustomer syncTurnCustomerResultDTO={}", syncTurnCustomerResultDTO);

        return syncTurnCustomerResultDTO;
    }

    @Override
    public SyncCognitiveResultDTO syncCognitive(SyncCognitiveDTO syncCognitiveDTO) {
        log.info("NssSyncServiceImpl syncCognitive syncCognitiveDTO={}", syncCognitiveDTO);

        SyncCognitiveResultDTO syncCognitiveResultDTO = new SyncCognitiveResultDTO();
        String errcode = "";
        String errmsg = "";
        Map map = new HashMap();
        String url = prefix_url + "/api/dictItemSvc/batch/save";
        log.info("NssSyncServiceImpl syncCognitive url={}", url);

        // 父节点类别 SD1021001 网络媒体 SD1021002 传统媒体 SD1021003 外拓渠道 SD1021004 老带新 SD1021005 朋友推荐 SD1021006 转介
        // SD1021007 路过 SD1021008 活动
        // 销售家 1 网络媒体 2 传统媒体 3 外拓渠道 4 老带新 5 朋友推荐 6转介 7 路过 8活动
        Map arr = new HashMap();
        arr.put("1", "SD1021001");
        arr.put("2", "SD1021002");
        arr.put("3", "SD1021003");
        arr.put("4", "SD1021004");
        arr.put("5", "SD1021005");
        arr.put("6", "SD1021006");
        arr.put("7", "SD1021007");
        arr.put("8", "SD1021008");

        // 封装参数
        Map post_data_item = new HashMap();
        post_data_item.put("batch", "");
        post_data_item.put("description", "");
        post_data_item.put("dictId", "SD1021");
        post_data_item.put("parentId", arr.get(syncCognitiveDTO.getClassValue()));
        post_data_item.put("isEnable", syncCognitiveDTO.getIsEnable());
        post_data_item.put("isEdit", "1");
        post_data_item.put("name", syncCognitiveDTO.getName());
        post_data_item.put("salesOrgId", syncCognitiveDTO.getSalesOrgCode());
        if (syncCognitiveDTO.getNssChannelId() != null && !syncCognitiveDTO.getNssChannelId().equals("")) { //
            post_data_item.put("id", syncCognitiveDTO.getNssChannelId());
        }
        Map post_data = new HashMap();
        post_data.put("dictItem", post_data_item);
        List list = new LinkedList();
        list.add(post_data);
        String jsonstr = JSONObject.toJSONString(list);
        map.put("send_content", jsonstr); // 发送内容

        log.info("NssSyncServiceImpl syncCognitive jsonstr={}", jsonstr);

        String response = HttpUtil.doPutWithJson(url, jsonstr, setHeader());
        log.info("NssSyncServiceImpl syncCognitive response={}", response);

        map.put("receive_content", response);
        JSONArray json_arr = JSON.parseArray(response);
        if (json_arr.size() > 0) {
            JSONObject json = (JSONObject) json_arr.get(0);
            JSONObject body = (JSONObject) json.get("body");
            JSONObject state = (JSONObject) json.get("state");
            // if (!((JSONObject)body.get("dictItem")).getString("id").equals("")){ //获取id不为空
            if (!"".equals(((JSONObject) body.get("dictItem")).getString("id"))) {
                String dictId = ((JSONObject) body.get("dictItem")).getString("id");
                String name = ((JSONObject) body.get("dictItem")).getString("name");
                syncCognitiveResultDTO.setNssChannelId(dictId);
                syncCognitiveResultDTO.setNssChannelName(name);
            }
            errcode = state.getString("errCode");
            errmsg = state.getString("errMsg");
        }
        syncCognitiveResultDTO.setErrCode(errcode);
        syncCognitiveResultDTO.setErrMsg(errmsg);

        log.info("NssSyncServiceImpl syncCognitive syncCognitiveResultDTO={}", syncCognitiveResultDTO);

        return syncCognitiveResultDTO;

    }

    @Override
    public List<SyncCustomerResultDTO> getInfoBySalesOrgIdAndCusId(String postData) {
        String url = prefix_url + "/api/customerSvc/getInfoBySalesOrgIdAndCusId";
        log.info("获取同一项目下所有客户 NssSyncServiceImpl getInfoBySalesOrgIdAndCusId postData ->{} url->{}", postData, url);
        // 调用销售系统接口
        JSONObject jsonObject;
        List<SyncCustomerResultDTO> resultDTOList = new ArrayList<>();
        try {
            String response = HttpUtil.doPostWithJson(url, postData, setHeader());
            jsonObject = JSON.parseObject(response);
            log.info("获取同一项目下所有客户 NssSyncServiceImpl getInfoBySalesOrgIdAndCusId response ->{}", response);
        } catch (Exception e) {
            log.info("获取同一项目下所有客户异常 NssSyncServiceImpl getInfoBySalesOrgIdAndCusId e->{}", e);
            return null;
        }

        try {
            JSONObject state = jsonObject.getJSONObject("state");
            if (!"10000".equals(state.getString("errCode"))) {
                log.info("获取同一项目下所有客户异常 解析销售系统返回失败 state->{}", state);
                return null;
            }
            JSONArray body = jsonObject.getJSONArray("body");
            if (body != null && body.size() > 0) {
                for (int i = 0; i < body.size(); i++) {
                    JSONObject jsonObject1 = body.getJSONObject(i);
                    SyncCustomerResultDTO syncCustomerResultDTO = new SyncCustomerResultDTO();
                    syncCustomerResultDTO.setSalesOrgCode(jsonObject1.getString("salesOrgId"));
                    syncCustomerResultDTO.setNssCustomerId(jsonObject1.getString("customerId"));
                    resultDTOList.add(syncCustomerResultDTO);
                }
            }
        } catch (Exception e) {
            log.info("获取同一项目下所有客户异常 解析销售系统返回失败 e->{}", e);
            return null;
        }
        return resultDTOList;
    }

    @Override
    public Boolean insertTeamRelation(String postData) {
        String url = prefix_url + "/api/object/teamRelation/insert";
        JSONObject jsonObject;

        log.info("团队新增，同步团队关系至翼销售 NssSyncServiceImpl insertTeamRelation postData ->{} url->{}", postData, url);
        try {
            String response = HttpUtil.doPostWithJson(url, postData, setHeader());
            log.info("团队新增，同步团队关系至翼销售 NssSyncServiceImpl insertTeamRelation response ->{}", response);
            jsonObject = JSON.parseObject(response);
        } catch (Exception e) {
            log.info("团队新增，同步团队关系至翼销售 NssSyncServiceImpl insertTeamRelation e->{}", e);
            return false;
        }

        try {
            JSONObject state = jsonObject.getJSONObject("state");
            if (!"10000".equals(state.getString("errCode"))) {
                log.info("团队新增，同步团队关系至翼销售 解析销售系统返回失败 state->{}", state);
                return false;
            }
            JSONObject body = jsonObject.getJSONObject("body");
            if (body != null && StrUtil.isNotBlank(body.getString("id"))) {
                return true;
            }
            return false;
        } catch (Exception e) {
            log.info("团队新增，同步团队关系至翼销售 解析销售系统返回失败 e->{}", e);
            return false;
        }
    }

    @Override
    public Boolean updateConsultantName(SysUserDTO sysUserDTO) {
        String url = prefix_url + "/api/object/sysUser/update";
        Map<String, Object> postMap = new HashMap<>();
        postMap.put("id", sysUserDTO.getNssConsultantId());
        postMap.put("name", sysUserDTO.getName());
        String postData = JsonUtil.obj2Str(postMap);
        JSONObject jsonObject;

        log.info("修改顾问名称 NssSyncServiceImpl updateConsultnatName postData ->{} url->{}", postData, url);
        String response = HttpUtil.doPutWithJson(url, postData, setHeader());
        log.info("修改顾问名称 NssSyncServiceImpl updateConsultnatName response ->{}", response);
        jsonObject = JSON.parseObject(response);
        JSONObject state = jsonObject.getJSONObject("state");
        if (!"10000".equals(state.getString("errCode"))) {
            log.error("修改顾问名称 解析销售系统返回失败 state->{}", state);
            return false;
        }
        JSONObject body = jsonObject.getJSONObject("body");
        if (body == null || StrUtil.isBlank(body.getString("id"))) {
            log.error("修改顾问名称 解析销售系统返回失败");
            return false;
        }
        return true;
    }

    @Override
    public Boolean changeTeam(String postData) {
        String url = prefix_url + "/api/saleMemberSvc/changeTeamAndTransferCustomer";
        JSONObject jsonObject;
        log.info("顾问换团队 NssSyncServiceImpl changeTeam postData ->{} url->{}", postData, url);
        String response = HttpUtil.doPostWithJson(url, postData, setHeader());
        jsonObject = JSON.parseObject(response);
        log.info("顾问换团队 NssSyncServiceImpl changeTeam response ->{}", response);
        JSONObject state = jsonObject.getJSONObject("state");
        if (!"10000".equals(state.getString("errCode"))) {
            JSONObject body = jsonObject.getJSONObject("body");
            String message = body.getString("message");
            if (StrUtil.isNotBlank(message)) {
                log.error("顾问换团队异常 异常Code：" + state.getString("errCode") + " 异常原因：" + message);
                return false;
            } else {
                log.error("顾问换团队异常 解析销售系统返回失败");
                return false;
            }
        }
        return true;
    }

    @Override
    public Boolean syncSalesteamDelete(String postData) {
        String url = prefix_url + "/api/object/teamRelation/delete";
        JSONObject jsonObject;

        log.info("同步删除团队 NssSyncServiceImpl syncSalesteamDelete postData ->{} url->{}", postData, url);
        try {
            String response = HttpUtil.doPostWithJson(url, postData, setHeader());
            jsonObject = JSON.parseObject(response);
            log.info("同步删除团队 NssSyncServiceImpl syncSalesteamDelete response ->{}", response);
        } catch (Exception e) {
            log.info("同步删除团队 NssSyncServiceImpl syncSalesteamDelete e->{}", e);
            return false;
        }
        try {
            JSONObject state = jsonObject.getJSONObject("state");
            if (!"10000".equals(state.getString("errCode"))) {
                log.info("同步删除团队异常 解析销售系统返回失败 state->{}", state);
                return false;
            }
        } catch (Exception e) {
            log.info("同步删除团队异常 解析销售系统返回失败 e->{}", e);
            return false;
        }
        return true;
    }

    @Override
    public Boolean saleMemberUpdate(SysUserDTO sysUserDTO) {
        String url = prefix_url + "/api/saleMemberSvc/batchUpdateSalesMember";
        Map<String, Object> postMap = new HashMap<>();
        postMap.put("accountId", sysUserDTO.getNssConsultantId());
        postMap.put("name", sysUserDTO.getName());
        postMap.put("orgId", sysUserDTO.getSalesOrgIds());
        String postData = JsonUtil.obj2Str(postMap);
        JSONObject jsonObject;

        log.info("修改顾问团队成员名称 NssSyncServiceImpl saleMemberUpdate postData ->{} url->{}", postData, url);
        String response = HttpUtil.doPostWithJson(url, postData, setHeader());
        log.info("修改顾问团队成员名称 NssSyncServiceImpl saleMemberUpdate response ->{}", response);
        jsonObject = JSON.parseObject(response);
        JSONObject state = jsonObject.getJSONObject("state");
        if (!"10000".equals(state.getString("errCode"))) {
            log.error("修改顾问团队成员名称 解析销售系统返回失败 state->{}", state);
            return false;
        }
        return true;
    }

    /**
     * 通知翼销售礼券信息
     *
     * @param syncCouponInfoDTO
     * @return
     */
    @Override
    public void notifyDiscountCoupon(SyncCouponInfoDTO syncCouponInfoDTO) {
        log.info("NssSyncServiceImpl notifyDiscountCoupon syncCouponInfoDTO:{}", syncCouponInfoDTO);
        try {
            // api/couponCustomerSvc/sync/sav
            String url = prefix_url + "/api/couponSvc/sync/save";
            String body = JSONUtil.toJsonStr(syncCouponInfoDTO);
            log.info("NssSyncServiceImpl notifyDiscountCoupon url:{} body:{}", url, body);
            String response = HttpUtil.doPostWithJson(url, body, setHeader());
            log.info("NssSyncServiceImpl notifyDiscountCoupon  response={}", response);
        } catch (Exception e) {
            log.error("notifyDiscountCoupon error:{}", ThrowableUtil.stackTraceToString(e));
        }
    }

    /**
     * 通知翼销售礼券信息
     *
     * @param syncCouponCustomerInfoDTO
     * @return
     */
    @Override
    public void notifyDiscountCouponCustomer(SyncCouponCustomerInfoDTO syncCouponCustomerInfoDTO) {
        log.info("NssSyncServiceImpl notifyDiscountCouponCustomer syncCouponCustomerInfoDTO:{}", syncCouponCustomerInfoDTO);
        try {
            String url = prefix_url + "/api/couponCustomerSvc/sync/save";
            String body = JSONUtil.toJsonStr(syncCouponCustomerInfoDTO);
            log.info("NssSyncServiceImpl notifyDiscountCouponCustomer url:{} body:{}", url, body);
            String response = HttpUtil.doPostWithJson(url, body, setHeader());
            log.info("NssSyncServiceImpl notifyDiscountCouponCustomer  response={}", response);
        } catch (Exception e) {
            log.error("notifyDiscountCouponCustomer error:{}", ThrowableUtil.stackTraceToString(e));
        }
    }
}

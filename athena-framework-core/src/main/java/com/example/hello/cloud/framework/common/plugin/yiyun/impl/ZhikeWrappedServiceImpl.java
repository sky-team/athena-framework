package com.example.hello.cloud.framework.common.plugin.yiyun.impl;

import com.google.common.collect.Lists;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.constant.CustomerTags;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.customer.QuerySpecialTagBizParam;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.customer.QuerySpecialTagResData;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.CustomerInfoService;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.helloWrappedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author zhoujj07
 * @ClassName: helloWrappedServiceImpl
 * @Description: 基于翼云的相关服务包装的服务
 * @date 2018/8/23
 */
@Service
public class helloWrappedServiceImpl implements helloWrappedService {

    private final CustomerInfoService customerInfoService;

    @Autowired
    public helloWrappedServiceImpl(CustomerInfoService customerInfoService) {
        this.customerInfoService = customerInfoService;
    }

    @Override
    public ApiResponse<Boolean> judgeEmployee(String phone) {
        String employeeTag = CustomerTags.example_EMPLOYEE_TAG;
        return judgeCustomerTag(phone, employeeTag, null);
    }

    @Override
    public ApiResponse<Boolean> judgeOwnerByCertificateNo(String phone, String certificateNo) {
        String ownerTag = CustomerTags.example_HOUSE_OWNER_TAG;
        return judgeCustomerTag(phone, ownerTag, certificateNo);
    }

    @Override
    public ApiResponse<Boolean> judgeOwnerByPhone(String phone) {
        String ownerTag = CustomerTags.example_HOUSE_OWNER_TAG;
        return judgeCustomerTag(phone, ownerTag, null);
    }

    private ApiResponse<Boolean> judgeCustomerTag(String phone, String tag, String certificateNo) {
        QuerySpecialTagBizParam bizParam = new QuerySpecialTagBizParam();
        bizParam.setPhoneNumber(phone);
        bizParam.setCertificateNo(certificateNo);
        List<String> tagList = Lists.newArrayList();
        tagList.add(tag);
        bizParam.setTags(tagList);
        ApiResponse<QuerySpecialTagResData> respResult = customerInfoService.querySpecialTag(bizParam);
        boolean judgeResult = false;
        if (respResult.isOk()) {
            QuerySpecialTagResData resData = respResult.getData();
            if (resData != null && !CollectionUtils.isEmpty(resData.getTagResultMap())) {
                if (resData.getTagResultMap().getOrDefault(tag, false)) {
                    judgeResult = true;
                }
            }
        }
        return ApiResponse.success(judgeResult);
    }

}

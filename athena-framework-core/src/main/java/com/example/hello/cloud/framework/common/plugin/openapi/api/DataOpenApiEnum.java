package com.example.hello.cloud.framework.common.plugin.openapi.api;

import com.example.hello.cloud.framework.common.plugin.openapi.core.OpenApiServiceType;
import com.example.hello.cloud.framework.common.plugin.openapi.core.OpenApiServiceType;

/**
 * 万科API服务市场接口服务 数据中台API
 *
 * @author v-linxb
 * @create 2019/9/4
 **/
public enum DataOpenApiEnum implements OpenApiServiceType {

    CUSQUERY_VKCRM_OWNER("/cusQuery/vkCrm/owner", "223", "万科物业业主身份信息API"),
    //    CUSQUERY_TAG_TREE("/hello/queryTagTreeList", "387", "查询标签树"),
//    CUSQUERY_TAG_CUS_LIST("/hello/queryCusListByTag", "388", "查询标签客户");
    CUSQUERY_TAG_TREE("/hello/queryTagTreeList", "360", "查询标签树"),
    CUSQUERY_TAG_CUS_LIST("/hello/queryCusListByTag", "361", "查询标签客户");

    private static final String BASIC_URL = "/v1.0/open-api/v1";

    private String url;
    private String serviceId;
    private String desc;

    DataOpenApiEnum(String url, String serviceId, String desc) {
        this.url = url;
        this.serviceId = serviceId;
        this.desc = desc;
    }

    /**
     * api地址
     *
     * @return api地址
     */
    @Override
    public String url() {
        return BASIC_URL + this.url;
    }

    /**
     * serviceId
     *
     * @return
     */
    @Override
    public String serviceId() {
        return this.serviceId;
    }

    /**
     * api描述
     *
     * @return api描述
     */
    @Override
    public String desc() {
        return this.desc;
    }
}

package com.example.hello.cloud.framework.common.plugin.openapi.inf;

import com.example.hello.cloud.framework.common.plugin.openapi.dto.VkCrmOwnerReq;
import com.example.hello.cloud.framework.common.plugin.openapi.dto.VkCrmOwnerResp;
import com.example.hello.cloud.framework.common.plugin.openapi.dto.VkCrmOwnerReq;
import com.example.hello.cloud.framework.common.plugin.openapi.dto.VkCrmOwnerResp;

import java.util.List;

/**
 * 万科物业业主身份信息service
 *
 * @author v-linxb
 * @create 2019/9/5
 **/
public interface VkCrmApiService {

    /**
     * 根据手机号/身份证号查询物业业主信息
     *
     * @param vkCrmOwnerReq
     * @return
     */
    List<VkCrmOwnerResp> crmOwnerQuery(VkCrmOwnerReq vkCrmOwnerReq);
}

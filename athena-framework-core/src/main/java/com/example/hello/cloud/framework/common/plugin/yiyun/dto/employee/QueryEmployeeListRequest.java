package com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;

import java.util.List;

/**
 * 员工信息请求入参
 */
@Data
public class QueryEmployeeListRequest extends BaseYiyunRequestData {
    /**
     * 手机号
     */
    private List<String> mobiles;
}
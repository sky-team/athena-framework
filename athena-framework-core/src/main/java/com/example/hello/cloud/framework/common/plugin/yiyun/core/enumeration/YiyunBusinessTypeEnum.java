package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

public enum YiyunBusinessTypeEnum {
    CHIP_ORDER_INIT("chipOrder_init","认筹_正常"),
    CHIP_ORDER_TOBUY("chipOrder_toBuy","已转认购"),
    CHIP_ORDER_CANCEL("chipOrder_cancel","认筹_已退"),
    CHIP_ORDER_TRANSFER("chipOrder_transfer","认筹_已转方案"),
    BUY_ORDER_PENDING_APPROVE("buyOrder_pendingApprove","认购_待审"),
    BUY_ORDER_INIT("buyOrder_init","认购_正常"),
    BUY_ORDER_TRANSFER("buyOrder_transfer","认购_已转"),
    BUY_ORDER_BUY_CANCEL("buyOrder_buyCancel","认购_已退"),
    BUY_ORDER_BUY_CHANGE("buyOrder_buyChange","认购_已换"),
    SIGN_ORDER_INIT("signOrder_init","签约_正常"),
    SIGN_ORDER_PENDING_APPROVE("signOrder_pendingApprove","签约_待审"),
    SIGN_ORDER_TRANSFER("signOrder_transfer","签约_已转"),
    SIGN_ORDER_SIGN_CHANGE("signOrder_signChange","签约_已转"),
    SIGN_ORDER_REVOCATION("signOrder_revocation","签约_撤销签约"),
    ;

    private String key;

    private String value;

    YiyunBusinessTypeEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

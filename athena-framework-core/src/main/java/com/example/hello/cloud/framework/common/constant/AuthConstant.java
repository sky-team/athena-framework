package com.example.hello.cloud.framework.common.constant;

/**
 * auth认证授权相关常量
 *
 * @author zhanj04
 * @date 2019/10/24 16:22
 */
public class AuthConstant {

    /**
     * JWT的生成秘钥
     */
    public static final String JWT_SECRET = "hello-cloud-auth";

    /**
     * JWT发行商
     */
    public static final String ISSUER = "hello";

    /**
     * 存放在header中的，由用户中心颁发的当前登录用户第三方ID
     */
    public static final String CURRENT_USER_HEADER_THIRD_ID = "thirdId";

    /**
     * 存放在header中的，由用户中心颁发的当前登录用户第三方ID类型
     */
    public static final String CURRENT_USER_HEADER_THIRD_TYPE = "thirdType";

    /**
     * 存放在header中的，由用户中心颁发的当前登录用户第三方sessionKey
     */
    public static final String CURRENT_USER_HEADER_SESSION_KEY = "session_key";

    /**
     * 存放在header中的，由用户中心颁发的当前登录用户主账号ID的key
     */
    public static final String CURRENT_USER_HEADER_MAIN_ACCOUNT_ID = "mainAccountId";

    /**
     * 存放在header中的，由用户中心颁发的当前登录用户微信账号ID的key
     */
    public static final String CURRENT_USER_HEADER_WECHAT_ACCOUNT_ID = "wechatAccountId";

    /**
     * 存放在header中的，由知客系统用户ID的key
     */
    public static final String CURRENT_USER_HEADER_USER_ID = "userId";

    /**
     * 存放在header中的，当前登录用户的JWT的key
     */
    public static final String AUTHORIZATION_HEADER = "Authorization";

    /**
     * 存储orgId
     */
    public static final String CURRENT_USER_ORG_ID = "orgId";

    /**
     * 存放在header中的，由用户中心颁发的当前登录用户手机号
     */
    public static final String CURRENT_USER_HEADER_PHONE = "phone";

    /**
     * 存放在header中的，由用户中心颁发的当前登录用户手机号
     */
    public static final String USER_ACCESS_INFO= "userAccessInfo";

    /**
     * 客户端IP
     */
    public static final String  CLIENT_IP = "clientIp";

    /**
     * JWT的最小长度
     */
    public static final Integer JWT_LEAST_LENGTH = 7;

    /**
     * Token 前缀
     */
    public static final String TOKEN_BEARER = "Bearer:";

    /**
     * JWT 生成算法
     */
    public static final String JWT_ALGORITHM = "HS512";

    /**
     * JWT 生成算法
     */
    public static final String JWT_TYPE = "JWT";

    /**
     * token黑名单 存储key
     */
    public static final String JWT_BLACKLIST_KEY = "JWT_BLACKLIST::%s";

    /**
     * refresh token 存储key
     */
    public static final String JWT_REFRESH_TOKEN_KEY = "JWT_REFRESH_TOKEN::%s";

    /**
     * JWT_TOKEN_KEY
     */
    public static final String JWT_TOKEN_KEY = "JWT_TOKEN::%s";

    /**
     * refresh_token
     */
    public static final String REFRESH_TOKEN = "refresh_token";

}

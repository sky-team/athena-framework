package com.example.hello.cloud.framework.common.config;

import com.example.hello.cloud.framework.common.threadpool.ThreadPoolCreate;
import com.example.hello.cloud.framework.common.threadpool.ThreadPoolCreate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.task.TaskExecutionAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 提供支持traceID的线程池
 *
 * @author yingc04
 * @create 2019/11/26
 */
@Configuration
@Slf4j
@EnableAutoConfiguration(exclude = TaskExecutionAutoConfiguration.class)
public class ThreadTraceConfig {
    @Value("${threadpool.corePoolSize:200}")
    private Integer corePoolSize;
    @Value("${threadpool.maxPoolSize:200}")
    private Integer maxPoolSize;
    @Value("${threadpool.queueCapacity:20000}")
    private Integer queueCapacity;
    @Value("${threadpool.waitForTasksToCompleteOnShutdown:true}")
    private Boolean waitForTasksToCompleteOnShutdown;
    @Value("${threadpool.awaitTerminationSeconds:60}")
    private Integer awaitTerminationSeconds;

    /**
     * 初始主线程池
     */
    @Bean("asyncExecutor")
    public ThreadPoolTaskExecutor initMainPool() {
        ThreadPoolCreate.ThreadPoolProperties properties = new ThreadPoolCreate.ThreadPoolProperties();
        properties.setCorePoolSize(corePoolSize);
        properties.setMaxPoolSize(maxPoolSize);
        properties.setQueueCapacity(queueCapacity);
        properties.setWaitForTasksToCompleteOnShutdown(waitForTasksToCompleteOnShutdown);
        properties.setAwaitTerminationSeconds(awaitTerminationSeconds);
        ThreadPoolTaskExecutor taskExecutor = null;
        try {
            taskExecutor = new ThreadPoolCreate(properties).getTaskExecutor(null);
        } catch (Exception e) {
            log.error("线程池初始化失败,原因是:{}",e.getMessage());
        }
        return taskExecutor;
    }
}

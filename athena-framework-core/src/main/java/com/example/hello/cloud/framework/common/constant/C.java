package com.example.hello.cloud.framework.common.constant;

/**
 * 公共常量
 *
 * @author zhanj04
 * @date 2017/8/4 10:40
 */
public class C {

    /**
     * 超级管理员ID
     */
    public static final Long SUPER_ADMIN_ID = 1L;

    /**
     * 超级管理员名称
     */
    public static final String SUPER_ADMIN_NAME = "admin";

    /**
     * 用户状态
     */
    public static class UserStatus {
        /**
         * 锁定
         */
        public static final int LOCK = 0;

        /**
         * 正常
         */
        public static final int NORMAL = 1;
    }

    /**
     * 菜单类型
     */
    public static class MenuType {
        /**
         * 目录
         */
        public static final int CATALOG = 0;

        /**
         * 菜单
         */
        public static final int MENU = 1;

        /**
         * 按钮
         */
        public static final int BUTTON = 2;
    }

    /**
     * 菜单
     */
    public static final String MENU = "0";

    /**
     * 编码
     */
    public static final String UTF8 = "UTF-8";

    /**
     * JSON 资源
     */
    public static final String CONTENT_TYPE_KEY = "CONTENT_TYPE";

    /**
     * JSON 资源
     */
    public static final String CONTENT_TYPE_VALUE = "application/json; charset=utf-8";

    /**
     * 成功标记
     */
    public static final Integer SUCCESS = 0;
    /**
     * 失败标记
     */
    public static final Integer FAIL = 1;

    /**
     * 默认为空消息
     */
    public static final String DEFAULT_NULL_MESSAGE = "暂无业务数据";
    /**
     * 默认成功消息
     */
    public static final String DEFAULT_SUCCESS_MESSAGE = "处理成功";
    /**
     * 默认失败消息
     */
    public static final String DEFAULT_FAILURE_MESSAGE = "处理失败";
    /**
     * 默认未授权消息
     */
    public static final String DEFAULT_UNAUTHORIZED_MESSAGE = "签名认证失败";

    /**
     * 服务名称常量
     *
     * @author hello
     */
    public static class ServiceName {
        /**
         * 认证服务的SERVICEID
         */
        public static final String AUTH_SERVICE = "hello-cloud-auth";

    }

}

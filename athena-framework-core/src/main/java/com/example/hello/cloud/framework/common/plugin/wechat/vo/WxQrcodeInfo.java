package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import java.io.Serializable;
import java.util.Map;

import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author lium44
 * @ClassName: WxQrcodeInfo
 * @Description: 微信二维码
 * @date 2018年10月01日
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WxQrcodeInfo implements Serializable {

    /**
     * 二维码Id
     */
    @NotEmpty(message = "二维码Id不为空")
    private String qrcodeId;
    /**
     * 二维码形状类型 0,圆型 1,方形
     */
    private String qrcodeType;

    /**
     * 生成二维码流水号
     */
    @NotEmpty(message = "生成二维码流水号")
    private String traceId;

    /**
     * 系统类型
     */
    private String domain;

    private String accessToken;

    /**
     * 小程序二维码名称
     */
    private String qrocdeName;

    /**
     * 跳转页
     */
    private String page;

    /**
     * 方形码URL
     */
    private String path;
    /**
     * 二维码宽度
     */
    private String width;
    /**
     * 使用 rgb 设置颜色 例如 {"r":"xxx","g":"xxx","b":"xxx"}
     */
    private Map<String,String> lineColor;
    /**
     * 是否需要透明底色
     */
    private Boolean hyaline;
}

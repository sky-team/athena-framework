package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author v-hut12
 * @date 2018/9/28
 * 微信用户保存更新入参
 */
@Data
public class DecodeDataParam implements Serializable {

    private static final long serialVersionUID = -3327314130192787704L;

    private String encryptedData;

    private String iv;

}

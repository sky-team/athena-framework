package com.example.hello.cloud.framework.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * excel注释.
 * @author will
 */

/**
 * Excel
 * excel注释
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Excel {
}

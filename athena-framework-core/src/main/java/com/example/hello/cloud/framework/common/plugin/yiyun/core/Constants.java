package com.example.hello.cloud.framework.common.plugin.yiyun.core;

/**
 * 翼云常量定义
 *
 * @author v-linxb
 * @create 2018/4/23
 **/
public final class Constants {
	private Constants() {
	}
	
    public static final String SIGN_METHOD_HMAC = "HMAC";

    public static final String SIGN_METHOD_MD5 = "MD5";

    public static final String CHARSET_UTF8 = "UTF-8";

}

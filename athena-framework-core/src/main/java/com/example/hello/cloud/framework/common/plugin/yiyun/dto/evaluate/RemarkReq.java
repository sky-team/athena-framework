package com.example.hello.cloud.framework.common.plugin.yiyun.dto.evaluate;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author c-yangym06
 * @date 2019/10/21
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RemarkReq implements Serializable {

    private String name;

    private String phone;
}

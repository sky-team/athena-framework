package com.example.hello.cloud.framework.common.plugin.yiyun.core;

import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunPayProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunServiceType;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunPayProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunServiceType;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;

import java.lang.reflect.Type;

/**
 * 翼云中台接口统一调用服务
 *
 * @author v-linxb
 * @create 2018/5/3
 **/
public interface YiyunService<T> {

    /**
     * 调用翼云中台接口方法
     *
     * @param type         接口类型
     * @param request      请求参数
     * @param responseType 泛型响应类型
     * @return 翼云接口响应数据
     */
    YiyunResponse<T> action(YiyunServiceType type, YiyunRequestData request, Type responseType);

    /**
     * 调用翼云中台接口方法
     *
     * @param type         接口类型
     * @param request      请求参数
     * @param responseType 泛型响应类型
     * @return 翼云接口响应数据
     */
    YiyunResponse<T> action2(YiyunServiceType type, YiyunRequestData request, Type responseType);

    /**
     * 在线商城调用翼云中台支付接口方法
     *
     * @param type         接口类型
     * @param request      请求参数
     * @param responseType 泛型响应类型
     * @return 翼云接口响应数据
     */
    YiyunResponse<T> zxjShopPaymentAction(YiyunServiceType type, YiyunRequestData request, Type responseType, YiyunPayProperties properties);

}

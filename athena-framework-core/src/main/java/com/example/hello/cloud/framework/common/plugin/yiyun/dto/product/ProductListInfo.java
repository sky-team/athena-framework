package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 产品信息
 *
 * @author v-linxb
 * @create 2018/5/28
 **/
@Data
public class ProductListInfo implements Serializable {

    private Long productId;
    private String name;
    private String status;
    private Long parentProductId;
    private Long categoryId;
    private String outerId;
    private String outerApp;
    private String innerId;
    private Integer bundle;
    private String createPerson;
    private String share;
    private Map<String, Object> propMap;
    //楼盘价格
    private List<EstatePrice> prices;
    //多媒体
    private List<Media> medias;

    private List<SubProdGroupInfo> subProductGroup;

    private List<ProductListInfoResp> children;

    /**
     * 读取产品属性值,默认只读取第一个
     * @param propMap 产品属性map
     * @param key     产品属性key
     * @return        产品属性值
     */
    public static Object getFirstMapValue(Map<String, Object> propMap, String key) {
        if (null != propMap.get(key)) {
            List mapValue = (ArrayList) (propMap.get(key));
            return mapValue.get(0);
        }
        return null;
    }

}

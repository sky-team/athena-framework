package com.example.hello.cloud.framework.common.image;

import com.example.hello.cloud.framework.common.util.oss.AliyunFolderTypeEnum;
import com.example.hello.cloud.framework.common.util.oss.AliyunOssService;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 图片工具类(缩放、加水印)
 *
 * @author guohg03
 * @date 2020/10/13 18:58
 */
@Slf4j
public class ImageUtil {
    /**
     * 对图片进行缩放(宽度和高度不变)
     *
     * @param fileUrl 文件路径
     * @param scale   缩放比例(例如1f，0.5f)
     * @return 压缩后文件路径
     */
    public static String scale(String fileUrl, float scale) {
        return scale(getInputStreamByUrl(fileUrl), scale);
    }

    /**
     * 对图片进行缩放(宽度和高度不变)
     *
     * @param srcFile 文件
     * @param scale   缩放比例(例如1f，0.5f)
     * @return 压缩后文件路径
     */
    public static String scale(File srcFile, float scale) {
        try {
            return scale(new FileInputStream(srcFile), scale);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 对图片进行缩放(宽度和高度不变)
     *
     * @param srcInputStream 输入流
     * @param scale          缩放比例(例如1f，0.5f)
     * @return 压缩后文件路径
     */
    public static String scale(InputStream srcInputStream, float scale) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Thumbnails.of(srcInputStream).scale(scale).outputQuality(1f).toOutputStream(outputStream);
            return new AliyunOssService().updateAliyunImageWithBytes(outputStream.toByteArray(), AliyunFolderTypeEnum.TEMP);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 按指定的宽度和高度对图片进行压缩
     *
     * @param srcInputStream 输入流
     * @param width          压缩后文件指定的宽度
     * @param height         压缩后文件指定的高度
     * @return 压缩后文件路径
     */
    public static String scale(InputStream srcInputStream, int width, int height) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Thumbnails.of(srcInputStream).size(width, height).toOutputStream(outputStream);
            return new AliyunOssService().updateAliyunImageWithBytes(outputStream.toByteArray(), AliyunFolderTypeEnum.TEMP);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 按指定的宽度和高度对图片进行压缩
     *
     * @param fileUrl 文件路径
     * @param width   压缩后文件指定的宽度
     * @param height  压缩后文件指定的高度
     * @return 压缩后文件路径
     */
    public static String scale(String fileUrl, int width, int height) {
        return scale(getInputStreamByUrl(fileUrl), width, height);
    }

    /**
     * 按指定的宽度和高度对图片进行压缩
     *
     * @param srcFile 文件
     * @param width   压缩后文件指定的宽度
     * @param height  压缩后文件指定的高度
     * @return 压缩后文件路径
     */
    public static String scale(File srcFile, int width, int height) {
        try {
            return scale(new FileInputStream(srcFile), width, height);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 对图片进行缩放(按1比1)
     *
     * @param srcInputStream 输入流
     * @return 压缩后文件路径
     */
    public static String scale(InputStream srcInputStream) {
        return scale(srcInputStream, 1);
    }

    /**
     * 对图片进行缩放(按1比1)
     *
     * @param fileUrl 文件路径
     * @return 压缩后文件路径
     */
    public static String scale(String fileUrl) {
        return scale(fileUrl, 1);
    }

    /**
     * 对图片进行缩放(按1比1)
     *
     * @param srcFile 文件
     * @return 压缩后文件路径
     */
    public static String scale(File srcFile) {
        return scale(srcFile, 1);
    }

    /**
     * 根据地址获取输入流
     */
    private static InputStream getInputStreamByUrl(String fileUrl) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(fileUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5 * 1000);
            return conn.getInputStream();
        } catch (Exception e) {
            log.error("根据地址获取输入流异常!errMsg:{}", e.getMessage(), e);
            return null;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }
}
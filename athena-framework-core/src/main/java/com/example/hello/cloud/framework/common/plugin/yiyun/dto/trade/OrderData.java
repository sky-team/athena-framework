package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 发票信息类
 * @author v-zhongj11
 * @create 2018/6/7
 */
@Data
public class OrderData implements Serializable{

    private Double amountWithoutTax;
    /**
     * 发票地址
     */
    private String eInvoiceUrl;

    private String invoiceCode;

    private String invoiceNo;

    private String paperDrewDate;

    private String invoiceTypeId;

    private String status;

    /**
     * 开票金额
     */
    private String amountWithTax;

    private String settlementNo;

    private String settlementItemNo;

    private Double quantity;

    /**
     * 领取人
     */
    private String invoicePerson;

    /**
     * 开票日期
     */
    private String invoiceGetTime;

    private String description;

    private String name;

    private String batch;

    private String isPaperInvoice;

    private String receiptNo;

    private Double taxAmount;

    private String invoiceId;

    /**
     * 发票名称
     */
    private String fundName;

    /**
     * 房间名称
     */
    private String roomName;

    /**
     * 名称
     */
    private String customerName;

    /**
     * 客户列表
     */
    private List<JSONObject> invoiceMQCustmoerObjectList;

}
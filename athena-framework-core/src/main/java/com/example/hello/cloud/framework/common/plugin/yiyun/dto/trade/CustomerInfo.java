package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import lombok.Data;

import java.io.Serializable;

/**
 * @author v-zhongj11
 * @create 2018/6/1
 */
@Data
public class CustomerInfo implements Serializable{

    /**
     * 成交人姓名
     */
    private String dealName;

    /**
     * 成交人电话
     */
    private String dealPhone;

    /**
     * 成交人证件类型
     */
    private String dealCardType;

    /**
     * 成交人证件号码
     */
    private String dealCardNumber;
}
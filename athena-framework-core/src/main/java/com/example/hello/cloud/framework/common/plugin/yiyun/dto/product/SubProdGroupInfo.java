package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import lombok.Data;

import java.io.Serializable;

/**
 * 中台定制返回参数 户型信息 根据户型标签统计
 * Created by v-zhangxj22 on 2018/5/30.
 */
@Data
public class SubProdGroupInfo implements Serializable {

    //rooms=2的产品统计分组, ,注意: 这个参数的值可能是整数/小数/字符串/日期  此处应为户型标签
    private String groupPropValue;

    //rooms=2(groupPropValue)的户型数量
    private Integer count;

    //2房的户型, 最大面积是80
    private String max;
}

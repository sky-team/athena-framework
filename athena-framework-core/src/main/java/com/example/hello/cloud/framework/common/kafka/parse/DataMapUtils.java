package com.example.hello.cloud.framework.common.kafka.parse;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class DataMapUtils {
    private DataMapUtils() {
    }

    /**
     * 把字符类型加字符串类型说明，映射成Java数据格式
     * @param dataMap
     * @param myDataType
     */
    public static void doJavaDataMap(Map dataMap, Map myDataType) {
        for (Object entry : myDataType.entrySet()) {
            Map.Entry<String, String> entitySet = (Map.Entry<String, String>) entry;
            String fieldName = entitySet.getKey();
            String fieldNameType = entitySet.getValue();
            if (dataMap.get(fieldName) == null || StringUtils.isEmpty(String.valueOf(dataMap.get(fieldName)))) {
                dataMap.remove(fieldName);
                continue;
            }
            if (fieldNameType.toLowerCase().contains("char") || fieldNameType.toLowerCase().contains("text")) {
                dataMap.put(fieldName, dataMap.get(fieldName).toString());

            } else if (fieldNameType.toLowerCase().contains("bigint")) {
                dataMap.put(fieldName, Long.valueOf(dataMap.get(fieldName).toString()));

            } else if (fieldNameType.toLowerCase().contains("int")) {
                dataMap.put(fieldName, Integer.valueOf(dataMap.get(fieldName).toString()));

            } else if (fieldNameType.toLowerCase().contains("float")) {
                dataMap.put(fieldName, Float.valueOf(dataMap.get(fieldName).toString()));

            } else if (fieldNameType.toLowerCase().contains("double")) {
                dataMap.put(fieldName, Double.valueOf(dataMap.get(fieldName).toString()));

            } else if (fieldNameType.toLowerCase().contains("datetime")) {
                dataMap.put(fieldName, dataMap.get(fieldName).toString());
            }
        }
    }

}
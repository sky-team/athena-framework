package com.example.hello.cloud.framework.common.plugin.nss;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.hello.cloud.framework.common.plugin.nss.impl.NssSyncServiceImpl;
import com.example.hello.cloud.framework.common.plugin.nss.impl.NssSyncServiceImpl;
import com.example.hello.cloud.framework.common.redis.RedisSingleCacheTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Create By 杨一鸣 on 2018/9/30
 */
@Slf4j
public class NssRegisterUtils {

    private final String synchroUserUrl;
    private final RedisSingleCacheTemplate redisSingleCacheTemplate;

    public NssRegisterUtils(String synchroUserUrl, RedisSingleCacheTemplate redisSingleCacheTemplate) {
        this.synchroUserUrl = synchroUserUrl;
        this.redisSingleCacheTemplate = redisSingleCacheTemplate;
    }

    /**
     * 证件类型：销售系统证件类型  SD1010001  身份证 SD1010002  军官证 SD1010003  护照  SD1010004  户口簿  SD1010005  台湾通行证 SD1010006  台湾身份证
     *     SD1010007  港澳身份证  SD1010008  营业执照  SD1010009  组织机构代码  SD1010010  税务登记证号   SD1010011  其他
     */

    /**
     * 注册销售系统
     *
     * @param name       姓名
     * @param phone      手机号码
     * @param cardType   证件类型：如上
     * @param cardNumber 证件号码
     * @param exampleId    万科id
     * @param sapId      城市主数据id
     * @return
     */
    public String registNss(String name, String phone, String cardType, String cardNumber, String exampleId, String sapId) {
        Map<String, Object> salesData = new LinkedHashMap<>();
        salesData.put("exampleId", exampleId);
        salesData.put("name", name);
        salesData.put("certificateType", cardType);
        salesData.put("certificate", cardNumber);
        salesData.put("secureMobile", phone);
        // 禁用：SD3013001，启用：SD3013002
        salesData.put("idmStatus", "SD3013002");
        salesData.put("sapId", sapId);
        salesData.put("userType", "S11");

        NssSyncService nssSyncService = new NssSyncServiceImpl(redisSingleCacheTemplate);
        String url = this.synchroUserUrl;
        log.info("注册Nss==== URL>" + url);
        log.info("注册Nss==== body>" + JSONObject.toJSONString(salesData));
        log.info("注册Nss==== token>" + nssSyncService.syncToken());
        return getResponseFromNssServer(url, salesData, nssSyncService.syncToken());
    }

    public String getResponseFromNssServer(String url, Map<String, Object> params, String token){
        HttpPost httpPost = new HttpPost(url);
        createSignHeader(httpPost, params, token);
        httpPost.setEntity(new ByteArrayEntity(JSON.toJSONString(params).getBytes(), ContentType.APPLICATION_JSON));
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(20000).setConnectTimeout(20000).build();
        httpPost.setConfig(requestConfig);
        return execute(httpPost);
    }

    private void createSignHeader(HttpUriRequest request, Map<String, Object> params, String token) {
        request.addHeader("Content-Type", "application/json");
        request.addHeader("Authorization", token);
    }

    private String execute(HttpUriRequest request){
        CloseableHttpClient client = null;
        CloseableHttpResponse resp = null;
        try {
            client = HttpClientBuilder.create().build();
            resp = client.execute(request);
            return EntityUtils.toString(resp.getEntity());
        }catch(Exception e) {
        	log.error(e.getMessage(),e);
        	return null;
        } finally {
        	if(resp != null) {
        		HttpClientUtils.closeQuietly(resp);
        	}
            if(client != null) {
            	HttpClientUtils.closeQuietly(client);
            }
        }
    }
}
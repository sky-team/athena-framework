package com.example.hello.cloud.framework.common.plugin.yiyun.dto.customer;

import com.example.hello.cloud.framework.common.plugin.yiyun.dto.CustomerIdentification;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 查询客户账号标签信息请求入参
 *
 * @author zhoujj07
 * @create 2018/5/7
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class QuerySpecialTagBizParam extends CustomerIdentification {

    private static final long serialVersionUID = -300799005522240066L;

    /**
     * 非必填，准确判断客户身份(员工/经纪人)时强烈建议填写，否则可能出现极小概率的误判
     */
    private String certificateNo;

    /**
     * 需要判断的标签代码列表
     */
    private List<String> tags;

}

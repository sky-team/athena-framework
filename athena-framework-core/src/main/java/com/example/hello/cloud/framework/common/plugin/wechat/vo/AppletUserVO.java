package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import lombok.Data;

/**
 * 微信用户信息
 *
 * @author zhoujj07
 * @create 2018/9/28
 */
@Data
public class AppletUserVO {
    private String unionId;
    private String openId;
    private String nickName;
    private Integer gender;
    private String language;
    private String city;
    private String province;
    private String country;
    private String avatarUrl;
    private Watermark watermark;
    private String phoneNumber;
    private String purePhoneNumber;
    private String countryCode;

    @Data
    public class Watermark {
        private String timestamp;
        private String appid;
    }

}

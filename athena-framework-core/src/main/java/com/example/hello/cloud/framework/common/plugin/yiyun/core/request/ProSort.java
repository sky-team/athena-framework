package com.example.hello.cloud.framework.common.plugin.yiyun.core.request;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by zhuf04 on 2018/6/7.
 */
@Data
public class ProSort implements Serializable {
    /**
     *fieldCode,pubPropCode,appPropCode,priceType四选一必填，如果有fieldCode则判断为字段条件，否则依次类推
     */
    private String fieldCode;
    /**
     *公有属性
     */
    private String pubPropCode;
    /**
     *私有属性
     */
    private String appPropCode;
    /**
     *价格类型
     */
    private String priceType;
    /**
     *值类型 默认string
     */
    private String valueType;
    /**
     *排序方式desc/asc 默认asc
     */
    private String order;
    /**
     * 当有多个值时的排序模式 min/max/avg/median/ sum 默认max
     */
    private String sortMode;


}

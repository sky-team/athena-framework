package com.example.hello.cloud.framework.common.plugin.yiyun.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标识包含的字段需要映射中台属性代码
 *
 * @author v-linxb
 * @create 2018/5/11
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface YiyunDto {
}

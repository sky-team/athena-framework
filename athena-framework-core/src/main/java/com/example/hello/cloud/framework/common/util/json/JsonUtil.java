package com.example.hello.cloud.framework.common.util.json;

import cn.hutool.core.map.MapUtil;
import cn.hutool.json.JSONObject;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: JsonUtil.java
 * @Description: JSON工具类
 * @author guohg03
 * @date 2019年11月11日
 */
public class JsonUtil {
	private JsonUtil() {
	}

    /**
     * 将对象转为JSON字符串
     */
    public static String obj2Str(Object object) {
        if (null == object) {
            return null;
        }
        return JSON.toJSONString(object);
    }

    /**
     * 将对象转为JSON字符串（格式化）
     */
    public static String obj2StrFormat(Object object) {
        if (null == object) {
            return null;
        }
        return JSON.toJSONString(object
                , SerializerFeature.PrettyFormat
                , SerializerFeature.WriteMapNullValue
                , SerializerFeature.WriteDateUseDateFormat);
    }

    /**
     * 将JSON字符串转换成对象
     *
     * @param jsonStr
     * @param type
     * @return
     */
    public static <T> T json2Obj(String jsonStr, Type type) {
        if (null == jsonStr || "".equals(jsonStr)) {
            return null;
        }
        return JSON.parseObject(jsonStr, type);
    }

    /**
     * 将JSON字符串转成list
     *
     * @param jsonStr
     * @param cls
     * @return
     */
    public static <T> List<T> json2List(String jsonStr, Class<T> cls) {
        if (null == jsonStr || "".equals(jsonStr)) {
            return null;
        }
        return JSON.parseArray(jsonStr, cls);
    }

    /**
     * 将JSON字符串转成map
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> json2Map(String jsonStr) {
        if (null == jsonStr || "".equals(jsonStr)) {
            return null;
        }
        return (Map<String, Object>) JSON.parse(jsonStr);
    }
    
    /**
     * 将map转换为JSON
     */
    public static String map2Json(Map<String,Object> map) {
        if (MapUtil.isEmpty(map)) {
            return null;
        }
        JSONObject jsonObj = new JSONObject(map);
        return jsonObj.toString();
    }
    
    /**
     * 获取json串对应key的值(一级嵌套)
     * @param jsonObj  json串
     * @param key      对应的key
     * @param subKey   嵌套对应的key的sub key
     */
    public static String getValWithOneLevel(String jsonObj, String key, String subKey) {
    	JSONObject object = new JSONObject(jsonObj);
    	JSONObject keyObject = object.getJSONObject(key);
    	if(keyObject != null) {
    		return keyObject.getStr(subKey);
    	}
    	return null;
    }
    
    /**
     * 获取json串对应key的值(无嵌套)
     * @param jsonObj  json串
     * @param key      对应的key
     */
    public static String getVal(String jsonObj, String key) {
    	JSONObject object = new JSONObject(jsonObj);
    	return object.getStr(key);
    }
}
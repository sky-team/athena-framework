package com.example.hello.cloud.framework.common.plugin.wechat.utils;

import com.alibaba.fastjson.JSON;
import com.example.hello.cloud.framework.common.constant.Constants;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.constant.Constants;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.WxEnvSetting;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.WxQrcodeInfo;
import com.example.hello.cloud.framework.common.util.oss.AliyunClassificationEnum;
import com.example.hello.cloud.framework.common.util.oss.AliyunFolderTypeEnum;
import com.example.hello.cloud.framework.common.util.oss.AliyunOssUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * 调用微信接口生成二维码的工具类
 * <p>
 * Created by v-yeyg on 2017/6/4.
 */

@Slf4j
public class MakeWechatCodeUtils {

    private static volatile AliyunOssUtil aliyunOSSUtil;

    private static final String ACCESS_TOKE_FILE = "accessToken.txt";

    /**
     * 获取AccessToken
     *
     * @param appId     微信appId
     * @param appSecret 微信appSecret
     * @return String
     */
    public static String getAccessToken(String appId, String appSecret, String url) throws IOException {
        CloseableHttpResponse httpResponse = null;
        CloseableHttpClient httpClient = null;
        HttpGet httpGet = null;
        try {
            // 用get方法发送http请求
            httpGet = new HttpGet(url + "&appid=" + appId + "&secret=" + appSecret);
            httpGet.setHeader(HttpHeaders.CONNECTION, "close");
            // 创建默认的httpClient实例
            httpClient = HttpClients.createDefault();
            httpResponse = httpClient.execute(httpGet);
            // response实体
            HttpEntity entity = httpResponse.getEntity();
            if (null != entity) {
                String accessToken = EntityUtils.toString(entity);
                String token = accessToken.substring(accessToken.indexOf(":") + 2, accessToken.indexOf(",") - 1);
                // 将token设置到oss中
                String accessTokenFile = appId + ACCESS_TOKE_FILE;
                aliyunOSSUtil.putTokens(AliyunClassificationEnum.IMAGE, AliyunFolderTypeEnum.TOKEN, accessTokenFile,
                        new InputStream() {
                            @Override
                            public int read() throws IOException {
                                return 0;
                            }
                        }, token);
                log.info("MakeWechatCodeUtils 获取token 文件名称： {} -> {}->{}", appId, accessTokenFile, token);
                return token;
            }
        } catch (Exception e) {
            log.error("获取accessTone异常", e);
        } finally {
            close(httpResponse, httpGet);
        }
        return null;
    }

    private static void close(CloseableHttpResponse httpResponse, HttpRequestBase httpRequestBase) {
        if (httpResponse != null) {
            EntityUtils.consumeQuietly(httpResponse.getEntity());
        }
        if (httpRequestBase != null) {
            httpRequestBase.releaseConnection();
        }
    }

    /**
     * 获得小程序二维码
     *
     * @param parameters
     * @param
     * @return 二维码路径
     */
    public static ApiResponse<String> getQrcode(String parameters, WxQrcodeInfo wxQrcodeInfo,
                                                  int retries, WxEnvSetting wxEnvSetting) throws Exception {
        log.info("MakeWechatCodeUtils getQrcode parameters={} wxQrcodeInfo={} retries={} wxEnvSetting.appId={}"
                , parameters, wxQrcodeInfo, retries, wxEnvSetting.getAppId());
        String imgUrl = "";
        String accessToken = wxQrcodeInfo.getAccessToken();
        HttpPost method = null;
        CloseableHttpResponse response = null;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClients.createDefault();
            // 原型码和方形码接口不一样
            if ("0".equals(wxQrcodeInfo.getQrcodeType())) {
                log.info("qrcodeUrl-{}", wxEnvSetting.getQrcodeUrl() + accessToken);
                method = new HttpPost(wxEnvSetting.getQrcodeUrl() + accessToken);
            } else {
                log.info("qrcodeUrlSquare-{}", wxEnvSetting.getQrcodeUrlSquare() + accessToken);
                method = new HttpPost(wxEnvSetting.getQrcodeUrlSquare() + accessToken);
            }
            if (StringUtils.isNotEmpty(parameters)) {
                // 用于存储欲传送的参数
                method.addHeader("Content-type", "application/json; charset=utf-8");
                method.setHeader("Accept", "application/json");
                method.setEntity(new StringEntity(parameters, Charset.forName("UTF-8")));
                method.setHeader(HttpHeaders.CONNECTION, "close");
                // 执行请求
                log.info("生成小程序码请求参数:" + JSON.toJSONString(method));
                response = httpClient.execute(method);
                log.info("生成小程序码返回参数 ->{}", response);
                long respContentLength = response.getEntity().getContentLength();
                log.info("二维码图片大小：" + respContentLength + "-equal-" + (respContentLength == -1L));
                if (respContentLength == -1L) {
                    log.error("page跳转的路径不存在，请检查!{}", wxQrcodeInfo.getPage());
                    return ApiResponse.error("page跳转的路径不存在，请检查!");
                }
                boolean failureFlag = respContentLength < Constants.WX_QRCODE_IMG_SIZE;
                // If failure
                Integer maxRetries = wxEnvSetting.getRetries();
                maxRetries = maxRetries == null ? 3 : maxRetries;
                if (failureFlag) {
                    log.error("MakeWechatCodeUtils getQrCodeUrl 失败");
                    if (retries < maxRetries) {
                        retries++;
                        ApiResponse<String> imgUrlR = MakeWechatCodeUtils.getQrcode(parameters,
                                wxQrcodeInfo, retries, wxEnvSetting);
                        imgUrl = imgUrlR.getData();
                    }
                } else {
                    // 获得文件输入流
                    InputStream inputStream = response.getEntity().getContent();
                    // 二维码图片名称
                    String qrCodeName = wxQrcodeInfo.getQrocdeName();
                    if (StringUtils.isEmpty(qrCodeName)) {
                        qrCodeName = wxQrcodeInfo.getTraceId();
                    }
                    qrCodeName = qrCodeName + ".png";
                    // 上传至oss云服务器
                    String filename = aliyunOSSUtil.putObject(AliyunClassificationEnum.IMAGE,
                            AliyunFolderTypeEnum.QRCODE, qrCodeName, inputStream,
                            response.getEntity().getContentLength());
                    // 获取上传的图片地址
                    imgUrl = aliyunOSSUtil.getUrl(AliyunClassificationEnum.IMAGE, AliyunFolderTypeEnum.QRCODE,
                            filename);
                }
            }
        } catch (Exception e) {
            log.error("生成二维码失败!" + e.getMessage(), e);
            return ApiResponse.error(e.getMessage());
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
            close(response, method);
        }
        return ApiResponse.success(imgUrl);
    }

    public static void setAliYunOSSUtil(AliyunOssUtil aliYunOSSUtil) {
        aliyunOSSUtil = aliYunOSSUtil;
    }
}
package com.example.hello.cloud.framework.common.util.excel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * ObjectTypeEnum
 * 对象类型定义
 *
 * @author yingc04
 * @create 2019/11/4
 */
public enum ObjectTypeEnum {

    /**
     * 整数
     */
    INT("整数", "int"),
    /**
     * 小数
     */
    DOUBLE("小数", "double"),
    /**
     * 字符串
     */
    STRING("字符串", "String"),
    /**
     * 日期
     */
    DATE("日期", "Date");

    private final String value;
	/**
	 * 唯一标示
	 */
	private final String labelKey;

    ObjectTypeEnum(String operator, String labelKey) {
        this.value = operator;
        this.labelKey = labelKey;
    }

    public static List<ObjectTypeEnum> getList() {
        List<ObjectTypeEnum> result = new ArrayList<>();
        Collections.addAll(result, ObjectTypeEnum.values());
        return result;
    }

    public static String format(String labelKey) {
        for (ObjectTypeEnum ae : ObjectTypeEnum.values()) {
            if (ae.getLabelKey().equals(labelKey)) {
                return ae.getValue();
            }
        }
        return labelKey;
    }

    public String getValue() {
        return value;
    }

    public String getLabelKey() {
        return labelKey;
    }
}

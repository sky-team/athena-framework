package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 产品多媒体类型
 *
 * @author v-linxb
 * @create 2018/5/17
 **/
public enum MediaFileTypeEnum {

    IMAGE(1, "图片"),
    VIDEO(2, "视频"),;

    MediaFileTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    private Integer value;

    private String desc;

    public Integer getValue() {
        return this.value;
    }

    public String getDesc() {
        return this.desc;
    }
}

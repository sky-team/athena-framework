package com.example.hello.cloud.framework.common.enums;

import java.util.*;

/**
 * 角色domain
 *
 * @author c-linxb
 * @create 2020/3/26
 **/
public enum RoleDomainEnum {

    AGENT("3", "agent", "经纪人", 3),
    CONSULTANT("2", "consultant", "顾问", 2),
    MANAGER("4", "manager", "管理员(含销售家经理)", 4),
    ORG("5", "org", "机构管理员", 5),
    C_SIDE_USER("1", "c-side-user", "客户", 6),
    SERVER("6", "server", "客服", 1);

    RoleDomainEnum(String value, String code, String name, Integer sort) {
        this.value = value;
        this.code = code;
        this.name = name;
        this.sort = sort;
    }

    private String value;

    private String code;

    private String name;

    private Integer sort;

    public String getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Integer getSort() {
        return sort;
    }

    /**
     * 根据code获取角色名称
     *
     * @param code 角色domain的code
     * @return
     */
    public static String getNameByCode(String code) {
        Optional<RoleDomainEnum> opt = Arrays.stream(RoleDomainEnum.values()).filter(item -> Objects.equals(item.getCode(), code)).findFirst();
        if (opt.isPresent()) {
            return opt.get().getName();
        }
        return "";
    }

    /**
     * 根据code获取最高角色code
     *
     * @param codes 角色domain的code集合
     * @return
     */
    public static RoleDomainEnum getHighestRoleByCodes(List<String> codes) {
        Optional<RoleDomainEnum> opt = Arrays.stream(RoleDomainEnum.values()).filter(item -> codes.contains(item.getCode()))
                .sorted(Comparator.comparing(RoleDomainEnum::getSort)).findFirst();
        if (opt.isPresent()) {
            return opt.get();
        }
        return null;
    }

}

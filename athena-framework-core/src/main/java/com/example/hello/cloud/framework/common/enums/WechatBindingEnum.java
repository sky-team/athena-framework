package com.example.hello.cloud.framework.common.enums;

/**
 * 绑定微信渠道类型枚举 (1:小程序 2:公众号,3.app）
 *
 * @author zhanj04
 * @date 2019/11/28
 */
public enum WechatBindingEnum {
    /**
     * 小程序
     */
    APPLET(1, "applet", "小程序"),
    /**
     * 公众号
     */
    PUBLIC_NUMBER(2, "public_number", "公众号"),
    /**
     * app
     */
    APP(3, "app", "APP");

    private Integer id;
    private String code;
    private String name;

    public Integer getId() {
        return id;
    }

    protected void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    protected void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    WechatBindingEnum(Integer id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    /**
     * @param code
     *            code
     * @return Integer
     */
    public static Integer getIdByCode(String code) {
        for (WechatBindingEnum wechatBindingEnum : WechatBindingEnum.values()) {
            if (wechatBindingEnum.getCode().equals(code)) {
                return wechatBindingEnum.getId();
            }
        }
        return null;
    }

    /**
     * @param id
     *            id
     * @return String
     */
    public static String getCodeById(Integer id) {
        for (WechatBindingEnum wechatBindingEnum : WechatBindingEnum.values()) {
            if (wechatBindingEnum.getId().equals(id)) {
                return wechatBindingEnum.getCode();
            }
        }
        return null;
    }
}

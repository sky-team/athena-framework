package com.example.hello.cloud.framework.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author guohg03
 * @ClassName: UserBaseRespVO
 * @Description: 用户VO
 * @date 2018年7月26日
 */
@Data
public class UserBaseRespVO implements Serializable {
    private static final long serialVersionUID = -7848046746192390528L;

    @ApiModelProperty(value = "id", name = "id")
    private String id;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号", name = "手机号")
    private String mobile;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码", name = "pwd")
    private String pwd;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱", name = "email")
    private String email;

    /**
     * 昵称，展示给客户的名称
     */
    @ApiModelProperty(value = "昵称，展示给客户的名称", name = "nickName")
    private String nickName;

    /**
     * 真实姓名
     */
    @ApiModelProperty(value = "真实姓名", name = "realName")
    private String realName;

    /**
     * 子账户标识
     */
    @ApiModelProperty(value = "子账户标识", name = "domain")
    private String domain;

    /**
     * 登录错误的次数
     */
    @ApiModelProperty(value = "登录错误的次数", name = "loginErrTimes")
    private Integer loginErrTimes;

    /**
     * 最后登录时间
     */
    @ApiModelProperty(value = "最后登录时间", name = "lastLoginDate")
    private Date lastLoginDate;

    /**
     * 锁定状态(1:锁定 0：未锁定)
     */
    @ApiModelProperty(value = "锁定状态(1:锁定 0：未锁定)", name = "lockStatus")
    private Integer lockStatus;

    /**
     * 是否重置过密码(1:重置  0：未重置)
     */
    @ApiModelProperty(value = "是否重置过密码(1:重置  0：未重置)", name = "resetPwd")
    private Integer resetPwd;
    /** 本月重置次数*/
	private Integer resetPwdNum;
	/** 最后重置密码时间*/
	private Integer lastResetPwdDate;
	/** 是否重置过密码(1:重置  0：未重置)*/
    /**
     * 启用状态（1：启用 0：禁用）
     */
    @ApiModelProperty(value = "启用状态（1：启用 0：禁用)", name = "enableFlag")
    private Integer enableFlag;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", name = "remark")
    private String remark;

    /**
     * 用户登录当前选择的机构id
     */
    @ApiModelProperty(value = "用户登录当前选择的组织id", name = "defaultOrgId")
    private String defaultOrgId;

    @ApiModelProperty(value = "用户登录当前选择的组织名称", name = "defaultOrgId")
    private String defaultOrgName;

    /**
     * 组织编码
     */
    @ApiModelProperty(value = "组织编码", name = "defaultOrgCode")
    private String defaultOrgCode;

    @ApiModelProperty(value = "有权限的产品code集", name = "proCodeList")
    private List<String> proCodeList;
}
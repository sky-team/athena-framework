//package com.example.hello.cloud.framework.common.kafka;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.context.annotation.Configuration;
//import lombok.extern.slf4j.Slf4j;
//
//@ConditionalOnProperty(prefix = "kafka",name = "consumer.servers",matchIfMissing = false)
//@Configuration
//@Slf4j
//public class KafKaListenerStopConfig implements CommandLineRunner {
//	@Autowired
//	ProgramExitSignalDealHandler programExitSignalDealHandler;
//    @Override
//    public void run(String... args) {
//    	log.info("term register KafKaListenerStop......");
//    	programExitSignalDealHandler.registerSignal("TERM");
//    }
//}

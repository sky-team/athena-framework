package com.example.hello.cloud.framework.common.trace;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Trace
 *
 * @author tongf01
 * @date 2019/10/12 17:56
 */
@Setter
@Getter
public class Trace implements Serializable {
    private static final long serialVersionUID = 2284957271994425682L;
    private String traceId;
    private String spanId;

}

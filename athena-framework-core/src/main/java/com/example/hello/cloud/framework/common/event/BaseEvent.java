package com.example.hello.cloud.framework.common.event;

/**
 * BaseEvent
 *
 * @author yingc04
 * @create 2019/11/5
 */
public class BaseEvent {
	private String eventName;

	public BaseEvent() {
    }

	public BaseEvent(String eventName) {
        this.eventName = eventName;
    }

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Override
	public String toString() {
		return "[eventName=" + eventName + "]";
	}
}

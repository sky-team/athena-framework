package com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;

import java.util.List;

/**
 * 员工信息响应参数
 *
 * @author luodf01
 */
@Data
public class QueryEmployeeListResponse implements YiyunResponseData {

    List<QueryEmployeeResponse> list;
}
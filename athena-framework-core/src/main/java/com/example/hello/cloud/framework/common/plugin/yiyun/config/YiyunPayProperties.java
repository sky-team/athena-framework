package com.example.hello.cloud.framework.common.plugin.yiyun.config;

import lombok.Data;

/**
 * 在线商城翼云支付配置
 *
 * @author mall
 * @create 2017/9/7
 */
@Data
public class YiyunPayProperties {

    private String host;
    private String appId;
    private String appSecret;
    private String tenantId;
    private String domain;
    private String userDomain;
    private String notifyUrl;

}

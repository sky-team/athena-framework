package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 经纪人小程序端身份验证请求参数
 *
 * @author v-linxb
 * @create 2018/10/4
 **/
@Data
public class IdentityAuthParam implements Serializable {

    private String phone;

}

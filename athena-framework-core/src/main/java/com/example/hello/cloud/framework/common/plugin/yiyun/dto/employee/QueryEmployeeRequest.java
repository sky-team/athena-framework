package com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;

/**
 * 员工信息请求入参
 */
@Data
public class QueryEmployeeRequest extends BaseYiyunRequestData {
    /**
     * 手机号
     */
    private String mobile;

    public QueryEmployeeRequest setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }
}
package com.example.hello.cloud.framework.common.http;

import com.example.hello.cloud.framework.common.http.builder.HttpFactory;
import com.example.hello.cloud.framework.common.http.common.HttpConfig;
import com.example.hello.cloud.framework.common.http.common.HttpMethods;
import com.example.hello.cloud.framework.common.http.common.Utils;
import com.example.hello.cloud.framework.common.http.exception.HttpProcessException;
import com.example.hello.cloud.framework.common.http.builder.HttpFactory;
import com.example.hello.cloud.framework.common.http.common.HttpConfig;
import com.example.hello.cloud.framework.common.http.common.HttpMethods;
import com.example.hello.cloud.framework.common.http.common.Utils;
import com.example.hello.cloud.framework.common.http.exception.HttpProcessException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.concurrent.FutureCallback;

/**
 * http异步请求工具类
 *
 * @author v-linxb
 */
public final class HttpAsyncClientUtil {

    /**
     * 以Get方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static void get(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        send(config.method(HttpMethods.GET), callback);
    }

    /**
     * 以Post方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static void post(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        send(config.method(HttpMethods.POST), callback);
    }

    /**
     * 以Put方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static void put(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        send(config.method(HttpMethods.PUT), callback);
    }

    /**
     * 以Delete方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static void delete(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        send(config.method(HttpMethods.DELETE), callback);
    }

    /**
     * 以Patch方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static void patch(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        send(config.method(HttpMethods.PATCH), callback);
    }

    /**
     * 以Head方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static void head(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        send(config.method(HttpMethods.HEAD), callback);
    }

    /**
     * 以Options方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static void options(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        send(config.method(HttpMethods.OPTIONS), callback);
    }

    /**
     * 以Trace方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static void trace(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        send(config.method(HttpMethods.TRACE), callback);
    }

    /**
     * 下载文件
     *
     * @param config 请求参数配置
     * @return 返回处理结果
     * @throws HttpProcessException
     */
    public static void down(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        execute(config.method(HttpMethods.GET), callback);
    }

    /**
     * 上传文件
     *
     * @param config 请求参数配置
     * @throws HttpProcessException
     */
    public static void upload(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        if (config.method() != HttpMethods.POST && config.method() != HttpMethods.PUT) {
            config.method(HttpMethods.POST);
        }
        send(config, callback);
    }

    /**
     * 查看资源链接情况，返回状态码
     *
     * @param config 请求参数配置
     * @throws HttpProcessException
     */
    public static void status(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        execute(config, callback);
    }

    /**
     * 请求资源或服务
     *
     * @param config 请求参数配置
     * @throws HttpProcessException
     */
    public static void send(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        execute(config, callback);
    }

    /**
     * 请求资源或服务
     *
     * @param config 请求参数配置
     * @throws HttpProcessException
     */
    private static void execute(HttpConfig config, FutureCallback<HttpResponse> callback) throws HttpProcessException {
        try {
            //创建请求对象
            HttpRequestBase request = Utils.getRequest(config.url(), config.method());
            //处理请求对象
            Utils.handlerRequest(request, config);
            //执行请求操作
            if (config.context() == null) {
                HttpFactory.getInstance().getHttpAsyncPool().custom().execute(request, callback);
            } else {
                HttpFactory.getInstance().getHttpAsyncPool().custom().execute(request, config.context(), callback);
            }
        } catch (Exception e) {
            throw new HttpProcessException(e);
        }
    }
}
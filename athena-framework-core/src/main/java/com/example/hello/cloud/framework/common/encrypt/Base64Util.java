package com.example.hello.cloud.framework.common.encrypt;

import org.apache.commons.codec.binary.Base64;

/**
 * @ClassName: Base64Util
 * @Description: Base64加解密算法工具类
 * @author guohg03
 * @date 2018年10月25日
 */
public class Base64Util {
	public static final String SALT = "hello";
	
	private Base64Util() {
	}
	
	/**
	 * 功能: 加密
	 */
	public static String encode(String str) {
		byte[] b = str.getBytes();
		Base64 base64 = new Base64();
		b = base64.encode(b);
		return new String(b);
	}

	/**
	 * 功能: 解密
	 */
	public static String decode(String str) {
		byte[] b = str.getBytes();
		Base64 base64 = new Base64();
		b = base64.decode(b);
		return new String(b);
	}

	/**
	 * 功能: 加密(带盐值)
	 */
	public static String encodeWithSalt(String str,String salt) {
		return encode(str+salt);
	}
	
	/**
	 * 功能: 解密(带盐值)
	 */
	public static String decodeWithSalt(String str,String salt) {
		String dStr = decode(str);
		return dStr.substring(0,dStr.lastIndexOf(salt));
	}
}
package com.example.hello.cloud.framework.common.plugin.yiyun.impl;

import com.google.gson.reflect.TypeToken;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunService;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunEvaluateApiEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunServiceType;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.evaluate.EvaluatePageReq;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.evaluate.EvaluatePageResp;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.YiyunEvaluateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;

/**
 * 翼云中台评价中心接口
 *
 * @author v-linxb
 * @create 2019/7/24
 **/
@Service
public class YiyunEvaluateServiceImpl implements YiyunEvaluateService {

    private final YiyunService yiyunService;

    @Autowired
    public YiyunEvaluateServiceImpl(YiyunService yiyunService) {
        this.yiyunService = yiyunService;
    }

    /**
     * 请求获取评价页面的地址
     *
     * @param req
     * @return
     */
    @Override
    public ApiResponse<EvaluatePageResp> evaluatePage(EvaluatePageReq req) {
        YiyunServiceType type = YiyunEvaluateApiEnum.PRODUCT_BASIC_QUERY;
        Type responseType = new TypeToken<YiyunResponse<EvaluatePageResp>>() {
        }.getType();
        YiyunResponse<EvaluatePageResp> yiyunResponse
                = yiyunService.action(type, req, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }
}

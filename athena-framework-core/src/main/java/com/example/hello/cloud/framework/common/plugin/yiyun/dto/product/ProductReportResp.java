package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.report.vo.Prop;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.report.vo.Prop;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author :v-zhousq04
 * @description: 中台返回的数据实体
 * @create date: 2018/8/25 17:21
 */
@Data
@EqualsAndHashCode
public class ProductReportResp implements YiyunResponseData {
    private static final long serialVersionUID = -236486552198991280L;

    private String productId;
    private String name;
    private String parentProductId;
    private String status;
    private String categoryId;
    private String outerApp;
    private String outerId;
    private String bundle;
    private String createPerson;
    private String share;
    private List<Prop> props;
    private String propExtAttrAutoFill;
    private String rootCatalogId;
    private String ownOrg;
    private String updatePerson;
    private String innerId;
}

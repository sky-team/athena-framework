package com.example.hello.cloud.framework.common.http.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author zhoujj
 * @ClassName: HttpClientUtil
 * @Description: httpClient帮助类
 * @date 2018年7月24日
 */
public class HttpClientUtil {

    private static final int TIME_OUT = 10 * 1000;

    private static CloseableHttpClient httpClient;

    private static final Charset DEFAULT_CHAR_SET = Charset.forName("UTF-8");

    private static Log logger = LogFactory.getLog(HttpClientUtil.class);

    private static CloseableHttpClient getHttpClient() {
    	if (httpClient == null) {
			httpClient = HttpClientFactory.getPooledClient();
		}
		return httpClient;
    }

    /**
     * POST请求URL获取内容
     */
    public static String post(String url, Map<String, Object> params) {
        HttpPost httpPost = new HttpPost(url);
        config(httpPost);
        setPostParams(httpPost, params);
        return getString(httpPost);
    }

    /**
     * GET请求URL获取内容
     */
    public static String get(String url) {
        HttpGet httpGet = new HttpGet(url);
        config(httpGet);
        return getString(httpGet);
    }

    public static String getString(HttpRequestBase request) {
        CloseableHttpResponse response = null;
        try {
            CloseableHttpClient httpClient = getHttpClient();
            response = httpClient.execute(request, HttpClientContext.create());
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity, DEFAULT_CHAR_SET);
            EntityUtils.consume(entity);
            return result;
        } catch (Exception e) {
            logger.error(e);
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                if (request != null) {
                    request.releaseConnection();
                }
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return null;
    }

    private static void setPostParams(HttpPost httPost, Map<String, Object> params) {
        List<NameValuePair> nvps = new ArrayList<>();
        Set<String> keySet = params.keySet();
        for (String key : keySet) {
            nvps.add(new BasicNameValuePair(key, params.get(key).toString()));
        }
        try {
            httPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            logger.error("http client post error!",e);
        }
    }

    private static void config(HttpRequestBase httpRequestBase) {
        // 配置请求的超时设置
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(TIME_OUT)
                .setConnectTimeout(TIME_OUT).setSocketTimeout(TIME_OUT).build();
        httpRequestBase.setHeader("Content-Type", "application/json");
        httpRequestBase.setConfig(requestConfig);
    }

}

package com.example.hello.cloud.framework.common.util.oss;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * AliyunOssPolicy
 * 阿里云OSS相关访问信息
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Data
@ApiModel(value = "阿里云OSS相关访问信息")
public class AliyunOssPolicy {

    @ApiModelProperty("访问Id")
    private String accessId;

    @ApiModelProperty("访问策略")
    private String policy;

    @ApiModelProperty("访问签名")
    private String signature;

    @ApiModelProperty("过期时间（s）")
    private String expire;

    @ApiModelProperty("目录")
    private String dir;

    @ApiModelProperty("url")
    private String host;

}

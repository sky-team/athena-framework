package com.example.hello.cloud.framework.common.annotation;

import org.apache.poi.hssf.util.HSSFColor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * excel的表头的注释.
 * @author will
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Hearder {
    /**
     *表头高度
     */
    int headerHigh() default 0;
    /**
     *背景颜色
     */
    int backGroundColor() default HSSFColor.WHITE.index;
    /**
     * 字体大小
     */
    int hearderFontPoint() default (short)28;

    /**
     * 冻结
     */
    boolean intFreeze() default false;

}

package com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 员工信息响应参数
 *
 * @author luodf01
 */
@Data
public class QueryEmployeeResponse implements YiyunResponseData {

    /**
     * 业务中台员工Id
     */
    @ApiModelProperty(name = "id", value = "业务中台员工Id")
    private Long id;

    /**
     * 入职时间'yyyy-MM-dd HH:mm:ss'
     */
    @ApiModelProperty(name = "hireDate", value = "入职时间'yyyy-MM-dd HH:mm:ss'")
    protected String hireDate;

    /**
     * 离职时间'yyyy-MM-dd HH:mm:ss'
     */
    @ApiModelProperty(name = "resignDate", value = "离职时间'yyyy-MM-dd HH:mm:ss'")
    private String resignDate;

    /**
     * 转正时间'yyyy-MM-dd HH:mm:ss'
     */
    @ApiModelProperty(name = "regularDate", value = "转正时间'yyyy-MM-dd HH:mm:ss'")
    private String regularDate;

    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept", value = "部门名称")
    private String dept;

    /**
     * 部门id
     */
    @ApiModelProperty(name = "deptId", value = "部门id")
    private String deptId;

    /**
     * 万科id
     */
    @ApiModelProperty(name = "uid", value = "万科id")
    private String uid;

    /**
     * 职务
     */
    @ApiModelProperty(name = "title", value = "职务")
    private String title;

    /**
     * 姓名
     */
    @ApiModelProperty(name = "name", value = "姓名")
    private String name;

    /**
     * 公司
     */
    @ApiModelProperty(name = "company", value = "公司")
    private String company;

    /**
     * 手机号
     */
    @ApiModelProperty(name = "mobile", value = "手机号")
    private String mobile;

    /**
     * 域账号
     */
    @ApiModelProperty(name = "adAccount", value = "域账号")
    private String adAccount;

    /**
     * 员工状态编码
     */
    @ApiModelProperty(name = "empStatus", value = "员工状态编码")
    private String empStatus;

    /**
     * 公司编码
     */
    @ApiModelProperty(name = "exampleCompanyCode", value = "公司编码")
    private String exampleCompanyCode;

    /**
     * 组织名称全路径
     */
    @ApiModelProperty(name = "userOrgNameFullPath", value = "组织名称全路径")
    private String userOrgNameFullPath;

    /**
     * 组织编号全路径
     */
    @ApiModelProperty(name = "userOrgNoFullPath", value = "组织编号全路径")
    private String userOrgNoFullPath;

    /**
     * 员工类型
     */
    @ApiModelProperty(name = "smartVkType", value = "员工类型")
    private String smartVkType;

    /**
     * 员工标识
     */
    @ApiModelProperty(name = "regularStaffFlag", value = "员工标识")
    private boolean regularStaffFlag;

}
package com.example.hello.cloud.framework.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author lium44
 * @ClassName UserLbs
 * @Description 用户地理位置
 * @date 2018/10/3
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserLbs implements Serializable {
	private static final long serialVersionUID = 4858563944127354119L;

	/**
     * 经度
     */
	@ApiModelProperty(value="经度",name="longitude")
    private String longitude;

    /**
     * 维度
     */
	@ApiModelProperty(value="维度",name="latitudes")
    private String latitude;

}

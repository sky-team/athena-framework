package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;


/**
 * 支付类型
 *
 * @author mall
 * @create 2019-04-03
 **/
public enum PaymentTypeEnum {
    WXPAY("WXPAY", "微信支付");

    private String credit;

    private String value;

    PaymentTypeEnum(String credit, String value) {
        this.credit = credit;
        this.value = value;
    }

    public String getCredit() {
        return credit;
    }

    public static String getCredit(String value) {
        for (PaymentTypeEnum c : PaymentTypeEnum.values()) {
            if (c.getValue().equals(value)) {
                return c.credit;
            }
        }
        return null;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public static String getValue(String credit) {
        for (PaymentTypeEnum c : PaymentTypeEnum.values()) {
            if (c.getCredit().equals(credit)) {
                return c.value;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String[] getValues(){
        PaymentTypeEnum[] ems = PaymentTypeEnum.values();
        String[] values =new String[ems.length];
        for (int i = 0;i<values.length;i++) {
            values[i]=ems[i].value;
        }
        return  values;
    }
}
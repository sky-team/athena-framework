package com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment.wxpay;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 微信支付入参
 *
 * @author mall
 */
@Data
public class ApplyPayWxRequest extends BaseYiyunRequestData implements Serializable {
    /*
       merchantNo	    String	商户号	             是
       tradeOrderNo	    String	交易订单号	         是   只能成功支付一次
       payProductCode	String	支付产品编号	     是
       buyerId      	String	买家id      	     是   微信：sub_openid, 支付宝:buyer_user_id
       amount	        Decimal	支付金额	         是   最大2二位小数
       goodsDesc	    String	商品描述	         否
       channel  	    String	支付渠道	         否   默认工行 ICBC
       expireTime	    String	订单有效期	         否	  默认5分钟, 如果要传，必须大于默认值
       notifyUrl	    String	异步回调地址    	 否
    */

    @ApiModelProperty("商户号")
    private String merchantNo;

    @ApiModelProperty("交易订单号")
    private String tradeOrderNo;

    @ApiModelProperty("支付产品编号")
    private String payProductCode;

    @ApiModelProperty("买家id")
    private String buyerId;

    @ApiModelProperty("支付金额")
    private String amount;

    @ApiModelProperty("商品描述")
    private String goodsDesc;

    @ApiModelProperty("支付渠道")
    private String channel;

    @ApiModelProperty("订单有效期")
    private String expireTime;

    @ApiModelProperty("异步回调地址")
    private String notifyUrl;

    @ApiModelProperty("appid")
    private String appId;

}
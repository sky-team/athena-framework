package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import lombok.Data;

import java.io.Serializable;

/**
 * 地产交易单据信息
 * @author v-zhongj11
 * @create 2018/5/31
 */
@Data
public class EstateOrder implements Serializable{

    /**
     * 单据流水号
     */
    private String orderNo;
    /**
     * 渠道ID
     */
    private String channelId;
    /**
     * 父订单ID,一般是空
     */
    private String parentId;
    /**
     * 会员ID
     */
    private String accountId;
    /**
     * 交易类型
     */
    private String transactionType;
    /**
     * 交易状态,每个不同的交易类型不同.
     */
    private String status;
    /**
     * 冻结状态,参考后文”数据字典-交易冻结状态
     */
    private String frozenStatus;
    /**
     * 购买的产品列表,由产品中心分配产品ID
     */
    private String[] productIds;
    /**
     * 所属的地产推广项目ID	,由产品中心分配的“推广项目”类目下的产品ID
     */
    private String mktEstateStageId;
    /**
     * 应付金额
     */
    private String totalAmount;
    /**
     * 优惠总金额
     */
    private String discountAmount;
    /**
     * 实付金额
     */
    private String payAmount;

    private EstateOrderDetail detail;
}
package com.example.hello.cloud.framework.common.http.common;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.ssl.SSLContexts;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

/**
 * 设置ssl
 * @author v-linxb
 */
@Slf4j
public class SSLs {

    private static final SSLHandler simpleVerifier = new SSLHandler();
    private static SSLSocketFactory sslFactory;
    private static SSLConnectionSocketFactory sslConnFactory;
    private static SSLs sslutil = new SSLs();
    private SSLContext sc;

    public static SSLs getInstance() {
        return sslutil;
    }

    public static SSLs custom() {
        return new SSLs();
    }

    /**
     * 重写X509TrustManager类的三个方法,信任服务器证书
     */
    private static class SSLHandler implements X509TrustManager, HostnameVerifier {

        @Override
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return new java.security.cert.X509Certificate[]{};
        }

        @Override
        public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                       String authType) throws CertificateException {
        	log.debug("checkServerTrusted...");
        }

        @Override
        public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                       String authType) throws CertificateException {
        	log.debug("checkClientTrusted...");
        }

        @Override
        public boolean verify(String paramString, SSLSession paramSSLSession) {
            return true;
        }
    }

    /**
     * 信任主机
     */
    public static HostnameVerifier getVerifier() {
        return simpleVerifier;
    }

    public synchronized SSLSocketFactory getSSLSF(SSLProtocolVersion sslpv){
        if (sslFactory != null) {
            return sslFactory;
        }
        try {
            SSLContext sc = getSSLContext(sslpv);
            sc.init(null, new TrustManager[]{simpleVerifier}, null);
            sslFactory = sc.getSocketFactory();
            return sslFactory;
        } catch (Exception e) {
        	log.error(e.getMessage(),e);
        	return null;
        }
    }

    public synchronized SSLConnectionSocketFactory getSSLCONNSF(SSLProtocolVersion sslpv){
        if (sslConnFactory != null) {
            return sslConnFactory;
        }
        try {
            SSLContext sc = getSSLContext(sslpv);
            sc.init(null, new TrustManager[]{simpleVerifier}, new java.security.SecureRandom());
            sslConnFactory = new SSLConnectionSocketFactory(sc, simpleVerifier);
            return sslConnFactory;
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return null;
        }
    }

    public SSLs customSSL(String keyStorePath, String keyStorepass) {
        KeyStore trustStore;
        try(FileInputStream instream = new FileInputStream(new File(keyStorePath))){
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(instream, keyStorepass.toCharArray());
            // 相信自己的CA和所有自签名的证书
            sc = SSLContexts.custom().loadTrustMaterial(trustStore, new TrustSelfSignedStrategy()).build();
        } catch (Exception e) {
        	log.error(e.getMessage(),e);
        }
        return this;
    }

    public SSLContext getSSLContext(SSLProtocolVersion sslpv) {
        try {
            if (sc == null) {
                sc = SSLContext.getInstance(sslpv.getName());
            }
            return sc;
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(),e);
            return null;
        }
    }

    /**
     * The SSL protocol version (SSLv3, TLSv1, TLSv1.1, TLSv1.2)
     */
    public enum SSLProtocolVersion {
        SSL("SSL"),
        SSLV3("SSLv3"),
        TLSV1("TLSv1"),
        TLSV1_1("TLSv1.1"),
        TLSV1_2("TLSv1.2"),;
        private String name;

        SSLProtocolVersion(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public static SSLProtocolVersion find(String name) {
            for (SSLProtocolVersion pv : SSLProtocolVersion.values()) {
                if (pv.getName().equalsIgnoreCase(name)) {
                    return pv;
                }
            }
            log.error("未支持当前ssl版本号：" + name);
            return null;
        }
    }
}
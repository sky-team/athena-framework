package com.example.hello.cloud.framework.common.util.oss;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.aliyuncs.sts.model.v20150401.AssumeRoleRequest;
import com.aliyuncs.sts.model.v20150401.AssumeRoleResponse;
import com.example.hello.cloud.framework.common.redis.RedisSingleCacheTemplate;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.redis.RedisSingleCacheTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

/**
 * AliyunOssStsService
 * 阿里云oss移动端直传需要的sts服务
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Slf4j
@Component
public class AliyunOssStsService {


    @Value("${oss.bucket:}")
    String bucket;

    @Value("${oss.domain:}")
    String domain;

    @Value("${oss.sts.accessKeyId:}")
    String accessKeyId;
    @Value("${oss.sts.accessKeySecret:}")
    String accessKeySecret;
    @Value("${oss.sts.roleArn:}")
    String roleArn;
    @Value("${oss.sts.roleSessionName:}")
    String roleSessionName;

    @Autowired(required = false)
    private AliyunOssUtil aliyunOssUtil;


    private String policy = "{\n" +
            "  \"Version\": \"1\",\n" +
            "  \"Statement\": [\n" +
            "    {\n" +
            "      \"Effect\": \"Allow\",\n" +
            "      \"Action\": [\n" +
            "        \"oss:*\"\n" +
            "      ],\n" +
            "      \"Resource\": [\n" +
            "        \"acs:oss:*:*:hello-a-szzb\",\n" +
            "        \"acs:oss:*:*:hello-a-szzb/*\"\n" +
            "      ],\n" +
            "      \"Condition\": {}\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public ApiResponse<AliyunOssStsPolicy> getAliyunOssStsPolicy(AliyunFolderTypeEnum folderType) {
        AssumeRoleResponse stsResponse = getAcsResponse();
        if (stsResponse == null) {
            return ApiResponse.error("获取STS凭证失败");
        }
        log.info("AliyunOssStsService getAliyunOssStsPolicy : " + policy);
        AliyunOssStsPolicy stsPolicy = new AliyunOssStsPolicy();
        stsPolicy.setAccessKeyId(stsResponse.getCredentials().getAccessKeyId());
        stsPolicy.setAccessKeySecret(stsResponse.getCredentials().getAccessKeySecret());
        stsPolicy.setSecurityToken(stsResponse.getCredentials().getSecurityToken());
        stsPolicy.setExpiration(stsResponse.getCredentials().getExpiration());
        stsPolicy.setBucket(bucket);
        stsPolicy.setDomain(domain);
        stsPolicy.setHost(aliyunOssUtil.getHttpDomain());
        String dir = AliyunClassificationEnum.IMAGE + "/" + folderType.getPath();
        stsPolicy.setDir(dir);
        return ApiResponse.success(stsPolicy);
    }

    private AssumeRoleResponse getAcsResponse() {
        String endpoint = "sts.aliyuncs.com";

        AssumeRoleResponse response = null;
        try {
            String regionId = "cn-shenzhen";
            // 添加Endpoint
            DefaultProfile.addEndpoint("华南 1-深圳", regionId, "Sts", endpoint);
            // 构造Default Profile
            IClientProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, accessKeySecret);
            // 用profile构造client
            DefaultAcsClient client = new DefaultAcsClient(profile);
            final AssumeRoleRequest request = new AssumeRoleRequest();
            request.setMethod(MethodType.POST);
            request.setRoleArn(roleArn);
            request.setRoleSessionName(roleSessionName);
            request.setPolicy(policy);
            response = client.getAcsResponse(request);

        } catch (ClientException e) {
            String errMsg = "AliyunOssStsService getAcsResponse Failed : Error code : "
                    + e.getErrCode()
                    + "Error message: "
                    + e.getErrMsg()
                    + "RequestId: "
                    + e.getRequestId();
            log.error(errMsg, e);

        }
        return response;
    }


    /**
     * 获取oss临时token
     */
    public ApiResponse<AliyunOssStsPolicy> getOssStsPolicy(String folder,
                                                           RedisSingleCacheTemplate redisSingleCacheTemplate,
                                                           String folderName) {
        AliyunFolderTypeEnum folderType = AliyunFolderTypeEnum.getFolderType(folder);
        String ossStsDataJson = (String) redisSingleCacheTemplate.get("ossStsPolicy");
        AliyunOssStsPolicy aliyunOssStsPolicy;
        if (StringUtils.isNotEmpty(ossStsDataJson)) {
            aliyunOssStsPolicy = JSONUtil.toBean(ossStsDataJson,
                    AliyunOssStsPolicy.class);
            Date expireDate = DateUtil.parse(aliyunOssStsPolicy.getExpiration(),
                    "yyyy-MM-dd'T'HH:mm:ss'Z'");
            expireDate = addHour(expireDate, 8);
            if (expireDate != null && expireDate.getTime() < System.currentTimeMillis()) {
                String dirPath = AliyunClassificationEnum.getClassification(folderName).getClassificationName();
                aliyunOssStsPolicy.setDir(
                        dirPath + "/" + folderType.getPath());
                aliyunOssStsPolicy.setExpiration(DateUtil.formatDateTime(expireDate));
                return ApiResponse.success(aliyunOssStsPolicy);
            }
        }
        ApiResponse<AliyunOssStsPolicy> apiRespResult = this.getAliyunOssStsPolicy(folderType);
        if (apiRespResult.isOk() && (aliyunOssStsPolicy = apiRespResult.getData()) != null) {
            String dirPath = AliyunClassificationEnum.getClassification(folderName).getClassificationName();
            aliyunOssStsPolicy.setDir(dirPath + "/" + folderType.getPath());
            redisSingleCacheTemplate.set("ossStsPolicy", JSONUtil.toJsonStr(aliyunOssStsPolicy),
                    3600L);
        }
        return apiRespResult;
    }

    /**
     * 在当前时间上增加指定小时
     *
     * @param date
     * @return
     */
    public static Date addHour(Date date, int hour) {
        if (date != null && hour != 0) {
            Calendar endDate = Calendar.getInstance();
            endDate.setTime(date);
            endDate.add(Calendar.HOUR, hour);
            date = endDate.getTime();
        }
        return date;
    }

    public AliyunOssUtil getAliyunOssUtil() {
        return aliyunOssUtil;
    }

    public void setAliyunOssUtil(AliyunOssUtil aliyunOssUtil) {
        this.aliyunOssUtil = aliyunOssUtil;
    }
}

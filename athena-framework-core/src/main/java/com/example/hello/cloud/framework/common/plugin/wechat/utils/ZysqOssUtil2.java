package com.example.hello.cloud.framework.common.plugin.wechat.utils;

import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.example.hello.cloud.framework.common.http.client.HttpUtil;
import com.example.hello.cloud.framework.common.plugin.wechat.WeChatConstString;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.WeChatConfigVO;
import com.example.hello.cloud.framework.common.util.ResourcesUtil;
import com.example.hello.cloud.framework.common.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.ObjectMetadata;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: ZysqOssUtil
 * @Description:
 * @author: v-chenh60
 * @date: 2018/11/21 5:17
 */
@Slf4j
@Component
public class ZysqOssUtil2 {
    private ZysqOssUtil2() {
    }

    private static OSSClient ossClient;
    private static final String JSAPIURL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";
    @PostConstruct
    public static void init() {
        ClientConfiguration conf = new ClientConfiguration();
        conf.setMaxConnections(2048);
        // 设置TCP连接超时为5000毫秒
        conf.setConnectionTimeout(5000);
        //连接空闲超时时间，超时则关闭连接
        conf.setIdleConnectionTime(2000);
        // 设置最大的重试次数为3
        conf.setMaxErrorRetry(3);
        // 设置Socket传输数据超时的时间为2000毫秒
        conf.setSocketTimeout(2000);
        String accessKeyId = ResourcesUtil.getProperty("oss.zysq.key"),
                secret = ResourcesUtil.getProperty("oss.zysq.secret");
        if (StringUtil.isNotEmpty(accessKeyId) && StringUtil.isNotEmpty(secret)) {
            ossClient = new OSSClient("oss-cn-shenzhen.aliyuncs.com", accessKeyId, secret, conf);
        } else {
            log.info("oss.zysq.key or oss.zysq.secret is null");
        }
    }

    /**
     * 获取小程序token(房屋管家、置业神器、分享家、天津分享家、拓客神器)
     *
     * @return String
     */
    public static String getTokenForZYSQ(String appId) {
        OSSObject ossObject = null;
        String tokens = "";
        try {
            if (StringUtil.isNullOrEmpty(appId)) {
                return tokens;
            }
            String tokenFileName = "";
            if ("wx1f6ef77faaff2c59".equals(appId)) {
                tokenFileName = "fangwu.txt";
            } else if ("wxe9692d75c9e71da7".equals(appId)) {
                tokenFileName = "judge_tokens.txt";
            } else if ("wx8e7bfe0f4e6d7b29".equals(appId)) {
                tokenFileName = "union_tokens.txt";
            } else if ("wx9f22cb8ef817791b".equals(appId)) {
                tokenFileName = "tjfxj_tokens.txt";
            } else if (WeChatConstString.TOKER_APPLET_APPID.equals(appId)) {
                tokenFileName = "toker_tokens.txt";
            } else if ("wx64e56457ec5c6338".equals(appId)) {
                tokenFileName = "wanx2_tokens.txt";
            }else if ("wx127951ea13ec5ad1".equals(appId)) {
                tokenFileName = "wxy_tokens.txt";
            }else if ("wxaac38610878d0feb".equals(appId)) {
                tokenFileName = "examplewanx2_tokens.txt";
            }
            else if ("wxbd4beeb611423f0b".equals(appId)||"wx1c12568e4427cdc1".equals(appId)) {
                tokenFileName = "ww_tokens.txt";
            }
            else {
                return tokens;
            }
            log.info("getTokenForZYSQ appId={} tokenFileName={}", appId, tokenFileName);
            String bucketName = "t-sjpt-oss";
            String ossKey = StringUtils.join("images/qrCode/", tokenFileName);
            if (ossClient == null) {
                init();
            }
            ossObject = ossClient.getObject(bucketName, ossKey);
            ObjectMetadata objectMetadata = ossObject.getObjectMetadata();
            Map<String, String> userMetadata = objectMetadata.getUserMetadata();
            tokens = userMetadata.get("tokens");
        } catch (Exception e) {
            log.error(" OSSUtil getTokenForZYSQ error={}", e);
        } finally {
            close(ossObject);
        }
        return tokens;
    }

    /**
     * 获取线上置业神器公众号token
     *
     * @return String
     */
    public static String getTokenForZYSQWechatPublic() {
        OSSObject ossObject = null;
        String tokens = "";
        try {
            String bucketName = "t-sjpt-oss";
            String tokenFileName = "wechat_public.txt";
            String ossKey = StringUtils.join("images/qrCode/", tokenFileName);
            if (ossClient == null) {
                init();
            }
            ossObject = ossClient.getObject(bucketName, ossKey);
            ObjectMetadata objectMetadata = ossObject.getObjectMetadata();
            Map<String, String> userMetadata = objectMetadata.getUserMetadata();
            tokens = userMetadata.get("tokens");
        } catch (Exception e) {
            log.error(" OSSUtil getTokenForZYSQ error={}", e);
        } finally {
            close(ossObject);
        }
        return tokens;
    }


    /**
     * 获取测试置业神器公众号token
     *
     * @return String
     */
    public static String getTokenForZYSQWechatPublicTest() {
        OSSObject ossObject = null;
        String tokens = "";
        try {
            String bucketName = "t-sjpt-oss";
            String tokenFileName = "wechatPublicTest.txt";
            String ossKey = StringUtils.join("images/qrCode/", tokenFileName);
            if (ossClient == null) {
                init();
            }
            ossObject = ossClient.getObject(bucketName, ossKey);
            ObjectMetadata objectMetadata = ossObject.getObjectMetadata();
            Map<String, String> userMetadata = objectMetadata.getUserMetadata();
            tokens = userMetadata.get("tokens");
        } catch (Exception e) {
            log.error(" OSSUtil getTokenForZYSQ error={}", e);
        } finally {
            close(ossObject);
        }
        return tokens;
    }
    /**
     *  获取线上置业神器公众号ticket
     * @return
     */
    public static String getTicket(String appid) {
    	String accessToken = getTokenForZYSQWechatPublic();
    	if("wx42f3fd236a303d66".equals(appid)) {// 测试公众号
    		accessToken = getTokenForZYSQWechatPublicTest();
    	}
    	String ticket ="";
    	String url = JSAPIURL+"?access_token="+accessToken+ "&type=jsapi";
    	String jsApiJSONStr = HttpUtil.doGet(url);
    	log.info("调用微信接口获取JSApi返回结果：{}",jsApiJSONStr);
          if (StringUtils.isEmpty(jsApiJSONStr)) {
              //微信接口调用失败会返回空字符串
              log.error("获取JSApi失败,原因：" + jsApiJSONStr);
              
          } else {
              JsonObject jsAPIJsonObject = new JsonParser().parse(jsApiJSONStr).getAsJsonObject();
              if (jsAPIJsonObject.get("errcode").getAsInt() != 0) {
                  log.error("调用微信接口获取JSApi异常,原因:" + jsApiJSONStr);
              }
              log.info("获取JSApi成功,JSApi:" + jsAPIJsonObject.get("ticket").getAsString());
              ticket = jsAPIJsonObject.get("ticket").getAsString();
               
          }
          return ticket;
    }
    public static WeChatConfigVO getWeChatToken(String appid, String url, String ticket) {
    	 WeChatConfigVO cfg = new WeChatConfigVO();
    	 try {
             String realURL=java.net.URLDecoder.decode(url,"UTF-8");
             log.info("获取签名的URL:" + realURL );
             String apiTicket = ticket;
             String timestamp = Long.toString(System.currentTimeMillis() / 1000);
             String sign = "";
             String string1 = "jsapi_ticket=" + apiTicket +
                     "&noncestr=" + timestamp +
                     "&timestamp=" + timestamp +
                     "&url=" + realURL;
             log.info("jsapi_ticket：" + string1);

             MessageDigest crypt = MessageDigest.getInstance("SHA-1");
             crypt.reset();
             crypt.update(string1.getBytes("UTF-8"));
             sign = byteToHex(crypt.digest());

             cfg.setTimestamp(timestamp);
             cfg.setAppId(appid);
             cfg.setNonceStr(timestamp);
             cfg.setSignature(sign);
             log.info("返回签名：" + cfg.toString());
         } catch (NoSuchAlgorithmException e) {
             log.error(e.getMessage());
         } catch (UnsupportedEncodingException e) {
        	 log.error(e.getMessage());
		}

         return cfg;
    }
    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
    /**
     * 流关闭
     */
    private static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                log.error(" OSSUtil getTokenForZYSQ exception {}", e);
            }
        }
    }
}

package com.example.hello.cloud.framework.common.util.excel4j;

/**
 * 更新excel下载进度接口
 */
public interface ProgressService {

    /**
     * 更新进度字段
     *
     * @param id       id
     * @param progress progress
     * @return
     */
    public Integer updateProgress(String id, Long progress);
}

package com.example.hello.cloud.framework.common.util;

import cn.hutool.core.util.ObjectUtil;
import com.example.hello.cloud.framework.common.redis.RedisSingleCacheTemplate;
import com.example.hello.cloud.framework.common.util.json.JsonUtil;
import com.example.hello.cloud.framework.common.dto.UserBaseRespVO;
import com.example.hello.cloud.framework.common.enums.SessionKeyEnum;
import com.example.hello.cloud.framework.common.redis.RedisSingleCacheTemplate;
import com.example.hello.cloud.framework.common.util.json.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName: AdminUserUtil
 * @Description: 获取后台登录用户信息
 * @author guohg03
 * @date 2018年7月29日
 */
@Component
@Slf4j
public class AdminUserUtil {
	/**登录用户redis key*/
	private static final String LOGIN_USER_REDIS_KEY = "login:user:key:";
	
	/**登录是否使用session*/
	private static final String ADMIN_ENABLE_SESSION = "admin.enable.session";
	
	@Autowired
    private RedisSingleCacheTemplate redisSingleCacheTemplate;

	/**
	 * 功能：获取用户缓存信息
	 */
	public UserBaseRespVO getCachedUserInfo(){
		String loginId = getLoginId();
		if(StringUtil.isNullOrEmpty(loginId)) {
			log.error("获取用户登录id为空");
			return null;
		}
		String loginKey  = LOGIN_USER_REDIS_KEY + loginId;
		Object obj = redisSingleCacheTemplate.get(loginKey);
		if(ObjectUtil.isNotNull(obj)) {
			return JsonUtil.json2Obj(obj.toString(), UserBaseRespVO.class);
		}
		log.error("缓存中获取用户信息为空，重新调用接口刷用户信息数据到redis!{}", loginKey);
		return null;
	}

	/**
	 * 功能: 获取缓存用户中的id
	 */
	public String getLoginId(){
		// 如果基于token，直接从token中解析出登录的id(userId)
		String authorization = ContextHolderUtil.getAuthorizationFromHeader();
		return JwtUtil.getUserIdByAuthorization(authorization);
	}
	/**
	 * 功能: 获取缓存用户中的手机号
	 */
	public String getMobile(){
		UserBaseRespVO userInfo = getCachedUserInfo();
		return userInfo != null ? userInfo.getMobile() : null;
	}
	
	/**
	 * 功能: 获取缓存用户中的姓名
	 */
	public String getRealName(){
		UserBaseRespVO userInfo = getCachedUserInfo();
		return userInfo != null ? userInfo.getRealName() : null;
	}
	
	/**
	 * 功能: 获取缓存用户中的默认组织id
	 */
	public String getDefaultOrgId(){
		UserBaseRespVO userInfo = getCachedUserInfo();
		return userInfo != null ? userInfo.getDefaultOrgId() : null;
	}
}

//package com.example.hello.cloud.framework.common.kafka;
//
//import cn.hutool.core.util.StrUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
//import org.springframework.kafka.listener.MessageListenerContainer;
//
//import java.util.Arrays;
//@ConditionalOnProperty(prefix = "kafka",name = "consumer.servers",matchIfMissing = false)
//@Configuration
//@Slf4j
//public class KafKaListenerStartUpConfig implements CommandLineRunner {
//    @Value("${hello.kafka.startup.listenerIds:all}")
//    private String startUplistenerIds;
//    @Autowired
//    private KafkaListenerEndpointRegistry registry;
//
//    /**
//     * 读配置启动监听器
//     */
//    public void startListenersReadConfig() {
//        //如果没有配置指定kafka listener id，监听器全部启动
//        try {
//            log.info("startUplistenerIds:{}", startUplistenerIds);
//            if (StrUtil.isEmpty(startUplistenerIds.trim()) || "all".equalsIgnoreCase(startUplistenerIds.trim())) {
//            	log.info("listeners start up registry..."+   registry.getAllListenerContainers());
//                registry.getAllListenerContainers().forEach(messageListenerContainer -> {
//                    if (null != messageListenerContainer) {
//                    	log.info("listeners start up..."+messageListenerContainer);
//                        startUpListener(messageListenerContainer);
//                    }
//                });
//                log.info("listeners all start up...");
//            } else {
//                Arrays.asList(startUplistenerIds.split(",")).forEach(listenerId -> {
//                    MessageListenerContainer container = registry.getListenerContainer(listenerId);
//                    if (null == container) {
//                        log.error("找不到 " + listenerId + " 的监听器...");
//                    }else{
//                        startUpListener(container);
//                        log.info(listenerId + " listener start up...");
//                    }
//                });
//            }
//        } catch (Exception e) {
//            log.error("读配置启动监听器异常...", e);
//        }
//    }
//
//    /**
//     * start up a listener
//     * @param container
//     */
//    private void startUpListener(MessageListenerContainer container) {
//        if (!container.isRunning()) {
//            container.start();
//        }
//        //恢复
//        container.resume();
//    }
//
//    @Override
//    public void run(String... args) {
//        log.info("startListenersReadConfig...");
//        startListenersReadConfig();
//    }
//}

package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;

import java.util.List;

@Data
public class QueryCustInfoResp extends YiyunBasePage implements YiyunResponseData {
    private static final long serialVersionUID = 1470289281480308420L;

    private List<QueryCustInfoVo> list;
}

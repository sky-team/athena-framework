package com.example.hello.cloud.framework.common.enums;

/**
 * @ClassName: SessionKeyEnum
 * @Description: Session key枚举类
 * @author guohg03
 * @date 2018年7月26日
 */
public enum SessionKeyEnum {
	/**
	 * 验证码
	 */
	CAPTCHA("验证码", "captchaSesson"),
	
	/**
	 * 用户session
	 */
	USERSESSION("用户session", "userSession"),
	;
	
	/**
	 * session对应解释
	 */
	private final String value;
	
	/**
	 * session对应key
	 */
	private final String labelKey;
	
	SessionKeyEnum(String value, String labelKey) {
		this.value = value;
		this.labelKey = labelKey;
	}

	public String getValue() {
		return value;
	}

	public String getLabelKey() {
		return labelKey;
	}
}
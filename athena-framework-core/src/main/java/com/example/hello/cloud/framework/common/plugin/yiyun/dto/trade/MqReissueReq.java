package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 交易单重推请求参数
 *
 * @author v-linxb
 * @create 2019/10/15
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class MqReissueReq extends BaseYiyunRequestData {

    /**
     * 成交手机号；三选一必填
     */
    private String mobile;

    /**
     * 单据流水号；三选一必填
     */
    private String orderNo;

    /**
     * 证件号；三选一必填
     */
    private String certificateNo;
}

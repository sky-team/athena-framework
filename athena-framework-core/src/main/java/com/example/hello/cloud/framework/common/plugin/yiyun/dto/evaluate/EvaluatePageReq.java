package com.example.hello.cloud.framework.common.plugin.yiyun.dto.evaluate;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 获取评价中心统一评价页面请求入参
 *
 * @author v-linxb
 * @create 2019/7/24
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class EvaluatePageReq extends BaseYiyunRequestData implements Serializable {

    /**
     * 模板编号	必填：是
     */
    private String code;

    /**
     * 评价人账号 必填：是
     */
    private String evaluateBy;

    /**
     * 评价人姓名 必填：是
     */
    private String evaluateName;

    /**
     * 被评价对象 必填：是
     */
    private String evaluateSubject;

    /**
     * 批次 必填：否
     */
    private String batch;

    /**
     * 维度页面显示标题，有多个维度时用 |分隔 必填：否 如果和管理后台配置的显示相同则不需要传值
     */
    private String dimensionTitle;

    /**
     * 备注 必填：否
     */
    private String remark;

    /**
     * 是否需要显示不再提醒按钮 Y 需要显示，N不需要显示（默认N） 必填：否
     */
    private String noLongerRemind;

}

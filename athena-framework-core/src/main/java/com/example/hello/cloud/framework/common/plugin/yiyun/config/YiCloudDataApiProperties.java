package com.example.hello.cloud.framework.common.plugin.yiyun.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 涂永康
 * @className
 * @date 2019/8/6 17:28
 * @description
 */
@Data
@ConfigurationProperties(prefix = "yicloud")
public class YiCloudDataApiProperties {

    private String username;

    private String secretKey;

}

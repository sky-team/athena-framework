package com.example.hello.cloud.framework.common.enums;

/**
 * 用户类型
 *
 * @author zhoujj07
 * @create 2018/10/4
 */
public enum UserTypeEnum {

    /**
     * 统一后台管理员
     */
    ADMINISTRATOR("administrator", "统一后台管理员"),

    /**
     * 分享家企业合作平台账号（第三方机构用户）
     */
    ORG("org", "机构管理员"),

    /**
     * 经纪人
     */
    AGENT("agent", "经纪人"),

    /**
     * 在线家用户
     */
    LIFE_CUSTOMER("lifeCustomer", "在线家用户"),

    /**
     * 销售家顾问
     */
    CONSULTANT("consultant", "销售家顾问"),

    /**
     * 销售家经理
     */
    MANAGER("manager", "销售家经理"),

    /**
     * 置业神器顾问
     */
    APPLET_CONSULTANT("appletConsultant", "置业神器顾问"),

    /**
     * 置业神器客户
     */
    APPLET_CUSTOMER("appletCustomer", "置业神器客户"),
    
    /**
     * 外部系统
     */
    EXTERNAL_SYSTEM("externalSystem", "外部系统"),

    /**
     * 置业神器经理
     */
    APPLET_MANAGER("appletManager","置业神器经理"),

    /**
     * 拓客神器
     */
    APPLET_TOKER("appletToker", "拓客神器"),

    /**
     * e选房头条
     */
    EFANG_TOUTIAO("efangToutiao", "e选房头条"),
    /**
     * 易选房H5
     */
    MKT_H5("mkth5", "易选房h5"),
    
    /**
     * 万科万小二小程序
     */
	WXE_NANFANG_APPLET_TOKER("wanx2", "万科万小二小程序");
	
	
    private String type;
    private String name;

    UserTypeEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

}

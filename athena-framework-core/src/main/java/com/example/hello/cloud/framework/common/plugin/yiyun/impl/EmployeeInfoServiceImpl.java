package com.example.hello.cloud.framework.common.plugin.yiyun.impl;

import com.google.gson.reflect.TypeToken;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunService;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunCustomerApiEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employ.EmployeeInfoReq;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employ.EmployeeInfoResp;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.EmployeeInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;

/**
 * @author guoc16
 */
@Service
public class EmployeeInfoServiceImpl implements EmployeeInfoService {

    @Autowired
    YiyunService yiyunService;

    @Override
    public ApiResponse<EmployeeInfoResp> queryEmployeeInfo(EmployeeInfoReq request) {
        YiyunCustomerApiEnum type = YiyunCustomerApiEnum.QUERY_EMPLOYEE_TAG;
        Type responseType = new TypeToken<YiyunResponse<EmployeeInfoResp>>() {
        }.getType();
        YiyunResponse<EmployeeInfoResp> yiyunResponse = yiyunService.action2(type, request, responseType);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }
}

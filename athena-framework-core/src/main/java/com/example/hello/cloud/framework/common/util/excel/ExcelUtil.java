package com.example.hello.cloud.framework.common.util.excel;


import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.example.hello.cloud.framework.common.annotation.Column;
import com.example.hello.cloud.framework.common.annotation.Excel;
import com.example.hello.cloud.framework.common.annotation.Column;
import com.example.hello.cloud.framework.common.annotation.Excel;
import com.example.hello.cloud.framework.common.util.BrowserUtil;
import com.example.hello.cloud.framework.common.util.ReflectionsUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Excel处理工具类
 *
 * @author yingc04
 * @create 2019/11/4
 */
@Slf4j
public final class ExcelUtil {
    public static final int MAX_ROW = 1000;
    private static final DecimalFormat df = new DecimalFormat("#");

    /**
     * 功能：导出到excel(根据模板导出)
     *
     * @param dataList       行列表(不包括表头)
     * @param request
     * @param response
     * @param inputStream    文件流
     * @param exportFileName 文件名(可以从枚举中获取)
     */
    public static void export(List<Object> dataList, HttpServletRequest request, HttpServletResponse response,
                              InputStream inputStream, String exportFileName) {
        try {
            //导出工具类
            POIXSSFExcelBuilder builder = new POIXSSFExcelBuilder(inputStream);
            //格式化与导出
            builder.setList(dataList, (fieldName, fieldValue, param) -> fieldValue);
            builder.put("totalRecordSize", dataList.size());
            builder.createSXSSFWb();
            ExcelUtil.wrapExcelExportResponse(exportFileName, request, response); // 对response处理使其支持Excel
            builder.write(response.getOutputStream());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 功能:转换列类型
     */
    public static String convertCellType(Cell cell) {
        String result = "";
        if (cell == null) {
            return result;
        }
        int cellType = cell.getCellType();
        switch (cellType) {
            case Cell.CELL_TYPE_NUMERIC:
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    Date d = cell.getDateCellValue();
                    result = DateUtil.formatDateTime(d);
                    break;
                }
                result = String.valueOf(cell.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_STRING:
                result = cell.getRichStringCellValue().getString();
                break;
            case Cell.CELL_TYPE_FORMULA:
                result = cell.getCellFormula();
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * 功能：读取 Excel文件内容
     */
    public static List<List<String>> readExcelByInputStream(InputStream inputStream) {
        return readExcelByInputStream(inputStream, null);
    }

    public static List<List<String>> readExcelByInputStream(InputStream inputStream, String fileName) {
        return readExcelByInputStream(inputStream, fileName, false);
    }

    public static List<List<String>> readTotalExcelByInputStream(InputStream inputStream, String fileName) {
        return readExcelByInputStream(inputStream, fileName, true);
    }

    /**
     * 读取 Excel文件内容
     *
     * @param inputStream
     * @param fileName
     * @param headerColumns --是否读取首行列头
     * @return
     * @throws Exception
     */
    public static List<List<String>> readExcelByInputStream(InputStream inputStream, String fileName,
                                                            boolean headerColumns) {
        try {
            List<List<String>> result = new ArrayList<>();
            Workbook xswb = null;
            String fileExt = ".xlsx";
            if (StrUtil.isNotEmpty(fileName)) {
                fileExt = fileName.substring(fileName.lastIndexOf('.'));
            }
            if (".xls".equals(fileExt)) {
                xswb = new HSSFWorkbook(inputStream);
            } else {
                xswb = new XSSFWorkbook(inputStream);
            }
            Sheet sheet = xswb.getSheetAt(0);
            int rowNum = sheet.getLastRowNum();
            log.debug("readExcelByInputStream->total rows :{}", rowNum);
            // 读取标题-获取首行列数-给中间的空单元格添加空值
            int firstColumnNumber = 0;
            if (rowNum >= 0) {
                Row row = sheet.getRow(0);
                firstColumnNumber = row.getLastCellNum();
            }
            log.debug("firstColumnNumber={}", firstColumnNumber);
            int readFirstRow = headerColumns ? 0 : 1;    // 从哪行开始读取数据
            for (int i = readFirstRow; i < rowNum + 1; i++) {
                fillResult(result, sheet, firstColumnNumber, i);
            }
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    private static boolean fillResult(List<List<String>> result, Sheet sheet, int firstColumnNumber, int i) {
        Row row = sheet.getRow(i);    // row中只会封装有值的单元格
        if (row == null) {
            return true;
        }
        List<String> oneRowResult = new ArrayList<>();
        int cur = 0;
        int lastCellNum = row.getLastCellNum();
        for (int j = 0; j < lastCellNum; j++) {
            Cell cell = row.getCell(j, Row.CREATE_NULL_AS_BLANK);
            switch (cell.getCellType()) {
                case XSSFCell.CELL_TYPE_STRING: // 字符串
                    oneRowResult.add(cell.getStringCellValue().trim());
                    break;
                case XSSFCell.CELL_TYPE_NUMERIC: // 数字
                    double cellVal = cell.getNumericCellValue();
                    String cellValStr = String.valueOf(cellVal);
                    if (cellValStr.indexOf('E') != -1) {
                        DecimalFormat df = new DecimalFormat("0"); // 解决科学计数法
                        oneRowResult.add(df.format(cellVal));
                    } else if (cellValStr.indexOf('.') != -1) {
                        oneRowResult.add(String.valueOf(convertDownFromDouble4(cellValStr).doubleValue()));
                    } else {
                        oneRowResult.add(cellValStr);
                    }
                    break;
                case XSSFCell.CELL_TYPE_BOOLEAN: // Boolean
                    oneRowResult.add(cell.getBooleanCellValue() + "");
                    break;
                case XSSFCell.CELL_TYPE_FORMULA: // 公式
                    oneRowResult.add(cell.getCellFormula() + "");
                    break;
                case XSSFCell.CELL_TYPE_BLANK: // 空值
                    oneRowResult.add("");
                    break;
                default:
                    break;
            }
            cur++;
        }
        // 给中间的空单元格添加空值
        if (firstColumnNumber > cur) {
            for (int j = cur; j < firstColumnNumber; j++) {
                oneRowResult.add("");
            }
        }
        result.add(oneRowResult);
        return false;
    }

    public static BigDecimal convertDownFromDouble4(String str) {
        if (StrUtil.isEmpty(str)) {
            return BigDecimal.ZERO.setScale(4, RoundingMode.DOWN);
        } else {
            return new BigDecimal(str).setScale(4, RoundingMode.DOWN);
        }
    }

    /**
     * 功能：包装excel导出response，使其支持excel输出（xlsx)
     *
     * @param codedFileName 文件名
     * @param request       request请求对象
     * @param response      response请求对象
     */
    public static void wrapExcelExportResponse(String codedFileName, HttpServletRequest request, HttpServletResponse response) {
        wrapExcelExportResponse(codedFileName, null, request, response);
    }

    public static void wrapExcelExportResponse(String codedFileName, String ext, HttpServletRequest request, HttpServletResponse response) {
        if (ext == null) {
            ext = ".xlsx";
        }
        try {
            response.setContentType("application/vnd.ms-excel");
            String browse = BrowserUtil.checkBrowse(request);
            if (browse.contains("MSIE") || browse.contains("rv")) { // 根据浏览器进行转码，使其支持中文文件名
                response.setHeader("content-disposition", "attachment;filename="
                        + java.net.URLEncoder.encode(codedFileName, "UTF-8") + DateUtil.format(new DateTime(), DatePattern.PURE_DATETIME_FORMAT) + ext);
            } else {
                String newtitle = new String(codedFileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
                response.setHeader("content-disposition",
                        "attachment;filename=" + newtitle + DateUtil.format(new DateTime(), DatePattern.PURE_DATETIME_FORMAT) + ext);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 导入excel
     *
     * @param data  excel文件
     * @param clazz 导出的类
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List fromExcelToList(byte[] data, Class clazz) {
        List list = new ArrayList<>();
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
            Workbook hssfWorkbook = null;
            hssfWorkbook = getWorkbook(inputStream);
            Annotation excel = clazz.getAnnotation(Excel.class);
            if (excel == null) {
                log.error("该类无法使用Excel4j处理！");
                return Collections.emptyList();
            }
            if (hssfWorkbook.getSheetAt(0) == null) {
                log.error("没有可供处理的sheet页！");
                return Collections.emptyList();
            }
            org.apache.poi.ss.usermodel.Sheet hssfSheet = hssfWorkbook.getSheetAt(0);
            //通过表头获得序列
            int[] fieldIndexes = new int[clazz.getDeclaredFields().length];
            if (hssfSheet.getLastRowNum() == 0) {
                log.error("sheet页为空！");
                return Collections.emptyList();
            }
            for (int fieldIndex = 0; fieldIndex < clazz.getDeclaredFields().length; fieldIndex++) {
                boolean isHas = false;
                for (int colIndex = 0; colIndex < hssfSheet.getRow(0).getLastCellNum(); colIndex++) {
                    Column column = clazz.getDeclaredFields()[fieldIndex].getAnnotation(Column.class);
                    if (column != null && column.title().equals(hssfSheet.getRow(0).getCell(colIndex).getStringCellValue())) {
                        fieldIndexes[fieldIndex] = colIndex;
                        isHas = true;
                        break;
                    }
                }
                if (!isHas) {
                    fieldIndexes[fieldIndex] = -1;
                }
            }
            //获取数据
            for (int rowIndex = 1; rowIndex <= hssfWorkbook.getSheetAt(0).getLastRowNum(); rowIndex++) {
                fillData2(clazz, list, hssfWorkbook, fieldIndexes, rowIndex);
            }
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    private static void fillData2(Class clazz, List list, Workbook hssfWorkbook, int[] fieldIndexes, int rowIndex) throws InstantiationException, IllegalAccessException {
        Row hssfRow = hssfWorkbook.getSheetAt(0).getRow(rowIndex);
        Object object = clazz.newInstance();
        for (int fieldIndex = 0; fieldIndex < fieldIndexes.length; fieldIndex++) {
            fillObject(clazz, hssfRow.getCell(fieldIndexes[fieldIndex]), object, fieldIndex);
        }
        list.add(object);
    }

    private static void fillObject(Class clazz, Cell cell, Object object, int fieldIndex2) {
        if (fieldIndex2 == -1) {
            return;
        }
        Field field = clazz.getDeclaredFields()[fieldIndex2];
        Cell hssfCell = cell;
        String fieldName = clazz.getDeclaredFields()[fieldIndex2].getName();
        if (null == hssfCell) {
            return;
        }
        if (hssfCell.getCellType() == Cell.CELL_TYPE_STRING) {
            String value = cell.getStringCellValue();
            if (field.getType() == String.class) {
                ReflectionsUtil.setFieldValue(object, fieldName, value);
            } else if (field.getType() == Integer.class || field.getType() == int.class) {
                ReflectionsUtil.setFieldValue(object, fieldName, Integer.parseInt(value));
            } else if (field.getType() == BigDecimal.class) {
                ReflectionsUtil.setFieldValue(object, fieldName, new BigDecimal(value));
            }
        } else if (hssfCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            double value = cell.getNumericCellValue();
            if (field.getType() == Integer.class || field.getType() == int.class) {
                ReflectionsUtil.setFieldValue(object, fieldName, (int) value);
            } else if (field.getType() == Date.class) {
                ReflectionsUtil.setFieldValue(object, fieldName, cell.getDateCellValue());
            } else if (field.getType() == BigDecimal.class) {
                ReflectionsUtil.setFieldValue(object, fieldName, new BigDecimal(String.valueOf(value)));
            } else if (field.getType() == String.class) {
                ReflectionsUtil.setFieldValue(object, fieldName, df.format(value));
            }
        }
    }

    private static Workbook getWorkbook(ByteArrayInputStream inputStream) throws IOException {
        Workbook hssfWorkbook;
        try {
            hssfWorkbook = new XSSFWorkbook(inputStream);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            hssfWorkbook = new HSSFWorkbook(inputStream);
        }
        return hssfWorkbook;
    }
}
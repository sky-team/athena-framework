package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 客户主账号状态枚举
 *
 * @author zhoujj07
 * @create 2018/5/3
 */
public enum CustomerStatusEnum {

    /**
     * 已禁用
     */
    DISABLED(0),
    /**
     * 正常
     */
    NORMAL(1),
    /**
     * 未激活
     */
    NONACTIVATED (2),;

    private Integer index;

    CustomerStatusEnum(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

}

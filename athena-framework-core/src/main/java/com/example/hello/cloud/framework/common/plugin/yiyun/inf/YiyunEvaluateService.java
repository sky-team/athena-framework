package com.example.hello.cloud.framework.common.plugin.yiyun.inf;

import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.evaluate.EvaluatePageReq;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.evaluate.EvaluatePageResp;

/**
 * 翼云中台评价中心接口
 *
 * @author v-linxb
 * @create 2019/7/24
 **/
public interface YiyunEvaluateService {

    /**
     * 请求获取评价页面的地址
     *
     * @param req
     * @return
     */
    ApiResponse<EvaluatePageResp> evaluatePage(EvaluatePageReq req);
}

package com.example.hello.cloud.framework.common.enums;

/**
 * 翼云-交易单据类型
 *
 * @author pengdm.
 * 2018/5/21.
 */
public enum TransactionTypeEnum {
    /**
     * 认筹单
     */
    CHIP_ORDER("chipOrder", "认筹单"),
    /**
     * 认购单
     */
    BUY_ORDER("buyOrder", "认购单"),
    /**
     * 签约单
     */
    SIGN_ORDER("signOrder", "签约单");

    private String key;

    private String value;

    TransactionTypeEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}

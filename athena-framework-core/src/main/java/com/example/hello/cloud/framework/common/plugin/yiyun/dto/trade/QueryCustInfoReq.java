package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;

@Data
public class QueryCustInfoReq extends BaseYiyunRequestData {
    /**
     * 手机号
     */
    private String mobile;
}

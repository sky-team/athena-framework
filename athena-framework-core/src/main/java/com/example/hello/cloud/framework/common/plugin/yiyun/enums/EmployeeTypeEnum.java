package com.example.hello.cloud.framework.common.plugin.yiyun.enums;

/**
 * 员工类别枚举
 *
 * @author guoc16
 * @date 2020/09/03
 */
public enum EmployeeTypeEnum {

    /**
     * IDM中E类别都归类为员工  ZK经纪人判断时 E1 E2 E4为员工
     * 员工
     * <p>
     * 合同制员工	    E1	全日制	E11 非全日制	E12
     * 非合同制员工	    E2	劳务派遣员工	E21 业务外包员工	E22
     * 合作方人员	    E4	合作方人员 E41
     * <p>
     * 非员工
     * <p>
     * 协议制人员	E3	退休返聘	E31 顾问（非返聘）E32 实习	E33
     * 挂靠人员	E5	家属	E51 董监事 E52 残障人员	E53
     */

    CONTRACT("E1", "合同制员工"),
    UN_CONTRACT("E2", "非合同制员工"),
    AGREE("E3", "协议制人员"),
    COOPERATE("E4", "合作方人员"),
    DEPEND("E5", "挂靠人员");

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    EmployeeTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getNameByCode(String code) {
        for (EmployeeTypeEnum agentTypeEnum : EmployeeTypeEnum.values()) {
            if (agentTypeEnum.getCode().equals(code)) {
                return agentTypeEnum.getName();
            }
        }
        return null;
    }
}

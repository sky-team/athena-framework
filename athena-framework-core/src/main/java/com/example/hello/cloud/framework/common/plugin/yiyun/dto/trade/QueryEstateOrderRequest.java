package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;

import java.util.List;

/**
 * 地产交易单据查询请求入参
 *
 * @author v-zhongj11
 * @create 2018/5/31
 */
@Data
public class QueryEstateOrderRequest extends BaseYiyunRequestData {
    private String orderNo;
    private String channelId;
    private String channelSerial;
    private List<String> transactionType;
    private String status;
    private Long accountId;
    private String mobile;
    private Integer certificateType;
    private String certificateNo;
    private Long productProjectId;
    private Long productStageId;
    private Long productBuildingId;
    private Long productRoomId;
    private String createTimeBegin;
    private String createTimeEnd;
    private String updateTimeBegin;
    private String updateTimeEnd;
    private Integer pageNo;
    private Integer pageSize;
}
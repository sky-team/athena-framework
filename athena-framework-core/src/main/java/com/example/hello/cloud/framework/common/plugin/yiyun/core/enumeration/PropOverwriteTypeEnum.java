package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 属性操作类型
 *
 * @author v-linxb
 * @create 2018/5/16
 **/
public enum PropOverwriteTypeEnum {

    /**
     * 更新
     */
    PUT("put"),
    /**
     * 追加
     */
    APPEND("append"),
    /**
     * 删除已有属性
     */
    REMOVE("remove");

    PropOverwriteTypeEnum(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return this.value;
    }

}

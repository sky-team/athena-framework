package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.report.vo.Prop;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.report.vo.Prop;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * author :v-zhousq04
 * description: 产品上报实体
 * create date: 2018/8/22 10:28
 */
@Data
@EqualsAndHashCode
public class ProductReportReq extends BaseYiyunRequestData implements YiyunResponseData {
    private static final long serialVersionUID = -6540291075273740506L;

    private String name;            //产品名称
    private String productId;       //中台返回产品ID
    private String parentProductId;//上级产品id
    private Integer status;         //产品状态
    private Integer categoryId;     //所属类目id
    private String outerApp;        //外部应用
    private String outerId;         //外部编码，可以存放productCode
    private String createPerson;    //创建人的accountId
    private String propCodes;       //需要返回的产品属代码
    /**
     * 是否特殊产品 0:普通商品 1:组合商品
     * 必填
     */
    private Integer bundle;
    /**
     * 是否对其他业务开放
     * app: 仅当前业务可用(默认值)，all:其他业务可读
     * 必填
     */
    private String share;
    private List<Prop> props;       //产品属性,必须与中台给予的属性匹配，才能上报成功
}

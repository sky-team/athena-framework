package com.example.hello.cloud.framework.common.trace;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

/**
 * ThreadMdcUtil
 *
 * @author tongf01
 * @date 2019/10/12 17:56
 */
@Slf4j
public class ThreadMdcUtil {
    public static <T> Callable<T> wrap(final Callable<T> callable, final String traceId) {
        return () -> {
            if (traceId != null) {
                TraceContext.setCurrentTrace(traceId);
            }
            try {
                return callable.call();
            } finally {
                TraceContext.clearCurrentTrace();
            }
        };
    }

    public static Runnable wrap(final Runnable runnable, final String traceId) {
        return () -> {
            if (traceId != null) {
                TraceContext.setCurrentTrace(traceId);
            }
            try {
                runnable.run();
            } finally {
                TraceContext.clearCurrentTrace();
            }
        };
    }
}

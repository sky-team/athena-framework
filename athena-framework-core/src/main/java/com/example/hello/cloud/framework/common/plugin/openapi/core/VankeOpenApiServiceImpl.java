package com.example.hello.cloud.framework.common.plugin.openapi.core;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.hello.cloud.framework.common.plugin.openapi.config.OpenApiProperties;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.plugin.openapi.config.OpenApiProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.Md5Util;
import com.example.hello.cloud.framework.common.util.ReflectionUtil;
import com.example.hello.cloud.framework.common.util.json.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 万科API服务市场接口
 *
 * @author v-linxb
 * @create 2019/9/4
 **/
@Slf4j
@Component
public class exampleOpenApiServiceImpl implements exampleOpenApiService {
    private static final String KEY_STATUS_CODE = "statusCode";
    private static final String KEY_CODE = "code";
    private static final String KEY_JSON_RESULT = "jsonResult";
    private static final int KEY_OK_CODE = 200;

    @Autowired
    private OpenApiProperties openApiProperties;

    /**
     * API服务市场数据中台接口方法(POST)
     *
     * @param api           接口地址
     * @param requestObject 请求参数
     * @return 序列化的json数据
     */
    @Override
    public String postAction(OpenApiServiceType api, OpenApiBaseReqData requestObject) {
        return postAction(api, objToMap(requestObject));
    }

    /**
     * API服务市场数据中台接口方法(POST)
     *
     * @param api        接口地址
     * @param requestMap 请求参数
     * @return 序列化的json数据
     */
    @Override
    public String postAction(OpenApiServiceType api, Map requestMap) {
        String url = openApiProperties.getUrl() + api.url();
        String serviceId = api.serviceId();
        String username = openApiProperties.getUsername();
        String appId = openApiProperties.getAppId();
        String secretKey = openApiProperties.getSk();

        Map<String, Object> map = new HashMap<>(10);
        //body业务参数项
        map.putAll(requestMap);
        //headers公共参数项
        map.put("openapi-username", username);
        map.put("openapi-api-interface-id", serviceId);
        map.put("openapi-appid", appId);
        //获取签名
        String signature = getSign(map, secretKey);
        log.info(signature);

        //服务调用
        Map<String, String> headerMap = new HashMap<>(10);
        //组装header参数
        headerMap.put("openapi-api-interface-id", serviceId);
        headerMap.put("openapi-username", username);
        headerMap.put("openapi-signature", signature);
        headerMap.put("openapi-appid", appId);

        //post请求
        Map<String, Object> result = sendPost(headerMap, requestMap, url);
        if (result.containsKey(KEY_STATUS_CODE) && KEY_OK_CODE == (int) result.get(KEY_STATUS_CODE)) {
            String requestResult = (String) result.get(KEY_JSON_RESULT);
            JSONObject jsonObject = JSON.parseObject(requestResult);
            if (jsonObject.containsKey(KEY_CODE) && KEY_OK_CODE == jsonObject.getIntValue(KEY_CODE)) {
                jsonObject.put(KEY_CODE, ApiResponseCodeEnum.SUCCESS.getCode());
                requestResult = JSON.toJSONString(jsonObject);
            }
            return requestResult;
        }
        log.warn("postAction error {}", JsonUtil.obj2Str(result));
        return null;
    }

    /**
     * API服务市场数据中台接口方法(GET)
     *
     * @param api           接口地址
     * @param requestObject 请求参数
     * @return 序列化的json数据
     */
    @Override
    public String getAction(OpenApiServiceType api, OpenApiBaseReqData requestObject) {
        return getAction(api, objToMap(requestObject));
    }

    /**
     * API服务市场数据中台接口方法(GET)
     *
     * @param api        接口地址
     * @param requestMap 请求参数
     * @return 序列化的json数据
     */
    @Override
    public String getAction(OpenApiServiceType api, Map requestMap) {
        String url = openApiProperties.getUrl() + api.url();
        String serviceId = api.serviceId();
        String username = openApiProperties.getUsername();
        String appId = openApiProperties.getAppId();
        String secretKey = openApiProperties.getSk();
        //业务参数
        Map<String, Object> bussMap = new HashMap<>(16);
        //公共参数
        Map<String, Object> publicMap = new HashMap<>(16);
        //GET请求业务参数项
        bussMap.putAll(requestMap);
        //headers公共参数项
        publicMap.put("openapi-username", username);
        publicMap.put("openapi-api-interface-id", serviceId);
        publicMap.put("openapi-appid", appId);
        bussMap.putAll(publicMap);
        //获取签名
        String signature = getSign(bussMap, secretKey);
        //服务调用
        Map<String, String> headerMap = new HashMap<>(4);
        //组装header参数
        headerMap.put("openapi-api-interface-id", serviceId);
        headerMap.put("openapi-username", username);
        headerMap.put("openapi-signature", signature);
        headerMap.put("openapi-appid", appId);
        //get请求
        Map<String, Object> result = sendGet(headerMap, url);
        if (result.containsKey(KEY_STATUS_CODE) && KEY_OK_CODE == (int) result.get(KEY_STATUS_CODE)) {
            return (String) result.get(KEY_JSON_RESULT);
        }
        log.warn("getAction error {}", JsonUtil.obj2Str(result));
        return "";
    }


    /**
     * 参数签名算法sign
     *
     * @param map
     * @param secretKey
     * @return
     */
//    private static String getSign(Map<String, String> map, String secretKey) {
//        ArrayList<String> list = new ArrayList<>();
//        for (Map.Entry<String, String> entry : map.entrySet()) {
//            if (!"".equals(entry.getValue()) && null != entry.getValue() && !"signature".equals(entry.getKey())) {
//                list.add(String.valueOf(entry.getKey()) + "=" + String.valueOf(entry.getValue()) + "&");
//            }
//        }
//        int size = list.size();
//        String[] arrayToSort = list.toArray(new String[size]);
//        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
//        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < size; i++) {
//            sb.append(arrayToSort[i]);
//        }
//        String result = sb.toString();
//        result += "secretKey=" + secretKey;
//        result = Md5Util.MD5Encode(result, "UTF-8").toUpperCase();
//        return result;
//    }
    public String getSign(Map<String, Object> map, String secretKey) {
        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(map));
        ArrayList<String> list = new ArrayList<>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (!"".equals(entry.getValue()) && null != entry.getValue() && !"signature".equals(entry.getKey())) {
                list.add(String.valueOf(entry.getKey()) + "=" + String.valueOf(jsonObject.get(entry.getKey())) + "&");
            }
        }
        int size = list.size();
        String[] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        result += "secretKey=" + secretKey;
        result = Md5Util.MD5Encode(result, "UTF-8").toUpperCase();
        return result;
    }

    /**
     * post请求,json字符串
     *
     * @param headerMap
     * @param bodyMap
     * @param url
     * @return
     */
    private static Map<String, Object> sendPost(Map<String, String> headerMap, Map<String, String> bodyMap, String url) {
        log.info("headerMap={},bodyMap={},url={}", JSON.toJSONString(headerMap), JSON.toJSONString(bodyMap), url);
        Map<String, Object> map = new HashMap<>(10);
        //1--创建httpclient
        HttpClient httpClient = HttpClients.createDefault();
        //2--设置头信息
        HttpPost httpPost = new HttpPost(url);
        headerMap.put("Content-Type", "application/json");
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            httpPost.setHeader(entry.getKey(), entry.getValue());
        }
        try {
            log.info("OpenApi请求URL {}", url);
            //3--设置body信息
            JSONObject jsonParam = new JSONObject();
            for (Map.Entry<String, String> entry : bodyMap.entrySet()) {
                jsonParam.put(entry.getKey(), entry.getValue());
            }
            StringEntity entity = new StringEntity(jsonParam.toString(), "utf-8");
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
            log.info("OpenApi请求内容 {}", JsonUtil.obj2Str(httpPost.getEntity()));
            HttpResponse response = httpClient.execute(httpPost);
            //获取请求对象中的响应行对象
            StatusLine statusLine = response.getStatusLine();
            //从状态行中获取状态码
            int responseCode = statusLine.getStatusCode();
            String str = EntityUtils.toString(response.getEntity(), "utf-8");
            log.info("OpenApi请求结果 statusCode {},jsonResult {}", responseCode, str);
            map.put(KEY_STATUS_CODE, responseCode);
            map.put(KEY_JSON_RESULT, str);
        } catch (Exception e) {
            log.error("调用API服务市场失败 {}", e);
        } finally {
            httpPost.releaseConnection();
        }
        return map;
    }

    /**
     * GET请求
     *
     * @param headerMap
     * @param url
     * @return
     */
    private static Map<String, Object> sendGet(Map<String, String> headerMap, String url) {
        Map<String, Object> map = new HashMap<>(3);
        //1--创建httpclient
        HttpClient httpClient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(url);
        //2--设置头信息
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            httpget.setHeader(entry.getKey(), entry.getValue());
        }
        try {
            log.info("OpenApi请求URL {}", url);
            //3--向服务器端发送请求 并且获取响应对象
            HttpResponse response = httpClient.execute(httpget);
            //4--获取响应对象中的响应码
            // 获取请求对象中的响应行对象
            StatusLine statusLine = response.getStatusLine();
            // 从状态行中获取状态码
            int responseCode = statusLine.getStatusCode();
            String str = EntityUtils.toString(response.getEntity(), "utf-8");
            map.put(KEY_STATUS_CODE, responseCode);
            map.put(KEY_JSON_RESULT, str);
            log.info("OpenApi请求结果 statusCode {},jsonResult {}", responseCode, str);
        } catch (Exception e) {
            log.error("调用API服务市场失败 {}", e);
        } finally {
            httpget.releaseConnection();
        }
        return map;
    }

    /**
     * 对象转Map（value为空不进行转换）
     */
    private Map<String, String> objToMap(OpenApiBaseReqData obj) {
        List<Field> fields = ReflectionUtil.getAccessibleFields(obj);
        Map<String, String> map = new HashMap<>(16);
        fields.forEach(field -> {
            Object value = ReflectionUtil.invokeGetter(obj, field.getName());
            if (null != value) {
                map.put(field.getName(), value.toString());
            }
        });
        return map;
    }
}

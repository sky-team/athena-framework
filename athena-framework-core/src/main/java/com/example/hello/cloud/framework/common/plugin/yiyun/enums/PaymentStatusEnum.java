package com.example.hello.cloud.framework.common.plugin.yiyun.enums;

/**
 * @ClassName: PaymentStatusEnum
 * @Description: yiyun 支付枚举
 */
public enum PaymentStatusEnum {
    NOT_EXIST("E0000018", "支付订单不存在，或存在多条记录");


    private String credit;
    private String value;

    PaymentStatusEnum(String credit, String value) {
        this.credit = credit;
        this.value = value;
    }

    public String getCredit() {
        return credit;
    }

    public String getValue() {
        return this.value;
    }
}

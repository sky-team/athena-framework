package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by v-hut12 on 2018/9/6.
 */
@Data
@ApiModel(value = "极验二次验证入参")
public class GeetParam implements Serializable {

    @ApiModelProperty("geetest_challenge")
    private String challenge;
    @ApiModelProperty("geetest_validate")
    private String validate;
    @ApiModelProperty("geetest_seccode")
    private String seccode;

}

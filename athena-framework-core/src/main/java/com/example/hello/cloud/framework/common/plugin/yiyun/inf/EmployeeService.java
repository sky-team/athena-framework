package com.example.hello.cloud.framework.common.plugin.yiyun.inf;

import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee.QueryEmployeeListRequest;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee.QueryEmployeeListResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee.QueryEmployeeRequest;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employee.QueryEmployeeResponse;

public interface EmployeeService {

    /**
     * 员工信息查询接口
     *
     * @param request
     * @return
     */
    ApiResponse<QueryEmployeeResponse> queryEmployeeInfo(QueryEmployeeRequest request);

    /**
     * 批量查询员工信息查询接口
     *
     * @param request
     * @return
     */
    ApiResponse<QueryEmployeeListResponse> queryEmployeeList(QueryEmployeeListRequest request);

}

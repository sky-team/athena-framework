package com.example.hello.cloud.framework.common.plugin.yiyun.core;

import com.example.hello.cloud.framework.common.exception.ArgumentException;
import com.example.hello.cloud.framework.common.plugin.openapi.core.exampleOpenApiService;
import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunPayProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunServiceType;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequest;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequest2;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;
import com.google.common.reflect.TypeToken;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.exception.ArgumentException;
import com.example.hello.cloud.framework.common.http.HttpSyncClientUtil;
import com.example.hello.cloud.framework.common.http.common.HttpConfig;
import com.example.hello.cloud.framework.common.http.exception.HttpProcessException;
import com.example.hello.cloud.framework.common.plugin.openapi.core.exampleOpenApiService;
import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunPayProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.config.YiyunProperties;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunServiceType;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequest;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequest2;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.YiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;
import com.example.hello.cloud.framework.common.util.ReflectionUtil;
import com.example.hello.cloud.framework.common.util.json.GsonUtil;
import com.example.hello.cloud.framework.common.util.json.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 翼云中台接口统一调用服务
 *
 * @author v-linxb
 * @create 2018/5/3
 **/
@Slf4j
@Component
public class YiyunServiceImpl<T> implements YiyunService {

    @Autowired
    private YiyunProperties yiyunProperties;

    @Autowired
    private com.example.hello.cloud.framework.common.plugin.openapi.core.exampleOpenApiService exampleOpenApiService;

    @Override
    public YiyunResponse<T> action(YiyunServiceType serviceType, YiyunRequestData request, Type responseType) {
        log.info("调用中台api[{}-{}]", serviceType.url(), serviceType.desc());
        if (serviceType.needTicket() && StringUtils.isEmpty(request.getTicket())) {
            throw new ArgumentException("访问翼云中台api需要传入ticket：" + serviceType.url());
        }
        YiyunRequest yiyunRequest = packRequest(request, yiyunProperties.getAppId());
        YiyunResponse<T> result = new YiyunResponse<>();
        try {
            result = post(serviceType.url(), yiyunRequest, yiyunProperties.getSecret(), responseType);
        } catch (Exception e) {
            String errMsg = "调用中台api异常" + serviceType.url();
            log.error(errMsg + "{}", e);
            result.setCode(ApiResponseCodeEnum.EXCEPTION.getCode().toString());
            result.setMsg(errMsg);
        }
        return result;
    }

    @Override
    public YiyunResponse action2(YiyunServiceType serviceType, YiyunRequestData request, Type responseType) {
        log.info("调用中台api[{}-{}]", serviceType.url(), serviceType.desc());
        if (serviceType.needTicket() && StringUtils.isEmpty(request.getTicket())) {
            throw new ArgumentException("访问翼云中台api需要传入ticket：" + serviceType.url());
        }
        YiyunRequest2 yiyunRequest = packRequest2(request, yiyunProperties.getAppId());
        YiyunResponse<T> result = new YiyunResponse<>();
        try {
            result = post2(serviceType.url(), yiyunRequest, yiyunProperties.getSecret(), responseType);
        } catch (Exception e) {
            String errMsg = "调用中台api异常" + serviceType.url();
            log.error(errMsg + "{}", e);
            result.setCode(ApiResponseCodeEnum.EXCEPTION.getCode().toString());
            result.setMsg(errMsg);
        }
        return result;
    }

    @Override
    public YiyunResponse<T> zxjShopPaymentAction(YiyunServiceType serviceType, YiyunRequestData request, Type responseType, YiyunPayProperties payProperties) {
        log.info("YiyunServiceImpl zxjShopPaymentAction serviceType:{},request:{},responseType:{},payProperties:{}", serviceType, request, responseType, payProperties);
        if (serviceType.needTicket() && StringUtils.isEmpty(request.getTicket())) {
            throw new ArgumentException("访问翼云中台支付api需要传入ticket：" + serviceType.url());
        }
        YiyunRequest yiyunRequest = packRequest(request, payProperties.getAppId());
        log.info("YiyunServiceImpl zxjShopPaymentAction yiyunRequest:{}", yiyunRequest);
        YiyunResponse<T> result = new YiyunResponse<>();
        try {
            result = payPost(serviceType.url(), yiyunRequest, payProperties.getAppSecret(), responseType, payProperties);
            log.info("YiyunPaymentServiceImpl zxjShopPaymentAction result:{}", result);
        } catch (Exception e) {
            log.info("YiyunPaymentServiceImpl zxjShopPaymentAction error:{}", e);
            String errMsg = "调用中台支付api异常" + serviceType.url();
            result.setCode(ApiResponseCodeEnum.EXCEPTION.getCode().toString());
            result.setMsg(errMsg);
        }
        return result;
    }

    /**
     * 组装请求数据
     */
    private YiyunRequest packRequest(YiyunRequestData request, String appId) {
        YiyunRequest yiyunRequest = new YiyunRequest();
        yiyunRequest.setAppId(appId);
        yiyunRequest.setTicket(request.getTicket());
        yiyunRequest.setTenantId(request.getTenantId());
        yiyunRequest.setBizParam(GsonUtil.toJson(request, false));
        return yiyunRequest;
    }

    /**
     * 组装请求数据
     */
    private YiyunRequest2 packRequest2(YiyunRequestData request, String appId) {
        YiyunRequest2 yiyunRequest = new YiyunRequest2();
        yiyunRequest.setAppId(appId);
        yiyunRequest.setTicket(request.getTicket());
        yiyunRequest.setTenantId(request.getTenantId());
        yiyunRequest.setBizParam(GsonUtil.toJson(request, false));
        return yiyunRequest;
    }

    private YiyunResponse<T> post(String url, YiyunRequest object, String secret, Type responseType) throws HttpProcessException, IOException {
        Map<String, String> map = objectToMap(object);
        map.put("sign", SignUtils.signTopRequest(map, secret, map.get("sign_method")));
        Map<String, Object> objectMap = strMapToObjMap(map);
        //发起请求
        HttpConfig config = HttpConfig
                .custom()
                .contentType(ContentType.APPLICATION_FORM_URLENCODED.getMimeType())
                .url(yiyunProperties.getHost() + url)
                .map(objectMap);
        String result = HttpSyncClientUtil.post(config);
        //封装结果
        try {
            return GsonUtil.fromJson(result, responseType);
        } catch (Exception e) {
            log.error("解析异常：{}", e);
            return GsonUtil.fromJson(result, new TypeToken<YiyunResponse<T>>() {
            }.getType());
        }
    }

    private YiyunResponse<T> post2(String url, YiyunRequest2 object, String secret, Type responseType) throws HttpProcessException, IOException {
        Map<String, String> map = objectToMap2(object);
        map.put("sign", SignUtils.signTopRequest(map, secret, map.get("sign_method")));
        Map<String, Object> objectMap = strMapToObjMap(map);
        //发起请求
        HttpConfig config = HttpConfig
                .custom()
                .contentType(ContentType.APPLICATION_FORM_URLENCODED.getMimeType())
                .url(yiyunProperties.getHost() + url)
                .map(objectMap);
        String result = HttpSyncClientUtil.post(config);
        //封装结果
        try {
            return GsonUtil.fromJson(result, responseType);
        } catch (Exception e) {
            log.error("解析异常：{}", e);
            return GsonUtil.fromJson(result, new TypeToken<YiyunResponse<T>>() {
            }.getType());
        }
    }

    private YiyunResponse<T> payPost(String url, YiyunRequest object, String secret, Type responseType, YiyunPayProperties payProperties) throws HttpProcessException, IOException {
        log.info("YiyunServiceImpl payPost url :{},object:{},secret:{},responseType:{},payProperties:{}", url, object, secret, responseType, payProperties);
        Map<String, String> map = objectToMap(object);
        map.put("sign", SignUtils.signTopRequest(map, secret, map.get("sign_method")));
        Map<String, Object> objectMap = strMapToObjMap(map);
        log.info("YiyunServiceImpl payPost objectMap :{}", objectMap);
        // 发起请求
        HttpConfig config = HttpConfig
                .custom()
                .contentType(ContentType.APPLICATION_FORM_URLENCODED.getMimeType())
                .url(payProperties.getHost() + url)
                .map(objectMap);
        log.info("YiyunServiceImpl payPost config :{}", JsonUtil.obj2Str(config));
        String result = HttpSyncClientUtil.post(config);
        log.info("YiyunServiceImpl payPost result :{}", JsonUtil.obj2Str(result));
        try {
            return GsonUtil.fromJson(result, responseType);
        } catch (Exception e) {
            log.info("YiyunServiceImpl payPost error :{}", e);
            return GsonUtil.fromJson(result, new TypeToken<YiyunResponse<T>>() {
            }.getType());
        }
    }


    /**
     * 对象转Map（value为空不进行转换）
     */
    private Map<String, String> objectToMap(YiyunRequest obj) {
        List<Field> fields = ReflectionUtil.getAccessibleFields(obj);
        Map<String, String> map = new HashMap<>(16);
        fields.forEach(field -> {
            Object value = ReflectionUtil.invokeGetter(obj, field.getName());
            if (null != value) {
                map.put(field.getName(), value.toString());
            }
        });
        return map;
    }

    /**
     * 对象转Map（value为空不进行转换）
     */
    private Map<String, String> objectToMap2(YiyunRequest2 obj) {
        List<Field> fields = ReflectionUtil.getAccessibleFields(obj);
        Map<String, String> map = new HashMap<>(16);
        fields.forEach(field -> {
            Object value = ReflectionUtil.invokeGetter(obj, field.getName());
            if (null != value) {
                map.put(field.getName(), value.toString());
            }
        });
        return map;
    }

    /**
     * 将Map<String,String>转成Map<String,Object>，并进行URLEncode
     */
    private Map<String, Object> strMapToObjMap(Map<String, String> map) {
        Iterator<String> it = map.keySet().iterator();
        Map<String, Object> stringObjectMap = new HashMap<>(16);
        while (it.hasNext()) {
            String key = it.next();
            String value = map.get(key);
            stringObjectMap.put(key, value);
        }
        return stringObjectMap;
    }
}
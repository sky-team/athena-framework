package com.example.hello.cloud.framework.common.enums;

/**
 * Status
 *
 * @author yingc04
 * @create 2019/11/5
 */
public enum Status {

    /**
     * 无效
     */
    DISABLED(0, "无效"),

    /**
     * 有效
     */
    AVAILABLE(1, "有效");

    private int status;

    private String desc;

    Status(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public static Status getStatusEnum(int status) {
        for (Status statusEnum : values()) {
            if (statusEnum.getStatus() == status) {
                return statusEnum;
            }
        }
        throw new IllegalArgumentException("Cannot convert " + status + " to " + Status.class.getSimpleName() + " by status value.");
    }

}

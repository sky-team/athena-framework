package com.example.hello.cloud.framework.common.plugin.yiyun.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 映射中台属性code(公有)
 *
 * @author v-linxb
 * @create 2018/5/11
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface YiyunPubCode {

    /**
     * 对应在中台那边的属性code
     */
    String code();

    /**
     * 数组中获取第几个值
     */
    int index() default 0;

    /**
     * 是否写入，主要用于区分同一个model同时充当入参和出参，而入参和出参使用的字段不一样，code又一样的情况
     */
    boolean write() default true;
}
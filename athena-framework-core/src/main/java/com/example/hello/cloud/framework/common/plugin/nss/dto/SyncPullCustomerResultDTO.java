package com.example.hello.cloud.framework.common.plugin.nss.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/9.
 */
@Data
public class SyncPullCustomerResultDTO extends SyncResultDTO implements Serializable {
    private String id;
    private String age;
    private String sex;
    private String name;
    private String phoneNumber;
    private String certificateType;
    private String licenseNumber;
    private String contactAddress;
    private String marketWayId;
    private String updateDate;
    @ApiModelProperty(value = "户籍省份")
    private String birthplacProvince;
    @ApiModelProperty(value = "户籍城市")//
    private String birthplacCity;

    @Override
    public String toString() {
        return "SyncPullCustomerResultDTO{" +
                "id='" + id + '\'' +
                ", age='" + age + '\'' +
                ", sex='" + sex + '\'' +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", certificateType='" + certificateType + '\'' +
                ", licenseNumber='" + licenseNumber + '\'' +
                ", contactAddress='" + contactAddress + '\'' +
                ", marketWayId='" + marketWayId + '\'' +
                ", updateDate='" + updateDate + '\'' +
                ", birthplacProvince='" + birthplacProvince + '\'' +
                ", birthplacCity='" + birthplacCity + '\'' +
                '}';
    }
}

package com.example.hello.cloud.framework.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.core.type.StandardMethodMetadata;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 反射帮助类
 * @author zhoujj07
 * @create 2018/4/3
 */
@Slf4j
public final class ReflectionUtil {
    private ReflectionUtil() {
    }

    public static Object getMethodFieldValue(Object obj, String fieldName) {
        Object v = null;
        try {
            String methodName = "get" + fieldName.substring(0, 1).toUpperCase()
                    + fieldName.substring(1);
            Method method = obj.getClass().getMethod(methodName);
            v = method.invoke(obj);
            return v;
        } catch (Exception e) {
        	log.error(e.getMessage(),e);
        	return null;
        }
    }

    public static void setFieldValue(Object obj, String fieldName, Object value) {
        Field field = getAccessibleField(obj, fieldName);
        if (field == null) {
        	log.error("obj不能找到field:{}",obj,fieldName);
        	return;
        }
        try {
            field.set(obj, value);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
    }

    public static Class<?> getUserClass(Class<?> clazz) {
        String recrdit = "$$";
        if ((clazz != null) && (clazz.getName().contains(recrdit))) {
            Class<?> superClass = clazz.getSuperclass();
            if ((superClass != null) && (!Object.class.equals(superClass))) {
                return superClass;
            }
        }
        return clazz;
    }

    private static Field getAccessibleField(Object obj, String fieldName) {
        Assert.notNull(obj, "object can't be null");
        Assert.isTrue(!StringUtils.isEmpty(fieldName), "fieldName can't be blank");
        for (Class<?> superClass = obj.getClass();
             superClass != Object.class;
             superClass = superClass.getSuperclass()) {
            try {
                Field field = superClass.getDeclaredField(fieldName);
                field.setAccessible(true);
                return field;
            } catch (Exception e) {
            	log.error(e.getMessage(),e);
            }
        }
        return null;
    }

    public static List<Field> getAccessibleFields(Object obj) {
        Assert.notNull(obj, "object can't be null");
        List<Field> fieldList = new ArrayList<>();
        for (Class<?> superClass = obj.getClass();
             superClass != Object.class;
             superClass = superClass.getSuperclass()) {
            try {
                Field[] fs = superClass.getDeclaredFields();
                fieldList.addAll(0, Arrays.asList(fs));
            } catch (Exception e) {
            	log.error(e.getMessage(),e);
            }
        }
        return fieldList;
    }

    public static <T> Stream<String> getBeanNamesByTypeWithAnnotation(
            Class<T> beanType,
            Class<? extends Annotation> annotationType,
            AbstractApplicationContext applicationContext) {
        String[] beanNames = applicationContext.getBeanNamesForType(beanType);
        return Stream.of(beanNames).filter(name -> {
            final BeanDefinition beanDefinition = applicationContext
                    .getBeanFactory()
                    .getBeanDefinition(name);
            final Map<String, Object> beansWithAnnotation = applicationContext
                    .getBeansWithAnnotation(annotationType);
            if (!beansWithAnnotation.isEmpty()) {
                return beansWithAnnotation.containsKey(name);
            } else if (beanDefinition.getSource() instanceof StandardMethodMetadata) {
                StandardMethodMetadata metadata = (StandardMethodMetadata) beanDefinition.getSource();
                return metadata.isAnnotated(annotationType.getName());
            }
            return false;
        });
    }

    public static Collection<Object> getTypedBeansWithAnnotation(
            Class<? extends Annotation> annotationType,
            AbstractApplicationContext applicationContext) {
        String[] beanNames = applicationContext.getBeanNamesForAnnotation(annotationType);
        return Stream.of(beanNames).filter(name -> {
            BeanDefinition beanDefinition = applicationContext.getBeanFactory().getBeanDefinition(name);
            if (beanDefinition.getSource() instanceof StandardMethodMetadata) {
                StandardMethodMetadata metadata = (StandardMethodMetadata) beanDefinition.getSource();
                return metadata.isAnnotated(annotationType.getName());
            }
            return null != applicationContext.getBeanFactory().findAnnotationOnBean(name, annotationType);
        }).map(name -> applicationContext.getBeanFactory().getBean(name)).collect(Collectors.toList());
    }

    public static Object invokeGetter(Object obj, String propertyName) {
        String getterMethodName = "get" + StringUtils.capitalize(propertyName);
        return invokeMethod(obj, getterMethodName, new Class[0], new Object[0]);
    }

    public static Object getFieldValue(Object obj, String fieldName) {
        Field field = getAccessibleField(obj, fieldName);
        if (field == null) {
            log.error("Could not find field [" + fieldName + "] on target [" + obj + "]");
            return null;
        }
        Object result = null;
        try {
            result = field.get(obj);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return null;
        }
    }

    private static Object invokeMethod(Object obj, String methodName, Class<?>[] parameterTypes, Object[] args) {
        Method method = getAccessibleMethod(obj, methodName, parameterTypes);
        if (method == null) {
            throw new IllegalArgumentException("Could not find method [" + methodName + "] on target [" + obj + "]");
        }
        try {
            return method.invoke(obj, args);
        } catch (Exception e) {
        	log.error(e.getMessage(),e);
        	return null;
        }
    }

    private static Method getAccessibleMethod(Object obj, String methodName, Class<?>... parameterTypes) {
        Validate.notNull(obj, "object can't be null");
        for (Class<?> superClass = obj.getClass(); superClass != Object.class; superClass = superClass.getSuperclass()) {
            try {
                Method method = superClass.getDeclaredMethod(methodName, parameterTypes);
                method.setAccessible(true);
                return method;
            } catch (NoSuchMethodException e) {
            	log.error(e.getMessage(),e);
            }
        }
        return null;
    }
}
package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author guohg03
 * @ClassName: UpdateAvatarParam
 * @Description: 更新微信头像
 * @date 2018年10月4日
 */
@Data
public class UpdateAvatarParam implements Serializable {
	private static final long serialVersionUID = -2660200331108513731L;

	private String encryptedData;

    private String iv;
    
    private String sessionKey;
}

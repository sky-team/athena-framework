package com.example.hello.cloud.framework.common.util.excel;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.InputStream;

/**
 * DownloadUtil
 * 文件下载工具类
 *
 * @author yingc04
 * @create 2019/11/4
 */
@Slf4j
public class DownloadUtil {

    /**
     * 功能: 下载操作
     */
    public static void download(String codedFileName, InputStream in, HttpServletRequest request,
                                HttpServletResponse response) {
        try (BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream())) {
            ExcelUtil.wrapExcelExportResponse(codedFileName, request, response);
            byte[] data = new byte[1024];
            int len = 0;
            while (-1 != (len = in.read(data, 0, data.length))) {
                out.write(data, 0, len);
            }
            out.flush();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
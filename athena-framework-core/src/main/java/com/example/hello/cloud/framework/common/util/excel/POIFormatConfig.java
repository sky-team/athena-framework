package com.example.hello.cloud.framework.common.util.excel;

/**
 * POIFormatConfig
 *
 * @author yingc04
 * @create 2019/11/4
 */
public interface POIFormatConfig<T> {
    /**
     * 格式值
     *
     * @param fieldName  字段名
     * @param fieldValue 字段值
     * @param param      参数
     * @return 格式后的值
     */
    Object formatValue(String fieldName, Object fieldValue, T param);
}

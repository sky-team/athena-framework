package com.example.hello.cloud.framework.common.plugin.yiyun.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.reflect.TypeToken;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunService;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunCustomerApiEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.api.YiyunTradeApiEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade.*;
import com.example.hello.cloud.framework.common.plugin.yiyun.inf.TradeInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Type;
import java.util.List;

/**
 * User: ${v-zhongj11}
 * Date: 2018-10-01
 */
@Service
@Slf4j
public class TradeInfoServiceImpl implements TradeInfoService {

    private final YiyunService yiyunService;

    @Autowired
    public TradeInfoServiceImpl(YiyunService yiyunService) {
        this.yiyunService = yiyunService;
    }

    @Override
    public ApiResponse<QueryEstateOrderRespons> queryEstateOrder(QueryEstateOrderRequest reportDTO) {
        log.info("TradeInfoServiceImpl queryEstateOrder param:{}", reportDTO);
        YiyunTradeApiEnum type = YiyunTradeApiEnum.QUERY_ESTATE_ORDER;
        Type responseType = new TypeToken<YiyunResponse<QueryEstateOrderRespons>>() {
        }.getType();
        YiyunResponse<QueryEstateOrderRespons> yiyunResponse = yiyunService.action(type, reportDTO, responseType);
        log.info("TradeInfoServiceImpl queryEstateOrder param:{} result:{}", reportDTO, yiyunResponse);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ApiResponse<OrderDataResponse> queryInvoiceInfo(OrderDataRequest request) {
        log.info("TradeInfoServiceImpl queryInvoiceInfo param:{}", request);
        YiyunTradeApiEnum type = YiyunTradeApiEnum.ORDER_DATA;
        Type responseType = new TypeToken<YiyunResponse<OrderDataResponse>>() {
        }.getType();
        YiyunResponse<OrderDataResponse> yiyunResponse = yiyunService.action(type, request, responseType);
        log.info("TradeInfoServiceImpl queryInvoiceInfo param:{} result:{}", request, yiyunResponse);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ApiResponse<QueryTradeInfoResp> queryTradeInfo(QueryTradeInfoReq request) {
        log.info("TradeInfoServiceImpl queryTradeInfo param:{}", request);
        YiyunCustomerApiEnum type = YiyunCustomerApiEnum.QUERY_TRADE_INFO;
        Type responseType = new TypeToken<YiyunResponse<QueryTradeInfoResp>>() {
        }.getType();
        YiyunResponse<QueryTradeInfoResp> yiyunResponse = yiyunService.action2(type, request, responseType);
        log.info("TradeInfoServiceImpl queryTradeInfo param:{} result:{}", yiyunResponse, yiyunResponse);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ApiResponse<QueryCustInfoResp> queryCustInfoVo(QueryCustInfoReq request) {
        log.info("TradeInfoServiceImpl queryCustInfoVo param:{}", request);
        YiyunCustomerApiEnum type = YiyunCustomerApiEnum.QUERY_CUST_INFO;
        Type responseType = new TypeToken<YiyunResponse<QueryCustInfoResp>>() {
        }.getType();
        YiyunResponse<QueryCustInfoResp> yiyunResponse = yiyunService.action2(type, request, responseType);
        log.info("TradeInfoServiceImpl queryCustInfoVo param:{} result:{}", yiyunResponse, yiyunResponse);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    /**
     * 交易单重推
     *
     * @param req
     * @return
     */
    @Override
    public ApiResponse<MqReissueResp> tradeMqReissue(MqReissueReq req) {
        log.info("TradeInfoServiceImpl tradeMqReissue param:{}", req);
        YiyunTradeApiEnum type = YiyunTradeApiEnum.ORDER_MQ_REISSUE;
        Type responseType = new TypeToken<YiyunResponse<MqReissueResp>>() {
        }.getType();
        YiyunResponse<MqReissueResp> yiyunResponse = yiyunService.action(type, req, responseType);
        log.info("TradeInfoServiceImpl tradeMqReissue result:{}", yiyunResponse, yiyunResponse);
        if (yiyunResponse.isSuccess()) {
            return ApiResponse.success(yiyunResponse.getData());
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }

    @Override
    public ApiResponse<List<OrderDataResponse>> getInvoiceByMobile(String mobile) {
        log.info("TradeInfoServiceImpl getInvoiceByMobile mobile:{}", mobile);
        YiyunTradeApiEnum type = YiyunTradeApiEnum.ORDER_INVOICE;
        OrderDataRequest request = new OrderDataRequest();
        request.setMobile(mobile);
        Type responseType = new TypeToken<YiyunResponse<OrderInvoiceResponse>>() {
        }.getType();
        YiyunResponse<OrderInvoiceResponse> yiyunResponse = yiyunService.action(type, request, responseType);
        log.info("TradeInfoServiceImpl getInvoiceByMobile param:{} result:{}", request, yiyunResponse);
        if (yiyunResponse.isSuccess()) {
            //拼接用户名称
            if (!CollectionUtils.isEmpty(yiyunResponse.getData().getList())) {
                yiyunResponse.getData().getList().forEach(e -> {
                    if (!CollectionUtils.isEmpty(e.getOrderData())) {
                        e.getOrderData().forEach(e1 -> {
                            if (!CollectionUtils.isEmpty(e1.getInvoiceMQCustmoerObjectList())) {
                                StringBuffer sb = null;
                                for (JSONObject customer : e1.getInvoiceMQCustmoerObjectList()) {
                                    sb = new StringBuffer();
                                    sb.append(customer.getString("customerName") + ",");
                                }
                                e1.setCustomerName(sb.toString().substring(0, sb.length() - 1));
                            }
                        });
                    }
                });
                return ApiResponse.success(yiyunResponse.getData().getList());
            }
            return ApiResponse.success(null);
        } else {
            return ApiResponse.error(yiyunResponse.getMsg());
        }
    }
}

package com.example.hello.cloud.framework.common.plugin.yiyun.core;

import lombok.Data;

import java.io.Serializable;

/**
 * 属性键值对信息
 *
 * @author zhoujj07
 * @create 2018/5/3
 */
@Data
public class YiyunPropertyInfo implements Serializable{

    /**
     * 公有属性字段 pubPropCode和appPropCode二选一必填
     * 如果两者都传, 则忽略pubPropCode。
     */
    private String pubPropCode;

    /**
     * 私有属性字段 pubPropCode和appPropCode二选一必填
     * 如果两者都传, 则忽略pubPropCode。
     */
    private String appPropCode;

    /**
     * 属性值
     */
    private Object value;

    /**
     * 是否时间日期类型。如果为true，则按“yyyy-MM-dd HH:mm:ss”格式解析value, 记录为时间日期类型
     */
    private boolean isTimeValue = false;

    /**
     * 产品属性扩展值
     */
    private String extAttr;

    /**
     * 操作类型:保存操作不需要赋值，修改时需要赋值,如果propCode+value+ extAttr已存在则忽略。
     * put: 默认值，如果已存在更新。
     * append:追加记录, 如果已存在则追加值
     * remove:删除已有属性, 必须propCode+value+extAttr完全一致才能删除
     */
    private String overwriteType;
}

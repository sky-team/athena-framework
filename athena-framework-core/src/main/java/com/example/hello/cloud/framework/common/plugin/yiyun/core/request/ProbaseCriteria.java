package com.example.hello.cloud.framework.common.plugin.yiyun.core.request;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by zhuf04 on 2018/6/7.
 */
@Data
public class ProbaseCriteria implements Serializable {


    /**
     *公有属性
     */
    private String pubPropCode;
    /**
     *私有属性
     */
    private String appPropCode;
    /**
     * 类型
     */
    private String valueType;
    /**
     * 值
     */
    private Object value;
    /**
     * 数组值
     */
    private Object[] values;

    /**
     * 是否模糊查询  默认否
     */
    private Boolean wildcard;

    private String fieldCode;
}

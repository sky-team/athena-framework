package com.example.hello.cloud.framework.common.enums;

/**
 * 推荐人类型
 *
 * @author guoc16
 * @date 2020/3/3
 */
public enum RecommendTypeEnum {

    /**
     * 业主经纪人
     */
    OWNER_AGENT(1, RoleTypeEnum.AGENT.getCode(), "业主经纪人"),
    /**
     * 员工经纪人
     */
    STAFF_AGENT(2, RoleTypeEnum.AGENT.getCode(), "员工经纪人"),
    /**
     * 机构经纪人
     */
    THIRD_ORG_AGENT(3, RoleTypeEnum.AGENT.getCode(), "机构经纪人"),
    /**
     * 独立经纪人
     */
    INDEPENDENT_AGENT(4, RoleTypeEnum.AGENT.getCode(), "独立经纪人"),
    /**
     * 客户
     */
    CUSTOMER(5, RoleTypeEnum.CUSTOMER.getCode(), "客户"),
    /**
     * 外盘人员
     */
    OTHER(6, RoleTypeEnum.CONSULTANT.getCode(), "外盘人员"),
    /**
     * 本盘顾问
     */
    PROD_CONSULTANT(7, RoleTypeEnum.CONSULTANT.getCode(), "本盘顾问"),
    /**
     * 团队经理
     */
    PROD_TEAM(8, RoleTypeEnum.MANAGER.getCode(), "本盘团队非顾问人员"),
    /**
     * 项目经理
     */
    PROD_MANAGER(9, RoleTypeEnum.MANAGER.getCode(), "本盘无团队人员"),
    /**
     * 拓展顾问
     */
    EXPAND_CONSULTANT(10, RoleTypeEnum.CUSTOMER.getCode(), "拓展顾问"),
    /**
     * 中台客服
     */
    PLATFORM_SERVER(11, RoleTypeEnum.SERVER.getCode(), "客服");

    RecommendTypeEnum(Integer type, Integer role, String desc) {
        this.type = type;
        this.role = role;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public Integer getRole() {
        return role;
    }

    public String getDesc() {
        return this.desc;
    }


    private Integer type;
    private Integer role;
    private String desc;

    public static RecommendTypeEnum match(Integer type) {
        if (type == null) {
            return null;
        }
        for (RecommendTypeEnum recommendTypeEnum : RecommendTypeEnum.values()) {
            if (recommendTypeEnum.getType().equals(type)) {
                return recommendTypeEnum;
            }
        }
        return null;
    }
}

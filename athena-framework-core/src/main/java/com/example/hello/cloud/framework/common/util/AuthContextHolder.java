package com.example.hello.cloud.framework.common.util;

import cn.hutool.json.JSONUtil;
import com.example.hello.cloud.framework.common.constant.AuthConstant;
import com.example.hello.cloud.framework.common.constant.AuthConstant;
import com.example.hello.cloud.framework.common.dto.UserAccessInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 当前已认证用户的信息上下文持有工具
 *
 * @author zhanj04
 * @date 2019/10/24 16:17
 */
public class AuthContextHolder {
    /**
     * 日志
     */
    private static final Logger log = LoggerFactory.getLogger(AuthContextHolder.class);

    /**
     * @param headerName http reuqest header中的属性名称
     * @return String
     */
    private static String getRequetHeader(String headerName) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            String value = request.getHeader(headerName);
            return value;
        }
        return null;
    }

    /**
     * @param attriName http reuqest header中的属性名称
     * @param val       http reuqest header中的属性值
     */
    private static void setRequestHeaderAttri(String attriName, Object val) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            request.setAttribute(attriName, val);
        }
    }

    /**
     * 各业务系统call该方法获取用户中心主账号ID
     *
     * @return 用户中心主账号ID
     */
    public static String getMainAccountId() {
        return getRequetHeader(AuthConstant.CURRENT_USER_HEADER_MAIN_ACCOUNT_ID);
    }

    /**
     * 各业务系统call该方法获取客户端IP
     *
     * @return 客户端IP
     */
    public static String getClientIp() {
        return getRequetHeader(AuthConstant.CLIENT_IP);
    }

    /**
     * 各业务系统call该方法获取用户中心微信账号ID
     *
     * @return 用户中心微信账号ID
     */
    public static String getWechatAccountId() {
        return getRequetHeader(AuthConstant.CURRENT_USER_HEADER_WECHAT_ACCOUNT_ID);
    }

    /**
     * 各业务系统call该方法获取知客用户ID
     *
     * @return 知客用户ID
     */
    public static String getUserId() {
        return getRequetHeader(AuthConstant.CURRENT_USER_HEADER_USER_ID);
    }

    /**
     * 各业务系统call该方法获取用户中心主账号上的手机号
     *
     * @return 用户中心主账号手机号
     */
    public static String getPhone() {
        return getRequetHeader(AuthConstant.CURRENT_USER_HEADER_PHONE);
    }

    /**
     * 各业务系统call该方法获取用户中心的第三方ID
     *
     * @return 用户中心主账号手机号
     */
    public static String getThirdId() {
        return getRequetHeader(AuthConstant.CURRENT_USER_HEADER_THIRD_ID);
    }

    /**
     * 各业务系统call该方法获取当前登录用户的JWT
     *
     * @return 当前登录用户的JWT
     */
    public static String getJwt() {
        return getRequetHeader(AuthConstant.AUTHORIZATION_HEADER);
    }


    /**
     * 将当前登录用户的组织机构ID set到 request header 中
     *
     * @param orgId
     */
    public static void setOrgId(String orgId) {
        setRequestHeaderAttri(AuthConstant.CURRENT_USER_ORG_ID, orgId);
    }

    public static String getOrgId() {
        return getRequetHeader(AuthConstant.CURRENT_USER_ORG_ID);
    }

    /**
     * 获取用户访问信息
     */
    public static UserAccessInfo getUserAccessInfo() {
        UserAccessInfo userAccessInfo = new UserAccessInfo();
        String accessJson = getRequetHeader(AuthConstant.USER_ACCESS_INFO);
        UserAccessInfo temp = JSONUtil.toBean(accessJson, UserAccessInfo.class);
        if (temp != null) {
            userAccessInfo = temp;
        }
        return userAccessInfo;
    }
}
package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/5.
 */
@Data
public class SyncTokenResultDTO extends SyncResultDTO implements Serializable {
    private  String token;

    @Override
    public String toString() {
        return "SyncTokenResultDTO{" +
                "token='" + token + '\'' +
                "errCode='" + getErrCode() + '\'' +
                "errMsg='" + getErrMsg() + '\'' +
                '}';
    }
}

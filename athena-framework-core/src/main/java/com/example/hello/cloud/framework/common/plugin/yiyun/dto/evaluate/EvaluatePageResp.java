package com.example.hello.cloud.framework.common.plugin.yiyun.dto.evaluate;

import lombok.Data;

import java.io.Serializable;

/**
 * 评价中心评价页面响应对象
 *
 * @author v-linxb
 * @create 2019/7/24
 **/
@Data
public class EvaluatePageResp implements Serializable {

    /**
     * 打开评价页面的URL地址
     */
    private String url;
}

package com.example.hello.cloud.framework.common.plugin.openapi.api;

import com.example.hello.cloud.framework.common.plugin.openapi.core.OpenApiServiceType;
import com.example.hello.cloud.framework.common.plugin.openapi.core.OpenApiServiceType;

/**
 * @author 涂永康
 * @className
 * @date 2019/8/6 16:48
 * @description
 */
public enum YiyunProCusProfileApiEnum implements OpenApiServiceType {

    PRD_CUS_BASIC_INFO_REPORT("/proCusProfile/prdCusBasicInfoReport", "231", "项目营销画像/首页/客户基础信息"),

    CUS_PRO_FILE_DETAILS("/proCusProfile/cusProfileDetails", "229", "项目营销画像/客户画像详情"),

    CUS_PROFILE_PAGE("/proCusProfile/cusProfilePage", "230", "项目营销画像/客户画像列表"),

    PRD_CUS_DEPICT_REPORT("/proCusProfile/prdCusDepictReport", "232", "项目营销画像/首页/客户购房意向画像");

    private static final String BASIC_URL = "/v1.0/hello";

    private String url;
    private String serviceId;
    private String desc;

    YiyunProCusProfileApiEnum(String url, String serviceId, String desc) {
        this.url = url;
        this.serviceId = serviceId;
        this.desc = desc;
    }

    @Override
    public String url() {
        return BASIC_URL + this.url;
    }

    @Override
    public String serviceId() {
        return this.serviceId;
    }

    @Override
    public String desc() {
        return this.desc;
    }

}

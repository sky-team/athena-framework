package com.example.hello.cloud.framework.common.kafka.parse;

import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;


public class DataParse {
	private DataParse(){}
	public static <T>CanalDataVo<T> dataParse(String jsonData, Class<T> tClass){
		CanalDataVo<T> dataBean = new CanalDataVo<>();
		JSONObject jsonObject = JSONObject.parseObject(jsonData); 
		JSONArray newDataArray = jsonObject.getJSONArray("data");
		JSONObject mysqlTypeJson = jsonObject.getJSONObject("mysqlType");
		String operateType = (String) jsonObject.get("type");
		dataBean.setType(operateType);
		if ("ALTER".equals(operateType)) {
			return null;
		}
		String database = (String) jsonObject.get("database");
		String table = (String) jsonObject.get("table");
		dataBean.setDatabase(database);
		dataBean.setTable(table);
		dataBean.setMysqlType((Map)mysqlTypeJson);
		if (isUpdateOrInsertOrDeleteOperation(newDataArray, operateType)) {
			for (Object object : newDataArray) {
				DataMapUtils.doJavaDataMap((Map) object, mysqlTypeJson);
			}
			dataBean.setData(JSON.parseArray(JSON.toJSONString(newDataArray), tClass));
			if (isUpdateOperation(newDataArray, operateType)) {
				JSONArray oldJson = jsonObject.getJSONArray("old");
				dataBean.setOldData(JSON.parseArray(JSON.toJSONString(oldJson, SerializerFeature.WriteMapNullValue), Map.class));
			}
			return dataBean;
		}
		return null;
	}

	private static boolean isUpdateOrInsertOrDeleteOperation(JSONArray newDataArray, String operateType) {
		return ("INSERT".equals(operateType) || ("UPDATE".equals(operateType) && CollectionUtils.isNotEmpty(newDataArray)) || ("DELETE".equals(operateType) && CollectionUtils.isNotEmpty(newDataArray)));
	}

	private static boolean isUpdateOperation(JSONArray newDataArray, String operateType) {
		return ("UPDATE".equals(operateType) && CollectionUtils.isNotEmpty(newDataArray));
	}
	
	public static String dataParseForTableName(String jsonData){
		JSONObject jsonObject = JSONObject.parseObject(jsonData); 
		String table = (String) jsonObject.get("table");
		return table;
	}
	
}
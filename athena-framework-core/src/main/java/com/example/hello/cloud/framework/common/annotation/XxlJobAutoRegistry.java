package com.example.hello.cloud.framework.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * annotation for method jobhandler
 *
 * @author yeyg 2020-11-03
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface XxlJobAutoRegistry {

	
	 String jobGroupName();		// 执行器主键ID
	 String jobCron();		// 任务执行CRON表达式
	 String jobDesc();

	 String author();		// 负责人
	 String alarmEmail();	// 报警邮件

	 String executorRouteStrategy() default "CONSISTENT_HASH";	// 执行器路由策略
	 String executorHandler();		    // 执行器，任务Handler名称
	 String executorBlockStrategy() default "SERIAL_EXECUTION";	// 阻塞处理策略
	 int executorTimeout() default 0;     		// 任务执行超时时间，单位秒
	 int executorFailRetryCount() default 0;		// 失败重试次数
	
	 String glueType() default "BEAN";		// GLUE类型	#com.xxl.job.core.glue.GlueTypeEnum

}

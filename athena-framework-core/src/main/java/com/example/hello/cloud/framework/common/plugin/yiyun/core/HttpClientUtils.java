package com.example.hello.cloud.framework.common.plugin.yiyun.core;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * HttpClient4.3工具类
 */
public class HttpClientUtils {

    private static Logger logger = LoggerFactory.getLogger(HttpClientUtils.class); // 日志记录

    private static RequestConfig requestConfig = null;

    static {
        // 设置请求和传输超时时间
        requestConfig = RequestConfig.custom().setSocketTimeout(2000).setConnectTimeout(2000).build();
    }

    /**
     * post请求传输json参数
     *
     * @param url       url地址
     * @param jsonParam 参数
     * @return
     */
    public static JSONObject httpPost(String url, JSONObject jsonParam) {
        // post请求返回结果
        JSONObject jsonResult  = null;
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

            HttpPost httpPost = new HttpPost(url);
            // 设置请求和传输超时时间
            httpPost.setConfig(requestConfig);
            try {
                if (null != jsonParam) {

                    List<NameValuePair> paramList = new ArrayList<NameValuePair>();
                    for (String key : jsonParam.keySet()) {
                        paramList.add(new BasicNameValuePair(key, jsonParam.getString(key)));
                    }
                    // 模拟表单
                    UrlEncodedFormEntity entity = new UrlEncodedFormEntity(paramList, "utf-8");
                    httpPost.setEntity(entity);
                }
                CloseableHttpResponse result = httpClient.execute(httpPost);
                // 请求发送成功，并得到响应
                if (result.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    jsonResult = analysisResult(result);
                } else {
                    logger.error("post请求提交失败:" + url);
                }
            } catch (IOException e) {
                logger.error("post请求提交失败:" + url, e);
            } finally {
                httpPost.releaseConnection();
            }
        }catch (Exception e){
            logger.error("发送post请求失败{},{}",url,jsonParam,e);
        }
        return jsonResult;
    }

    /**
     * 发送get请求
     *
     * @param url 路径
     * @return
     */
    public static JSONObject httpGet(String url) {
        // get请求返回结果
        JSONObject jsonResult = null;
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            // 发送get请求
            HttpGet request = new HttpGet(url);
            request.setConfig(requestConfig);
            try {
                CloseableHttpResponse response = client.execute(request);
                // 请求发送成功，并得到响应
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    jsonResult = analysisResult(response);
                } else {
                    logger.error("get请求提交失败:" + url);
                }
            } catch (IOException e) {
                logger.error("get请求提交失败:" + url, e);
            } finally {
                request.releaseConnection();
            }
        }catch (Exception e){
            logger.error("发送get请求失败{},{}",url,e);
        }
        return jsonResult;
    }

    /**
     * 解析返回结果
     *
     * @param result
     * @return
     * @throws IOException
     */
    private static JSONObject analysisResult(CloseableHttpResponse result) throws IOException {
        // 读取服务器返回过来的json字符串数据
        String str = EntityUtils.toString(result.getEntity(), "utf-8");
        // 把json字符串转换成json对象
        JSONObject jsonResult = JSONObject.parseObject(str);
        return jsonResult;
    }

}
package com.example.hello.cloud.framework.common.plugin.yiyun.core.request;

import lombok.Data;

/**
 * 中台请求入参基类
 *
 * @author zhoujj07
 * @create 2018/5/8
 */
@Data
public abstract class BaseYiyunRequestData implements YiyunRequestData {

    /**
     * 中台需要的请求标识，不需要序列化到bizParam中
     */
    private String requestTicket;

    /**
     * 租户ID
     */
    private String requestTenantId;

    @Override
    public String getTicket() {
        return requestTicket;
    }

    @Override
    public void setTicket(String ticket) {
        this.requestTicket = ticket;
    }

    @Override
    public String getTenantId() {
        return requestTenantId;
    }

    @Override
    public void setTenantId(String tenantId) {
        this.requestTenantId = tenantId;
    }
}

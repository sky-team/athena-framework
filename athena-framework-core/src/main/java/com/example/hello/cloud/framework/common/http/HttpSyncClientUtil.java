package com.example.hello.cloud.framework.common.http;


import com.example.hello.cloud.framework.common.http.builder.HttpFactory;
import com.example.hello.cloud.framework.common.http.common.HttpConfig;
import com.example.hello.cloud.framework.common.http.common.HttpMethods;
import com.example.hello.cloud.framework.common.http.common.Utils;
import com.example.hello.cloud.framework.common.http.exception.HttpProcessException;
import com.example.hello.cloud.framework.common.http.builder.HttpFactory;
import com.example.hello.cloud.framework.common.http.common.HttpConfig;
import com.example.hello.cloud.framework.common.http.common.HttpMethods;
import com.example.hello.cloud.framework.common.http.common.Utils;
import com.example.hello.cloud.framework.common.http.exception.HttpProcessException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.IOException;
import java.io.OutputStream;

/**
 * http同步请求工具类
 *
 * @author v-linxb
 */
public final class HttpSyncClientUtil {

    /**
     * 以Get方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static String get(HttpConfig config) throws HttpProcessException {
        return send(config.method(HttpMethods.GET));
    }

    /**
     * 以Post方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static String post(HttpConfig config) throws HttpProcessException {
        return send(config.method(HttpMethods.POST));
    }

    /**
     * 以Put方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static String put(HttpConfig config) throws HttpProcessException {
        return send(config.method(HttpMethods.PUT));
    }

    /**
     * 以Delete方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static String delete(HttpConfig config) throws HttpProcessException {
        return send(config.method(HttpMethods.DELETE));
    }

    /**
     * 以Patch方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static String patch(HttpConfig config) throws HttpProcessException {
        return send(config.method(HttpMethods.PATCH));
    }

    /**
     * 以Head方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static String head(HttpConfig config) throws HttpProcessException {
        return send(config.method(HttpMethods.HEAD));
    }

    /**
     * 以Options方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static String options(HttpConfig config) throws HttpProcessException {
        return send(config.method(HttpMethods.OPTIONS));
    }

    /**
     * 以Trace方式，请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static String trace(HttpConfig config) throws HttpProcessException {
        return send(config.method(HttpMethods.TRACE));
    }

    /**
     * 下载文件
     *
     * @param config 请求参数配置
     * @return 返回处理结果
     * @throws HttpProcessException
     */
    public static OutputStream down(HttpConfig config) throws HttpProcessException {
        return Utils.fmt2Stream(execute(config.method(HttpMethods.GET)), config.out());
    }

    /**
     * 上传文件
     *
     * @param config 请求参数配置
     * @return 返回处理结果
     * @throws HttpProcessException
     */
    public static String upload(HttpConfig config) throws HttpProcessException {
        if (config.method() != HttpMethods.POST && config.method() != HttpMethods.PUT) {
            config.method(HttpMethods.POST);
        }
        return send(config);
    }

    /**
     * 查看资源链接情况，返回状态码
     *
     * @param config 请求参数配置
     * @return 返回处理结果
     * @throws HttpProcessException
     */
    public static int status(HttpConfig config) throws HttpProcessException {
        return Utils.fmt2Int(execute(config));
    }

    /**
     * 请求资源或服务
     *
     * @param config 请求参数配置
     * @return
     * @throws HttpProcessException
     */
    public static String send(HttpConfig config) throws HttpProcessException {
        return Utils.fmt2String(execute(config), config.outenc());
    }

    /**
     * 请求资源或服务
     *
     * @param config 请求参数配置
     * @return 返回HttpResponse对象
     * @throws HttpProcessException
     */
    private static HttpResponse execute(HttpConfig config) throws HttpProcessException {
        HttpResponse resp;
        try {
            //创建请求对象
            HttpRequestBase request = Utils.getRequest(config.url(), config.method());
            //处理请求对象
            Utils.handlerRequest(request, config);
            //执行请求操作，并拿到结果（同步阻塞）
            if (config.context() == null) {
                resp = HttpFactory.getInstance().getHttpSyncPool().custom().execute(request);
            } else {
                resp = HttpFactory.getInstance().getHttpSyncPool().custom().execute(request, config.context());
            }
            if (config.isReturnRespHeaders()) {
                //获取所有response的header信息
                config.headers(resp.getAllHeaders());
            }
            //获取结果实体
            return resp;

        } catch (IOException e) {
            throw new HttpProcessException(e);
        }
    }
}
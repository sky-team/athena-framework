package com.example.hello.cloud.framework.common.plugin.yiyun.core.dict;

/**
 * 产品中心字典
 *
 * @author v-linxb
 * @create 2018/5/5
 **/
public final class YiyunProductDict {

    /**
     * 工程域分期
     */
    public final static Long PROJECT_STAGE = 10001L;
    /**
     * 工程域楼栋
     */
    public final static Long PROJECT_BUILDING = 10002L;
    /**
     * 工程域房间
     */
    public final static Long PROJECT_ROOM = 10003L;

    /**
     * 房间相关属性
     */
    public final static String HOUSE_CLASS = "houseClass";
    public final static String HOUSE_AREA = "sapPredictedBuildingArea";
    /**
     * 户型
     */
    public final static String HOUSE_TYPE = "houseLayoutName";
}

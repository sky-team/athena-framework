package com.example.hello.cloud.framework.common.plugin.idm;

import java.io.Serializable;

public class RegisterEntity implements Serializable {

    /**姓名*/
    private String usercn;

    /**手机号*/
    private String mobile;

    /**用户ID*/
    private String username;

    /**邮箱*/
    private String mail;

    /**身份证号*/
    private String idcardnumber;

    /**证件类型*/
    private String certificateType;

    /**用户密码*/
    private String userpassword;

    /**密码生成策略*/
    private String pwdPolicy;

    /**万科用户类型*/
    private String vktype;

    /**上级万科id*/
    private String managerid;

    /**合作伙伴公司名称*/
    private String partnercompanyName;

    /**姓拼音*/
    private String lastnamepy;

    /**名拼音*/
    private String firstnamepy;

    public String getUsercn() {
        return usercn;
    }

    public String getMobile() {
        return mobile;
    }

    public String getIdcardnumber() {
        return idcardnumber;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public String getPwdPolicy() {
        return pwdPolicy;
    }

    public String getVktype() {
        return vktype;
    }

    public String getManagerid() {
        return managerid;
    }

    public String getPartnercompanyName() {
        return partnercompanyName;
    }

    public String getLastnamepy() {
        return lastnamepy;
    }

    public String getFirstnamepy() {
        return firstnamepy;
    }

    public void setUsercn(String usercn) {
        this.usercn = usercn;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setIdcardnumber(String idcardnumber) {
        this.idcardnumber = idcardnumber;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

    public void setPwdPolicy(String pwdPolicy) {
        this.pwdPolicy = pwdPolicy;
    }

    public void setVktype(String vktype) {
        this.vktype = vktype;
    }

    public void setManagerid(String managerid) {
        this.managerid = managerid;
    }

    public void setPartnercompanyName(String partnercompanyName) {
        this.partnercompanyName = partnercompanyName;
    }

    public void setLastnamepy(String lastnamepy) {
        this.lastnamepy = lastnamepy;
    }

    public void setFirstnamepy(String firstnamepy) {
        this.firstnamepy = firstnamepy;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public String getUsername() {
        return username;
    }

    public String getMail() {
        return mail;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}

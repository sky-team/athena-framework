package com.example.hello.cloud.framework.common.plugin.yiyun.inf;

import com.example.hello.cloud.framework.common.ApiResponse;

/**
 * @author zhoujj07
 * @ClassName: helloWrappedService
 * @Description: 基于翼云的相关服务包装的服务
 * @date 2018/8/23
 */
public interface helloWrappedService {

    /**
     * 鉴定是否员工
     *
     * @param phone 手机号
     * @return 是否员工
     */
    ApiResponse<Boolean> judgeEmployee(String phone);

    /**
     * 通过身份证鉴定是否业主
     *
     * @param phone         手机号
     * @param certificateNo 身份证号码
     * @return 是否业主
     */
    ApiResponse<Boolean> judgeOwnerByCertificateNo(String phone, String certificateNo);

    /**
     * 通过手机号鉴定是否业主
     *
     * @param phone 手机号
     * @return 是否业主
     */
    ApiResponse<Boolean> judgeOwnerByPhone(String phone);

}

package com.example.hello.cloud.framework.common.plugin.yiyun.inf;

import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employ.EmployeeInfoReq;
import com.example.hello.cloud.framework.common.plugin.yiyun.dto.employ.EmployeeInfoResp;

/**
 * @author guoc16
 * 翼云中台员工信息服务
 */
public interface EmployeeInfoService {


    /**
     * 手机号查询员工信息
     *
     * @param request
     * @return
     */
    ApiResponse<EmployeeInfoResp> queryEmployeeInfo(EmployeeInfoReq request);

}

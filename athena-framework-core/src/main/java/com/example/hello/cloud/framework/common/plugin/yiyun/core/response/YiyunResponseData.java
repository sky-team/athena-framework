package com.example.hello.cloud.framework.common.plugin.yiyun.core.response;

import java.io.Serializable;

/**
 * 中台公共返回数据接口标识
 *
 * @author v-linxb
 * @create 2018/4/24
 **/
public interface YiyunResponseData extends Serializable {

}

package com.example.hello.cloud.framework.common.exception;

import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

/**
 * 全局异常处理器
 * 
 * 依赖spring3.2提供的新注解@RestControllerAdvice，从名字上可以看出大体意思是控制器增强。原理是使用AOP对Controller控制器进行增强（前置增强、后置增强、环绕增强，）；
 * 
 * 那么我们可以自行对控制器的方法进行调用前（前置增强）和调用后（后置增强）的处理
 *
 * @author zhanj04
 * @date 2017-08-27 20:01
 */
@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    /**
     * @param e
     *            MissingServletRequestParameterException
     * @return ApiResponse
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ApiResponse handleError(MissingServletRequestParameterException e) {
        log.error("Missing Request Parameter：{}", e);
        String message = String.format("Missing Request Parameter: %s", e.getParameterName());
        return ApiResponse.error(message);
    }

    /**
     * @param e
     *            MethodArgumentTypeMismatchException
     * @return ApiResponse
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ApiResponse handleError(MethodArgumentTypeMismatchException e) {
        log.error("Method Argument Type Mismatch：{}", e);
        String message = String.format("Method Argument Type Mismatch: %s", e.getName());
        return ApiResponse.error(ApiResponseCodeEnum.PARAM_TYPE_ERROR, message);
    }

    /**
     * @param e
     *            MethodArgumentNotValidException
     * @return ApiResponse
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiResponse handleError(MethodArgumentNotValidException e) {
        log.error("Method Argument Not Valid：{}", e);
        BindingResult result = e.getBindingResult();
        FieldError error = result.getFieldError();
        String message = String.format("%s:%s", error.getField(), error.getDefaultMessage());
        return ApiResponse.error(ApiResponseCodeEnum.PARAM_VALID_ERROR.getCode(), message);
    }

    /**
     * @param e
     *            BindException
     * @return ApiResponse
     */
    @ExceptionHandler(BindException.class)
    public ApiResponse handleError(BindException e) {
        log.error("Bind Exception：{}", e);
        FieldError error = e.getFieldError();
        String message = String.format("%s:%s", error.getField(), error.getDefaultMessage());
        return ApiResponse.error(ApiResponseCodeEnum.PARAM_BIND_ERROR.getCode(), message);
    }

    /**
     * @param e
     *            ConstraintViolationException
     * @return ApiResponse
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ApiResponse handleError(ConstraintViolationException e) {
        log.error("Constraint Violation：{}", e);
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        ConstraintViolation<?> violation = violations.iterator().next();
        String path = ((PathImpl)violation.getPropertyPath()).getLeafNode().getName();
        String message = String.format("%s:%s", path, violation.getMessage());
        return ApiResponse.error(ApiResponseCodeEnum.PARAM_VALID_ERROR.getCode(), message);
    }

    /**
     * @param e
     *            HttpMessageNotReadableException
     * @return ApiResponse
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ApiResponse handleError(HttpMessageNotReadableException e) {
        log.error("Message Not Readable：{}", e);
        return ApiResponse.error(ApiResponseCodeEnum.MSG_NOT_READABLE.getCode(), e.getMessage());
    }

    /**
     * 处理由HttpRequestMethodNotSupportedException导致的异常
     *
     * @param e
     *            HttpRequestMethodNotSupportedException
     * @return ApiResponse
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ApiResponse handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        log.error("不支持的方法请求类型：{}", e);
        return ApiResponse.error(ApiResponseCodeEnum.HTTP_REQUEST_METHOD_NOT_SUPPORTED.getCode(), e.getMessage());
    }

    /**
     * @param e
     *            HttpMediaTypeNotSupportedException
     * @return ApiResponse
     */
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ApiResponse handleError(HttpMediaTypeNotSupportedException e) {
        log.error("Media Type Not Supported：{}", e);
        return ApiResponse.error(ApiResponseCodeEnum.MEDIA_TYPE_NOT_SUPPORTED.getCode(), e.getMessage());
    }

    /**
     * 处理由ValidatorKit操作的参数检查异常
     * 
     * @param e
     *            CheckedException
     * @return ApiResponse
     */
    @ExceptionHandler(CheckedException.class)
    @ResponseStatus(HttpStatus.OK)
    public ApiResponse handleValidException(CheckedException e) {
        log.error("参数校验不合法：{}", "", e);
        return ApiResponse.error(ApiResponseCodeEnum.PARAM_VALID_ERROR.getCode(), e.getMessage());
    }

    /**
     * @param e
     *            Throwable
     * @return ApiResponse
     */
    @ExceptionHandler(Throwable.class)
    public ApiResponse handleError(Throwable e) {
        log.error("Internal Server Error：{}", e);
        return ApiResponse.error("服务发生了一点小故障，请稍后再试");
    }

    /**
     * 全局异常
     * 
     * @param e
     *            Exception
     * @return ApiResponse
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiResponse exception(Exception e) {
        log.error("全局异常信息，异常堆栈信息:{}", e);
        return ApiResponse.error("服务发生了一点小故障，请稍后再试");
    }

}

package com.example.hello.cloud.framework.common.annotation;

import java.lang.annotation.*;

/**
 * 权限注解
 * 
 * @author zhanj04
 * @date 2017/8/27 15:21
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequirePermissons {

    /**
     * 操作类型
     */
    String[] value();

    /**
     * 是否登录就有此权限
     *
     * @return boolean
     */
    boolean hasPrivalige() default false;

}

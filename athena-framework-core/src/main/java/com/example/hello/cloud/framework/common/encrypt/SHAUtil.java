package com.example.hello.cloud.framework.common.encrypt;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * @ClassName: SHA1Util
 * @Description: SHA加密解密工具类
 * @author guohg03
 * @date 2018年8月11日
 */
public class SHAUtil {
	public static String shaEncode(String content) {
		return DigestUtils.shaHex(content);
	}
	
	/**
	 * 功能: sha1加密
	 */
	public static String sha1Encode(String content) {
		return DigestUtils.sha1Hex(content);
	}

	/**
	 * 功能: sha256加密
	 */
	public static String sha256Encode(String content) {
		return DigestUtils.sha256Hex(content);
	}
	
	/**
	 * 功能: sha384加密
	 */
	public static String sha384Encode(String content) {
		return DigestUtils.sha384Hex(content);
	}
	
	/**
	 * 功能: sha512加密
	 */
	public static String sha512Encode(String content) {
		return DigestUtils.sha512Hex(content);
	}

	public static void main(String[] args) {
		System.out.println(SHAUtil.sha1Encode("13543297233"));
		System.out.println(SHAUtil.sha256Encode("sshshshjijkjijkj"));
	}
}

package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author v-zhongj11
 * @create 2018/6/7
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrderDataResponse extends YiyunBasePage implements YiyunResponseData {

    private String orderNo;

    private String dataType;

    private List<OrderData> orderData;

}
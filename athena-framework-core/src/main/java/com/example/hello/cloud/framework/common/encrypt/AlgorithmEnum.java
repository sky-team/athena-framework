package com.example.hello.cloud.framework.common.encrypt;

/**
 * @ClassName: AlgorithmEnum
 * @Description: 算法类型
 * @author guohg03
 * @date 2018年8月11日
 */
public enum AlgorithmEnum {
	/**MD5*/
	MD5("md5", "MD5"),
	/**SHA 1算法*/
	SHA1("SHA-1", "SHA 1算法"),
	/**SHA 384算法*/
	SHA384("SHA-384", "SHA 384算法"),
	/**SHA 256算法*/
	SHA256("SHA-256", "SHA 256算法"),
	/**SHA 512算法*/
	SHA512("SHA-512", "SHA 512算法"),
	;

    private String code;
    private String name;

    AlgorithmEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

	public String getCode() {
		return code;
	}

	protected void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}
}
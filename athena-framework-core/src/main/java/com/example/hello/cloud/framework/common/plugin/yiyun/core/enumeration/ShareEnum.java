package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 属性开放程度
 *
 * @author v-linxb
 * @create 2018/5/18
 **/
public enum ShareEnum {
    /**
     * 仅当前业务可读(默认)
     */
    APP("app"),
    /**
     * 其它业务可读
     */
    ALL("all");

    ShareEnum(String value) {
        this.value = value;
    }

    private String value;


    public String getValue() {
        return this.value;
    }
}

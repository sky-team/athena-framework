//package com.example.hello.cloud.framework.common.kafka;
//
//import java.util.Arrays;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
//import org.springframework.kafka.listener.MessageListenerContainer;
//import org.springframework.stereotype.Component;
//
//import cn.hutool.core.util.StrUtil;
//import lombok.extern.slf4j.Slf4j;
//import sun.misc.Signal;
//import sun.misc.SignalHandler;
//
///**
// * 
// * @author 作者yuanxm01
// * 
// * @version 创建时间：2019年6月5日 上午11:08:05
// * 
// *          类说明
// * 
// */
//@SuppressWarnings("restriction")
//@Slf4j
//@Component
//public class ProgramExitSignalDealHandler implements SignalHandler {
//
//	@Value("${hello.kafka.startup.listenerIds:all}")
//	private String stoplistenerIds;
//
////	@Autowired
////	private KafkaListenerEndpointRegistry registry;
//	
//	public void registerSignal(String signalName) {
//		Signal signal = new Signal(signalName);
//		Signal.handle(signal, this);
//	}
//
//	@Override
//	public void handle(Signal signal) {
//		try {
//			log.info(signal.getName() + " is recevied");
//			if (signal.getName().equals("TERM")) {
//				handleTerm();
//			}
//			for (int i = 0; i < 50; i++) {
//				Thread.sleep(1000);
//				log.info("k8s pod stoping ... {} {}", i,"s");
//			}
//		} catch (Exception e) {
//			log.error("读配置启动监听器异常...", e);
//		}
//	}
//
//	private void handleTerm() {
//		log.info("stoplistenerIds:{}", stoplistenerIds);
//		//TODO 关闭kafka监听
////		if (StrUtil.isEmpty(stoplistenerIds.trim()) || "all".equalsIgnoreCase(stoplistenerIds.trim())) {
////			registry.getListenerContainers().forEach(messageListenerContainer -> {
////				if (null != messageListenerContainer) {
////					stopListener(messageListenerContainer);
////				}
////			});
////			log.info("listeners all stop up...");
////		} else {
////			Arrays.asList(stoplistenerIds.split(",")).forEach(listenerId -> {
////				MessageListenerContainer container = registry.getListenerContainer(listenerId);
////				if (null == container) {
////					log.error("找不到 " + listenerId + " 的监听器...");
////				}else{
////					stopListener(container);
////					log.info(listenerId + " listener stop up...");
////				}
////			});
////		}
//	}
//
//	private void stopListener(MessageListenerContainer container) {
//		if (container.isRunning()) {
//			container.stop();
//			log.info("listener stoped  ");
//		}
//	}
//}

package com.example.hello.cloud.framework.common.validator;


import com.example.hello.cloud.framework.common.annotation.Mobile;
import com.example.hello.cloud.framework.common.annotation.Mobile;
import org.apache.commons.lang.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 手机号校验
 *
 * @author guohg03
 * @date 2019/8/1 9:29
 */
public class MobileValidator implements ConstraintValidator<Mobile, String> {
    private volatile String regex;

    @Override
    public void initialize(Mobile constraintAnnotation) {
        if (StringUtils.isEmpty(regex)) {
            regex = "^(" +
                    "(13[0-9])|(14[0-9])|(15([0-9]))|(16[0-9])|" +
                    "(17[0-9])|(18[0-9])|(19[0-9])" +
                    ")" +
                    "\\d{8}$";
        }
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext validatorContext) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        //+开头的也支持
        if (value.startsWith("+")) {
            return true;
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }
}

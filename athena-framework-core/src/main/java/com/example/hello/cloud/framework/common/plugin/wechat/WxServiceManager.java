package com.example.hello.cloud.framework.common.plugin.wechat;


import java.util.HashMap;
import java.util.Map;

import com.example.hello.cloud.framework.common.plugin.wechat.utils.MakeWechatCodeUtils;
import com.example.hello.cloud.framework.common.plugin.wechat.utils.ZysqOssUtil2;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.WxEnvSetting;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.WxQrcodeInfo;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.enums.DomainEnum;
import com.example.hello.cloud.framework.common.plugin.wechat.utils.MakeWechatCodeUtils;
import com.example.hello.cloud.framework.common.plugin.wechat.utils.ZysqOssUtil2;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.WxEnvSetting;
import com.example.hello.cloud.framework.common.plugin.wechat.vo.WxQrcodeInfo;
import com.example.hello.cloud.framework.common.util.StringUtil;
import com.example.hello.cloud.framework.common.util.json.JsonUtil;
import com.example.hello.cloud.framework.common.util.oss.AliyunClassificationEnum;
import com.example.hello.cloud.framework.common.util.oss.AliyunFolderTypeEnum;
import com.example.hello.cloud.framework.common.util.oss.AliyunOssUtil;
import org.apache.commons.lang.StringUtils;



import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

@Slf4j
public class WxServiceManager {

    private AliyunOssUtil aliyunOSSUtil;

    private WxEnvSetting wxEnvSetting;

    public WxServiceManager(AliyunOssUtil aliyunOSSUtil, WxEnvSetting wxEnvSetting) {
        this.aliyunOSSUtil = aliyunOSSUtil;
        this.wxEnvSetting = wxEnvSetting;
        MakeWechatCodeUtils.setAliYunOSSUtil(aliyunOSSUtil);
    }

    /**
     * 生成圆形二维码
     *
     * @param wxQrcodeInfo WxQrcodeInfo
     * @return ApiRespResult
     */
    public ApiResponse<String> getQrocde(WxQrcodeInfo wxQrcodeInfo) {
        log.info("生成圆形二维码请求{}", JsonUtil.obj2Str(wxQrcodeInfo));
        String traceId = "";
        try {
            traceId = wxQrcodeInfo.getTraceId();
            //获得AccessToken
            String fileName = wxEnvSetting.getAppId() + "accessToken.txt";
            log.info("getQrocde fileName={}", fileName);
            String accessToken = "";
            // 获取置业神器的同步 token
            if ("wx1f6ef77faaff2c59".equals(wxEnvSetting.getAppId()) ||
                    "wxe9692d75c9e71da7".equals(wxEnvSetting.getAppId()) ||
                    "wx8e7bfe0f4e6d7b29".equals(wxEnvSetting.getAppId()) ||
                    "wx9f22cb8ef817791b".equals(wxEnvSetting.getAppId()) ||
                    "wx127951ea13ec5ad1".equals(wxEnvSetting.getAppId()) ||
                    "wxbd4beeb611423f0b".equals(wxEnvSetting.getAppId()) ||
                    "wx1c12568e4427cdc1".equals(wxEnvSetting.getAppId()) ||
                    "wxaac38610878d0feb".equals(wxEnvSetting.getAppId()) ||
                    WeChatConstString.TOKER_APPLET_APPID.equals(wxEnvSetting.getAppId())) {
                accessToken = ZysqOssUtil2.getTokenForZYSQ(wxEnvSetting.getAppId());
                if (StringUtil.isNullOrEmpty(accessToken)) {
                    accessToken = aliyunOSSUtil.getTokens(AliyunClassificationEnum.IMAGE, AliyunFolderTypeEnum.TOKEN, fileName);
                }
            } else if (DomainEnum.ZYSQ_BARGAIN_APPLET.getCode().equals(wxQrcodeInfo.getDomain())) {
                // 置业神器砍价小程序处理
                String tokenApiUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
                accessToken = MakeWechatCodeUtils.getAccessToken(wxEnvSetting.getAppId(), wxEnvSetting.getAppSecret(), tokenApiUrl);
            }else if( "wx64e56457ec5c6338".equals(wxEnvSetting.getAppId())){ //东莞万小二
                accessToken = wxQrcodeInfo.getAccessToken();
            }
            else {
                accessToken = aliyunOSSUtil.getTokens(AliyunClassificationEnum.IMAGE, AliyunFolderTypeEnum.TOKEN, fileName);
            }

            wxQrcodeInfo.setAccessToken(accessToken);
            //获得图片url
            Map<String, String> colorMap = new HashMap<>(4);
            Map<String, Object> paramMap = new HashMap<>(4);
            colorMap.put("r", "3");
            colorMap.put("g", "31");
            colorMap.put("b", "55");

            //跳转到的页面
            paramMap.put("scene", wxQrcodeInfo.getQrcodeId());
            String page = wxQrcodeInfo.getPage();
            if (StringUtils.isEmpty(page)) {
                page = "pages/index/index";
            }
            paramMap.put("page", page);

            paramMap.put("width", StringUtil.isNotEmpty(wxQrcodeInfo.getWidth()) ? wxQrcodeInfo.getWidth() : "430");
            paramMap.put("line_color", JSONObject.fromObject(wxQrcodeInfo.getLineColor() == null ? colorMap : wxQrcodeInfo.getLineColor()));
            if (wxQrcodeInfo.getHyaline() != null && wxQrcodeInfo.getHyaline()) {
                paramMap.put("is_hyaline", true);
            }
            JSONObject qrcodeParams = JSONObject.fromObject(paramMap);
            log.info("WxServiceManager getQrocdeSquare 生成圆形二维码 param={} wxQrcodeInfo={} wxEnvSetting={}",
                    qrcodeParams,wxQrcodeInfo, traceId, wxEnvSetting);
            return MakeWechatCodeUtils.getQrcode(qrcodeParams.toString(), wxQrcodeInfo, 0, this.wxEnvSetting);
        } catch (Exception exp) {
            log.error("WxServiceManager getQrocde 生成圆形二维码异常 param={} traceId={} error={}", wxQrcodeInfo, traceId, exp);
            return ApiResponse.error(exp.getMessage());
        }
    }

    /**
     * 生成方形二维码
     *
     * @param wxQrcodeInfo WxQrcodeInfo
     * @return ApiRespResult
     */
    public ApiResponse<String> getQrocdeSquare(WxQrcodeInfo wxQrcodeInfo) {
        log.info("生成方形二维码请求{}", JsonUtil.obj2Str(wxQrcodeInfo));
        String traceId = "";
        try {
            traceId = wxQrcodeInfo.getTraceId();
            //获得AccessToken
            String fileName = wxEnvSetting.getAppId() + "accessToken.txt";
            String accessToken = "";
            // 获取置业神器（房屋管家、分享家、拓客神器）的同步 token  其他小程序按照原来逻辑
            if ("wx1f6ef77faaff2c59".equals(wxEnvSetting.getAppId()) ||
                    "wxe9692d75c9e71da7".equals(wxEnvSetting.getAppId()) ||
                    "wx8e7bfe0f4e6d7b29".equals(wxEnvSetting.getAppId()) ||
                    WeChatConstString.TOKER_APPLET_APPID.equals(wxEnvSetting.getAppId())) {
                accessToken = ZysqOssUtil2.getTokenForZYSQ(wxEnvSetting.getAppId());
                if (StringUtil.isNullOrEmpty(accessToken)) {
                    accessToken = aliyunOSSUtil.getTokens(AliyunClassificationEnum.IMAGE, AliyunFolderTypeEnum.TOKEN, fileName);
                }
            } else if (DomainEnum.ZYSQ_BARGAIN_APPLET.getCode().equals(wxQrcodeInfo.getDomain())) {
                // 置业神器砍价小程序处理
                String tokenApiUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
                accessToken = MakeWechatCodeUtils.getAccessToken(wxEnvSetting.getAppId(), wxEnvSetting.getAppSecret(), tokenApiUrl);
            } else {
                accessToken = aliyunOSSUtil.getTokens(AliyunClassificationEnum.IMAGE, AliyunFolderTypeEnum.TOKEN, fileName);
            }

            wxQrcodeInfo.setAccessToken(accessToken);
            //获得图片url
            Map<String, Object> paramMap = new HashMap<>(4);
            //跳转到的页面
            String path = wxQrcodeInfo.getPath();
            paramMap.put("path", path);
            paramMap.put("width", "430");
            JSONObject qrcodeParams = JSONObject.fromObject(paramMap);
            log.info("WxServiceManager getQrocdeSquare 生成方形二维码 param={} wxQrcodeInfo={} wxEnvSetting={}",
                    qrcodeParams,wxQrcodeInfo, traceId, wxEnvSetting);
            return MakeWechatCodeUtils.getQrcode(qrcodeParams.toString(), wxQrcodeInfo, 0, this.wxEnvSetting);
        } catch (Exception exp) {
            log.error("WxServiceManager getQrocdeSquare 生成方形二维码异常 param={} traceId={} error={}", wxQrcodeInfo, traceId, exp);
            return ApiResponse.error(exp.getMessage());
        }
    }
}

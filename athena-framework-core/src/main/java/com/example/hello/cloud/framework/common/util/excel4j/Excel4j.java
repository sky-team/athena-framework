package com.example.hello.cloud.framework.common.util.excel4j;

import cn.hutool.core.collection.CollUtil;
import com.example.hello.cloud.framework.common.annotation.Column;
import com.example.hello.cloud.framework.common.annotation.Excel;
import com.example.hello.cloud.framework.common.annotation.Hearder;
import com.example.hello.cloud.framework.common.annotation.Sheet;
import com.example.hello.cloud.framework.common.enums.Status;
import com.example.hello.cloud.framework.common.annotation.Column;
import com.example.hello.cloud.framework.common.annotation.Excel;
import com.example.hello.cloud.framework.common.annotation.Hearder;
import com.example.hello.cloud.framework.common.annotation.Sheet;
import com.example.hello.cloud.framework.common.enums.Status;
import com.example.hello.cloud.framework.common.util.Reflections;
import com.example.hello.cloud.framework.common.util.excel4j.exception.ExcelException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddressList;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Excel4j
 * excel操作工具类
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Slf4j
public class Excel4j {

    private static final DecimalFormat df = new DecimalFormat("#");

    /**
     * 导出excel
     *
     * @param list 数据
     * @return
     * @throws ExcelException
     */
    public static byte[] toListFromExcel(List list) throws ExcelException {
        if (CollUtil.isEmpty(list)) {
            throw new ExcelException("没有数据！");
        }
        if (list.size() > 65535) {
            throw new ExcelException("数据量过大,请选择查询条件！");
        }
        Object object = list.get(0);
        Class clazz = Reflections.getUserClass(object.getClass());
        Annotation excel = clazz.getAnnotation(Excel.class);
        if (excel == null) {
            throw new ExcelException("该类无法使用Excel4j处理！ ");
        }
        Annotation sheet = clazz.getAnnotation(Sheet.class);
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet hssfSheet;
        if (sheet == null) {
            hssfSheet = workbook.createSheet();
        } else {
            hssfSheet = workbook.createSheet(((Sheet) sheet).sheetname());
        }
        //表头
        HSSFRow hssfRowHeader = hssfSheet.createRow(0);
        int colIndex = 0;
        for (Field field : Reflections.getAccessibleFields(object)) {
            Annotation column = field.getAnnotation(Column.class);
            if (column != null) {
                HSSFCell hssfCell = hssfRowHeader.createCell(colIndex);
                hssfCell.setCellValue(((Column) column).title());
                // 设置每列的宽
                hssfSheet.setColumnWidth(colIndex, ((Column) column).width());
                colIndex++;
            }
        }
        //内容
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        for (int rowIndex = 0; rowIndex < list.size(); rowIndex++) {
            fillHssfCell(list, workbook, hssfSheet, cellStyle, rowIndex);
        }
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return outputStream.toByteArray();
        } catch (IOException e) {
            log.error("文件写入失败！", e);
            throw new ExcelException("文件写入失败 ！");
        }
    }

    private static void fillHssfCell(List list, HSSFWorkbook workbook, HSSFSheet hssfSheet, HSSFCellStyle cellStyle, int rowIndex) {
        int colIndex;
        Object data = list.get(rowIndex);
        colIndex = 0;
        HSSFRow hssfRowContent = hssfSheet.createRow(rowIndex + 1);
        for (Field field : Reflections.getAccessibleFields(data)) {//获取父类属性
            Column column = field.getAnnotation(Column.class);
            if (column != null) {
                Object value = Reflections.getMethodFieldValue(data, field.getName());
                HSSFCell hssfCell = hssfRowContent.createCell(colIndex);
                if (value == null) {
                    hssfCell.setCellValue("");
                } else if (value instanceof Date) {
                    HSSFDataFormat format = workbook.createDataFormat();
                    cellStyle.setDataFormat(format.getFormat(column.dateFormat()));
                    hssfCell.setCellStyle(cellStyle);
                    hssfCell.setCellValue((Date) value);
                } else if (value instanceof Integer) {
                    hssfCell.setCellValue((Integer) value);
                } else if (value instanceof BigDecimal) {
                    hssfCell.setCellValue(((BigDecimal) value).doubleValue());
                } else if (value instanceof Status) {
                    hssfCell.setCellValue(((Status) value).getDesc());
                } else {
                    hssfCell.setCellValue(value.toString());
                }
                colIndex++;
            }
        }
    }

    /**
     * 导入excel
     *
     * @param data  excel文件
     * @param clazz 导出的类
     * @return
     * @throws ExcelException
     */
    public static List fromExcelToList(byte[] data, Class clazz) throws ExcelException {
        List list = new ArrayList<>();
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
            HSSFWorkbook hssfWorkbook = new HSSFWorkbook(inputStream);
            Annotation excel = clazz.getAnnotation(Excel.class);
            if (excel == null) {
                throw new ExcelException("该类无法使用Excel4j处理 ！");
            }
            int[] fieldIndexes = excelToList(clazz, hssfWorkbook);
            //获取数据
            for (int rowIndex = 1; rowIndex <= hssfWorkbook.getSheetAt(0).getLastRowNum(); rowIndex++) {
                fillList(clazz, list, hssfWorkbook, fieldIndexes, rowIndex);
            }

        } catch (IOException e) {
            log.error("excel无法读取！{}", e);
            throw new ExcelException("excel无法读取！");
        } catch (InstantiationException e) {
            log.error("Class无法实例化！{}", e);
            throw new ExcelException("Class无法实例化！");
        } catch (IllegalAccessException e) {
            log.error("无法读取Class的信息！{}", e);
            throw new ExcelException("无法读取Class的信息！");
        }
        return list;
    }

    private static void fillList(Class clazz, List list, HSSFWorkbook hssfWorkbook, int[] fieldIndexes, int rowIndex) throws InstantiationException, IllegalAccessException {
        HSSFRow hssfRow = hssfWorkbook.getSheetAt(0).getRow(rowIndex);
        Object object = clazz.newInstance();
        for (int fieldIndex = 0; fieldIndex < fieldIndexes.length; fieldIndex++) {
            fillObject(clazz, fieldIndexes, hssfRow, object, fieldIndex);
        }
        list.add(object);
    }

    private static void fillObject(Class clazz, int[] fieldIndexes, HSSFRow hssfRow, Object object, int fieldIndex) {
        if (fieldIndexes[fieldIndex] == -1) {
            return;
        }
        Field field = clazz.getDeclaredFields()[fieldIndex];
        HSSFCell hssfCell = hssfRow.getCell(fieldIndexes[fieldIndex]);
        String fieldName = clazz.getDeclaredFields()[fieldIndex].getName();
        if (null == hssfCell) {
            Reflections.setFieldValue(object, fieldName, "");
        } else {
            if (hssfCell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                fromExcelDataToList2(fieldIndexes, hssfRow, object, fieldIndex, field, fieldName);
            } else if (hssfCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                double value = hssfRow.getCell(fieldIndexes[fieldIndex]).getNumericCellValue();
                if (field.getType() == Integer.class || field.getType() == int.class) {
                    Reflections.setFieldValue(object, fieldName, (int) value);
                } else if (field.getType() == Date.class) {
                    Reflections.setFieldValue(object, fieldName, hssfRow.getCell(fieldIndexes[fieldIndex]).getDateCellValue());
                } else if (field.getType() == BigDecimal.class) {
                    Reflections.setFieldValue(object, fieldName, new BigDecimal(String.valueOf(value)));
                } else if (field.getType() == String.class) {
                    Reflections.setFieldValue(object, fieldName, df.format(value));
                }
            }
        }
    }


    /**
     * 导出excel,重写 解决导出样式异常问题
     *
     * @param list 数据
     * @return
     * @throws ExcelException
     */
    public static byte[] toExcelFromList(List list) throws ExcelException {
        if (CollUtil.isEmpty(list)) {
            throw new ExcelException("没有数据！");
        }
        if (list.size() > 65535) {
            throw new ExcelException("数据量过大,请选择查询条件！");
        }
        Object object = list.get(0);
        Class clazz = Reflections.getUserClass(object.getClass());
        Annotation excel = clazz.getAnnotation(Excel.class);
        if (excel == null) {
            throw new ExcelException("该类无法使用Excel4j处理！");
        }
        Annotation sheet = clazz.getAnnotation(Sheet.class);
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet hssfSheet;
        if (sheet == null) {
            hssfSheet = workbook.createSheet();
        } else {
            hssfSheet = workbook.createSheet(((Sheet) sheet).sheetname());
        }
        //表头
        HSSFRow hssfRowHeader = hssfSheet.createRow(0);
        // 表头样式
        addHearderCellStyle(hssfRowHeader, hssfSheet, workbook, clazz);

        int colIndex = 0;
        Map<Integer, HSSFCellStyle> styleMap = new HashMap<>();
        for (Field field : Reflections.getAccessibleFields(object)) {
            Column column = field.getAnnotation(Column.class);
            if (column != null) {
                // 增加列样式
                HSSFCellStyle style = workbook.createCellStyle();
                styleMap.put(Integer.valueOf(colIndex), style);
                addColumnStyle(workbook, hssfSheet, column, colIndex, styleMap.get(colIndex));
                // 设置表头列名
                HSSFCell hssfCell = hssfRowHeader.createCell(colIndex);
                hssfCell.setCellValue((column).title());
                // 设置每列的宽
                hssfSheet.setColumnWidth(colIndex, column.width());
                colIndex++;
            }
        }
        for (int rowIndex = 0; rowIndex < list.size(); rowIndex++) {
            Object data = list.get(rowIndex);
            colIndex = 0;
            HSSFRow hssfRowContent = hssfSheet.createRow(rowIndex + 1);
            for (Field field : Reflections.getAccessibleFields(data)) {//获取父类属性
                Column column = field.getAnnotation(Column.class);
                if (column != null) {
                    Object value = Reflections.getMethodFieldValue(data, field.getName());
                    HSSFCell hssfCell = hssfRowContent.createCell(colIndex);
                    hssfCell.setCellStyle(styleMap.get(colIndex));
                    // 去掉小三角
                    setValue(value, hssfCell);
                    colIndex++;
                }
            }
        }
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return outputStream.toByteArray();
        } catch (IOException e) {
            log.error("文件写入失败！{}", e);
            throw new ExcelException("文件写入失败！");
        }
    }

    /**
     * 导入excel
     *
     * @param data  excel文件
     * @param clazz 导出的类
     * @return
     * @throws ExcelException
     */
    public static List fromExcelDataToList(byte[] data, Class clazz) throws ExcelException {
        List list = new ArrayList<>();
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
            HSSFWorkbook hssfWorkbook = new HSSFWorkbook(inputStream);
            Annotation excel = clazz.getAnnotation(Excel.class);
            if (excel == null) {
                throw new ExcelException("该类无法使用Excel4j处理！");
            }
            int[] fieldIndexes = excelToList(clazz, hssfWorkbook);
            //获取数据
            for (int rowIndex = 1; rowIndex <= hssfWorkbook.getSheetAt(0).getLastRowNum(); rowIndex++) {
                fillList2(clazz, list, hssfWorkbook, fieldIndexes, rowIndex);
            }

        } catch (IOException e) {
            log.error("excel无法读取！{}", e);
            throw new ExcelException("excel无法读取！");
        } catch (InstantiationException e) {
            log.error("Class无法实例化！{}", e);
            throw new ExcelException("Class无法实例化！");
        } catch (IllegalAccessException e) {
            log.error("无法读取Class的信息！{}", e);
            throw new ExcelException("无法读取Class的信息！");
        }
        return list;
    }

    private static void fillList2(Class clazz, List list, HSSFWorkbook hssfWorkbook, int[] fieldIndexes, int rowIndex) throws InstantiationException, IllegalAccessException {
        HSSFRow hssfRow = hssfWorkbook.getSheetAt(0).getRow(rowIndex);
        Object object = clazz.newInstance();
        for (int fieldIndex = 0; fieldIndex < fieldIndexes.length; fieldIndex++) {
            Column column = clazz.getDeclaredFields()[fieldIndex].getAnnotation(Column.class);
            fillObject2(clazz, fieldIndexes, hssfRow, object, fieldIndex, column);
        }
        list.add(object);
    }

    private static void fillObject2(Class clazz, int[] fieldIndexes, HSSFRow hssfRow, Object object, int fieldIndex, Column column) {
        if (fieldIndexes[fieldIndex] == -1) {
            return;
        }
        Field field = clazz.getDeclaredFields()[fieldIndex];
        HSSFCell hssfCell = hssfRow.getCell(fieldIndexes[fieldIndex]);
        String fieldName = clazz.getDeclaredFields()[fieldIndex].getName();
        if (null == hssfCell) {
            Reflections.setFieldValue(object, fieldName, "");
            return;
        }
        if (hssfCell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
            fromExcelDataToList2(fieldIndexes, hssfRow, object, fieldIndex, field, fieldName);
            return;
        }
        if (hssfCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
            double value = hssfRow.getCell(fieldIndexes[fieldIndex]).getNumericCellValue();
            if (field.getType() == Integer.class || field.getType() == int.class) {
                Reflections.setFieldValue(object, fieldName, (int) value);
            } else if (field.getType() == Date.class) {
                Reflections.setFieldValue(object, fieldName, hssfRow.getCell(fieldIndexes[fieldIndex]).getDateCellValue());
            } else if (field.getType() == BigDecimal.class) {
                Reflections.setFieldValue(object, fieldName, new BigDecimal(String.valueOf(value)));
            } else if (field.getType() == String.class) {
                if (column.format().contains(".")) {
                    Reflections.setFieldValue(object, fieldName, new DecimalFormat(column.format()).format(value));
                } else {
                    Reflections.setFieldValue(object, fieldName, df.format(value));
                }
            }
        }
    }

    private static int[] excelToList(Class clazz, HSSFWorkbook hssfWorkbook) {
        if (hssfWorkbook.getSheetAt(0) == null) {
            throw new ExcelException("没有可供处理的sheet页！");
        }
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        //通过表头获得序列
        int[] fieldIndexes = new int[clazz.getDeclaredFields().length];
        if (hssfSheet.getLastRowNum() == 0) {
            throw new ExcelException("sheet页为空！");
        }
        for (int fieldIndex = 0; fieldIndex < clazz.getDeclaredFields().length; fieldIndex++) {
            boolean isHas = false;
            for (int colIndex = 0; colIndex < hssfSheet.getRow(0).getLastCellNum(); colIndex++) {
                Column column = clazz.getDeclaredFields()[fieldIndex].getAnnotation(Column.class);
                if (column != null && column.title().equals(hssfSheet.getRow(0).getCell(colIndex).getStringCellValue())) {
                    fieldIndexes[fieldIndex] = colIndex;
                    isHas = true;
                    break;
                }
            }
            if (!isHas) {
                fieldIndexes[fieldIndex] = -1;
            }
        }
        return fieldIndexes;
    }

    private static void fromExcelDataToList2(int[] fieldIndexes, HSSFRow hssfRow, Object object, int fieldIndex, Field field, String fieldName) {
        String value = hssfRow.getCell(fieldIndexes[fieldIndex]).getStringCellValue();
        if (field.getType() == String.class) {
            Reflections.setFieldValue(object, fieldName, value);
        } else if (field.getType() == Integer.class || field.getType() == int.class) {
            Reflections.setFieldValue(object, fieldName, Integer.parseInt(value));
        } else if (field.getType() == BigDecimal.class) {
            Reflections.setFieldValue(object, fieldName, new BigDecimal(value));
        }
    }


    private static void setValue(Object value, HSSFCell hssfCell) {

        if (null == value || StringUtils.isNoneBlank(value.toString())) {
            hssfCell.setCellType(Cell.CELL_TYPE_STRING);
            hssfCell.setCellValue(value == null ? null : value.toString());
        } else if (isrealNumber(value.toString())) {
            // 设置当前单元格值类型
            hssfCell.setCellType(Cell.CELL_TYPE_STRING);
            if (isIntegralNum(value.toString())) {
                hssfCell.setCellValue(Long.parseLong(value.toString()));
            } else {
                hssfCell.setCellValue(Double.parseDouble(value.toString()));
            }
        } else {
            hssfCell.setCellType(Cell.CELL_TYPE_STRING);
            hssfCell.setCellValue(value.toString());
        }

    }

    /**
     * 整数
     */
    private static final Pattern ISINTEGRALNUM_P = Pattern.compile("^(-)?[0-9]*$");

    /**
     * 实数
     */
    private static final Pattern REALNUMBER_P = Pattern.compile("^(-?\\d+)(\\.\\d+)?$");

    private static boolean isIntegralNum(String str) {
        return ISINTEGRALNUM_P.matcher(str).matches();
    }

    private static boolean isrealNumber(String str) {
        return REALNUMBER_P.matcher(str).matches();
    }

    private static void addColumnStyle(HSSFWorkbook workbook, HSSFSheet hssfSheet, Column column, int colIndex, HSSFCellStyle cellStyle) {
        if (!column.edit()) {
            cellStyle.setLocked(false);
        }
        CellRangeAddressList regions = new CellRangeAddressList(1, 65535, colIndex, colIndex);
        if (column.between().length == 2 && isIntegralNum(column.between()[0]) && isIntegralNum(column.between()[1])) {

            DVConstraint constraint = DVConstraint.createNumericConstraint(DVConstraint.ValidationType.INTEGER, DVConstraint.OperatorType.BETWEEN, column.between()[0], column.between()[1]);
            HSSFDataValidation dataValidate = new HSSFDataValidation(regions, constraint);
            dataValidate.createErrorBox("错误！", "请输入数字，最小值" + column.between()[0] + "，最大值" + column.between()[1]);
            hssfSheet.addValidationData(dataValidate);
        }

        if (column.betweenfloat().length == 2 && isrealNumber(column.betweenfloat()[0]) && isrealNumber(column.betweenfloat()[1])) {
            DVConstraint constraint = DVConstraint.createNumericConstraint(DVConstraint.ValidationType.DECIMAL, DVConstraint.OperatorType.BETWEEN, column.betweenfloat()[0], column.betweenfloat()[1]);
            HSSFDataValidation dataValidate = new HSSFDataValidation(regions, constraint);
            dataValidate.createErrorBox("错误！", "请输入数字，最小值" + column.betweenfloat()[0] + "，最大值" + column.betweenfloat()[1]);
            hssfSheet.addValidationData(dataValidate);
        }

        if (column.allowString().length > 0) {
            DVConstraint constraint = DVConstraint.createExplicitListConstraint(column.allowString());
            HSSFDataValidation dataValidate = new HSSFDataValidation(regions, constraint);
            dataValidate.createErrorBox("错误！", "请输入指定值");
            dataValidate.createPromptBox(" 有效值", StringUtils.join(column.allowString(), ","));
            hssfSheet.addValidationData(dataValidate);
        }

        if (column.length().length == 2 && column.length()[0] <= column.length()[1] && column.length()[0] >= 0 && column.length()[1] >= 0) {
            DVConstraint constraint = DVConstraint.createNumericConstraint(DVConstraint.ValidationType.TEXT_LENGTH, DVConstraint.OperatorType.BETWEEN, column.length()[0] + "", column.length()[1] + "");
            HSSFDataValidation dataValidate = new HSSFDataValidation(regions, constraint);
            dataValidate.createErrorBox("错误！", "请输入字符，最小长度" + column.length()[0] + "位" + "，最大长度" + column.length()[1] + "位");
            hssfSheet.addValidationData(dataValidate);
        }

        if (!StringUtils.isBlank(column.format())) {
            HSSFDataFormat format = workbook.createDataFormat();
            cellStyle.setDataFormat(format.getFormat(column.format()));
        }


    }

    private static void addHearderCellStyle(HSSFRow hssfRowHeader, HSSFSheet hssfSheet, HSSFWorkbook workbook, Class clazz) {
        Annotation hearder = clazz.getAnnotation(Hearder.class);
        if (hearder != null) {
            HSSFCellStyle hearderStyle = workbook.createCellStyle();
            if (((Hearder) hearder).headerHigh() > 0) {
                hssfRowHeader.setHeight(Short.valueOf("" + ((Hearder) hearder).headerHigh()));
            }
            if (((Hearder) hearder).backGroundColor() != HSSFColor.WHITE.index) {
                hearderStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                hearderStyle.setFillBackgroundColor(Short.valueOf("" + ((Hearder) hearder).backGroundColor()));
            }
            if (((Hearder) hearder).intFreeze()) {
                hssfSheet.createFreezePane(0, 1);
            }
            hssfRowHeader.setRowStyle(hearderStyle);
        }
    }
}

package com.example.hello.cloud.framework.common.plugin.nss;

import com.example.hello.cloud.framework.common.ApiResponse;

/**
 * @author v-yangym10
 * @date 2019/4/16
 */
public interface NssOrgService {
    ApiResponse<String> createSalesOrg(String postDate) throws Exception;

    ApiResponse saveSalesProduct(String postDate) throws Exception;

    ApiResponse saveSalesOrgAndProduct(String postDate) throws Exception;
}

package com.example.hello.cloud.framework.common.plugin.yiyun.dto.payment.wxpay;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;

/**
 * 支付信息
 *
 * @author mall
 */
@Data
public class QueryResponse implements YiyunResponseData {

    /*
     * merchantNo	    字符串	商户号
     * paymentOrderNo	字符串	支付订单号
     * tradeOrderNo	    字符串	交易订单号
     * tradeOrderTime	字符串	交易时间	    交易时间格式YYYY-MM-DD HH:MM:SS
     * goodDesc 	    字符串	产品描述
     * status	        int	    交易结果状态	0待付款1已付款2部分退款3已退款6已撤销
     * payType	        字符串	支付方式
     * amount	        字符串	支付金额
     * refundAmount	    字符串	退款金额
     *
     * */
    private String merchantNo;
    private String paymentOrderNo;
    private String tradeOrderNo;
    private String tradeOrderTime;
    private String goodDesc;
    private Integer status;
    private String payType;
    private String amount;
    private String refundAmount;

}
package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/9.
 */
@Data
public class SyncTurnCustomerResultDTO extends SyncResultDTO implements Serializable {
    private  String sNumber;

    @Override
    public String toString() {
        return "SyncTurnCustomerResultDTO{" +
                "sNumber='" + sNumber + '\'' +
                '}';
    }
}

package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author v-yangym10
 * @date 2019/9/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysUserDTO implements Serializable {

    private String nssConsultantId;

    private String name;

    private String exampleId;

    /** 销售组织id，用逗号拼接 */
    private String salesOrgIds;
}

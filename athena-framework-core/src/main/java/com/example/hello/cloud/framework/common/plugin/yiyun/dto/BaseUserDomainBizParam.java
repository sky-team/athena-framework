package com.example.hello.cloud.framework.common.plugin.yiyun.dto;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration.UserDomainEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration.UserDomainEnum;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 中台分配的分享家应用标识
 *
 * @author zhoujj07
 * @create 2018/5/9
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseUserDomainBizParam extends BaseYiyunRequestData {

    private static final long serialVersionUID = 7310504341524168914L;

    /**
     * 应用系统自行管理的子系统标记
     */
    private String userDomain = UserDomainEnum.ECIF.getIndex();

}

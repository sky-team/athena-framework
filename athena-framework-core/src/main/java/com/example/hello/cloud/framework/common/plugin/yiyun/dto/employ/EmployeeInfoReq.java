package com.example.hello.cloud.framework.common.plugin.yiyun.dto.employ;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 员工信息查询
 *
 * @author guoc16
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class EmployeeInfoReq extends BaseYiyunRequestData {

    /**
     * 手机号
     */
    private String mobile;

}

package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author c-linhc
 * @date 2019/9/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SyncCouponCustomerInfoDTO implements Serializable {
    /**
     * 礼券
     */
    private String couponCode;
    /**
     * 礼券密码
     */
    private String cardNo;
    /**
     * 客户名称
     */
    private String custName;
    /**
     * 客户手机号
     */
    private String custPhone;
}

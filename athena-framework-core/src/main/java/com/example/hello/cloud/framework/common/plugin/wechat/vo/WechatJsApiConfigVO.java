package com.example.hello.cloud.framework.common.plugin.wechat.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 微信jsapi签名数据
 *
 * @author c-linxb
 * @create 2019/12/9
 **/
@Data
@ApiModel("微信jsapi签名数据")
public class WechatJsApiConfigVO implements Serializable {

    private static final long serialVersionUID = 466745190562722365L;

    @ApiModelProperty("公众号的唯一标识")
    private String appId;

    @ApiModelProperty("生成签名的时间戳")
    private Long timestamp;

    @ApiModelProperty("生成签名的随机串")
    private String nonceStr;

    @ApiModelProperty("入参传递，调用JS接口页面的完整URL")
    private String url;

    @ApiModelProperty("签名")
    private String signature;

}

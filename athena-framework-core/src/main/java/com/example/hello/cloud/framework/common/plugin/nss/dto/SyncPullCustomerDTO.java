package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/10/5.
 */
@Data
public class SyncPullCustomerDTO implements Serializable {
   private  String updateTime;

    @Override
    public String toString() {
        return "SyncPullCustomerDTO{" +
                "updateTime='" + updateTime + '\'' +
                '}';
    }
}

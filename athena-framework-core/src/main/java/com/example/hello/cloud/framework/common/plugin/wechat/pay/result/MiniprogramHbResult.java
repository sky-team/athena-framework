package com.example.hello.cloud.framework.common.plugin.wechat.pay.result;


import java.io.Serializable;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import com.example.hello.cloud.framework.common.plugin.wechat.pay.util.XStreamInitializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName: MiniprogramHbResult.java
 * @Description: 小程序红包返回结果
 * @author guohg03
 * @date 2020年2月28日
 */
@Data
@XStreamAlias("xml")
@Accessors(chain = true)
public class MiniprogramHbResult implements Serializable {
	private static final long serialVersionUID = 5779200441240916736L;

	/**
	 * 返回状态码.
	 */
	@XStreamAlias("return_code")
	protected String returnCode;

	/**
	 * 返回信息.
	 */
	@XStreamAlias("return_msg")
	protected String returnMsg;

	/**
	 * 业务结果.
	 */
	@XStreamAlias("result_code")
	private String resultCode;

	/**
	 * 错误代码.
	 */
	@XStreamAlias("err_code")
	private String errCode;
	/**
	 * 错误代码描述.
	 */
	@XStreamAlias("err_code_des")
	private String errCodeDes;

	/**
	 * 商户订单号(每个订单号必须唯一 取值范围 0~9 a~z A~Z).
	 */
	@XStreamAlias("mch_billno")
	protected String mchBillno;

	/**
	 * 微信支付分配的商户号.
	 */
	@XStreamAlias("mch_id")
	protected String mchId;

	/**
	 * 微信分配的公众号id
	 */
	@XStreamAlias("wxappid")
	protected String wxappid;

	/**
	 * 用户openid(接受红包)
	 */
	@XStreamAlias("re_openid")
	protected String reOpenid;

	/**
	 * 付款金额，单位分
	 */
	@XStreamAlias("total_amount")
	protected Integer totalAmount;

	/**
	 * 红包订单的微信单号
	 */
	@XStreamAlias("send_listid")
	protected String sendListid;
	
	/**
	 * 返回jaspi的入参package的值
	 */
	@XStreamAlias("package")
	protected String jaspiPackage;

	@SuppressWarnings("unchecked")
	public static <T> T fromXML(String xmlString, Class<T> clz) {
		XStream xstream = XStreamInitializer.getInstance();
		xstream.processAnnotations(clz);
		return (T) xstream.fromXML(xmlString);
	}
}
package com.example.hello.cloud.framework.common.plugin.wechat.pay.service;

import com.example.hello.cloud.framework.common.plugin.wechat.pay.request.MiniprogramCorpPayRequest;
import com.example.hello.cloud.framework.common.plugin.wechat.pay.request.MiniprogramHbSendRequest;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.plugin.wechat.pay.enums.PayAppEnum;
import com.example.hello.cloud.framework.common.plugin.wechat.pay.enums.PayMchEnum;
import com.example.hello.cloud.framework.common.plugin.wechat.pay.request.MiniprogramCorpPayRequest;
import com.example.hello.cloud.framework.common.plugin.wechat.pay.request.MiniprogramHbSendRequest;
import com.example.hello.cloud.framework.common.plugin.wechat.pay.result.MiniprogramCorpPayResult;
import com.example.hello.cloud.framework.common.plugin.wechat.pay.result.MiniprogramHbResult;
import com.example.hello.cloud.framework.common.plugin.wechat.pay.util.PaySignUtils;
import com.example.hello.cloud.framework.common.plugin.wechat.pay.util.ReqHttpUtil;
import com.example.hello.cloud.framework.common.util.StringUtil;
import com.example.hello.cloud.framework.common.util.json.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: MiniprogramPayService.java
 * @Description: 小程序-支付业务处理
 * @author guohg03
 * @date 2020年2月28日
 */
@Slf4j
@Component
public class MiniprogramPayService {
	@Autowired
	private ReqHttpUtil reqHttpUtil;
	
	/**返回值*/
	private static final String SUCCESS = "SUCCESS";

	/**
	 * 功能：发红包
	 */
	public ApiResponse<MiniprogramHbResult> sendRedpack(MiniprogramHbSendRequest request) {
		String nonceStr = String.valueOf(System.currentTimeMillis());
	    request.setNonceStr(nonceStr);
	    if(StringUtil.isNullOrEmpty(request.getNotifyWay())) {
	    	// 默认小程序红包,传MINI_PROGRAM_JSAPI
	    	request.setNotifyWay("MINI_PROGRAM_JSAPI");
	    }
	    if(request.getTotalNum() == null) {
	    	request.setTotalNum(1);
	    }
	    request.setSign(PaySignUtils.createSign(request,null));
	    log.info("小程序发红包参数-{}", JsonUtil.obj2Str(request));
	    String redPackUrl = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
	    try {
	    	String responseContent = reqHttpUtil.post(redPackUrl, request.toXML(), true);
		    if(StringUtil.isNotEmpty(responseContent)) {
		    	MiniprogramHbResult payResult = MiniprogramHbResult.fromXML(
	    		    responseContent,MiniprogramHbResult.class);
	    		log.info("小程序发红包返回的结果-result-{},json-{}",responseContent,
	    		    JsonUtil.obj2Str(payResult));
	    		return ApiResponse.success(payResult);
	    	}
		    log.error("小程序发红包返回结果请求异常!");
		    return ApiResponse.success(new MiniprogramHbResult());
	    } catch(Exception e) {
	    	log.error("小程序发红包返回结果系统异常!" + e.getMessage(),e);
	    	return ApiResponse.error(e.getLocalizedMessage());
	    }
	}

	/**
	 * 企业零钱到付
	 * @param openId   		接收方的小程序openId
	 * @param amount   		发送的金额(单位为元)
	 * @param billNo   		订单号
	 * @param description   企业付款描述信息(如果做活动发钱，这个传活动的名称)
	 * @param clientIp      应用ip
	 * @param payMchEnum    支持商户信息
	 * @param payAppEnum    发放应用信息
	 * @return  调用结果
	 */
	public ApiResponse<MiniprogramCorpPayResult> sendSimpleCorpPay(String openId,
																	 String amount, String billNo, String description, String clientIp,
																	 PayMchEnum payMchEnum, PayAppEnum payAppEnum) {
		log.info("common企业零钱到付入参-openId-{},amount-{},billNo-{},description-{},clientIp-{}"
			+ "mchId:{},appId:{}", openId, amount, billNo,description,clientIp,
			payMchEnum.getMchNo(),payAppEnum.getAppId());
		MiniprogramCorpPayRequest request = new MiniprogramCorpPayRequest();
		request.setOpenid(openId);
		request.setAmount(MiniprogramCorpPayRequest.yuanToFen(amount));
		request.setPartnerTradeNo(billNo);
		request.setDescription(description);
		request.setSpbillCreateIp(clientIp);
		return sendCorpPay(request,payMchEnum,payAppEnum);
	}
	
	/**
	 * 功能：企业零钱到付
	 */
	public ApiResponse<MiniprogramCorpPayResult> sendCorpPay(MiniprogramCorpPayRequest request,
		PayMchEnum payMchEnum, PayAppEnum payAppEnum) {
		request.setMchAppid(payAppEnum.getAppId());
		request.setMchid(payMchEnum.getMchNo());
		if(StringUtil.isNullOrEmpty(request.getCheckName())) {
			request.setCheckName("NO_CHECK");	
		}
		String nonceStr = String.valueOf(System.currentTimeMillis());
	    request.setNonceStr(nonceStr);
		request.setSign(PaySignUtils.createSign(request, payMchEnum.getMchKey()));
		log.info("企业到付零钱签名-{}",request.getSign());
	    String corpUrl = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
	    try {
	    	String responseContent = reqHttpUtil.post(corpUrl, request.toXML(), true);
	    	if(StringUtil.isNotEmpty(responseContent)) {
	    		MiniprogramCorpPayResult payResult = MiniprogramCorpPayResult.fromXML(
	    		    responseContent,MiniprogramCorpPayResult.class);
	    		log.info("企业到付零钱返回结果!result-{}",responseContent);
	    		if(SUCCESS.equalsIgnoreCase(payResult.getReturnCode())) {
	    			if(SUCCESS.equalsIgnoreCase(payResult.getResultCode())) {
	    				log.info("企业到付零钱成功!openId-{},payNo-{}", 
	    					request.getOpenid(),payResult.getPaymentNo());
	    			} else {
	    				log.error("企业到付零钱失败!openId-{},payNo-{},errCode-{},erCodeDes-{}",
	    					request.getOpenid(),payResult.getPaymentNo(),
	    					payResult.getErrCode(),payResult.getErrCodeDes());
	    			}
	    			return ApiResponse.success(payResult);
	    		}
	    		log.error("微信企业零钱到付失败!");
	    		return ApiResponse.error("微信企业零钱到付失败!");
	    	}
		    log.error("微信企业零钱到付出现异常!");
		    return ApiResponse.error("微信企业零钱到付出现异常!");
	    } catch(Exception e) {
	    	log.error("企业到付零钱系统异常!" + e.getMessage(),e);
	    	return ApiResponse.error(e.getMessage());
	    }
	}
}
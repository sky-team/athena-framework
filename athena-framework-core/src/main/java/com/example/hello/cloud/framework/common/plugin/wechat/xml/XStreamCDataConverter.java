package com.example.hello.cloud.framework.common.plugin.wechat.xml;

import com.thoughtworks.xstream.converters.basic.StringConverter;

/**
 * CDATA 内容转换器，加上CDATA标签.
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 * @date 2019-08-22
 */
public class XStreamCDataConverter extends StringConverter {

	@Override
	public String toString(Object obj) {
		return "<![CDATA[" + super.toString(obj) + "]]>";
	}
}

package com.example.hello.cloud.framework.common.http.monitor;

import com.example.hello.cloud.framework.common.http.common.Utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.util.concurrent.TimeUnit;

/**
 * 监控空闲线程（同步连接）
 *
 * @author v-linxb
 */
@Slf4j
public class SyncConnectionMonitorThread extends Thread {

    private final PoolingHttpClientConnectionManager connMgr;

    private volatile boolean shutdown;

    private int timeout = 30;

    private long interval = 60000L;

    public SyncConnectionMonitorThread(PoolingHttpClientConnectionManager connMgr) {
        super();
        this.connMgr = connMgr;
    }

    @Override
    public void run() {
        try {
            while (!shutdown) {
                synchronized (this) {
                    wait(this.interval);
                    connMgr.closeExpiredConnections();
                    connMgr.closeIdleConnections(this.timeout, TimeUnit.SECONDS);
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            Utils.errorException("{}", ex);
        }
    }

    public void shutdown() {
        shutdown = true;
        synchronized (this) {
            notifyAll();
        }
    }
}

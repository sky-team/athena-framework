package com.example.hello.cloud.framework.common.enums;

/**
 * 根应用标示，主要用于区分不同应用登录账号验证；
 * 
 * 1.目前在线家，销售家顾问端，分享家，用密码登录的是同一个密码； 2.知客后台，销售家经理端是共用一个密码； 3.其他应用各自独立应用自己的密码；
 *
 * @author zhoujj07
 * @create 2019/10/17
 */
public enum RootDomainEnum {
    /**
     * 置业神器
     */
    ZYSQ(1, "zysq", "置业神器"),
    /**
     * 在线家
     */
    ZXJ(2, "zxj", "在线家"),
    /**
     * 分享家
     */
    FXJ(3, "fxj", "分享家"),
    /**
     * 销售家顾问端
     */
    XSJ_CONSULTANT(4, "xsj_consultant", "销售家顾问端"),
    /**
     * 销售家经理端
     */
    XSJ_MANAGER(5, "xsj_manager", "销售家经理端"),
    /**
     * 知客后台
     */
    hello_ADMIN(6, "hello_admin", "知客后台"),
    /**
     * 拓客神器
     */
    TOKER_APPLET(7, "toker_applet", "拓客神器"),
    /**
     * 机构管理后台
     */
    ORG_ADMIN(8, "org_admin", "机构管理后台"),
    /**
     * 在线选房
     */
    ZXXF(9, "zxxf", "在线选房"),
    /**
     * e选房
     */
    E_CHOOSE_ROOM(10, "e_choose_room", "e选房"),

    /**
     * 万科万小二
     */
    WANX2(11, "wanx2", "万小二");

    private Integer id;
    private String code;
    private String name;

    public Integer getId() {
        return id;
    }

    protected void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    protected void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    RootDomainEnum(Integer id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    /**
     * @param code
     *            code
     * @return
     */
    public static Integer getIdByCode(String code) {
        for (RootDomainEnum rootDomainEnum : RootDomainEnum.values()) {
            if (rootDomainEnum.getCode().equals(code)) {
                return rootDomainEnum.getId();
            }
        }
        return null;
    }

    /**
     * @param id
     *            id
     * @return
     */
    public static String getCodeById(Integer id) {
        for (RootDomainEnum rootDomainEnum : RootDomainEnum.values()) {
            if (rootDomainEnum.getId().equals(id)) {
                return rootDomainEnum.getCode();
            }
        }
        return null;
    }

}

package com.example.hello.cloud.framework.common.plugin.yiyun.dto.product;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;

/**
 * 查询产品详细信息
 *
 * @author v-linxb
 * @create 2018/5/7
 **/
@Data
public class ProductBasicInfoReq extends BaseYiyunRequestData {

    /**
     * 数值	产品id	是
     */
    private Long id;

    /**
     * 列表	需要返回的产品私有属性代码
     */
    private String[] returnAppProps;
    /**
     * 列表	需要返回的产品公有属性代码
     */
    private String[] returnPubProps;
    /**
     * 需要返回的产品属性组合分组类型
     * 1:楼盘户型交付标准
     * 2:楼盘装修亮点表
     * 3:楼盘亮点（交通、教育、医疗、商业）
     * 4:楼盘分期
     */
    private Integer[] returnPropGroupType;

    /**
     * 可选值：
     * none：不返回扩展式，默认值
     * underProp：基于客户属性附加扩展系
     * upperProp：基于扩展值对客户属性进行分组
     * list：以列表形式返回客户属性
     */
    private String extAttrReturnType;
    /**
     * 需要返回的直属下级产品类目id
     */
    private Integer returnSubCategoryId;

    /**
     * 下级产品分组属性
     */
    private String subProductGroupBy;

    /**
     * 下级产品分组下需要返回值的最大的产品属性
     */
    private String maxSubProductProp;

}

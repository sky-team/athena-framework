package com.example.hello.cloud.framework.common;

import com.alibaba.fastjson.annotation.JSONField;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.enums.BaseResponseCode;
import com.example.hello.cloud.framework.common.trace.TraceContext;
import com.example.hello.cloud.framework.common.util.ResourcesUtil;
import com.example.hello.cloud.framework.common.util.StringUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.enums.BaseResponseCode;
import com.example.hello.cloud.framework.common.trace.TraceContext;
import com.example.hello.cloud.framework.common.util.ResourcesUtil;
import com.example.hello.cloud.framework.common.util.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;

import java.io.Serializable;
import java.util.List;

/**
 * 接口返回结果对象
 * 
 * @author guohg03
 * @date 2018年9月7日
 */
@ApiModel
public class ApiResponse<T> implements Serializable {

    private static final long serialVersionUID = 8724365909968157425L;

    /**
     * 结果码
     */
    @ApiModelProperty(value = "结果码", example = "0")
    private Integer code = ApiResponseCodeEnum.SUCCESS.getCode();
    /**
     * 结果信息
     */
    @ApiModelProperty(value = "结果信息", example = "处理成功")
    private String msg = ApiResponseCodeEnum.SUCCESS.getShowMessage();

    /**
     * 请求traceId
     */
    @ApiModelProperty(value = "请求traceId", example = "12147.44.15731836762110001")
    private String traceId;

    /**
     * 请求spanId
     */
    @ApiModelProperty(value = "请求spanId", example = "12147.44.15731836762110001")
    private String spanId;

    /**
     * 环境
     */
    @ApiModelProperty(value = "环境", example = "hello-cloud-dev")
    private String namespace;

    /**
     * 返回结果JSON对象
     */
    @ApiModelProperty(value = "返回结果JSON对象")
    private T data;

    public ApiResponse() {}

    /**
     * @param code
     *            响应码
     */
    public ApiResponse(Integer code) {
        this.code = code;
        this.traceId = TraceContext.getCurrentTrace().getTraceId();
        this.spanId = TraceContext.getCurrentTrace().getSpanId();
        this.namespace = ResourcesUtil.getK8sNamespace("k8s.namespace");
    }

    /**
     * @param code
     *            响应码
     * @param showMessage
     *            响应信息
     */
    public ApiResponse(Integer code, String showMessage) {
        this.code = code;
        this.msg = showMessage;
        this.traceId = TraceContext.getCurrentTrace().getTraceId();
        this.spanId = TraceContext.getCurrentTrace().getSpanId();
        this.namespace = ResourcesUtil.getK8sNamespace("k8s.namespace");

    }

    /**
     * @param baseResponseCode
     *            响应码枚举
     */
    public ApiResponse(BaseResponseCode baseResponseCode) {
        this.code = baseResponseCode.getCode();
        this.msg = StringUtil.isEmpty(baseResponseCode.getShowMessage()) ? baseResponseCode.getMessage() : baseResponseCode.getShowMessage();
        this.traceId = TraceContext.getCurrentTrace().getTraceId();
        this.spanId = TraceContext.getCurrentTrace().getSpanId();
        this.namespace = ResourcesUtil.getK8sNamespace("k8s.namespace");

    }

    /**
     * @return 布尔值
     */
    @JSONField(serialize = false)
    @JsonIgnore
    public boolean isOk() {
        return ApiResponseCodeEnum.SUCCESS.getCode().equals(code);
    }

    /**
     * @return 布尔值
     */
    @JSONField(serialize = false)
    @JsonIgnore
    public boolean isNotOk() {
        return !this.isOk();
    }

    /**
     * @param baseResponseCode
     *            响应码枚举
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> error(BaseResponseCode baseResponseCode) {
        return new ApiResponse<>(baseResponseCode);
    }

    /**
     * @param baseResponseCode
     *            响应码枚举
     * @param data
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> error(BaseResponseCode baseResponseCode, T data) {
        return new ApiResponse<T>(baseResponseCode).setData(data);
    }

    /**
     * @param baseResponseCode
     *            响应码枚举
     * @param params
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> error(BaseResponseCode baseResponseCode, Object... params) {
        return new ApiResponse<>(baseResponseCode.getCode(), String.format(baseResponseCode.getShowMessage(), params));
    }

    /**
     * @param baseResponseCode
     *            响应码枚举
     * @param data
     * @param params
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> error(BaseResponseCode baseResponseCode, T data, Object... params) {
        return new ApiResponse<T>(baseResponseCode.getCode(), String.format(baseResponseCode.getShowMessage(), params))
            .setData(data);
    }

    /**
     * @param msg
     *            响应信息
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> error(String msg) {
        return new ApiResponse<>(ApiResponseCodeEnum.FAIL.getCode(), msg);
    }

    /**
     * @param msg
     *            响应信息
     * @param data
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> error(String msg, T data) {
        return new ApiResponse<T>(ApiResponseCodeEnum.FAIL.getCode(), msg).setData(data);
    }

    /**
     * @param code
     *            响应码
     * @param msg
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> error(Integer code, String msg) {
        return new ApiResponse<>(code, msg);
    }

    /**
     * @param code
     *            响应码
     * @param msg
     *            响应信息
     * @param data
     *            响应内容
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> error(Integer code, String msg, T data) {
        return new ApiResponse<T>(code, msg).setData(data);
    }

    /**
     * @param codeAndMsgList
     *            响应码和响应信息List
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> error(List<Object> codeAndMsgList) {
        return new ApiResponse<>(Integer.valueOf(codeAndMsgList.get(0).toString()), codeAndMsgList.get(1).toString());
    }

    /**
     * @param codeAndMsg
     *            响应码和响应信息
     * @param data
     *            响应内容
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> error(List<Object> codeAndMsg, T data) {
        ApiResponse<T> ar = error(codeAndMsg);
        ar.setData(data);
        return ar;
    }

    /**
     * @return ApiResponse
     */
    public static ApiResponse<Void> success() {
        return new ApiResponse<>(ApiResponseCodeEnum.SUCCESS);
    }

    /**
     * @param data
     *            响应内容
     * @param <T>
     * @return ApiResponse
     */
    public static <T> ApiResponse<T> success(T data) {
        ApiResponse<T> ar = new ApiResponse<>(ApiResponseCodeEnum.SUCCESS);
        ar.setData(data);
        return ar;
    }

    public ApiResponse<T> setRespMsg(BaseResponseCode baseResponseCode) {
        this.code = baseResponseCode.getCode();
        this.msg = baseResponseCode.getShowMessage();
        return this;
    }

    public ApiResponse<T> setRespMsgFromList(List<Object> codeAndMsg) {
        this.code = Integer.valueOf(codeAndMsg.get(0).toString());
        this.msg = codeAndMsg.get(1).toString();
        return this;
    }

    public ApiResponse<T> setRespMsg(List<Object> codeAndMsg, T data) {
        this.code = Integer.valueOf(codeAndMsg.get(0).toString());
        this.msg = codeAndMsg.get(1).toString();
        this.data = data;
        return this;
    }

    public ApiResponse<T> setRespMsg(BaseResponseCode apiResponseCodeEnum, T data) {
        this.code = apiResponseCodeEnum.getCode();
        this.msg = apiResponseCodeEnum.getShowMessage();
        this.data = data;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public ApiResponse<T> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public ApiResponse<T> setMsg(String msg, Object... params) {
        this.msg = String.format(msg, params);
        return this;
    }

    public T getData() {
        return data;
    }

    public ApiResponse<T> setData(T data) {
        this.data = data;
        return this;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getTraceId() {
        return this.traceId;
    }

    public String getSpanId() {
        return spanId;
    }

    public void setSpanId(String spanId) {
        this.spanId = spanId;
    }


    @Override
    public String toString() {
        return "ApiResponse{" + "code=" + code + ", msg='" + msg + '\'' + ", traceId='" + traceId + '\'' + ", spanId='" + spanId + '\''
            + ", namespace='" + namespace + '\'' + ", data=" + data + '}';
    }

    /**
     * @param result
     *            result
     * @param log
     *            log
     * @param successMsg
     *            成功信息
     * @param errorMsg
     *            错误信息
     * @return ApiResponse
     */
    public static ApiResponse<Boolean> getReturnResult(ApiResponse<Boolean> result, Logger log, String successMsg,
        String errorMsg) {
        if (result != null) {
            if (result.getData()) {
                log.info(successMsg);
                return ApiResponse.success(true);
            } else {
                log.error(errorMsg);
                return ApiResponse.error(result.getCode(), result.getMsg());
            }
        } else {
            return error(ApiResponseCodeEnum.FAIL);
        }
    }
}

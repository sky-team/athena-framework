package com.example.hello.cloud.framework.common.plugin.yiyun.dto.employ;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.YiyunBasePage;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.response.YiyunResponseData;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 员工信息查询
 *
 * @author guoc16
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class EmployeeInfoResp extends YiyunBasePage implements YiyunResponseData {

    /**
     * 业务中台员工Id
     */
    private Long id;

    /**
     * 入职时间'yyyy-MM-dd HH:mm:ss
     */
    protected String hireDate;

    /**
     * 离职时间'yyyy-MM-dd HH:mm:ss'
     */
    private String resignDate;

    /**
     * 转正时间'yyyy-MM-dd HH:mm:ss'
     */
    private String regularDate;

    /**
     * 部门名称
     */
    private String dept;

    /**
     * 部门id
     */
    private String deptId;

    /**
     * 万科id
     */
    private String uid;

    /**
     * 职务
     */
    private String title;

    /**
     * 姓名
     */
    private String name;

    /**
     * 公司
     */
    private String company;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 域账号
     */
    private String adAccount;

    /**
     * 员工状态编码
     * 1.在职在岗，2.在职不在岗，3.离职，4.退休
     */
    private String empStatus;

    /**
     * 公司编码
     */
    private String exampleCompanyCode;

    /**
     * 组织名称全路径
     */
    private String userOrgNameFullPath;

    /**
     * 组织编号全路径
     */
    private String userOrgNoFullPath;

    /**
     * 员工类型
     */
    private String smartVkType;

    /**
     * 员工标识
     */
    private boolean regularStaffFlag;

}

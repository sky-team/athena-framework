package com.example.hello.cloud.framework.common.plugin.yiyun.core;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 客户属性信息
 *
 * @author zhoujj0
 * @create 2018/5/7
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class YiyunCustomerPropertyInfo extends YiyunPropertyInfo {
}

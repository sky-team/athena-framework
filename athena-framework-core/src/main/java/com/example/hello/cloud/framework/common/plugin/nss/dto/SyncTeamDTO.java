package com.example.hello.cloud.framework.common.plugin.nss.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Alikes on 2018/9/30.
 */
@Data
public class SyncTeamDTO  implements Serializable {
    /*
      销售系统销售组织
    * */
    private String consultantOrgId;
    private String salesTeamPid;
    private String salesTeamPName;
    private String salesTeamId;
    private String salesTeamName;
    private String consultantId;
    private String consultantName;
    private String managerId;
    private String managerName;
    private String agentId;
    private String agentName;
    private String jobType;
    private  String sNumber;

    @Override
    public String toString() {
        return "SyncTeamDTO{" +
                "consultantOrgId='" + consultantOrgId + '\'' +
                ", salesTeamPid='" + salesTeamPid + '\'' +
                ", salesTeamPName='" + salesTeamPName + '\'' +
                ", salesTeamId='" + salesTeamId + '\'' +
                ", salesTeamName='" + salesTeamName + '\'' +
                ", consultantId='" + consultantId + '\'' +
                ", consultantName='" + consultantName + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", agentId='" + agentId + '\'' +
                ", agentName='" + agentName + '\'' +
                ", jobType='" + jobType + '\'' +
                ", sNumber='" + sNumber + '\'' +
                '}';
    }
}

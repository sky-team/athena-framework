package com.example.hello.cloud.framework.common.util;

import java.security.Key;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.example.hello.cloud.framework.common.constant.AuthConstant;
import com.example.hello.cloud.framework.common.constant.AuthConstant;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.Base64Codec;
import lombok.extern.slf4j.Slf4j;

/**
 * JWT工具类
 *
 * @author zhanj04
 * @date 2017/7/29 13:03
 */
@Slf4j
public class JwtUtil {

    private static final String SPLIT = "\\.";

    /**
     * 构建 JWT
     *
     * @param mainAccountId  主账号ID
     * @param phone          手机号
     * @param ttlMillis      Token过期时间
     * @param base64Security base64秘钥
     * @return JWT
     */
    public static String build(String mainAccountId, String phone, long ttlMillis, String base64Security) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS512;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        // 生成签名密钥
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(base64Security);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        // 添加构成JWT的参数
        JwtBuilder builder = Jwts.builder().setHeaderParam("typ", AuthConstant.JWT_TYPE)
                .setHeaderParam("alg", AuthConstant.JWT_ALGORITHM)
                .claim(AuthConstant.CURRENT_USER_HEADER_MAIN_ACCOUNT_ID, mainAccountId)
                .claim(AuthConstant.CURRENT_USER_HEADER_PHONE, phone).signWith(signatureAlgorithm, signingKey);
        // 添加Token过期时间
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp).setNotBefore(now);
        }
        // 生成JWT
        return builder.compact();
    }

    /**
     * 反向解析JWT
     *
     * @param jsonWebToken
     * @param base64Security
     * @return
     */
    public static Claims parse(String jsonWebToken, String base64Security) {
        try {
            Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(base64Security))
                    .parseClaimsJws(jsonWebToken).getBody();
            return claims;
        } catch (Exception e) {
            log.error("执行JwtUtil.parse()方法,发生异常：{}", e);
            return null;
        }
    }

    /**
     * 校验Token过期时间是否有效
     *
     * @param jwt
     * @return
     */
    public static boolean checkExp(String jwt) {
        log.info("jwt:" + jwt);
        if (!StrUtil.isEmpty(jwt)) {
            if (jwt.split(SPLIT).length == 3) {
                String[] split = jwt.split(SPLIT);
                String payload = split[1];
                String payloadClear = Base64Codec.BASE64URL.decodeToString(payload);
                log.info("payloadClear:" + payloadClear);
                JSONObject payloadJson = JSONUtil.parseObj(payloadClear);

                long nowMillis = System.currentTimeMillis();
                Number expiresSecond = (Number) payloadJson.get("exp");

                // 判断是否过期
                if (nowMillis > expiresSecond.longValue() * 1000L) {
                    return true;
                } else {
                    return false;
                }

            } else {
                return true;
            }
        }
        return true;
    }

//    public static void main(String[] args) {
//        String jwt="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwaG9uZSI6IjE2OTEyMzQxMjM0IiwiaXNzIjoiemhpa2UiLCJleHAiOjQ3NDk1MDM1MTUsImlhdCI6MTU5NTkwMzUxNSwidXNlcklkIjoiNjQzODcxNzI4MDM3OTYxNzI4IiwibWFpbkFjY291bnRJZCI6IjY0Mzg3MTcyODAzNzk2MTcyOCJ9.eV1TIU35PhqkUpXj78_NxsjgjvyXOUQXBNzPHie1spQ";
//        System.out.println(checkExp(jwt));
//        System.out.println(DateUtil.formatDateTime(new Date(4749503515L*1000),"yyyy-MM-dd HH:mm:ss"));
//    }

    /**
     * 获取主账号ID
     *
     * @param jwt 用户jwt
     * @return 登录用户主账号ID
     */
    public static String getId(String jwt) {
        return getClaim(jwt, AuthConstant.CURRENT_USER_HEADER_MAIN_ACCOUNT_ID);
    }

    /**
     * 获取主账号的手机号
     *
     * @param jwt 用户jwt
     * @return 登录用户主账号的手机号
     */
    public static String getPhone(String jwt) {
        return getClaim(jwt, AuthConstant.CURRENT_USER_HEADER_PHONE);
    }

    /**
     * 获取微信账号ID
     *
     * @param jwt 用户jwt
     * @return 登录用户微信账号ID
     */
    public static String getWechatAccountId(String jwt) {
        return getClaim(jwt, AuthConstant.CURRENT_USER_HEADER_WECHAT_ACCOUNT_ID);
    }

    /**
     * 获取知客用户ID
     *
     * @param jwt 用户jwt
     * @return 知客用户ID
     */
    public static String getUserId(String jwt) {
        return getClaim(jwt, AuthConstant.CURRENT_USER_HEADER_USER_ID);
    }

    /**
     * 获取jwt中的payload中业务属性值
     *
     * @param jwt     用户jwt
     * @param claimId claimId
     * @return 业务属性值
     */
    public static String getClaim(String jwt, String claimId) {
        if (StrUtil.isBlank(jwt) || jwt.split(SPLIT).length != 3) {
            return "";
        }
        String[] split = jwt.split(SPLIT);
        String payload = split[1];
        String payloadClear = Base64Codec.BASE64URL.decodeToString(payload);
        JSONObject payloadJson = JSONUtil.parseObj(payloadClear);
        if (null == payloadJson) {
            return "";
        }
        return payloadJson.getStr(claimId);
    }

    /**
     * 获取第三方账号ID
     *
     * @param jwt 用户jwt
     * @return 登录用户第三方账号ID
     */
    public static String getThirdId(String jwt) {
        return getClaim(jwt, AuthConstant.CURRENT_USER_HEADER_THIRD_ID);
    }


    /**
     * 根据authorization获取知客用户ID
     *
     * @param authorization
     *            用户authorization
     * @return 知客用户ID
     */
    public static String getUserIdByAuthorization(String authorization) {
        if(StringUtil.isEmpty(authorization)) {
            return "";
        }
        return getClaim(getJwt(authorization), AuthConstant.CURRENT_USER_HEADER_USER_ID);
    }

    /**
     * 从authorization获取Token
     */
    public static String getJwt(String authorization) {
        return authorization.substring(AuthConstant.JWT_LEAST_LENGTH);
    }

}

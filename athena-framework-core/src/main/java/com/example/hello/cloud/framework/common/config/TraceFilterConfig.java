package com.example.hello.cloud.framework.common.config;

import com.example.hello.cloud.framework.common.trace.TraceFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置traceFilter
 *
 * @author yingc04
 * @create 2019/11/21
 */
@Configuration
public class TraceFilterConfig {
    @Bean
    public FilterRegistrationBean someFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(traceFilter());
        registration.addUrlPatterns("/*");
        registration.setName("traceFilter");
        registration.setOrder(1);
        return registration;
    }

    /**
     * 创建一个bean
     * @return
     */
    @Bean(name = "traceFilter")
    public TraceFilter traceFilter() {
        return new TraceFilter();
    }
}

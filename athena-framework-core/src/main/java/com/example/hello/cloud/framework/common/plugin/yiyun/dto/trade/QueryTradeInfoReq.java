package com.example.hello.cloud.framework.common.plugin.yiyun.dto.trade;

import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import com.example.hello.cloud.framework.common.plugin.yiyun.core.request.BaseYiyunRequestData;
import lombok.Data;


@Data
public class QueryTradeInfoReq extends BaseYiyunRequestData {
    /**
     * 证件类型
     */
    private String certType;
    /**
     * 证件号
     */
    private String certNo;
    /**
     * 页号
     */
    private String pageNo;
    /**
     * 每页多少行
     */
    private String pageSize = "10";
}

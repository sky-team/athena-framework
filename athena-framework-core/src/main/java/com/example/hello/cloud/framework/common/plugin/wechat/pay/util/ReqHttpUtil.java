package com.example.hello.cloud.framework.common.plugin.wechat.pay.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;

import javax.net.ssl.SSLContext;

import com.example.hello.cloud.framework.common.util.ResourcesUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import cn.hutool.core.codec.Base64;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: ReqHttpUtil.java
 * @Description: 发放请求工具类
 * @author guohg03
 * @date 2020年2月28日
 */
@Slf4j
@Component
public class ReqHttpUtil {
	/**
	 * p12证书文件内容的字节数组.
	 */
	private byte[] keyContent;

	public byte[] postForBytes(String url, String requestStr, boolean useKey) {
		try {
			HttpClientBuilder httpClientBuilder = createHttpClientBuilder(useKey);
			HttpPost httpPost = this.createHttpPost(url, requestStr);
			try (CloseableHttpClient httpClient = httpClientBuilder.build()) {
				try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
					final byte[] bytes = EntityUtils.toByteArray(response.getEntity());
					final String responseData = Base64.encode(bytes);
					log.info("\n【请求地址】：{}\n【请求数据】：{}\n【响应数据(Base64编码后)】：{}", url,
						requestStr, responseData);
					return bytes;
				}
			} finally {
				httpPost.releaseConnection();
			}
		} catch (Exception e) {
			log.error("\n【请求地址】：{}\n【请求数据】：{}\n【异常信息】：{}", url, requestStr, e.getMessage());
			return new byte[] {};
		}
	}

	public String post(String url, String requestStr, boolean useKey) {
		try {
			HttpClientBuilder httpClientBuilder = this.createHttpClientBuilder(useKey);
			HttpPost httpPost = this.createHttpPost(url, requestStr);
			try (CloseableHttpClient httpClient = httpClientBuilder.build()) {
				try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
					String responseString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
					log.info("\n【请求地址】：{}\n【请求数据】：{}\n【响应数据】：{}", url, requestStr, responseString);
					return responseString;
				}
			} finally {
				httpPost.releaseConnection();
			}
		} catch (Exception e) {
			log.error("\n【请求地址】：{}\n【请求数据】：{}\n【异常信息】：{}", url, requestStr, e.getMessage());
			return null;
		}
	}

	private StringEntity createEntry(String requestStr) {
		try {
			return new StringEntity(
					new String(requestStr.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1));
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

	private HttpClientBuilder createHttpClientBuilder(boolean useKey) {
		HttpClientBuilder httpClientBuilder = HttpClients.custom();
		if (useKey) {
			this.initSSLContext(httpClientBuilder);
		}
		return httpClientBuilder;
	}

	private HttpPost createHttpPost(String url, String requestStr) {
		int connectionTimeOut = ResourcesUtil.getInteger("pay.connection.timeout", 5000);
		int timeOut = ResourcesUtil.getInteger("pay.timeout", 10000);
		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(this.createEntry(requestStr));
		httpPost.setConfig(RequestConfig.custom().setConnectionRequestTimeout(connectionTimeOut)
				.setConnectTimeout(connectionTimeOut).setSocketTimeout(timeOut).build());
		return httpPost;
	}

	/**
	 * 初始化ssl.
	 */
	public void initSSLContext(HttpClientBuilder httpClientBuilder){
		InputStream inputStream;
		if (this.keyContent != null) {
			inputStream = new ByteArrayInputStream(this.keyContent);
		} else {
			// p12证书文件的绝对路径或者以classpath:开头的类路径. D:\\apiclient_cert.p12
			String keyPath = ResourcesUtil.getProperty("pay.key.path",
				"/opt/wanx2/cert/apiclient_cert.p12");
			if (StringUtils.isBlank(keyPath)) {
				log.error("请确保证书文件地址keyPath已配置");
				return;
			}
			final String prefix = "classpath:";
			String fileHasProblemMsg = "证书文件【" + keyPath + "】有问题，请核实！";
			String fileNotFoundMsg = "证书文件【" + keyPath + "】不存在，请核实！";
			if (keyPath.startsWith(prefix)) {
				String path = keyPath.substring(keyPath.indexOf(prefix));
				if (!path.startsWith("/")) {
					path = File.separator + path;
				}
				inputStream = ReqHttpUtil.class.getResourceAsStream(path);
				if (inputStream == null) {
					log.error(fileNotFoundMsg);
					return;
				}
			} else if (keyPath.startsWith("http://") || keyPath.startsWith("https://")) {
				try {
					inputStream = new URL(keyPath).openStream();
				} catch (IOException e) {
					log.error("errmsg-{}", fileNotFoundMsg, e.getMessage());
					return;
				}
			} else {
				try {
					File file = new File(keyPath);
					if (!file.exists()) {
						log.error("找不到文件-{}", fileNotFoundMsg);
						return;
					}
					inputStream = new FileInputStream(file);
				} catch (IOException e) {
					log.error("文件流不存在-{}", fileHasProblemMsg, e.getMessage());
					return;
				}
			}
		}
		try {
			KeyStore keystore = KeyStore.getInstance("PKCS12");
			String mchId = ResourcesUtil.getProperty("pay.mch.id", "1493581312");
			char[] partnerId2charArray = mchId.toCharArray();
			keystore.load(inputStream, partnerId2charArray);
			SSLContext sslContext = SSLContexts.custom().loadKeyMaterial(keystore, partnerId2charArray).build();
			SSLConnectionSocketFactory connectionSocketFactory = new SSLConnectionSocketFactory(sslContext,
					new String[] { "TLSv1" }, null, new DefaultHostnameVerifier());
			httpClientBuilder.setSSLSocketFactory(connectionSocketFactory);
		} catch (Exception e) {
			log.error("证书文件有问题，请核实!", e);
		} finally {
			try {
				inputStream.close();
			}catch(Exception e){
				log.error(e.getMessage());
			}
		}
	}
}
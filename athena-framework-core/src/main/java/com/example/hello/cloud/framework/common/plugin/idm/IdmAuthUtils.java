package com.example.hello.cloud.framework.common.plugin.idm;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.hello.cloud.framework.common.plugin.nss.dto.SysUserDTO;
import com.example.hello.cloud.framework.common.plugin.nss.dto.SysUserDTO;
import com.example.hello.cloud.framework.common.util.ResourcesUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class IdmAuthUtils {

    private final String appuser;
    private final String appkey;
    private final String appinfo;
    private final String registerUrl;
    private final String permIds;
    private final String addUserPermUrl;

    public IdmAuthUtils(String appuser, String appkey, String appinfo, String registerUrl, String permIds,
        String addUserPermUrl) {
        this.appuser = appuser;
        this.appkey = appkey;
        this.appinfo = appinfo;
        this.registerUrl = registerUrl;
        this.permIds = permIds;
        this.addUserPermUrl = addUserPermUrl;
    }

    /**
     * 注册并授权IDM
     *
     * @param name
     *            姓名
     * @param phone
     *            手机号码
     * @param cardNumber
     *            身份证号码
     * @return
     * @throws Exception
     */
    public String registAndAccreditDIM(String name, String phone, String cardNumber) {
        log.info("IDM实例化参数 appuser ->{} appkey ->{} appinfo ->{} registerUrl ->{} permIds ->{} addUserPermUrl ->{}",
            this.appuser, this.appkey, this.appinfo, this.registerUrl, this.permIds, this.addUserPermUrl);
        log.info("IDM入参 name ->{} phone ->{} cardNumber ->{}", name, phone, cardNumber);
        String exampleId = null;
        String mobile = null;
        String registIDMRetuen = null;
        try {
            registIDMRetuen = registIDM(name, phone, cardNumber);
            JSONObject wsUser = JSONObject.parseObject(registIDMRetuen).getJSONObject("wsUser");
            JSONArray attributes = wsUser.getJSONArray("attributes");
            for (int i = 0; i < attributes.size(); i++) {
                JSONObject jsonObject = (JSONObject)attributes.get(i);
                String keyName = jsonObject.getString("name");
                if (keyName.equals("exampleid")) {
                    exampleId = jsonObject.getString("value");
                }
                if (keyName.equals("mobile")) {
                    mobile = jsonObject.getString("value");
                }
            }
        } catch (Exception e) {
            log.info("IDM出参 获取IDM注册数据异常 ->{} 异常 ->{}", registIDMRetuen, e);
            return registIDMRetuen;
        }
        if (StringUtils.isBlank(exampleId) || StringUtils.isBlank(mobile)) {
            log.info("IDM出参 获取IDM注册数据失败 ->{}", registIDMRetuen);
            return registIDMRetuen;// 注册失败
        }
        try {
            accreditIDM(exampleId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("IDM出参 ->{} " + registIDMRetuen);
        return registIDMRetuen;
    }

    public void updateConsultnatName(SysUserDTO sysUserDTO) {
        Map<String, Object> bodyMap = new LinkedHashMap<>();
        List<Map<String, Object>> propertiesList = new LinkedList<>();

        Map<String, Object> nameMap = new LinkedHashMap<>();
        nameMap.put("name", "usercn");
        nameMap.put("value", sysUserDTO.getName());
        propertiesList.add(nameMap);

        Map<String, Object> exampleIdMap = new LinkedHashMap<>();
        exampleIdMap.put("name", "username");
        exampleIdMap.put("value", sysUserDTO.getexampleId());
        propertiesList.add(exampleIdMap);

        bodyMap.put("attributes", propertiesList);

        getResponseFromIdmServer(ResourcesUtil.getProperty("idm.updateConsultantUrl", ""), bodyMap);
    }

    /**
     * 注册IDM
     *
     * @param name
     *            姓名
     * @param phone
     *            手机号码
     * @param cardNumber
     *            身份证号码
     * @return
     * @throws Exception
     */
    private String registIDM(String name, String phone, String cardNumber) {
        Map<String, Object> bodyMap = new LinkedHashMap<>();
        List<Map<String, Object>> propertiesList = new LinkedList<>();

        Map<String, Object> nameMap = new LinkedHashMap<>();
        nameMap.put("name", "usercn");
        nameMap.put("value", name);
        propertiesList.add(nameMap);

        Map<String, Object> mobileMap = new LinkedHashMap<>();
        mobileMap.put("name", "mobile");
        mobileMap.put("value", phone);
        propertiesList.add(mobileMap);

        Map<String, Object> idcardnumberMap = new LinkedHashMap<>();
        idcardnumberMap.put("name", "idcardnumber");
        idcardnumberMap.put("value", cardNumber);
        propertiesList.add(idcardnumberMap);

        Map<String, Object> userpasswordMap = new LinkedHashMap<>();
        userpasswordMap.put("name", "userpassword");
        userpasswordMap.put("value", phone + "@hello");
        propertiesList.add(userpasswordMap);

        Map<String, Object> vktypeMap = new LinkedHashMap<>();
        vktypeMap.put("name", "vktype");
        vktypeMap.put("value", "S11");
        propertiesList.add(vktypeMap);

        bodyMap.put("attributes", propertiesList);
        return getResponseFromIdmServer(this.registerUrl, bodyMap);
    }

    /**
     * IDM 授权
     * 
     * @param exampleId
     *            万科id
     * @return
     * @throws Exception
     */
    private String accreditIDM(String exampleId) {
        String permIds = this.permIds;
        Map<String, Object> accreditData = new LinkedHashMap<>();
        accreditData.put("userId", exampleId);
        accreditData.put("appCode", "SSO");
        List<String> permIdsList = new ArrayList<String>();
        permIdsList.add(permIds);
        accreditData.put("permIds", permIdsList);
        return getResponseFromIdmServer(this.addUserPermUrl, accreditData);
    }

    private String getResponseFromIdmServer(String url, Map<String, Object> params) {
        HttpPost httpPost = new HttpPost(url);
        createSignHeader(httpPost, params);
        httpPost.addHeader("Content-type", "application/json; charset=utf-8");
        httpPost.setHeader("Accept", "application/json");

        StringEntity entity = new StringEntity(JSON.toJSONString(params), ContentType.APPLICATION_JSON);
        entity.setContentEncoding("UTF-8");
        entity.setContentType("application/json");

        httpPost.setEntity(entity);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(20000).setConnectTimeout(20000).build();
        httpPost.setConfig(requestConfig);
        log.info("请求IDM接口 header-->" + JSONObject.toJSONString(httpPost.getAllHeaders()));
        log.info("请求IDM接口 body-->" + JSON.toJSONString(params));
        log.info("请求IDM接口 url-->" + url);
        return execute(httpPost);
    }

    private String execute(HttpUriRequest request) {
        CloseableHttpClient client = null;
        CloseableHttpResponse resp = null;
        try {
            long start = System.currentTimeMillis();
            client = HttpClientBuilder.create().build();
            resp = client.execute(request);
            String result = EntityUtils.toString(resp.getEntity(), "utf-8");
            log.info("请求IDM接口 ->response result:{}，url:{}，cost time:{}ms！", result, request.getURI(),
                System.currentTimeMillis() - start);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        } finally {
            if (resp != null) {
                HttpClientUtils.closeQuietly(resp);
            }
            if (client != null) {
                HttpClientUtils.closeQuietly(client);
            }
        }
    }

    private void createSignHeader(HttpUriRequest request, Map<String, Object> params) {
        request.addHeader("appuser", appuser);

        String randomcode = RandomStringUtils.random(10, true, true);
        request.addHeader("randomcode", randomcode);

        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss'Z'");
        String timestamp = sdf.format(now.getTime());
        request.addHeader("timestamp", timestamp);

        String data = StringUtils.join(appuser, randomcode, timestamp);
        String encodekey = DigestUtils.sha256Hex(StringUtils.join(data, "{", appkey, "}"));
        request.addHeader("encodekey", encodekey);

        String paramData = StringUtils.join(JSON.toJSONString(params), "&", appkey);
        String signature = DigestUtils.md5Hex(paramData).toUpperCase();
        request.addHeader("sign", signature);

        request.addHeader("appinfo", appinfo);
        request.addHeader("Content-Type", "application/json");
    }
}

package com.example.hello.cloud.framework.common.exception;

/**
 * @author zhoujj07
 * @ClassName: ArgumentException
 * @Description: 参数异常类
 * @date 2018/7/28
 */
public class ArgumentException extends RuntimeException {

    public ArgumentException(String msg) {
        super(msg);
    }

}

package com.example.hello.cloud.framework.common.plugin.openapi.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 万科API服务市场配置
 *
 * @author v-linxb
 * @create 2019/9/2
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "openapi.example")
public class OpenApiProperties {

    private String url;

    private String username;

    private String sk;

    private String appId;

}

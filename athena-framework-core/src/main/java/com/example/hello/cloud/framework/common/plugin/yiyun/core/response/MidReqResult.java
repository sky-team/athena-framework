package com.example.hello.cloud.framework.common.plugin.yiyun.core.response;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 中台对业务参数的响应
 */
public class MidReqResult {
	/**
	 * 网络请求状态码
	 */
	private String httpStatus;
	/**
	 * 业务处理结果状态码 -1 表示连接中台失败,此时业务应用可输出errStack查看具体错误原因, 一般为SocketTimeoutException
	 */
	private String code;
	/**
	 * 业务处理结果说明,一般处理成功返回succ,处理失败返回失败说明
	 */
	private String message;
	/**
	 * 业务响应参数, 具体由每个接口定义,这里以"修改客户账号基本信息接口"为范例
	 */
	private JSONObject data;

	/**
	 * 业务响应参数, 具体由每个接口定义,这里以"修改客户账号基本信息接口"为范例
	 */
	private JSONArray dataArray;

	/**
	 * 处理失败的异常堆栈
	 */
	private Throwable errStack;

	public MidReqResult() {
	}

	public MidReqResult(String code, String message, JSONObject data, String httpStatus) {
		this.code = code;
		this.message = message;
		this.data = data;
		this.httpStatus = httpStatus;
	}

	public MidReqResult(String code, String message, JSONObject data, Throwable errStack, String httpStatus) {
		this.code = code;
		this.message = message;
		this.data = data;
		this.errStack = errStack;
		this.httpStatus = httpStatus;
	}

	public MidReqResult(String code, String message, JSONArray dataArray, String httpStatus) {
		this.code = code;
		this.message = message;
		this.dataArray = dataArray;
		this.httpStatus = httpStatus;
	}

	public MidReqResult(String code, String message, JSONArray dataArray, Throwable errStack, String httpStatus) {
		this.code = code;
		this.message = message;
		this.dataArray = dataArray;
		this.errStack = errStack;
		this.httpStatus = httpStatus;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public JSONObject getData() {
		return data;
	}

	public void setData(JSONObject data) {
		this.data = data;
	}

	public Throwable getErrStack() {
		return errStack;
	}

	public void setErrStack(Throwable errStack) {
		this.errStack = errStack;
	}

	public String getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
	}

	public JSONArray getDataArray() {
		return dataArray;
	}

	public void setDataArray(JSONArray dataArray) {
		this.dataArray = dataArray;
	}
}
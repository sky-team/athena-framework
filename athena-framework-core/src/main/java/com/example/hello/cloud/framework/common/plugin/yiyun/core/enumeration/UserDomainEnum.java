package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 子应用标识
 *
 * @author zhoujj07
 * @create 2018/5/10
 */
public enum UserDomainEnum {
    /**
     * 分享家子应用
     */
    UNION("fxjBiz"),
    /**
     * 在线家应用
     */
    ZXJ("zxjBiz"),
    /**
     * ecif应用，登录注册相关的api传这个
     */
    ECIF("ecif"),
    /**
     * 销售家子应用
     */
    XSJ("xsj"),
    /**
     * 融合后台
     */
    RH_ADMIN("rhAdmin");
    private String index;

    UserDomainEnum(String index) {
        this.index = index;
    }

    public String getIndex() {
        return index;
    }
}

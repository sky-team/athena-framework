package com.example.hello.cloud.framework.common.plugin.report.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Prop implements Serializable{

    private Long id;
    /**
     *推送数据给中台，设置propCode属性名称，value属性值
     */
    private String pubPropCode;
    /**
     * 中台返回使用propCode接受属性名称，value接受属性值
     */
    private String propCode;
    private String appPropCode;
    private Object value;
    private String extAttr;
}
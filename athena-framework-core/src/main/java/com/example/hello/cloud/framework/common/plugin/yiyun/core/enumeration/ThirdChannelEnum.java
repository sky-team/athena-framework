package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 第三方平台代码
 *
 * @author zhoujj07
 * @create 2018/5/9
 */
public enum ThirdChannelEnum {

    /**
     * 微信
     */
    WECHAT("wechat", "微信"),
    /**
     * 支付宝
     */
    ALIPAY("alipay", "支付宝"),
    /**
     * 微博
     */
    WEIBO("weibo", "微博"),;

    private String name;
    private String index;

    ThirdChannelEnum(String index, String name) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public String getIndex() {
        return index;
    }
}

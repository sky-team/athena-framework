package com.example.hello.cloud.framework.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Column
 * excel列注释
 *
 * @author yingc04
 * @create 2019/11/5
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
    /**
     * title
     */
    String title();

    /**
     * dateFormat
     */
    String dateFormat() default "yyyy-MM-dd";

    /**
     * 列宽
     */
    int width() default 4500;

    /**
     * 可编辑
     */
    boolean edit() default true;

    /**
     * 可选字符列表
     */
    String[] allowString() default {};

    /**
     * format
     */
    String format() default "@";

    /**
     * 字符长度访问，示例"{2,9}"
     */
    int[] length() default {0, 10000};


    /**
     * 整数范围值 ，示例"{"0","100"}"
     */
    String[] between() default {};

    /**
     * 浮点数范围值 ，示例"{"0.0","100.99"}"
     */
    String[] betweenfloat() default {};

}
package com.example.hello.cloud.framework.common.plugin.yiyun.core.enumeration;

/**
 * 产品价格类型
 *
 * @author v-linxb
 * @create 2018/5/29
 **/
public enum PriceTypeEnum {

    TOTAL_PRICE("total", "总价"),
    SINGLE_PRICE("single", "单价"),
    AVERAGE_PRICE("average", "均价");

    PriceTypeEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    private String value;

    private String desc;

    public String getValue() {
        return this.value;
    }

    public String getDesc() {
        return this.desc;
    }

    public static String getDesc(String value) {
        for (PriceTypeEnum c : PriceTypeEnum.values()) {
            if (c.getValue().equals(value)) {
                return c.getDesc();
            }
        }
        return null;
    }
}

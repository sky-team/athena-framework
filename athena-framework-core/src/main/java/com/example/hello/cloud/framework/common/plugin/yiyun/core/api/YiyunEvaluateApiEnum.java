package com.example.hello.cloud.framework.common.plugin.yiyun.core.api;

/**
 * 评价中心api地址枚举
 *
 * @author v-linxb
 * @create 2019/7/24
 **/
public enum YiyunEvaluateApiEnum implements YiyunServiceType {

    /**
     * 获取统一评价页面
     */
    PRODUCT_BASIC_QUERY("v1/evaluate/page", "获取统一评价页面", false),
    ;


    YiyunEvaluateApiEnum(String url, String desc, boolean needTicket) {
        this.url = url;
        this.desc = desc;
        this.needTicket = needTicket;
    }

    private final static String center = "evaluate/";
    private String url;
    private String desc;
    private boolean needTicket;

    @Override
    public String url() {
        return center + this.url;
    }

    @Override
    public String desc() {
        return this.desc;
    }

    @Override
    public boolean needTicket() {
        return this.needTicket;
    }
}

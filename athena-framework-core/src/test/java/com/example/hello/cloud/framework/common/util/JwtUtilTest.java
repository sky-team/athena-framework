package com.example.hello.cloud.framework.common.util;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zhanj04
 * @date 2019/10/25 15:10
 */
public class JwtUtilTest {

    /**
     * 日志
     */
    private static final Logger log = LoggerFactory.getLogger(JwtUtilTest.class);

    @Test
    public void build() {
        // hello的base64编码
        String sec = "emhpa2U=";
        log.info("执行JwtUtilTest.build()方法,jwt密钥:{}", sec);
        String jwt = JwtUtil.build("18011515485648800001", "oldbird", 5 * 24 * 60 * 60 * 1000, sec);
        log.info("生成的jwt:{}", jwt);
    }

}
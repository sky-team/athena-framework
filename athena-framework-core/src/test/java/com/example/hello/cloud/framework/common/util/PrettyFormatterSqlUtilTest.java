package com.example.hello.cloud.framework.common.util;

import org.junit.Test;

/**
 * @author zhanj04
 * @date 2019/10/25 15:04
 */
public class PrettyFormatterSqlUtilTest {

    @Test
    public void getPrettySql() {

        String sql = "select a.会计科目编码,a.会计科目名称,b.余额方向,b.期初余额,sum(a.借方发生额) 借方发生额,";
        sql += "sum(a.贷方发生额) 贷方发生额,b.期末余额 FROM 基础表_会计管理数据_凭证辅助明细表 a ";
        sql += "LEFT JOIN 基础表_会计管理数据_全年辅助余额表 b on(a.会计科目编码 = b.会计科目编码 ";
        sql += "and a.辅助编码 = b.辅助编码 and a.会计电子账簿编号 = b.会计电子账簿编号) ";
        sql += "WHERE a.辅助编码 = '' AND a.会计电子账簿编号 = '' ";
        sql += "GROUP BY a.会计科目编码,a.会计科目名称,b.余额方向,b.期初余额,b.期末余额 order by a.会计科目编码 ";

        String a = new PrettyFormatterSqlUtil().getPrettySql(sql);
        System.out.println(a);
    }
}
/**
 * CRM Logger is used to decouple and uniform logger usage with different vendors, typical DIP.
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
package com.example.hello.cloud.framework.core.logger;
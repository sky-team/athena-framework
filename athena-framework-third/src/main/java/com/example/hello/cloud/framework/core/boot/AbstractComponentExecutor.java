package com.example.hello.cloud.framework.core.boot;

import java.util.function.Consumer;
import java.util.function.Function;

import com.example.hello.cloud.framework.core.extension.BizScenario;
import com.example.hello.cloud.framework.core.extension.ExtensionCoordinate;

/**
 * AbstractComponentExecutor
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public abstract class AbstractComponentExecutor {

    /**
     * Execute extension with Response
     *
     * @param targetClz
     * @param bizScenario
     * @param exeFunction
     * @param <R>
     *            Response Type
     * @param <T>
     *            Parameter Type
     * @return
     */
    public <R, T> R execute(Class<T> targetClz, BizScenario bizScenario, Function<T, R> exeFunction) {
        T component = locateComponent(targetClz, bizScenario);
        return exeFunction.apply(component);
    }

    /**
     * @param extensionCoordinate
     * @param exeFunction
     * @param <R>
     * @param <T>
     * @return
     */
    public <R, T> R execute(ExtensionCoordinate extensionCoordinate, Function<T, R> exeFunction) {
        return execute((Class<T>)extensionCoordinate.getExtensionPointClass(), extensionCoordinate.getBizScenario(),
            exeFunction);
    }

    /**
     * Execute extension without Response
     *
     * @param targetClz
     * @param context
     * @param exeFunction
     * @param <T>
     *            Parameter Type
     */
    public <T> void executeVoid(Class<T> targetClz, BizScenario context, Consumer<T> exeFunction) {
        T component = locateComponent(targetClz, context);
        exeFunction.accept(component);
    }

    /**
     * @param extensionCoordinate
     * @param exeFunction
     * @param <T>
     */
    public <T> void executeVoid(ExtensionCoordinate extensionCoordinate, Consumer<T> exeFunction) {
        executeVoid(extensionCoordinate.getExtensionPointClass(), extensionCoordinate.getBizScenario(), exeFunction);
    }

    /**
     * @param targetClz
     * @param context
     * @param <C>
     * @return
     */
    protected abstract <C> C locateComponent(Class<C> targetClz, BizScenario context);
}

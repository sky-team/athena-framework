package com.example.hello.cloud.framework.core.extension;

import lombok.Data;

/**
 * Extension Coordinate(扩展坐标) is used to uniquely position an Extension
 *
 * 扩展设计中引入的概念：业务、用例、场景
 * 
 * 业务（Business）：就是一个自负盈亏的财务主体，比如tmall、淘宝和零售通就是三个不同的业务
 * 
 * 用例（Use Case）：描述了用户和系统之间的互动，每个用例提供了一个或多个场景。比如，支付订单就是一个典型的用例
 * 
 * 场景（Scenario）：场景也被称为用例的实例（Instance），包括用例所有的可能情况（正常的和异常的）。 比如对于“订单支付”这个用例，就有“可以使用花呗”，“支付宝余额不足”，“银行账户余额不足”等多个场景
 * 
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Data
public class ExtensionCoordinate {

    private String extensionPointName;
    private String bizScenarioUniqueIdentity;

    /**
     * Wrapper
     */
    private Class extensionPointClass;
    private BizScenario bizScenario;

    public static ExtensionCoordinate valueOf(Class extPtClass, BizScenario bizScenario) {
        return new ExtensionCoordinate(extPtClass, bizScenario);
    }

    public ExtensionCoordinate(Class extPtClass, BizScenario bizScenario) {
        this.extensionPointClass = extPtClass;
        this.extensionPointName = extPtClass.getName();
        this.bizScenario = bizScenario;
        this.bizScenarioUniqueIdentity = bizScenario.getUniqueIdentity();
    }

    /**
     * @param extensionPoint
     *            extensionPoint
     * @param bizScenario
     *            bizScenario
     */
    public ExtensionCoordinate(String extensionPoint, String bizScenario) {
        this.extensionPointName = extensionPoint;
        this.bizScenarioUniqueIdentity = bizScenario;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bizScenarioUniqueIdentity == null) ? 0 : bizScenarioUniqueIdentity.hashCode());
        result = prime * result + ((extensionPointName == null) ? 0 : extensionPointName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ExtensionCoordinate other = (ExtensionCoordinate)obj;
        if (bizScenarioUniqueIdentity == null) {
            if (other.bizScenarioUniqueIdentity != null) {
                return false;
            }
        } else if (!bizScenarioUniqueIdentity.equals(other.bizScenarioUniqueIdentity)) {
            return false;
        }
        if (extensionPointName == null) {
            if (other.extensionPointName != null) {
                return false;
            }
        } else if (!extensionPointName.equals(other.extensionPointName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ExtensionCoordinate [extensionPointName=" + extensionPointName + ", bizScenarioUniqueIdentity="
            + bizScenarioUniqueIdentity + "]";
    }

}

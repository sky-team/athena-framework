package com.example.hello.cloud.framework.core.domain;

import com.example.hello.cloud.framework.core.common.ApplicationContextHelper;

/**
 * BaseDomainFactory
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class BaseDomainFactory {

    public static <T extends EntityObject> T create(Class<T> entityClz) {
        return ApplicationContextHelper.getBean(entityClz);
    }

    public static <T> T getBean(Class<T> entityClz) {
        return ApplicationContextHelper.getBean(entityClz);
    }
}

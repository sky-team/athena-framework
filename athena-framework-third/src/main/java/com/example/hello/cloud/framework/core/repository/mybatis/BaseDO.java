package com.example.hello.cloud.framework.core.repository.mybatis;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 数据表实体基类
 *
 * 在项目中，我们有一些公共的字段需要做修改 如：
 *
 * create_date：创建时间
 * 
 * creat_user：创建人
 * 
 * update_date：修改时间
 * 
 * updateUser：修改人 这时候我们可以采用 MyBatis-Plus 中的字段自动填充功能去实现
 * 
 * 思路：抽取公用字段封装到BaseDO类中，再将使用到此公共字段的类继承基类，最后由 MyBatis-Plus 帮我们实现自动填充，这样我们便可以在service服务类中减少一定代码重复量
 * 
 * @author zhanj04
 * @date 2019/10/21 19:30
 */
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class BaseDO extends Model {

    private static final long serialVersionUID = 6783797492759893579L;

    /**
     * 创建时间
     */
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;

    /**
     * 修改时间
     */
    @TableField(value = "update_date", fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;

    /**
     * 逻辑删除
     */
    @TableField(value = "delete_flag", fill = FieldFill.INSERT)
    @TableLogic
    private Integer deleteFlag;

    /**
     * 创建人
     */
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    /**
     * 修改人
     */
    @TableField(value = "update_user", fill = FieldFill.INSERT_UPDATE)
    private String updateUser;

}

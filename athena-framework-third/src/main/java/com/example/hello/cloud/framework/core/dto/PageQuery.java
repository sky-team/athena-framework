package com.example.hello.cloud.framework.core.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * PageQuery
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public abstract class PageQuery implements Query {

    private static final long serialVersionUID = -558528504723940572L;
    private int pageNum = 1;
    private int pageSize = 10;
    private boolean needTotalCount = true;
    private List<OrderDesc> orderDescs;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isNeedTotalCount() {
        return needTotalCount;
    }

    public void setNeedTotalCount(boolean needTotalCount) {
        this.needTotalCount = needTotalCount;
    }

    public List<OrderDesc> getOrderDescs() {
        return orderDescs;
    }

    public void addOrderDesc(OrderDesc orderDesc) {
        if (null == orderDescs) {
            orderDescs = new ArrayList<>();
        }
        orderDescs.add(orderDesc);
    }

    public int getOffset() {
        return pageNum > 0 ? (pageNum - 1) * pageSize : 0;
    }
}

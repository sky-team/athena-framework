package com.example.hello.cloud.framework.core.domain;

import java.lang.annotation.*;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Entity, Entity Object is prototype and is not thread-safe
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public @interface Entity {}

package com.example.hello.cloud.framework.core.event;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.example.hello.cloud.framework.core.exception.framework.BaseException;
import com.example.hello.cloud.framework.core.exception.framework.helloException;
import com.example.hello.cloud.framework.core.logger.Logger;
import com.example.hello.cloud.framework.core.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.enums.BaseResponseCode;
import com.example.hello.cloud.framework.core.exception.framework.BaseException;
import com.example.hello.cloud.framework.core.exception.framework.helloException;
import com.example.hello.cloud.framework.core.logger.Logger;
import com.example.hello.cloud.framework.core.logger.LoggerFactory;

/**
 * Event Bus
 *
 * @author zhoujj07
 * @create 2019/10/22
 */
@Component
public class BaseEventBus implements EventBus {
    Logger logger = LoggerFactory.getLogger(BaseEventBus.class);

    /**
     * 默认线程池 如果处理器无定制线程池，则使用此默认
     */
    ExecutorService defaultExecutor = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors() + 1,
        Runtime.getRuntime().availableProcessors() + 1, 0L, TimeUnit.MILLISECONDS,
        new LinkedBlockingQueue<Runnable>(1000), new ThreadFactoryBuilder().setNameFormat("event-bus-pool-%d").build());

    @Autowired
    private EventHub eventHub;

    @SuppressWarnings("unchecked")
    @Override
    public ApiResponse fire(Event event) {
        ApiResponse response = null;
        SuperEventHandler eventHandler = null;
        try {
            eventHandler = eventHub.getEventHandler(event.getClass()).get(0);
            response = eventHandler.execute(event);
        } catch (Exception exception) {
            logger.error("执行EventBus.fire()方法,发生异常：{}", exception);
            assert eventHandler != null;
            response = handleException(eventHandler, exception);
        }
        return response;
    }

    @Override
    public void fireAll(Event event) {
        eventHub.getEventHandler(event.getClass()).stream().map(p -> {
            ApiResponse response = null;
            try {
                response = p.execute(event);
            } catch (Exception exception) {
                response = handleException(p, exception);
            }
            return response;
        }).collect(Collectors.toList());
    }

    @Override
    public void asyncFire(Event event) {
        eventHub.getEventHandler(event.getClass()).parallelStream().map(p -> {
            ApiResponse response = null;
            try {
                if (null != p.getExecutor()) {
                    p.getExecutor().submit(() -> p.execute(event));
                } else {
                    defaultExecutor.submit(() -> p.execute(event));
                }
            } catch (Exception exception) {
                response = handleException(p, exception);
            }
            return response;
        }).collect(Collectors.toList());
    }

    private ApiResponse handleException(SuperEventHandler handler, Exception exception) {
        logger.error(exception.getMessage(), exception);
        Class responseClz = eventHub.getResponseRepository().get(handler.getClass());
        ApiResponse response;
        try {
            response = (ApiResponse)responseClz.newInstance();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new helloException(e.getMessage());
        }
        if (exception instanceof BaseException) {
            BaseResponseCode errCode = ((BaseException)exception).getErrCode();
            response.setCode(errCode.getCode());
        } else {
            response.setCode(ApiResponseCodeEnum.EXCEPTION.getCode());
        }
        response.setMsg(exception.getMessage());
        return response;
    }
}

/**
 * Assembler classes used to assemble parameters to invoke other layer or other services.
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
package com.example.hello.cloud.framework.core.assembler;
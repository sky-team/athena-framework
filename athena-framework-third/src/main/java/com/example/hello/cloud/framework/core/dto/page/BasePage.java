package com.example.hello.cloud.framework.core.dto.page;

import lombok.ToString;

import java.io.Serializable;

/**
 * 分页基类
 *
 * @author yingc04
 * @create 2019/11/5
 */
@ToString
public class BasePage implements Serializable {

    private static final Integer DEFAULT_SIZE = 10;
    private static final Integer DEFAULT_NO = 1;
    private static final long serialVersionUID = 6152924163783866020L;

    public BasePage() {
        this.pageNo = DEFAULT_NO;
        this.pageSize = DEFAULT_SIZE;
    }

    BasePage(Integer pageNo, Integer pageSize) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }

    /**
     * 页码,默认1
     */
    private Integer pageNo;

    /**
     * 每页数量，默认10
     */
    private Integer pageSize;

    public Integer getPageNo() {
        if (pageNo == null || pageNo <= 0) {
            pageNo = DEFAULT_NO;
        }
        return pageNo;
    }

    public void setPageNo(Integer pageIndex) {
        this.pageNo = pageIndex;
    }

    public Integer getPageSize() {
        if (pageSize == null || pageSize <= 0) {
            pageSize = DEFAULT_SIZE;
        }
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}

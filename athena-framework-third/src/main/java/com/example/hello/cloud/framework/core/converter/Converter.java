package com.example.hello.cloud.framework.core.converter;

/**
 * Converter are used to convert Objects among Client Object, Domain Object and Data Object.
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface Converter {
}

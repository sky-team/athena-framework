package com.example.hello.cloud.framework.core.boot;

import java.util.Comparator;

/**
 * ClassNameComparator
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class ClassNameComparator implements Comparator<Class<?>> {
    @Override
    public int compare(Class<?> o1, Class<?> o2) {
        if (o1 == null) {
            return -1;
        }
        if (o2 == null) {
            return 1;
        }
        return o1.getName().compareToIgnoreCase(o2.getName());
    }
}

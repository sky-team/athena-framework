package com.example.hello.cloud.framework.core.common;

import com.example.hello.cloud.framework.core.exception.SysException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.core.exception.SysException;

/**
 * ApplicationContextHelper
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Component
public class ApplicationContextHelper implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ApplicationContextHelper.applicationContext = applicationContext;
    }

    public static <T> T getBean(Class<T> targetClz) {
        T beanInstance = null;
        // 优先按type查
        try {
            beanInstance = (T)applicationContext.getBean(targetClz);
        } catch (Exception e) {
        }
        // 按name查
        if (beanInstance == null) {
            String simpleName = targetClz.getSimpleName();
            // 首字母小写
            simpleName = Character.toLowerCase(simpleName.charAt(0)) + simpleName.substring(1);
            beanInstance = (T)applicationContext.getBean(simpleName);
        }
        if (beanInstance == null) {
            throw new SysException(ApiResponseCodeEnum.EXCEPTION,
                "Component " + targetClz + " can not be found in Spring Container");
        }
        return beanInstance;
    }

    public static Object getBean(String claz) {
        return ApplicationContextHelper.applicationContext.getBean(claz);
    }
}

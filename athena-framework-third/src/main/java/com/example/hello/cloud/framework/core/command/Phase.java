package com.example.hello.cloud.framework.core.command;

import java.lang.annotation.*;

import org.springframework.stereotype.Component;

/**
 * Phase 在处理复杂Command时候，可以分成多个阶段（Phase）处理，每个阶段可以分成多个步骤（Step）。
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Component
public @interface Phase {}

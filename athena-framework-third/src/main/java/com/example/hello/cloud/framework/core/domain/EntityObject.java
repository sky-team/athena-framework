package com.example.hello.cloud.framework.core.domain;

import com.example.hello.cloud.framework.core.extension.BizScenario;
import com.example.hello.cloud.framework.core.extension.BizScenario;

import lombok.Data;

/**
 * Entity Object
 *
 * This is the parent object of all domain objects
 * 
 * 允许继承domainmodel
 * 
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Data
public abstract class EntityObject {
    private BizScenario bizScenario;
}

package com.example.hello.cloud.framework.core.boot;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.hello.cloud.framework.core.exception.framework.helloException;

import lombok.Getter;
import lombok.Setter;

/**
 * Extension <B>应用的核心引导启动类</B>
 * <p>
 * 负责扫描在applicationContext.xml中配置的packages. 获取到CommandExecutors, intercepters, extensions, validators等 交给各个注册器进行注册
 * 
 * hello-cloud-core 是参照COLA2.0 @see https://github.com/alibaba/COLA COLA
 * 
 * COLA 是基于DDD+CQRS+扩展点 的应用框架，致力于采用领域驱动的设计思想，规范控制程序员的随心所欲，从而解决软件的复杂性问题
 *
 * 设计原则简单，即在高内聚，低耦合，可扩展，易理解的指导思想下，尽可能的贯彻面向对象的设计思想和领域驱动设计的原则
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class Bootstrap {
    @Getter
    @Setter
    private List<String> packages;
    private ClassPathScanHandler handler;

    @Autowired
    private RegisterFactory registerFactory;

    public void init() {
        Set<Class<?>> classSet = scanConfiguredPackages();
        registerBeans(classSet);
    }

    /**
     * @param classSet
     */
    private void registerBeans(Set<Class<?>> classSet) {
        for (Class<?> targetClz : classSet) {
            Register register = registerFactory.getRegister(targetClz);
            if (null != register) {
                register.doRegistration(targetClz);
            }
        }

    }

    /**
     * Scan the packages configured in Spring xml
     *
     * @return
     */
    private Set<Class<?>> scanConfiguredPackages() {
        if (packages == null) {
            throw new helloException("Command packages is not specified");
        }
        String[] pkgs = new String[packages.size()];
        handler = new ClassPathScanHandler(packages.toArray(pkgs));

        Set<Class<?>> classSet = new TreeSet<>(new ClassNameComparator());
        for (String pakName : packages) {
            classSet.addAll(handler.getPackageAllClasses(pakName, true));
        }
        return classSet;
    }
}

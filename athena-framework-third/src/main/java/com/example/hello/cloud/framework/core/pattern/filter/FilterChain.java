package com.example.hello.cloud.framework.core.pattern.filter;

/**
 * 拦截器链
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class FilterChain<T> {

    private FilterInvoker header;

    public void doFilter(T context) {
        header.invoke(context);
    }

    public void setHeader(FilterInvoker header) {
        this.header = header;
    }
}

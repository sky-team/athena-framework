package com.example.hello.cloud.framework.core.event;

/**
 * Domain Event (领域事件）
 *
 * Domain Event(领域事件)，是领域实体发生状态变化后，向外界publish出来的事件。 该事件既可以在当前的Bounded Context下面被消费，也可以被其它Bounded Context消费。
 *
 * 其命名规则是：领域名称+动词的一般过去式+Event
 *
 * 这里的动词的一般过去式，非常关键，因为在语义上表达的是发生过的事情，因为Event总是在动作发生后再发出的。下面是几个举例：
 *
 * 1.CustomerCreatedEvent,表示客户创建后发出的领域事件。
 * 
 * 2.OpportunityTransferedEvent，表示机会转移后发出的领域事件。
 * 
 * 3.LeadsCreatedEvent，表示线索创建后发出的领域事件。
 * 
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface DomainEvent extends Event {}

package com.example.hello.cloud.framework.core.event;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.hello.cloud.framework.core.exception.framework.helloException;
import org.springframework.stereotype.Component;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.example.hello.cloud.framework.core.exception.framework.helloException;

import lombok.Getter;
import lombok.Setter;

/**
 * 事件控制中枢
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@SuppressWarnings("rawtypes")
@Component
public class EventHub {
    @Getter
    @Setter
    private ListMultimap<Class, SuperEventHandler> eventRepository = ArrayListMultimap.create();

    @Getter
    private Map<Class, Class> responseRepository = new HashMap<>();

    public List<SuperEventHandler> getEventHandler(Class eventClass) {
        List<SuperEventHandler> eventHandlerList = findHandler(eventClass);
        if (eventHandlerList == null || eventHandlerList.size() == 0) {
            throw new helloException(eventClass + "is not registered in eventHub, please register first");
        }
        return eventHandlerList;
    }

    /**
     * 注册事件
     * 
     * @param eventClz
     * @param executor
     */
    public void register(Class<? extends Event> eventClz, SuperEventHandler executor) {
        eventRepository.put(eventClz, executor);
    }

    private List<SuperEventHandler> findHandler(Class<? extends Event> eventClass) {
        List<SuperEventHandler> eventHandlerList = null;
        Class cls = eventClass;
        eventHandlerList = eventRepository.get(cls);
        return eventHandlerList;
    }

}

package com.example.hello.cloud.framework.core.command;

import java.util.List;

import com.example.hello.cloud.framework.core.dto.Command;
import com.example.hello.cloud.framework.core.exception.framework.ExceptionHandlerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.collect.FluentIterable;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.core.dto.Command;
import com.example.hello.cloud.framework.core.exception.framework.ExceptionHandlerFactory;
import com.example.hello.cloud.framework.core.logger.Logger;
import com.example.hello.cloud.framework.core.logger.LoggerFactory;

import lombok.Setter;

/**
 * CommandInvocation
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CommandInvocation {

    private static Logger logger = LoggerFactory.getLogger(CommandInvocation.class);

    @Setter
    private CommandExecutor commandExecutor;
    @Setter
    private Iterable<CommandInterceptor> preInterceptors;
    @Setter
    private Iterable<CommandInterceptor> postInterceptors;

    @Autowired
    private CommandHub commandHub;

    public CommandInvocation() {

    }

    public CommandInvocation(CommandExecutor commandExecutor, List<CommandInterceptor> preInterceptors,
        List<CommandInterceptor> postInterceptors) {
        this.commandExecutor = commandExecutor;
        this.preInterceptors = preInterceptors;
        this.postInterceptors = postInterceptors;
    }

    public ApiResponse invoke(com.example.hello.cloud.framework.core.dto.Command command) {
        ApiResponse response = null;
        try {
            preIntercept(command);
            response = commandExecutor.execute(command);
        } catch (Exception e) {
            response = getResponseInstance(command);
            ExceptionHandlerFactory.getExceptionHandler().handleException(command, response, e);
        } finally {
            // make sure post interceptors performs even though exception happens
            postIntercept(command, response);
        }
        return response;
    }

    private void postIntercept(com.example.hello.cloud.framework.core.dto.Command command, ApiResponse response) {
        try {
            for (CommandInterceptor postInterceptor : FluentIterable.from(postInterceptors).toSet()) {
                postInterceptor.postIntercept(command, response);
            }
        } catch (Exception e) {
            logger.error("postInterceptor error:" + e.getMessage(), e);
        }
    }

    private void preIntercept(com.example.hello.cloud.framework.core.dto.Command command) {
        for (CommandInterceptor preInterceptor : FluentIterable.from(preInterceptors).toSet()) {
            preInterceptor.preIntercept(command);
        }
    }

    private ApiResponse getResponseInstance(Command cmd) {
        Class responseClz = commandHub.getResponseRepository().get(cmd.getClass());
        try {
            return (ApiResponse)responseClz.newInstance();
        } catch (Exception e) {
            return new ApiResponse();
        }
    }
}

package com.example.hello.cloud.framework.core.exception.framework;

import com.example.hello.cloud.framework.core.dto.Command;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.enums.BaseResponseCode;
import com.example.hello.cloud.framework.core.dto.Command;
import com.example.hello.cloud.framework.core.exception.ExceptionHandler;
import com.example.hello.cloud.framework.core.logger.Logger;
import com.example.hello.cloud.framework.core.logger.LoggerFactory;

/**
 * DefaultExceptionHandler
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class DefaultExceptionHandler implements ExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    static DefaultExceptionHandler singleton = new DefaultExceptionHandler();

    @Override
    public void handleException(Command cmd, ApiResponse response, Exception exception) {
        buildResponse(response, exception);
        printLog(cmd, response, exception);
    }

    private void printLog(Command cmd, ApiResponse response, Exception exception) {
        if (exception instanceof BaseException) {
            // biz exception is expected, only warn it
            logger.warn(buildErrorMsg(cmd, response));
        } else {
            // sys exception should be monitored, and pay attention to it
            logger.error(buildErrorMsg(cmd, response), exception);
        }
    }

    private String buildErrorMsg(Command cmd, ApiResponse response) {
        return "Process [" + cmd + "] failed, errorCode: " + response.getCode() + " errorMsg:" + response.getMsg();
    }

    private void buildResponse(ApiResponse response, Exception exception) {
        if (exception instanceof BaseException) {
            BaseResponseCode errCode = ((BaseException)exception).getErrCode();
            response.setCode(errCode.getCode());
        } else {
            response.setCode(ApiResponseCodeEnum.EXCEPTION.getCode());
        }
        response.setMsg(exception.getMessage());
    }
}

package com.example.hello.cloud.framework.core.boot;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import com.example.hello.cloud.framework.core.exception.framework.helloException;

/**
 * AbstractRegister
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public abstract class AbstractRegister implements Register, ApplicationContextAware {

    protected ApplicationContext applicationContext;

    /**
     * @param targetClz
     * @param <T>
     * @return
     */
    protected <T> T getBean(Class targetClz) {
        T beanInstance = null;
        // 优先按type查
        try {
            beanInstance = (T)applicationContext.getBean(targetClz);
        } catch (Exception e) {
        }
        // 按name查
        if (beanInstance == null) {
            String simpleName = targetClz.getSimpleName();
            // 首字母小写
            simpleName = Character.toLowerCase(simpleName.charAt(0)) + simpleName.substring(1);
            beanInstance = (T)applicationContext.getBean(simpleName);
        }
        if (beanInstance == null) {
            throw new helloException("Component " + targetClz + " can not be found in Spring Container");
        }
        return beanInstance;
    }

    /**
     * 根据Order注解排序
     * 
     * @param interceptorList
     *            拦截器list
     */
    protected <T> void order(List<T> interceptorList) {
        if (interceptorList == null || interceptorList.size() <= 1) {
            return;
        }
        T newInterceptor = interceptorList.get(interceptorList.size() - 1);
        Order order = newInterceptor.getClass().getDeclaredAnnotation(Order.class);
        if (order == null) {
            return;
        }
        int index = interceptorList.size() - 1;
        for (int i = interceptorList.size() - 2; i >= 0; i--) {
            int itemOrderInt = Ordered.LOWEST_PRECEDENCE;
            Order itemOrder = interceptorList.get(i).getClass().getDeclaredAnnotation(Order.class);
            if (itemOrder != null) {
                itemOrderInt = itemOrder.value();
            }
            if (itemOrderInt > order.value()) {
                interceptorList.set(index, interceptorList.get(i));
                index = i;
            } else {
                break;
            }
        }
        if (index < interceptorList.size() - 1) {
            interceptorList.set(index, newInterceptor);
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

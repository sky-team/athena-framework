package com.example.hello.cloud.framework.core.boot;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.hello.cloud.framework.core.common.ApplicationContextHelper;
import com.example.hello.cloud.framework.core.common.Constant;
import com.example.hello.cloud.framework.core.exception.framework.helloException;
import com.example.hello.cloud.framework.core.extension.*;

/**
 * ExtensionRegister
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Component
public class ExtensionRegister implements Register {

    @Autowired
    private ExtensionRepository extensionRepository;

    @Override
    public void doRegistration(Class<?> targetClz) {
        ExtensionPoint extension = (ExtensionPoint)ApplicationContextHelper.getBean(targetClz);
        Extension extensionAnn = targetClz.getDeclaredAnnotation(Extension.class);
        String extPtClassName = calculateExtensionPoint(targetClz);
        BizScenario bizScenario =
            BizScenario.valueOf(extensionAnn.bizId(), extensionAnn.useCase(), extensionAnn.scenario());
        ExtensionCoordinate extensionCoordinate =
            new ExtensionCoordinate(extPtClassName, bizScenario.getUniqueIdentity());
        ExtensionPoint preVal = extensionRepository.getExtensionRepo().put(extensionCoordinate, extension);
        if (preVal != null) {
            throw new helloException("Duplicate registration is not allowed for :" + extensionCoordinate);
        }
    }

    /**
     * @param targetClz
     * @return
     */
    private String calculateExtensionPoint(Class<?> targetClz) {
        Class[] interfaces = targetClz.getInterfaces();
        if (ArrayUtils.isEmpty(interfaces)) {
            throw new helloException("Please assign a extension point interface for " + targetClz);
        }
        for (Class intf : interfaces) {
            String extensionPoint = intf.getSimpleName();
            if (StringUtils.contains(extensionPoint, Constant.EXTENSION_EXTPT_NAMING)) {
                return intf.getName();
            }
        }
        throw new helloException("Your name of ExtensionPoint for " + targetClz + " is not valid, must be end of "
            + Constant.EXTENSION_EXTPT_NAMING);
    }

}
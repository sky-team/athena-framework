package com.example.hello.cloud.framework.core.dto;

import java.io.Serializable;

/**
 * OrderDesc
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class OrderDesc implements Serializable {

    private static final long serialVersionUID = -4079578504657399726L;
    private String col;
    private boolean asc = true;

    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }
}

package com.example.hello.cloud.framework.core.exception;

import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.enums.BaseResponseCode;
import com.example.hello.cloud.framework.core.exception.framework.BaseException;

/**
 * BizException is known Exception, no need retry
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class BizException extends BaseException {

    private static final long serialVersionUID = 1L;

    public BizException(String errMessage) {
        super(errMessage);
        this.setErrCode(ApiResponseCodeEnum.BIZ_ERROR);
    }

    public BizException(BaseResponseCode errCode) {
        super(errCode.getShowMessage());
        this.setErrCode(errCode);
    }

    public BizException(BaseResponseCode errCode, String errMessage) {
        super(errMessage);
        this.setErrCode(errCode);
    }

    public BizException(String errMessage, Throwable e) {
        super(errMessage, e);
        this.setErrCode(ApiResponseCodeEnum.BIZ_ERROR);
    }
}
package com.example.hello.cloud.framework.core.command;

import com.example.hello.cloud.framework.core.dto.Command;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.core.dto.Command;

/**
 * CommandBus
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface CommandBus {

    /**
     * Send command to CommandBus, then the command will be executed by CommandExecutor
     * 
     * @param cmd
     *            or Query
     * @return Response
     */
    ApiResponse send(Command cmd);
}

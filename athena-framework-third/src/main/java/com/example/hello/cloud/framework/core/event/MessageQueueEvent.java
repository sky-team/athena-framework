package com.example.hello.cloud.framework.core.event;

/**
 * MessageQueueEvent
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface MessageQueueEvent extends Event {

    /**
     * 获取事件类型
     * 
     * @return String
     */
    String getEventType();

    /**
     * 获取事件主题
     * 
     * @return String
     */
    String getEventTopic();
}

package com.example.hello.cloud.framework.core.command;

import com.example.hello.cloud.framework.core.dto.Command;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.core.dto.Command;

/**
 * QueryExecutor
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface QueryExecutor<R extends ApiResponse, C extends Command> extends CommandExecutor<R, C> {

}

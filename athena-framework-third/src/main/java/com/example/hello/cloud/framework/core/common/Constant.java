package com.example.hello.cloud.framework.core.common;

/**
 * DDD 框架全局基础常量
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class Constant {

    /**
     * 常量分隔符
     */
    public final static String DOT_SEPARATOR = ".";

    /**
     * 扩展点命名
     */
    public final static String EXTENSION_EXTPT_NAMING = "ExtPt";

    /**
     * 方法执行
     */
    public final static String EXE_METHOD = "execute";
}

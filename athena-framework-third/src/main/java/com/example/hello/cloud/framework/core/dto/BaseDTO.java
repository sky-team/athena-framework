package com.example.hello.cloud.framework.core.dto;

import java.io.Serializable;

/**
 * 数据传输对象基类
 *
 * 服务的二方库无外乎就是用来暴露接口和传递数据的（DTO）。不过，往深层次思考，它并不是一个简单的问题，因为它涉及到不同界限上下文（Bounded Context）之间的协作问题。
 * 它是分布式环境下，不同服务（SOA，RPC，微服务，叫法不同，本质一样）之间如何协作的重要架构设计问题。
 * 
 * @author zhoujj07
 * @create 2019/10/22
 */
public abstract class BaseDTO implements Serializable {

    private static final long serialVersionUID = 1L;

}

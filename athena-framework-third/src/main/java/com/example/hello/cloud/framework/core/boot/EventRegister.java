package com.example.hello.cloud.framework.core.boot;

import java.lang.reflect.Method;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.hello.cloud.framework.core.common.ApplicationContextHelper;
import com.example.hello.cloud.framework.core.common.Constant;
import com.example.hello.cloud.framework.core.event.Event;
import com.example.hello.cloud.framework.core.event.EventHub;
import com.example.hello.cloud.framework.core.event.SuperEventHandler;
import com.example.hello.cloud.framework.core.exception.framework.helloException;

/**
 * EventRegister
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Component
public class EventRegister implements Register {

    @Autowired
    private EventHub eventHub;

    @Override
    public void doRegistration(Class<?> targetClz) {
        Class<? extends Event> eventClz = getEventFromExecutor(targetClz);
        SuperEventHandler executor = (SuperEventHandler)ApplicationContextHelper.getBean(targetClz);
        eventHub.register(eventClz, executor);
    }

    /**
     * @param eventExecutorClz
     * @return
     */
    private Class<? extends Event> getEventFromExecutor(Class<?> eventExecutorClz) {
        Method[] methods = eventExecutorClz.getDeclaredMethods();
        for (Method method : methods) {
            if (isExecuteMethod(method)) {
                return checkAndGetEventParamType(method);
            }
        }
        throw new helloException(
            "Event param in " + eventExecutorClz + " " + Constant.EXE_METHOD + "() is not detected");
    }

    private boolean isExecuteMethod(Method method) {
        return Constant.EXE_METHOD.equals(method.getName()) && !method.isBridge();
    }

    /**
     * @param method
     * @return
     */
    private Class checkAndGetEventParamType(Method method) {
        Class<?>[] exeParams = method.getParameterTypes();
        if (exeParams.length == 0) {
            throw new helloException(
                "Execute method in " + method.getDeclaringClass() + " should at least have one parameter");
        }
        if (!Event.class.isAssignableFrom(exeParams[0])) {
            throw new helloException(
                "Execute method in " + method.getDeclaringClass() + " should be the subClass of Event");
        }
        return exeParams[0];
    }
}

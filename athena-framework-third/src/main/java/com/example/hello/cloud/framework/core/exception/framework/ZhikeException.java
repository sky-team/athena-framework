package com.example.hello.cloud.framework.core.exception.framework;

import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;

/**
 * Infrastructure Exception
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class helloException extends BaseException {

    private static final long serialVersionUID = 1L;

    public helloException(String errMessage) {
        super(errMessage);
        this.setErrCode(ApiResponseCodeEnum.EXCEPTION);
    }

    public helloException(String errMessage, Throwable e) {
        super(errMessage, e);
        this.setErrCode(ApiResponseCodeEnum.EXCEPTION);
    }
}
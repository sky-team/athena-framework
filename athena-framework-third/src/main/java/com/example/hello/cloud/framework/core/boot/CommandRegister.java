package com.example.hello.cloud.framework.core.boot;

import java.lang.reflect.Method;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;
import com.example.hello.cloud.framework.core.command.CommandExecutor;
import com.example.hello.cloud.framework.core.command.CommandHub;
import com.example.hello.cloud.framework.core.command.CommandInterceptor;
import com.example.hello.cloud.framework.core.command.CommandInvocation;
import com.example.hello.cloud.framework.core.common.ApplicationContextHelper;
import com.example.hello.cloud.framework.core.common.Constant;
import com.example.hello.cloud.framework.core.dto.Command;
import com.example.hello.cloud.framework.core.exception.framework.helloException;

/**
 * CommandRegister
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Component
public class CommandRegister implements Register {

    @Autowired

    private CommandHub commandHub;

    @Override
    public void doRegistration(Class<?> targetClz) {
        Class<? extends Command> commandClz = getCommandFromExecutor(targetClz);
        CommandInvocation commandInvocation = ApplicationContextHelper.getBean(CommandInvocation.class);
        commandInvocation.setCommandExecutor((CommandExecutor)ApplicationContextHelper.getBean(targetClz));
        commandInvocation.setPreInterceptors(collectInterceptors(commandClz, true));
        commandInvocation.setPostInterceptors(collectInterceptors(commandClz, false));
        commandHub.getCommandRepository().put(commandClz, commandInvocation);
    }

    /**
     * @param commandExecutorClz
     *            commandExecutorClz
     * @return
     */
    private Class<? extends Command> getCommandFromExecutor(Class<?> commandExecutorClz) {
        Method[] methods = commandExecutorClz.getDeclaredMethods();
        for (Method method : methods) {
            if (isExecuteMethod(method)) {
                Class commandClz = checkAndGetCommandParamType(method);
                commandHub.getResponseRepository().put(commandClz, method.getReturnType());
                return (Class<? extends Command>)commandClz;
            }
        }
        throw new helloException(" There is no " + Constant.EXE_METHOD + "() in " + commandExecutorClz);
    }

    private boolean isExecuteMethod(Method method) {
        return Constant.EXE_METHOD.equals(method.getName()) && !method.isBridge();
    }

    /**
     * @param method
     * @return
     */
    private Class checkAndGetCommandParamType(Method method) {
        Class<?>[] exeParams = method.getParameterTypes();
        if (exeParams.length == 0) {
            throw new helloException(
                "Execute method in " + method.getDeclaringClass() + " should at least have one parameter");
        }
        if (!Command.class.isAssignableFrom(exeParams[0])) {
            throw new helloException(
                "Execute method in " + method.getDeclaringClass() + " should be the subClass of Command");
        }
        return exeParams[0];
    }

    /**
     * @param commandClass
     * @param pre
     * @return
     */
    private Iterable<CommandInterceptor> collectInterceptors(Class<? extends Command> commandClass, boolean pre) {
        /**
         * add 通用的Interceptors
         */
        Iterable<CommandInterceptor> commandItr =
            Iterables.concat((pre ? commandHub.getGlobalPreInterceptors() : commandHub.getGlobalPostInterceptors()));

        return commandItr;
    }

}

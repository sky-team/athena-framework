package com.example.hello.cloud.framework.core.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.hello.cloud.framework.core.exception.framework.helloException;
import org.springframework.stereotype.Component;

import com.example.hello.cloud.framework.core.exception.framework.helloException;

import lombok.Getter;
import lombok.Setter;

/**
 * Command Hub holds all the important information about Command
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@SuppressWarnings("rawtypes")
@Component
public class CommandHub {

    @Getter
    @Setter
    /**
     * 全局通用的PreInterceptors
     */
    private List<CommandInterceptor> globalPreInterceptors = new ArrayList<>();
    @Getter
    @Setter
    /**
     * 全局通用的PostInterceptors
     */
    private List<CommandInterceptor> globalPostInterceptors = new ArrayList<>();
    @Getter
    @Setter
    private Map<Class, CommandInvocation> commandRepository = new HashMap<>();

    @Getter
    /**
     * This Repository is used for return right response type on exception scenarios CommandClz ResponseClz
     */
    private Map<Class, Class> responseRepository = new HashMap<>();

    public CommandInvocation getCommandInvocation(Class cmdClass) {
        CommandInvocation commandInvocation = commandRepository.get(cmdClass);
        if (commandRepository.get(cmdClass) == null) {
            throw new helloException(cmdClass + " is not registered in CommandHub, please register first");
        }
        return commandInvocation;
    }
}

package com.example.hello.cloud.framework.core.event;

import com.example.hello.cloud.framework.common.ApiResponse;

/**
 * EventBus interface
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface EventBus {

    /**
     * Send event to EventBus
     * 
     * @param event
     * @return Response
     */
    ApiResponse fire(Event event);

    /**
     * fire all handlers which registed the event
     *
     * @param event
     *            event
     */
    void fireAll(Event event);

    /**
     * Async fire all handlers
     * 
     * @param event
     *            event
     */
    void asyncFire(Event event);
}

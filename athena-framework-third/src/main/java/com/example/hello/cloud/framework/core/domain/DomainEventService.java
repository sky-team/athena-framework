package com.example.hello.cloud.framework.core.domain;

import com.example.hello.cloud.framework.core.event.DomainEvent;

/**
 * DomainEventService 领域事件服务
 * 
 * 用来提供更粗粒度的领域能力
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface DomainEventService {
    /**
     * 发布领域事件
     *
     * 具体的事件处理机制，由应用自己实现，在sofa-extension中，我们提供了MetaQ的实现
     * 
     * @param domainEvent
     */
    void publish(DomainEvent domainEvent);
}

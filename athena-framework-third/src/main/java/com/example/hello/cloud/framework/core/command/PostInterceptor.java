package com.example.hello.cloud.framework.core.command;

import java.lang.annotation.*;

import com.example.hello.cloud.framework.core.dto.Command;
import org.springframework.stereotype.Component;

import com.example.hello.cloud.framework.core.dto.Command;

/**
 * PostInterceptor
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Inherited
@Component
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PostInterceptor {

    Class<? extends Command>[] commands() default {};

}

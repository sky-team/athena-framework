package com.example.hello.cloud.framework.core.exception;

import com.example.hello.cloud.framework.core.dto.Command;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.core.dto.Command;

/**
 * ExceptionHandler provide a backdoor that Application can override the default Exception handling
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface ExceptionHandler {
    /**
     * 异常处理
     * 
     * @param cmd
     *            Command
     * @param response
     *            Response
     * @param exception
     *            Exception
     */
    void handleException(Command cmd, ApiResponse response, Exception exception);
}

package com.example.hello.cloud.framework.core.exception.framework;

import com.example.hello.cloud.framework.common.enums.BaseResponseCode;

/**
 * Base Exception is the parent of all exceptions
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public abstract class BaseException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private BaseResponseCode errCode;

    public BaseException(String errMessage) {
        super(errMessage);
    }

    public BaseException(String errMessage, Throwable e) {
        super(errMessage, e);
    }

    public BaseResponseCode getErrCode() {
        return errCode;
    }

    protected void setErrCode(BaseResponseCode errCode) {
        this.errCode = errCode;
    }

}

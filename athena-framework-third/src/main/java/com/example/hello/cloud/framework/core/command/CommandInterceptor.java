package com.example.hello.cloud.framework.core.command;

import com.example.hello.cloud.framework.core.dto.Command;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.core.dto.Command;

/**
 * Interceptor will do AOP processing before or after Command Execution
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface CommandInterceptor {

    /**
     * Pre-processing before command execution
     * 
     * @param command
     *            cmd
     */
    default void preIntercept(com.example.hello.cloud.framework.core.dto.Command command) {}

    /**
     * Post-processing after command execution
     * 
     * Note that response could be null, check it before use
     * 
     * @param command
     *            command
     * @param response
     *            response
     * 
     */
    default void postIntercept(Command command, ApiResponse response) {}

}

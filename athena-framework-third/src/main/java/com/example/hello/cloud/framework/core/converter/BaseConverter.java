package com.example.hello.cloud.framework.core.converter;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 类型转换基类，将源对象S，转换伟目标对象
 *
 * @author zhoujj07
 * @create 2019/10/23
 */
public abstract class BaseConverter<S, T> implements  Converter{

    private final Function<S, T> function;

    public BaseConverter(final Function<S, T> function) {
        this.function = function;
    }

    public final T convert(final S source) {
        return function.apply(source);
    }

    public final List<T> convertToList(final Collection<S> sourceList) {
        return sourceList.stream().map(this::convert).collect(Collectors.toList());
    }
}

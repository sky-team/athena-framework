package com.example.hello.cloud.framework.core.boot;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.example.hello.cloud.framework.core.command.CommandHub;
import com.example.hello.cloud.framework.core.command.CommandInterceptor;
import com.example.hello.cloud.framework.core.command.PostInterceptor;
import com.example.hello.cloud.framework.core.common.ApplicationContextHelper;
import com.example.hello.cloud.framework.core.dto.Command;

/**
 * PostInterceptorRegister
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Component
public class PostInterceptorRegister extends AbstractRegister {

    @Autowired
    private CommandHub commandHub;

    @Override
    public void doRegistration(Class<?> targetClz) {
        CommandInterceptor commandInterceptor = (CommandInterceptor)ApplicationContextHelper.getBean(targetClz);
        PostInterceptor postInterceptorAnn = targetClz.getDeclaredAnnotation(PostInterceptor.class);
        Class<? extends Command>[] supportClasses = postInterceptorAnn.commands();
        registerInterceptor(supportClasses, commandInterceptor);
    }

    private void registerInterceptor(Class<? extends Command>[] supportClasses, CommandInterceptor commandInterceptor) {
        if (null == supportClasses || supportClasses.length == 0) {
            commandHub.getGlobalPostInterceptors().add(commandInterceptor);
            order(commandHub.getGlobalPostInterceptors());
            return;
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

package com.example.hello.cloud.framework.core.exception;

import com.example.hello.cloud.framework.common.enums.ApiResponseCodeEnum;
import com.example.hello.cloud.framework.common.enums.BaseResponseCode;
import com.example.hello.cloud.framework.core.exception.framework.BaseException;

/**
 * System Exception is unexpected Exception, retry might work again
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class SysException extends BaseException {

    private static final long serialVersionUID = 4355163994767354840L;

    public SysException(String errMessage) {
        super(errMessage);
        this.setErrCode(ApiResponseCodeEnum.EXCEPTION);
    }

    public SysException(BaseResponseCode errCode, String errMessage) {
        super(errMessage);
        this.setErrCode(errCode);
    }

    public SysException(String errMessage, Throwable e) {
        super(errMessage, e);
        this.setErrCode(ApiResponseCodeEnum.EXCEPTION);
    }

    public SysException(String errMessage, BaseResponseCode errorCode, Throwable e) {
        super(errMessage, e);
        this.setErrCode(errorCode);
    }
}

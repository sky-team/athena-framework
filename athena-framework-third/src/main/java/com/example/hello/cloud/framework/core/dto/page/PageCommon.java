package com.example.hello.cloud.framework.core.dto.page;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * PageCommon
 * 分页返回对象
 *
 * @author yingc04
 * @create 2019/11/5
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PageCommon<T> extends BasePage  {
    private static final long serialVersionUID = 7102020427972136037L;

    /**
     * 总记录数
     */
    private Integer total = 0;

    /**
     * 数据记录列表
     */
    private List<T> list;

    /**
     * 附带信息
     */
    private Object otherObj;

    public PageCommon() {
        super();
    }

    public PageCommon(Integer pageNo, Integer pageSize, Integer total, List<T> list) {
        super(pageNo, pageSize);
        this.list = list;
        this.total = total;
    }

    public PageCommon(IPage<T> iPage) {
        super((int) iPage.getCurrent(), (int) iPage.getSize());
        this.list = iPage.getRecords();
        this.total = (int) iPage.getTotal();
    }

    public void setIPage(IPage<T> iPage) {
        this.list = iPage.getRecords();
        this.total = (int) iPage.getTotal();
        setPageNo((int) iPage.getCurrent());
        setPageSize((int) iPage.getSize());
    }
}

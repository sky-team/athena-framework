package com.example.hello.cloud.framework.core.repository.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.example.hello.cloud.framework.common.util.AuthContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * mybatisPlus 自定义字段自动填充处理类，实体类中使用@TableField 注解
 * <p>
 * 自定义MyMetaObjectHandler字段自动填充处理类继承MetaObjectHandler 注：在 Spring Boot 中需要声明@Component 注入
 * <p>
 * 我们在更新字段的时候要使用 et.字段名 或者 param1.字段 才会生效！
 * <p>
 * 另外一个注意点就是，自动填充是在执行完插入或更新方法之后，也就是说，MyBatis-Plus会在方法之后判断@TableField注解的字段有没有被手动更新，如果没有才会走自定义的实现类MyMetaObjectHandler！
 *
 * @author zhanj04
 * @date 2019/10/21 19:40
 */
@Configuration
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {
    private static final String TABLE_ID_STRING = "id";
    private static final String MYBATIS_PLUS_ID_TYPE_NONE_STRING = "none";
    @Autowired(required = false)
    private SnowFlakeGenerator snowFlakeGenerator;
    @Value("${mybatis-plus.global-config.db-config.id-type:'id_worker_str'}")
    private String idType;
    /**
     * 未删除
     */
    private static final Integer NOT_DELETED = 0;

    /**
     * 新增的时候自动填充
     *
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        String phone = AuthContextHolder.getPhone() == null ? "" : AuthContextHolder.getPhone();
        this.setFieldValByName("createDate", new Date(), metaObject);
        this.setFieldValByName("createUser", phone, metaObject);
        this.setFieldValByName("updateDate", new Date(), metaObject);
        this.setFieldValByName("deleteFlag", NOT_DELETED, metaObject);
        String createIp = "createIp";
        boolean hasCreateIp = metaObject.hasGetter(createIp);
        if (hasCreateIp) {
            this.setFieldValByName(createIp, AuthContextHolder.getClientIp(), metaObject);
        }
        String updateIp = "updateIp";
        boolean hasUpdateIp = metaObject.hasGetter(updateIp);
        if (hasUpdateIp) {
            this.setFieldValByName(updateIp, AuthContextHolder.getClientIp(), metaObject);
        }
        log.info("DO是否包含createIp-{},updateIp-{},{}", hasCreateIp, hasUpdateIp, AuthContextHolder.getClientIp());
        /**
         * 不用mybatis plus默认的生成方式，把 id-type 设置为none。 在此处如果用户没有设置ID，则会自动生成一个ID。
         * 自动生成ID时，需要在ID字段上加 @TableField(value = "id", fill = FieldFill.INSERT)
         *  id 生成器，需要redis检查IP取模运算的值 是否被使用。如果值被使用，需要重试生成新的
         * **/
        if ((metaObject.getValue(TABLE_ID_STRING) == null || "".equals(metaObject.getValue(TABLE_ID_STRING).toString())) && MYBATIS_PLUS_ID_TYPE_NONE_STRING.equals(idType)) {
            this.setFieldValByName("id", snowFlakeGenerator.nextId(), metaObject);
        }
    }

    /**
     * 更新后的字段会自动填充
     *
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        String phone = AuthContextHolder.getPhone() == null ? "" : AuthContextHolder.getPhone();
        this.setFieldValByName("updateDate", new Date(), metaObject);
        this.setFieldValByName("updateUser", phone, metaObject);
        String updateIp = "updateIp";
        boolean hasUpdateIp = metaObject.hasGetter(updateIp);
        if (hasUpdateIp) {
            this.setFieldValByName(updateIp, AuthContextHolder.getClientIp(), metaObject);
        }
    }
}

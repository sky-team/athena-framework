package com.example.hello.cloud.framework.core.dto;

/**
 * 查询入参对象接口
 *
 * @author zhoujj07
 * @create 2019/10/22
 */
public interface Query extends Command {}

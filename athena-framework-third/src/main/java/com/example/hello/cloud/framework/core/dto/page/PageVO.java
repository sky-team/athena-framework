package com.example.hello.cloud.framework.core.dto.page;

import lombok.ToString;

import java.io.Serializable;
import java.util.List;


/**
 * @author guohg03
 * @ClassName: PageVO
 * @Description: 分页請求對象
 * @date 2018年7月28日
 */
@ToString
public class PageVO extends BasePage implements Serializable{

    private static final long serialVersionUID = 658735604421213500L;

    /**
     * 排序字段名
     */
    private String sort;

    /**
     * 排序順序
     */
    private String order;

    /**
     * 用于产品权限过滤
     */
    private List<String> permProCodeList;

    /**
     *  用于组织权限过滤：所在组织的code
     * @return
     */
    private String permOrgCode;

    /**
     *  用于组织权限过滤：所在销售团队的id
     * @return
     */
    private String permTeamId;

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public List<String> getPermProCodeList() {
        return permProCodeList;
    }

    public void setPermProCodeList(List<String> permProCodeList) {
        this.permProCodeList = permProCodeList;
    }

    public String getPermOrgCode() {
        return permOrgCode;
    }

    public void setPermOrgCode(String permOrgCode) {
        this.permOrgCode = permOrgCode;
    }

    public String getPermTeamId() {
        return permTeamId;
    }

    public void setPermTeamId(String permTeamId) {
        this.permTeamId = permTeamId;
    }

}

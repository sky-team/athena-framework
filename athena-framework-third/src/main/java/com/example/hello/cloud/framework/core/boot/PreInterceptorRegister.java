package com.example.hello.cloud.framework.core.boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.hello.cloud.framework.core.command.CommandHub;
import com.example.hello.cloud.framework.core.command.CommandInterceptor;
import com.example.hello.cloud.framework.core.command.PreInterceptor;
import com.example.hello.cloud.framework.core.common.ApplicationContextHelper;
import com.example.hello.cloud.framework.core.dto.Command;

/**
 * PreInterceptorRegister
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Component
public class PreInterceptorRegister extends AbstractRegister {

    @Autowired
    private CommandHub commandHub;

    @Override
    public void doRegistration(Class<?> targetClz) {
        CommandInterceptor commandInterceptor = (CommandInterceptor)ApplicationContextHelper.getBean(targetClz);
        PreInterceptor preInterceptorAnn = targetClz.getDeclaredAnnotation(PreInterceptor.class);
        Class<? extends Command>[] supportClasses = preInterceptorAnn.commands();
        registerInterceptor(supportClasses, commandInterceptor);
    }

    private void registerInterceptor(Class<? extends Command>[] supportClasses, CommandInterceptor commandInterceptor) {
        if (null == supportClasses || supportClasses.length == 0) {
            commandHub.getGlobalPreInterceptors().add(commandInterceptor);
            order(commandHub.getGlobalPreInterceptors());
            return;
        }
    }
}

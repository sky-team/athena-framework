package com.example.hello.cloud.framework.core.boot;

/**
 * Register Interface
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface Register {

    /**
     * 实现注册
     * 
     * @param targetClz
     */
    void doRegistration(Class<?> targetClz);
}

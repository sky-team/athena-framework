package com.example.hello.cloud.framework.core.command;

import com.example.hello.cloud.framework.core.dto.Command;
import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.core.dto.Command;

/**
 * CommandExecutor
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface CommandExecutor<R extends ApiResponse, C extends Command> {

    /**
     * 执行command
     * 
     * @param cmd
     * @return
     */
    R execute(C cmd);
}

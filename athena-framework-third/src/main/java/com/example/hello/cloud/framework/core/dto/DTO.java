package com.example.hello.cloud.framework.core.dto;

import java.io.Serializable;

/**
 * 数据传输对象接口
 *
 * @author zhoujj07
 * @create 2019/10/22
 */
public interface DTO extends Serializable {}

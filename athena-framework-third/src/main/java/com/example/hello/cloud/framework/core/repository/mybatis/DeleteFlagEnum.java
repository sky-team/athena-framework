package com.example.hello.cloud.framework.core.repository.mybatis;

/**
 * 删除状态枚举
 *
 * @author guoc16
 * @date 2020/2/10
 */
public enum DeleteFlagEnum {

    /**
     * 未删除
     */
    ENABLE(0, "未删除"),
    /**
     * 已删除
     */
    DISABLE(1, "已删除");

    DeleteFlagEnum(Integer flag, String desc) {
        this.flag = flag;
        this.desc = desc;
    }

    private Integer flag;

    private String desc;

    public Integer getFlag() {
        return flag;
    }

    public String getDesc() {
        return desc;
    }
}

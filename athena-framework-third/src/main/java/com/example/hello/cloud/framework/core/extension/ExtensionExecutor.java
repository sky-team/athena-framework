package com.example.hello.cloud.framework.core.extension;

import com.example.hello.cloud.framework.core.boot.AbstractComponentExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.hello.cloud.framework.core.boot.AbstractComponentExecutor;
import com.example.hello.cloud.framework.core.common.Constant;
import com.example.hello.cloud.framework.core.exception.framework.helloException;
import com.example.hello.cloud.framework.core.logger.Logger;
import com.example.hello.cloud.framework.core.logger.LoggerFactory;

/**
 * ExtensionExecutor
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Component
public class ExtensionExecutor extends AbstractComponentExecutor {

    private Logger logger = LoggerFactory.getLogger(ExtensionExecutor.class);

    @Autowired
    private ExtensionRepository extensionRepository;

    @Override
    protected <C> C locateComponent(Class<C> targetClz, BizScenario bizScenario) {
        C extension = locateExtension(targetClz, bizScenario);
        logger.debug("[Located Extension]: " + extension.getClass().getSimpleName());
        return extension;
    }

    /**
     * if the bizScenarioUniqueIdentity is "ali.tmall.supermarket"
     *
     * the search path is as below: 1、first try to get extension by "ali.tmall.supermarket", if get, return it. 2、loop
     * try to get extension by "ali.tmall", if get, return it. 3、loop try to get extension by "ali", if get, return it.
     * 4、if not found, try the default extension
     * 
     * @param targetClz
     *            targetClz
     */
    private <Ext> Ext locateExtension(Class<Ext> targetClz, BizScenario bizScenario) {
        checkNull(bizScenario);

        Ext extension;
        String bizScenarioUniqueIdentity = bizScenario.getUniqueIdentity();
        logger.debug("BizScenario in locateExtension is : " + bizScenarioUniqueIdentity);

        // first try
        extension = firstTry(targetClz, bizScenarioUniqueIdentity);
        if (extension != null) {
            return extension;
        }

        // loop try
        extension = loopTry(targetClz, bizScenarioUniqueIdentity);
        if (extension != null) {
            return extension;
        }

        throw new helloException(
            "Can not find extension with ExtensionPoint: " + targetClz + " BizScenario:" + bizScenarioUniqueIdentity);
    }

    private <Ext> Ext firstTry(Class<Ext> targetClz, String bizScenario) {
        return (Ext)extensionRepository.getExtensionRepo()
            .get(new ExtensionCoordinate(targetClz.getName(), bizScenario));
    }

    private <Ext> Ext loopTry(Class<Ext> targetClz, String bizScenario) {
        Ext extension;
        if (bizScenario == null) {
            return null;
        }
        int lastDotIndex = bizScenario.lastIndexOf(Constant.DOT_SEPARATOR);
        while (lastDotIndex != -1) {
            bizScenario = bizScenario.substring(0, lastDotIndex);
            extension = (Ext)extensionRepository.getExtensionRepo()
                .get(new ExtensionCoordinate(targetClz.getName(), bizScenario));
            if (extension != null) {
                return extension;
            }
            lastDotIndex = bizScenario.lastIndexOf(Constant.DOT_SEPARATOR);
        }
        return null;
    }

    private void checkNull(BizScenario bizScenario) {
        if (bizScenario == null) {
            throw new helloException("BizScenario can not be null for extension");
        }
    }

}

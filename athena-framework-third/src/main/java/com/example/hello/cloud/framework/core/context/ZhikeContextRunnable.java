package com.example.hello.cloud.framework.core.context;

import java.util.Set;

/**
 * 能够自动context穿透的Runnable 线程池中，加入SofaContextRunnable对象，在构造函数中，将context传递到子线程 若不需考虑context穿透，可直接用普通Runnable
 * 使用示例参见测试用例：TestSofaContext.testSofaContextRunnable
 * 
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public abstract class helloContextRunnable implements Runnable {

    private Set<helloContextSupprot> sofaContextSet;

    private helloContextRunnable() {}

    public helloContextRunnable(Set<helloContextSupprot> sofaContextSet) {
        this.sofaContextSet = sofaContextSet;
    }

    @Override
    public void run() {
        sofaContextSet.forEach(helloContext::setContext);
        execute();
        helloContext.clearAllContext();
    }

    public abstract void execute();
}

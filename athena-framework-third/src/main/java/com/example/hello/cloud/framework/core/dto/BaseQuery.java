package com.example.hello.cloud.framework.core.dto;

/**
 * 查询参数基类
 *
 * @author zhoujj07
 * @create 2019/10/22
 */
public abstract class BaseQuery extends BaseCommand {}

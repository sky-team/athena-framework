package com.example.hello.cloud.framework.core.assembler;

/**
 * 适用于消息，查询，HSF等接口的参数装配 Assembler Interface
 * 
 * 主要应用在application module层
 * 
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface Assembler {}

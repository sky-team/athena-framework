package com.example.hello.cloud.framework.core.pattern.filter;

/**
 * FilterInvoker
 *
 * @author zhanj04
 * @date 2019/10/21 19:30
 */
public interface FilterInvoker<T> {

    default public void invoke(T context) {};
}

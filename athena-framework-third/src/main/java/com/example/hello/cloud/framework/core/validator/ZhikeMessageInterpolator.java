package com.example.hello.cloud.framework.core.validator;

import java.util.Locale;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;

/**
 * helloMessageInterpolator
 *
 * @author zhanj04
 * @create 2019/10/10
 */
public class helloMessageInterpolator extends ResourceBundleMessageInterpolator {

    @Override
    public String interpolate(String message, Context context) {
        // Use English Locale
        return super.interpolate(message, context, Locale.ENGLISH);
    }
}

/**
 * This package defines all kinds of Exceptions of Application.
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
package com.example.hello.cloud.framework.core.exception;
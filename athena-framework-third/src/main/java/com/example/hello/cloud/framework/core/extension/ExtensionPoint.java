package com.example.hello.cloud.framework.core.extension;

/**
 * ExtensionPointI is the parent interface of all ExtensionPoints
 * 扩展点表示一块逻辑在不同的业务有不同的实现，使用扩展点做接口申明，然后用Extension（扩展）去实现扩展点。
 * 
 * 扩展点的设计是这样的，所有的扩展点（ExtensionPoint）必须通过接口申明，扩展实现（Extension）是通过Annotation的方式标注的，Extension里面使用BizCode和TenantId两个属性用来标识身份，框架的Bootstrap类会在Spring启动的时候做类扫描，进行Extension注册，在Runtime的时候，通过TenantContext来选择要使用的Extension。
 * 
 * TenantContext是通过Interceptor在调用业务逻辑之前进行初始化的
 * 
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface ExtensionPoint {

}

package com.example.hello.cloud.framework.core.command;

import java.lang.annotation.*;

import org.springframework.stereotype.Component;

/**
 * Command Annotation
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Component
public @interface Command {

}

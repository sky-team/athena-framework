package com.example.hello.cloud.framework.core.domain;

/**
 * 领域工厂
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface DomainFactory<T extends EntityObject> {

    T create();

}

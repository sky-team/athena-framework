package com.example.hello.cloud.framework.core.exception.framework;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;

import com.example.hello.cloud.framework.core.common.ApplicationContextHelper;
import com.example.hello.cloud.framework.core.exception.ExceptionHandler;

/**
 * ExceptionHandlerFactory
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class ExceptionHandlerFactory {

    public static ExceptionHandler getExceptionHandler() {
        try {
            return ApplicationContextHelper.getBean(ExceptionHandler.class);
        } catch (NoSuchBeanDefinitionException ex) {
            return DefaultExceptionHandler.singleton;
        }
    }

}

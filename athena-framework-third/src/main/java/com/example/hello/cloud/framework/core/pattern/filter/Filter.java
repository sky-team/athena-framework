package com.example.hello.cloud.framework.core.pattern.filter;

/**
 * Filter
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface Filter<T> {

    void doFilter(T context, FilterInvoker nextFilter);

}
package com.example.hello.cloud.framework.core.dto;

import com.example.hello.cloud.framework.core.extension.BizScenario;

/**
 * 带业务场景的命令入参基类
 *
 * @author zhoujj07
 * @create 2019/10/22
 */
public abstract class BaseCommand extends BaseDTO {

    private BizScenario bizScenario;

    public BizScenario getBizScenario() {
        return bizScenario;
    }

    public void setBizScenario(BizScenario bizScenario) {
        this.bizScenario = bizScenario;
    }

}

package com.example.hello.cloud.framework.core.extension;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 * ExtensionRepository
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Component
public class ExtensionRepository {

    @Getter
    private Map<ExtensionCoordinate, ExtensionPoint> extensionRepo = new HashMap<>();

}

package com.example.hello.cloud.framework.core.command;

import com.example.hello.cloud.framework.core.dto.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.hello.cloud.framework.common.ApiResponse;
import com.example.hello.cloud.framework.core.dto.Command;

/**
 * Just send Command to CommandBus
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Component
public class BaseCommandBus implements CommandBus {

    @Autowired
    private CommandHub commandHub;

    @Override
    public ApiResponse send(Command cmd) {
        return commandHub.getCommandInvocation(cmd.getClass()).invoke(cmd);
    }

}

/**
 * Repository pattern is used to decouple domain layer with different data tunnels.
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
package com.example.hello.cloud.framework.core.repository;
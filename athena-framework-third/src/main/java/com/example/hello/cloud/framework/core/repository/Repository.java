package com.example.hello.cloud.framework.core.repository;

/**
 * This is Repository pattern which decouples the data access from concrete data tunnels.
 *
 * 存储相关，是gateway的特化，主要用来做本域的数据CRUD操作
 * 
 * @author zhoujj07
 * @create 2019/9/29
 */
public interface Repository {}

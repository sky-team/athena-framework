package com.example.hello.cloud.framework.core.event;

import java.lang.annotation.*;

import org.springframework.stereotype.Component;

/**
 * EventHandler
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Component
public @interface EventHandler {}

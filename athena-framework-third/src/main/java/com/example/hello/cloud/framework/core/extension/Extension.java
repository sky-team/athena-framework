package com.example.hello.cloud.framework.core.extension;

import java.lang.annotation.*;

import org.springframework.stereotype.Component;

/**
 * Extension
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Component
public @interface Extension {
    String bizId() default BizScenario.DEFAULT_BIZ_ID;

    String useCase() default BizScenario.DEFAULT_USE_CASE;

    String scenario() default BizScenario.DEFAULT_SCENARIO;
}

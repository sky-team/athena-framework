package com.example.hello.cloud.framework.core.event;

import java.util.concurrent.ExecutorService;

import com.example.hello.cloud.framework.common.ApiResponse;

/**
 * event handler
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public interface SuperEventHandler<R extends ApiResponse, E extends Event> {

    /**
     * 获取executor
     * 
     * @return ExecutorService
     */
    default ExecutorService getExecutor() {
        return null;
    }

    /**
     * 执行
     * 
     * @param e
     *            e
     * @return R
     */
    R execute(E e);
}

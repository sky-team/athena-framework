package com.example.hello.cloud.framework.core.boot;

import java.lang.reflect.Modifier;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * ClassInterfaceChecker
 *
 * @author zhanj04
 * @date 2019/10/11 9:29
 */
public class ClassInterfaceChecker {

    /**
     * @param targetClz
     * @param expectedName
     * @return
     */
    public static boolean check(Class<?> targetClz, String expectedName) {
        // If it is interface, just return false
        if (targetClz.isInterface()) {
            return false;
        }

        // If it is abstract class, just return false
        if (Modifier.isAbstract(targetClz.getModifiers())) {
            return false;
        }

        Class[] interfaces = targetClz.getInterfaces();
        if (ArrayUtils.isEmpty(interfaces)) {
            return false;
        }
        for (Class intf : interfaces) {
            String intfSimpleName = intf.getSimpleName();
            if (StringUtils.equals(intfSimpleName, expectedName)) {
                return true;
            }
        }
        return false;
    }
}

package com.athena.framework.tool.bloomfilter;

import cn.hutool.bloomfilter.BitMapBloomFilter;
import cn.hutool.bloomfilter.BitSetBloomFilter;
import cn.hutool.bloomfilter.BloomFilterUtil;
import cn.hutool.bloomfilter.bitMap.IntMap;
import cn.hutool.bloomfilter.bitMap.LongMap;

/**
 * 布隆过滤器工具
 *
 * @author looly
 * @since 4.1.5
 */
public class SBloomFilterUtil extends BloomFilterUtil {

    public static void addWithMap(BitMapBloomFilter filter, String str) {
        filter.add(str);
    }

    public static boolean containsWithMap(BitMapBloomFilter filter, String str) {
        return filter.contains(str);
    }

    public static void addWithSet(BitSetBloomFilter filter, String str) {
        filter.add(str);
    }

    public static boolean containsWithSet(BitSetBloomFilter filter, String str) {
        return filter.contains(str);
    }

    public static IntMap createInitMap(int m) {
        return new IntMap(m);
    }

    public static void addWithIntMap(IntMap filter, int str) {
        filter.add(str);
    }

    public static boolean containsWithIntMap(IntMap filter, int str) {
        return filter.contains(str);
    }

    public static LongMap createLongMap(int m) {
        return new LongMap(m);
    }

    public static void addWithLongMap(LongMap filter, long str) {
        filter.add(str);
    }

    public static boolean containsWithLongMap(LongMap filter, long str) {
        return filter.contains(str);
    }
}
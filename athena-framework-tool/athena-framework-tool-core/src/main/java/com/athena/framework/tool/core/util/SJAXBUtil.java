package com.athena.framework.tool.core.util;

import cn.hutool.core.util.JAXBUtil;
import cn.hutool.core.util.XmlUtil;

/**
 * JAXB（Java Architecture for XML Binding），根据XML Schema产生Java对象，即实现xml和Bean互转。
 * <p>
 * 相关介绍：
 * <ul>
 * <li>https://www.cnblogs.com/yanghaolie/p/11110991.html</li>
 * <li>https://my.oschina.net/u/4266515/blog/3330113</li>
 * </ul>
 *
 * @author dazer
 * @see XmlUtil
 * @since 5.7.3
 */
public class SJAXBUtil extends JAXBUtil {

}

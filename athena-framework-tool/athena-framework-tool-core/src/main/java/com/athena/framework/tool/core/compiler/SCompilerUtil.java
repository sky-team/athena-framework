package com.athena.framework.tool.core.compiler;

import cn.hutool.core.comparator.CompareUtil;

import javax.tools.JavaCompiler;

/**
 * 源码编译工具类，主要封装{@link JavaCompiler} 相关功能
 *
 * @author leo
 * @date 2021/12/5 19:12
 */
public class SCompilerUtil extends CompareUtil {

}

package com.athena.framework.tool.core.util;

import cn.hutool.core.util.PrimitiveArrayUtil;

/**
 * 原始类型数组工具类
 *
 * @author looly
 * @since 5.5.2
 */
public class SPrimitiveArrayUtil extends PrimitiveArrayUtil {

}

package com.athena.framework.tool.core.io;

import cn.hutool.core.io.NioUtil;

/**
 * NIO相关工具封装，主要针对Channel读写、拷贝等封装
 *
 * @author looly
 * @since 5.5.3
 */
public class SNioUtil extends NioUtil {

}

package com.athena.framework.tool.core.net;

import cn.hutool.core.net.Ipv4Util;

/**
 * IPV4地址工具类
 *
 * <p>pr自：https://gitee.com/loolly/hutool/pulls/161</p>
 *
 * @author ZhuKun
 * @since 5.4.1
 */
public class SIpv4Util extends Ipv4Util {

}

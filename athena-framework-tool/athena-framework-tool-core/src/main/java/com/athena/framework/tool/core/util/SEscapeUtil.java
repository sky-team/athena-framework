package com.athena.framework.tool.core.util;

import cn.hutool.core.util.EscapeUtil;

/**
 * 转义和反转义工具类Escape / Unescape<br>
 * escape采用ISO Latin字符集对指定的字符串进行编码。<br>
 * 所有的空格符、标点符号、特殊字符以及其他非ASCII字符都将被转化成%xx格式的字符编码(xx等于该字符在字符集表里面的编码的16进制数字)。
 *
 * @author xiaoleilu
 */
public class SEscapeUtil extends EscapeUtil {

}

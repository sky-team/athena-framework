package com.athena.framework.tool.core.util;

import cn.hutool.core.util.ObjectUtil;

/**
 * 对象工具类，包括判空、克隆、序列化等操作
 *
 * @author Looly
 */
public class SObjectUtil extends ObjectUtil {

}

package com.athena.framework.tool.core.util;

import cn.hutool.core.util.TypeUtil;

import java.lang.reflect.Type;

/**
 * 针对 {@link Type} 的工具类封装<br>
 * 最主要功能包括：
 *
 * <pre>
 * 1. 获取方法的参数和返回值类型（包括Type和Class）
 * 2. 获取泛型参数类型（包括对象的泛型参数或集合元素的泛型类型）
 * </pre>
 *
 * @author Looly
 * @since 3.0.8
 */
public class STypeUtil extends TypeUtil {

}

package com.athena.framework.tool.core.util;

import cn.hutool.core.util.DesensitizedUtil;

/**
 * 脱敏工具类，支持以下类型信息的脱敏自动处理：
 *
 * <ul>
 * <li>用户ID</li>
 * <li>中文名</li>
 * <li>身份证</li>
 * <li>座机号</li>
 * <li>手机号</li>
 * <li>地址</li>
 * <li>电子邮件</li>
 * <li>密码</li>
 * <li>车牌</li>
 * <li>银行卡号</li>
 * </ul>
 *
 * @author dazer and neusoft and qiaomu
 * @since 5.6.2
 */
public class SDesensitizedUtil extends DesensitizedUtil {

}

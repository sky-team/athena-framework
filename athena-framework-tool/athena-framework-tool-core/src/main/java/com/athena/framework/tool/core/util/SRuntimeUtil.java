package com.athena.framework.tool.core.util;

import cn.hutool.core.util.RuntimeUtil;

/**
 * 系统运行时工具类，用于执行系统命令的工具
 *
 * @author Looly
 * @since 3.1.1
 */
public class SRuntimeUtil extends RuntimeUtil {

}

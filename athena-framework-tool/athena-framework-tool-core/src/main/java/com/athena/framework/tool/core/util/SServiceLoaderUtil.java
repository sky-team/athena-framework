package com.athena.framework.tool.core.util;

import cn.hutool.core.util.ServiceLoaderUtil;

/**
 * SPI机制中的服务加载工具类，流程如下
 *
 * <pre>
 *     1、创建接口，并创建实现类
 *     2、ClassPath/META-INF/services下创建与接口全限定类名相同的文件
 *     3、文件内容填写实现类的全限定类名
 * </pre>
 * 相关介绍见：https://www.jianshu.com/p/3a3edbcd8f24
 *
 * @author looly
 * @since 5.1.6
 */
public class SServiceLoaderUtil extends ServiceLoaderUtil {

}

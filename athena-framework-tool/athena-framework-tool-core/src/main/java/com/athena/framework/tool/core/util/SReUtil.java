package com.athena.framework.tool.core.util;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.ReUtil;

/**
 * 正则相关工具类<br>
 * 常用正则请见 {@link Validator}
 *
 * @author xiaoleilu
 */
public class SReUtil extends ReUtil {

}

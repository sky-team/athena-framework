package com.athena.framework.tool.core.io;

import cn.hutool.core.io.IoUtil;

/**
 * IO工具类<br>
 * IO工具类只是辅助流的读写，并不负责关闭流。原因是流可能被多次读写，读写关闭后容易造成问题。
 *
 * @author xiaoleilu
 */
public class SIoUtil extends IoUtil {

}

package com.athena.framework.tool.core.util;

import cn.hutool.core.util.RadixUtil;

/**
 * 进制转换工具类，可以转换为任意进制
 * <p>
 * 把一个十进制整数根据自己定义的进制规则进行转换<br>
 * from：https://gitee.com/loolly/hutool/pulls/260
 * <p>
 * 主要应用一下情况：
 * <ul>
 * <li>根据ID生成邀请码,并且尽可能的缩短。并且不希望直接猜测出和ID的关联</li>
 * <li>短连接的生成，根据ID转成短连接，同样不希望被猜测到</li>
 * <li>数字加密，通过两次不同进制的转换，让有规律的数字看起来没有任何规律</li>
 * <li>....</li>
 * </ul>
 *
 * @author xl7@qq.com
 * @since 5.5.8
 */

public class SRadixUtil extends RadixUtil {

}

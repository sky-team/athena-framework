package com.athena.framework.tool.core.io.watch;

import cn.hutool.core.io.watch.WatchUtil;

/**
 * 监听工具类<br>
 * 主要负责文件监听器的快捷创建
 *
 * @author leo
 * @since 3.1.0
 */
public class SWatchUtil extends WatchUtil {

}

package com.athena.framework.tool.core.io;

import cn.hutool.core.io.ManifestUtil;

/**
 * Jar包中manifest.mf文件获取和解析工具类
 * 来自Jodd
 *
 * @author looly, jodd
 * @since 5.7.0
 */
public class SManifestUtil extends ManifestUtil {

}

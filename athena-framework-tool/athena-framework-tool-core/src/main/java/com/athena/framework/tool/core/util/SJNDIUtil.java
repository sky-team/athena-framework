package com.athena.framework.tool.core.util;

import cn.hutool.core.util.JNDIUtil;

/**
 * JNDI工具类<br>
 * JNDI是Java Naming and Directory Interface（JAVA命名和目录接口）的英文简写，<br>
 * 它是为JAVA应用程序提供命名和目录访问服务的API（Application Programing Interface，应用程序编程接口）。
 *
 * <p>
 * 见：https://blog.csdn.net/u010430304/article/details/54601302
 * </p>
 *
 * @author loolY
 * @since 5.7.7
 */
public class SJNDIUtil extends JNDIUtil {

}
